unit uCheckListboxDlg;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types,
  Forms, CheckLst, ExtCtrls, StdCtrls,
  uTypesConstsUtils;

type

  TMultiSelIntegerDlg = class(TForm)
  private
    CheckLB: TCheckListBox;
    ButtonPanel: TPanel;
    SelectAllButton: TButton;
    OKButton: TButton;
    CloseButton: TButton;
    DeselectAllButton: TButton;
    procedure DeselectAllButtonClick(Sender: TObject);
    procedure ItemClick(Sender: TObject; {%H-}Index: integer);
    procedure SelectAllButtonClick(Sender: TObject);
  protected
    procedure DoShow; override;
  public
    intArr: TIntegerDynArray;
    preSelectChecks: Boolean;
    constructor Create(const aTitle: String; preSelectAllChecks: Boolean=False); reintroduce;
  end;

  function GetStatNamesDlg(const aTitle: String; checksPreselected: Boolean=True): TIntegerDynArray;

implementation

uses
  Controls,
  uEngine;

function GetStatNamesDlg(const aTitle: String; checksPreselected: Boolean): TIntegerDynArray;
var
  dlg: TMultiSelIntegerDlg;
  mr: Integer;
begin
  dlg := TMultiSelIntegerDlg.Create(aTitle, checksPreselected);
  try
    mr := dlg.ShowModal;
    case mr of
      mrOK: Result := dlg.intArr;
      else  Result := Nil;
    end;
  finally
    dlg.Free;
  end;
end;

{ TMultiSelIntegerDlg }

procedure TMultiSelIntegerDlg.ItemClick(Sender: TObject; Index: integer);
var
  i: Integer;
begin
  intArr := Nil;
  for i := 0 to CheckLB.Items.Count-1 do
    if CheckLB.Checked[i] then
      begin
        SetLength(intArr, Succ(Length(intArr)));
        intArr[High(intArr)] := i;
      end;
end;

procedure TMultiSelIntegerDlg.SelectAllButtonClick(Sender: TObject);
begin
  CheckLB.CheckAll(cbChecked, False, False);
end;

procedure TMultiSelIntegerDlg.DeselectAllButtonClick(Sender: TObject);
begin
  CheckLB.CheckAll(cbUnchecked, False, False);
end;

procedure TMultiSelIntegerDlg.DoShow;
var
  i: Integer;
begin
  inherited DoShow;
  if preSelectChecks then
    begin
      CheckLB.CheckAll(cbChecked, False, False);
      SetLength(intArr, CheckLB.Items.Count);
      for i := 0 to High(intArr) do
        intArr[i] := i;
    end;
end;

constructor TMultiSelIntegerDlg.Create(const aTitle: String; preSelectAllChecks: Boolean);
var
  i: Integer;
begin
  inherited CreateNew(Nil);
  Caption := aTitle;
  Position := poScreenCenter;
  BorderStyle := bsDialog;
  AutoSize := True;

  CheckLB := TCheckListBox.Create(Self);
  ButtonPanel := TPanel.Create(Self);
  SelectAllButton := TButton.Create(Self);
  OKButton := TButton.Create(Self);
  CloseButton := TButton.Create(Self);
  DeselectAllButton := TButton.Create(Self);
  preSelectChecks := preSelectAllChecks;
  intArr := Nil;

  with CheckLB do begin
    Align := alTop;
    BorderSpacing.Around := Margin;
    Constraints.MinHeight := 500;
    Constraints.MaxHeight := 500;
    Constraints.MaxWidth := 600;
    Constraints.MinWidth := 600;
    ScrollWidth := 700;
    OnItemClick  := @ItemClick;
    for i := 1 to High(BehestsExperiment1) do
      if BehestsExperiment1[i].rv.bk = c then
        Items.Add(BehestsExperiment1[i].rv.cp.sn);
    Parent := Self;
  end;

  with ButtonPanel do begin
    Align := alTop;
    Top := 1;
    BorderSpacing.Around := Margin;
    BevelOuter := bvNone;
    Parent := Self;
  end;

  with DeselectAllButton do begin
    Caption := 'Deselect all';
    AutoSize := True;
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := ButtonPanel;
    OnClick  := @DeselectAllButtonClick;
    Parent := ButtonPanel;
  end;

  with SelectAllButton do begin
    Caption := 'Select all';
    AutoSize := True;
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := DeselectAllButton;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Left := Margin;
    OnClick   := @SelectAllButtonClick;
    Parent := ButtonPanel;
  end;

  with CloseButton do begin
    Caption := 'Close';
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := ButtonPanel;
    AnchorSideRight.Side := asrRight;
    ModalResult := mrClose;
    Parent := ButtonPanel;
  end;

  with OKButton do begin
    Caption := 'OK';
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := CloseButton;
    AnchorSideRight.Side := asrLeft;
    BorderSpacing.Right := Margin;
    ModalResult := mrOK;
    Parent := ButtonPanel;
  end;

end;

end.


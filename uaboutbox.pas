unit uAboutBox;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  Forms, StdCtrls, ExtCtrls;

type

  TAboutDialog = class(TForm)
  private
    buttonPanel: TPanel;
    copyrightLabel: TLabel;
    descrLabel: TLabel;
    designedLabel: TLabel;
    infoPanel: TPanel;
    license2Label: TLabel;
    licenseLabel: TLabel;
    linkLabel: TLabel;
    OKButton: TButton;
    programmedLabel: TLabel;
    technicalButton: TButton;
    titleLabel: TLabel;
    versionLabel: TLabel;

    procedure LinkLabelClick(Sender: TObject);
    procedure technicalButtonClick(Sender: TObject);
  public
    constructor Create(TheOwner: TComponent); override;
  end;

  procedure ShowAboutBox;

implementation

uses
  Controls, Graphics, LCLIntf,
  uTypesConstsUtils, uExptCharacteristics;

procedure ShowAboutBox;
var
  dlg: TAboutDialog;
begin
  dlg := TAboutDialog.Create(Nil);
  try
    dlg.ShowModal;
  finally
    dlg.Free;
  end;
end;

procedure TAboutDialog.LinkLabelClick(Sender: TObject);
begin
  OpenURL(rsGNUurl);
end;

procedure TAboutDialog.technicalButtonClick(Sender: TObject);
begin
  ShowExperimentCharacteristicsDlg;
end;

constructor TAboutDialog.Create(TheOwner: TComponent);
begin
  inherited CreateNew(TheOwner);
  BorderStyle := bsToolWindow;
  Position := poScreenCenter;
  Caption := rsAboutADP;
  AutoSize := True;

  infoPanel := TPanel.Create(Self);
  buttonPanel := TPanel.Create(Self);
  titleLabel := TLabel.Create(Self);
  descrLabel := TLabel.Create(Self);
  licenseLabel := TLabel.Create(Self);
  designedLabel := TLabel.Create(Self);
  programmedLabel := TLabel.Create(Self);
  copyrightLabel := TLabel.Create(Self);
  linkLabel := TLabel.Create(Self);
  license2Label := TLabel.Create(Self);
  versionLabel := TLabel.Create(Self);
  OKButton := TButton.Create(Self);
  technicalButton := TButton.Create(Self);

  with infoPanel do begin
    Align := alTop;
    Constraints.MinWidth := 580;
    Constraints.MinHeight := 400;
    BorderSpacing.Around := Margin;
    AutoSize := True;
    Parent := Self;
  end;

  with buttonPanel do begin
    Align := alTop;
    Top := 1;
    BevelOuter := bvNone;
    Height := 40;
    Parent := Self;
  end;

  with OKButton do begin
    ModalResult := mrOK;
    Caption := rsOK;
    Anchors := [akTop, akRight];
    BorderSpacing.Right := Margin;
    AnchorSideTop.Control := buttonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := buttonPanel;
    AnchorSideRight.Side := asrRight;
    Parent := Self;
  end;

  with technicalButton do begin
    Caption := rsTechnicalDetails;
    AutoSize := True;
    BorderSpacing.Left := Margin;
    AnchorSideTop.Control := buttonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := buttonPanel;
    OnClick  := @technicalButtonClick;
    Parent := Self;
  end;

  with titleLabel do begin
    Font.Size := 13;
    Font.Style := [fsBold];
    Caption := rsADP;
    AnchorSideTop.Control := infoPanel;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := DoubleMargin;
    BorderSpacing.Left := DoubleMargin;
    BorderSpacing.Bottom := DoubleMargin;
    Parent := infoPanel;
  end;

  with versionLabel do begin
    Font.Size := 13;
    Caption := ' ' + GetVersionResourceString;
    AnchorSideTop.Control := titleLabel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := titleLabel;
    AnchorSideLeft.Side := asrRight;
    Parent := infoPanel;
  end;

  with descrLabel do begin
    WordWrap := True;
    Caption := rsADPIsATool;
    AnchorSideTop.Control := titleLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin;
    Parent := infoPanel;
  end;

  with copyrightLabel do begin
    Caption := rsCopyrightGF;
    AnchorSideTop.Control := descrLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin;
    Parent := infoPanel;
  end;

  with designedLabel do begin
    Caption := rsDesignedByGF;
    AnchorSideTop.Control := copyrightLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin;
    Parent := infoPanel;
  end;

  with programmedLabel do begin
    Caption := rsProgrammedByHPC;
    AnchorSideTop.Control := designedLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin;
    Parent := infoPanel;
  end;

  with licenseLabel do begin
    WordWrap := True;
    Caption := rsGNULicence;
    AnchorSideTop.Control := programmedLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin;
    Parent := infoPanel;
  end;

  with linkLabel do begin
    Caption := '<' + rsGNUurl + '>';
    Font.Color := clBlue;
    font.Style := [fsUnderline];
    AnchorSideTop.Control := licenseLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin div 2;
    OnClick  := @LinkLabelClick;
    Parent := infoPanel;
  end;

  with license2Label do begin
    WordWrap := True;
    Caption := rsYouCanAlsoObtain;
    AnchorSideTop.Control := linkLabel;
    AnchorSideTop.Side := asrBottom;
    AnchorSideLeft.Control := infoPanel;
    AnchorSideLeft.Side := asrCenter;
    BorderSpacing.Top := Margin div 2;
    BorderSpacing.Bottom := Margin shl 1;
    Parent := infoPanel;
  end;

end;

end.


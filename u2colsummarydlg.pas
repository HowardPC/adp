unit u2ColSummaryDlg;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types,
  Forms, Controls, StdCtrls, ExtCtrls,
  uEngine, uTagMemo, uTypesConstsUtils;

type

  T2ColSummaryDialog = class(TForm)
  private
    FStatIDS: TIntegerDynArray;
    FParticipant: TParticipant;

    ButtonPanel: TPanel;
    CloseButton: TButton;
    Holder: TTMHolder;
    PrintButton: TButton;
    SaveToHTMLButton: TButton;

    procedure PrintButtonClick(Sender: TObject);
    procedure SaveToHTMLButtonClick(Sender: TObject);
  protected
    procedure DoShow; override;
  public
    constructor Create(aStatIDs: TIntegerDynArray; aParticipant: TParticipant); reintroduce;
    destructor Destroy; override;
  end;

  procedure Show2ColSummaryDlg(aStatIds: TIntegerDynArray; aParticipant: TParticipant);

implementation

uses
  StrUtils,
  Dialogs, Printers, PrintersDlgs;

procedure Show2ColSummaryDlg(aStatIds: TIntegerDynArray; aParticipant: TParticipant);
var
  dlg: T2ColSummaryDialog;
begin
  dlg := T2ColSummaryDialog.Create(aStatIds, aParticipant);
  try
    dlg.ShowModal;
  finally
    dlg.Free;
  end;
end;


procedure T2ColSummaryDialog.PrintButtonClick(Sender: TObject);
var
  i, lineCount, pageCount, linesPerPage: Integer;
  lh: Single;
  prtDlg: TPrintDialog;
  s: String;
  lMargin: String = '      ';
  sa: TStringArray = Nil;
begin
  SetLength(sa, Length(FStatIDS));
  for i := 0 to High(FStatIDS) do
    begin
      s := Format('%s %s%10.5f',[lMargin, Copy(GetStatisticName(FStatIDS[i])+rsLeader, 1, 111), FParticipant.StatisticValue[FStatIDS[i]]]);
      if Copy(s, Length(s)-5, 6) = '.00000' then
      Delete(s, Length(s)-5, 6);
      sa[i] := s;
    end;

  prtDlg := TPrintDialog.Create(Nil);
  try
    if prtDlg.Execute then
    begin
      Printer.BeginDoc;
      try
        Printer.Orientation := poLandscape;
        Printer.Canvas.Font.Assign(Holder.FTagMemo.Canvas.Font);
        Printer.Canvas.Font.Size := 10;
        lh := Printer.Canvas.TextHeight('Xg')*1.15;
        linesPerPage := Trunc(Size(Printer.PaperSize.PaperRect.WorkRect).cy / lh) - 1;

        lineCount := Succ(linesPerPage);
        pageCount := 0;
        for i := 0 to High(sa) do
          begin
            if (lineCount > linesPerPage) then
              begin
                lineCount:=1;
                Inc(pageCount);
                if pageCount > 1 then
                  Printer.NewPage;
                s := Format(rsSSummaryReportFor, [lMargin, FParticipant.participantID, FormatDateTime('d mmm yyyy',Date), pageCount]);
                Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
                s := lMargin + FParticipant.HeaderInfoText;
                Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
                s := Format(rsSExperimentWasPerformed, [lMargin, FParticipant.date]);
                Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount,2);
                s := lMargin + rsVariableNameValueP;
                Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
                s := lMargin + rsUnderlineDashesP;
                Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
              end;
            Printer.Canvas.TextOut(0, trunc(lh*lineCount), sa[i]);
            Inc(lineCount);
          end;
      finally
        Printer.EndDoc;
      end;
    end;
  finally
    prtDlg.Free;
  end;
end;

procedure T2ColSummaryDialog.SaveToHTMLButtonClick(Sender: TObject);
var
  sl: TStringList;
  fNme: String;
  i: Integer;
  statStr, valueStr: String;
begin
  if FParticipant = Nil then Exit;
  fNme := IncludeTrailingPathDelimiter(ExtractFilePath(processor.OutputFullFilename)) +
               FParticipant.participantID + '_' + processor.ExperimentName + '_' + SummaryReportDotHtml;
  sl := TStringList.Create;
  try
    sl.Add(rsHTMLHeader);
    sl.Add(rsPreBody);
    sl.Add(FParticipant.HeaderInfoText);
    sl.Add(Format(rsSRanThisExperiment, [FParticipant.participantID, FParticipant.date, FormatDateTime('dd mmmm, yyyy',Date)]));
    sl.Add('<hr>');
    sl.Add(rsVariableNameValueH);
    sl.Add(rsUnderlineDashesH);

    for i := 0 to High(FStatIDS) do
      begin
        statStr := GetPaddedRightDots(GetStatisticName(FStatIDS[i]), 86);
        case FParticipant.statsArray[FStatIDS[i]].isFloat of
          True:  begin
                   valueStr := PadLeft(FormatFloat('###0.00000', FParticipant.statsArray[FStatIDS[i]].value), 10);
                   if valueStr = '   0.00000' then valueStr := '   0.0';
                 end;
          False: valueStr := PadLeft(Trunc(FParticipant.statsArray[FStatIDS[i]].value).ToString, 4);
        end;
        sl.Add(' %s %s',[statStr, valueStr]);
      end;
    sl.Add(rsPostBody);
    sl.SaveToFile(fNme);
    ShowMessageFmt(rsHtmlReportSaved, [FParticipant.participantID, fNme]);
  finally
    sl.Free;
  end;
end;

procedure T2ColSummaryDialog.DoShow;
var
  i: Integer;
  s: String;
begin
  inherited DoShow;
  Holder.AddLine(FParticipant.HeaderInfoText);
  Holder.AddLine(rsSummaryReport + FormatDateTime('d mmm yyyy', Date));
  Holder.AddLine(FParticipant.participantID + rsDidThisExperiment + FParticipant.date);
  Holder.AddLine('<h>');
  Holder.AddLine(PadRight(rsUVariableName, 86) + rsUValue);
  for i := 0 to High(FStatIDS) do
    begin
      s := FormatFloat('0.00000', FParticipant.StatisticValue[FStatIDS[i]]);
      case Length(s) of
        7: s := '   ' + s;
        8: s := '  ' + s;
        9: s := ' ' + s;
        10: ;
        otherwise
          Assert(False,'unexpected float string length of '+IntToStr(Length(s)));
      end;
      Holder.AddLine(GetPaddedRightDots(GetStatisticName(FStatIDS[i]), 80) + s);
    end;
end;

constructor T2ColSummaryDialog.Create(aStatIDs: TIntegerDynArray; aParticipant: TParticipant);
begin
  inherited CreateNew(Nil);
  FStatIDS := aStatIDs;
  FParticipant := aParticipant;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  SetInitialBounds(0, 0, 880, 580);
  AutoSize := True;
  Caption := rsProcessedReportByADP + DateToStr(Date); //Processed data summary report';

  Holder := TTMHolder.Create(Self);
  ButtonPanel := TPanel.Create(Self);
  PrintButton := TButton.Create(Self);
  SaveToHTMLButton := TButton.Create(Self);
  CloseButton := TButton.Create(Self);

  with Holder do begin
    Height := 500;
    Constraints.MinWidth := 980;
    Align := alTop;
    BorderSpacing.Around := Margin;
    TabStop := False;
    Parent := Self;
  end;

  with ButtonPanel do begin
    Top := 524;
    Align := alTop;
    BorderSpacing.Around := Margin;
    Constraints.MinHeight := 3*Margin;
    BevelOuter := bvNone;
    TabOrder := 0;
    Parent := Self;
  end;

  with PrintButton do begin
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := ButtonPanel;
    AutoSize := True;
    Caption := rsPrintA4Landscape;
    OnClick := @PrintButtonClick;
    TabOrder := 0;
    Parent := ButtonPanel;
  end;

  with SaveToHTMLButton do begin
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := PrintButton;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Left := Margin;
    AutoSize := True;
    Caption := rsSaveToHTML;
    OnClick := @SaveToHTMLButtonClick;
    TabOrder := 1;
    Parent := ButtonPanel;
  end;

  with CloseButton do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := ButtonPanel;
    AnchorSideRight.Side := asrRight;
    Caption := rsClose;
    ModalResult := mrClose;
    TabOrder := 2;
    Parent := ButtonPanel;
  end;
end;

destructor T2ColSummaryDialog.Destroy;
begin
  inherited Destroy;
end;

end.

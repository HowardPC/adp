unit uProgressWindow;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  Forms, ComCtrls, Controls;

type

  TProgressWindow = class(TForm)
  private
    ProgressBar: TProgressBar;
    procedure SetProgress(aValue: Integer);
  public
    constructor Create(TheOwner: TComponent); override;
    property Progress: Integer write SetProgress;
  end;

implementation

procedure TProgressWindow.SetProgress(aValue: Integer);
begin
  ProgressBar.Position := aValue;
end;

constructor TProgressWindow.Create(TheOwner: TComponent);
begin
  inherited CreateNew(TheOwner);
  BorderStyle := bsDialog;
  FormStyle := fsStayOnTop;
  Position := poScreenCenter;
  SetInitialBounds(0, 0, 220, 40);
  AutoSize := True;
  ProgressBar := TProgressBar.Create(Self);
  Caption := 'Working ...';
  Visible := False;

  with ProgressBar do begin
    Align := alClient;
    BorderSpacing.Around := 10;
    Constraints.MinWidth := 200;
    Parent := Self;
  end;
end;

end.


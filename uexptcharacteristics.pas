unit uExptCharacteristics;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  Forms, StdCtrls, ExtCtrls, Grids, ComCtrls, Controls, Dialogs,
  uEngine;

type
  TExptCharacteristicForm = class(TForm)
  private
    buttonPanel: TPanel;
    closeButton: TButton;
    generateDATbutton: TButton;
    grid: TStringGrid;
    opendlg: TOpenDialog;
    splitter: TSplitter;
    treeView: TTreeView;

    pex: PExperiment;
    procedure closeButtonClick(Sender: TObject);
    procedure DoProgress(const aPercentage: Single);
    procedure generateInitialDATClick(Sender: TObject);
    procedure gridPrepareCanvas(sender: TObject; aCol, aRow: Integer; {%H-}aState: TGridDrawState);
    procedure treeViewSelectionChanged(Sender: TObject);
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TDraw = class(TObject)
  public
    FEnabled: Boolean;
    constructor Create(drawEnabled: Boolean = True); reintroduce;
  end;

  procedure ShowExperimentCharacteristicsDlg;

implementation

uses
  Graphics, LazFileUtils,
  uTypesConstsUtils, uMainForm;

procedure ShowExperimentCharacteristicsDlg;
begin
  with TExptCharacteristicForm.Create(Nil) do
    begin
      ShowModal;
      Free;
    end;
end;

constructor TDraw.Create(drawEnabled: Boolean);
begin
  FEnabled := drawEnabled;
end;

constructor TExptCharacteristicForm.Create(TheOwner: TComponent);
var
  exptItem: TTreeNode;
  i, c, r: Integer;
begin
  inherited CreateNew(TheOwner);
  SetInitialBounds(0, 0, 900, 400);
  Caption := rsSupptdExptChars;
  BorderStyle := bsDialog;
  Position := poScreenCenter;

  buttonPanel := TPanel.Create(Self);
  closeButton := TButton.Create(Self);
  generateDATbutton := TButton.Create(Self);
  grid := TStringGrid.Create(Self);
  splitter := TSplitter.Create(Self);
  treeView := TTreeView.Create(Self);

  with buttonPanel do begin
    Align := alBottom;
    BevelOuter := bvNone;
    TabOrder := 1;
    Parent := Self;
  end;

  with closeButton do begin
    Caption := rsClose;
    ModalResult := mrClose;
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := buttonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := buttonPanel;
    AnchorSideRight.Side := asrRight;
    BorderSpacing.Right := Margin;
    Parent := buttonPanel;
  end;

  with generateDATbutton do begin
    Caption := rsGenerateDAT;
    ModalResult := mrNone;
    AutoSize := True;
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := buttonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := closeButton;
    AnchorSideRight.Side := asrLeft;
    BorderSpacing.Right := Margin;
    OnClick  := @generateInitialDATClick;
    Visible := False;
    Parent := buttonPanel;
  end;

  with treeView do begin
    Align := alLeft;
    Width := 280;
    BorderSpacing.Left := Margin;
    BorderSpacing.Top := Margin;
    AutoExpand := True;
    ReadOnly := True;
    TabOrder := 0;
    ScrollBars := ssAutoHorizontal;
    OnSelectionChanged := @TreeViewSelectionChanged;
    Options := [tvoAutoExpand, tvoAutoItemHeight, tvoHideSelection, tvoKeepCollapsedNodes, tvoReadOnly, tvoShowButtons, tvoShowLines, tvoShowRoot, tvoToolTips, tvoThemedDraw];
    Parent := Self;
  end;

  with splitter do begin
    Align := alLeft;
    Left := 1;
    BorderSpacing.Top := Margin;
    Parent := Self;
  end;

  with grid do begin
    Align := alClient;
    Left := 2;
    BorderSpacing.Top := Margin;
    BorderSpacing.Right := Margin;
    AutoEdit := False;
    DefaultRowHeight := 26;
    ColCount := 2;
    with Columns.Add do begin
      ReadOnly := True;
      SizePriority := 0;
      Title.Caption := rsExptCharacteristic;
      Width := 185;
    end;
    with Columns.Add do begin
      ReadOnly := True;
      SizePriority := 0;
      Title.Caption := rsValue;
      Width := 500;
    end;
    FixedCols := 0;
    Options := [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goSmoothScroll];
    RowCount := Succ(LastRow);
    TabStop := False;
    TitleFont.Style := [fsBold];
    for c := 0 to ColCount-1 do
      for r := 0 to RowCount-1 do
        Objects[c, r] := TDraw.Create;
    OnPrepareCanvas  := @gridPrepareCanvas;
    //ExtendedColSizing := True;
    Parent := Self;
  end;

  exptItem := treeView.Items.AddChild(Nil, rsSupptdExpts);
  for i := Low(RExperiments) to High(RExperiments) do
    treeView.Items.AddChildObject(exptItem, RExperiments[i].Name, PExperiments[i]);

end;

procedure TExptCharacteristicForm.closeButtonClick(Sender: TObject);
begin
  Close;
end;

destructor TExptCharacteristicForm.Destroy;
var
  c, r: Integer;
begin
  for c := 0 to grid.ColCount-1 do
    for r := 0 to grid.RowCount-1 do
      TDraw(grid.Objects[c, r]).Free;
  pex := Nil;
  inherited Destroy;
end;

procedure TExptCharacteristicForm.DoProgress(const aPercentage: Single);
begin
  if not MainForm.ProgressWindow.Visible then
    MainForm.ProgressWindow.Visible := True;
  MainForm.ProgressWindow.Progress := Succ(Round(aPercentage));
end;

procedure TExptCharacteristicForm.generateInitialDATClick(Sender: TObject);
var
  fpro: TFileProcessor;
  outcome: EnOutcome;
  dataStr: AnsiString;
  rejects: TStringArray;
  fExists: Boolean;
begin
  if not Assigned(pex) then
    Exit;

  generatingFirstDAT := True;
  generateDATbutton.Enabled := False;
  opendlg := TOpenDialog.Create(Self);
  opendlg.Filter := 'tab-delimited text files|*.txt';
  opendlg.Options := [ofPathMustExist, ofFileMustExist, ofNoNetworkButton];
  opendlg.Title := 'Locate text file with merged participants'' data';
  case opendlg.Execute of
    True: begin
            fpro := TFileProcessor.Create(opendlg.FileName, @DoProgress, outcome, dataStr, rejects, fExists);
            try
              MainForm.ProgressWindow.Hide;
              if (outcome = _oSuccess) then
                case (fpro.ParticipantCount > MinViableNormCount) of
                  True: begin
                          pex^.normList := TNormList.Create(fpro, GetExperimentIdx(pex), outcome, True);
                          case (outcome = _oSuccess) of
                            True:  begin
                                     pex^.normDATFilename := AppendPathDelim(GetConfigDirectory) + 'Norm_' + pex^.Name + '.dat';
                                     pex^.normDATFileExists := FileExistsUTF8(pex^.normDATFilename);
                                     grid.Cells[ValuesCol, DATexistsRow] := BoolToStr(pex^.normDATFileExists, True);
                                     if pex^.normDATFileExists then
                                       begin
                                         grid.Cells[ValuesCol, DATfilenameRow] := pex^.NormDATFilename;
                                         pex^.normCSVFilename := AppendPathDelim(GetConfigDirectory) + 'Norm_' + pex^.name + '.txt';
                                         grid.Cells[ValuesCol, CSVfilenameRow] := pex^.NormCSVFilename;
                                         grid.Cells[ValuesCol, ParticipantCountRow] := pex^.NormList.DataItemCount.ToString;
                                         ShowMessageFmt('Created new normative database with %d entries'#10'Application will now close',[pex^.normList.DataItemCount]);
                                         MainForm.Close;
                                       end;
                                   end;
                            False: ShowMessageFmt('Failed to create normative database from %s',[ExtractFileName(opendlg.FileName)]);
                          end;
                        end;
                  False: ShowMessageFmt('%s does not contain data from enough participants to start a normative database',[ExtractFileName(opendlg.FileName)]);
                end;
            finally
              fpro.Free;
             // pex^.normList.Free;
            end;
          end;
    False: generateDATbutton.Enabled := True;
  end;
  generatingFirstDAT := False;
end;

procedure TExptCharacteristicForm.gridPrepareCanvas(sender: TObject; aCol, aRow: Integer; aState: TGridDrawState);
begin
  if not TDraw(grid.Objects[aCol, aRow]).FEnabled then
    begin
      grid.Canvas.Font.Size := 8;
      grid.Canvas.Pen.Color := clGrayText;
      grid.Canvas.Brush.Color := clInactiveCaption;
    end;
end;

procedure TExptCharacteristicForm.treeViewSelectionChanged(Sender: TObject);
begin
  if Assigned(TreeView.Selected) and Assigned(TreeView.Selected.Data) then
    begin
      pex := TreeView.Selected.Data;
      grid.Cells[NameCol, SupportsNormRow] := 'supports normative data';
      grid.Cells[NameCol, DATexistsRow] := 'normative DAT file exists';
      grid.Cells[NameCol, DATfilenameRow] := 'normative DAT filename';
      grid.Cells[NameCol, CSVfilenameRow] := 'normative CSV filename';
      grid.Cells[NameCol, ParticipantCountRow] := 'normative participant count';
      grid.Cells[NameCol, FemaleCountRow] := 'female count';
      grid.Cells[NameCol, MaleCountRow] := 'male count';
      grid.Cells[NameCol, RighthandedRow] := 'righthanded count';
      grid.Cells[NameCol, LefthandedRow] := 'lefthanded count';
      grid.Cells[NameCol, AgesRow] := 'participants'' ages';
      grid.Cells[NameCol, SessionsRow] := 'sessions';

      grid.Cells[ValuesCol, SupportsNormRow] := BoolToStr(pex^.SupportsNormativeData, True);
      case pex^.SupportsNormativeData of
        True: begin
                grid.Cells[ValuesCol, DATexistsRow] := BoolToStr(pex^.normDATFileExists, True);
                case pex^.normDATFileExists of
                  True: begin
                          generateDATbutton.Visible := False;
                          grid.Cells[ValuesCol, DATfilenameRow] := pex^.NormDATFilename;
                          grid.Cells[ValuesCol, CSVfilenameRow] := pex^.NormCSVFilename;
                          if Assigned(pex^.NormList) then begin
                            grid.Cells[ValuesCol, ParticipantCountRow] := pex^.NormList.DataItemCount.ToString;
                            grid.Cells[ValuesCol, FemaleCountRow] := pex^.normList.FemaleCount.ToString;
                            grid.Cells[ValuesCol, MaleCountRow] := pex^.normList.MaleCount.ToString;
                            grid.Cells[ValuesCol, RighthandedRow] := pex^.normList.RighthandedCount.ToString;
                            grid.Cells[ValuesCol, LefthandedRow] := pex^.normList.LefthandedCount.ToString;
                            grid.Cells[ValuesCol, AgesRow] := ByteSetToStr(pex^.normList.AgeSet);
                            grid.Cells[ValuesCol, SessionsRow] := ByteSetToStr(pex^.normList.SessionSet);
                          end;
                        end;
                  False: begin
                           generateDATbutton.Caption := rsGenerateDAT + pex^.name;
                           generateDATbutton.Visible := True;
                         end;
                end;
              end;
        False: begin
                 TDraw(grid.Objects[NameCol, DATexistsRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, DATfilenameRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, CSVfilenameRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, ParticipantCountRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, FemaleCountRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, MaleCountRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, RighthandedRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, LefthandedRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, AgesRow]).FEnabled := False;
                 TDraw(grid.Objects[NameCol, SessionsRow]).FEnabled := False;
               end;
      end;
    end;
end;

end.


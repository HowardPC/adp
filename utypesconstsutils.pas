unit uTypesConstsUtils;

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}

interface

uses
  Classes, SysUtils;

resourcestring
  rsAboutADP                = 'About ADP';
  rsAddedDataToNormative    = 'Added data from'#10'  %s'#10' to the normative database which now has %d entries';
  rsAddResults              = 'Add results to normative data';
  rsADP                     = 'Automatic Data Processor (ADP)';
  rsADPIsATool              = 'ADP is a tool for automatic processing of output data'#10'         from the Working Memory Analyser program';
  rsADPReport               = 'Automatic Data Processor report created %s for an experiment performed %s';
  rsADPSettings             = 'ADP Settings';
  rsAlthoughSupportsNorm    = 'Although the experiment supports use of normative data the required normative database is missing.'#10'Features using normative data will be disabled.';
  rsAnd                     = 'and';
  rsAnExistingFile          = '  an existing file "%s" was overwritten with new data';
  rsAParticipantWasFound    = '  A participant was found (%s) with invalid data which will be ignored';
  rsAttemptToAddParticipant = 'Attempt to add participant data to normative data failed';
  rsBody                    = '  <body>';
  rsBodyLine9               = '<p class="ps"><span class="tscour9">';
  rsChooseStatisticsTo      = 'Choose statistics to show';
  rsClose                   = ' Close ';
  rsComma                   = 'Comma';
  rsCopyrightGF             = 'Copyright © 2021 Giorgio Fuggetta';
  rsCurrentValue            = 'Current value';
  rsDataCfgParsed           = 'The data file has been validated, the data processed successfully,'#10 + 'and the processed data saved in a tab-delimited text file.'#10'This data is not yet in the normative database.';
  rsDataFileSParsed         = '  data file "%s" parsed successfully';
  rsDataFirstLine           = 'experiment	date	start_time	trial_order_file_no	participantid	age	sex	handedness	display_type	observer_number	session_no	s1_marker	s2_marker	s3_marker	s4_marker	s1_shape	s1_quad	s1_duration	s1_s2_isi	s2_shape_position_1(nw)	s2_shape_position_2(ne)	s2_shape_position_3(se)	s2_shape_position_4(sw)	s2_shape_position_5(centre)	s2_duration	s2_s3_isi	s3_shape	s3_distractor_shape	s3_quad	s3_duration	s3_s4_isi	s4_shape	s4_distractor_shape	s4_quad	s4_duration	response_time_after_s4	feedback_shape	feedback_duration_after_response_time	iti_after_feedback	s1_colour	s2_colour_position_1(nw)	s2_colour_position_2(ne)	s2_colour_position_3(se)	s2_colour_position_4(sw)	s2_colour_position_5(centre)	s3_colour	s3_distractor_colour	s4_colour	s4_distractor_colour	keymapping	tasktype	tms_s3_soa	experimental_condition	response	accuracy	rt_ms	rt_ms_minus_constant_error	s1_onsettime_ms	s2_onsettime_ms	s3_onsettime_ms	tms_onsettime_ms	s4_onsettime_ms	response_onsettime_ms	feedback_onsettime_ms	blank_onsettime_ms	user_paused_the_trial	factor_1	factor_2	factor_3	factor_4	factor_5	factor_6	factor_7	factor_8	factor_9';
  rsDataProcessedSuccess    = 'Data processed successfully';
  rsDesignedByGF            = 'Designed by Giorgio Fuggetta';
  rsDidThisExperiment       = ' did this experiment ';
  rsDocType                 = '<!DOCTYPE html>';
  rsDontAddResults          = 'Don''t add results to normative data';
  rsDoYouWantToAdd          = 'Do you want to add the data from'#10 + '  %s'#10 + 'to the normative data?';
  rsDParticipantsFromNorm   = '%d participants from the normative data are being used to compute z-scores, percentiles and p-values for %s';
  rsDPValuesSignificant     = 'Note: %d p-values are statistically significant with p ≤ 0.05, in bold';
  rsEditCurrentSettings     = 'Edit current settings';
  rsEllipsis                = '...';
  rsEndBodyLine             = '</span></p>';
  rsEndHead                 = '  </head>';
  rsEndStyle                = '    </style>';
  rsEndTitle                = '    </title>';
  rsExcludingTraining       = '  Excluding training trials, %s did %d trials';
  rsExcludingTrainingEach   = '  Excluding training trials, each participant did %d trials';
  rsExperimentWasPerformed  = '%sExperiment was performed %s';
  rsExptCharacteristic      = 'Experiment characteristic';
  rsFailedToSaveOutput      = 'Failed to save output file with comma delimiter';
  rsFalse                   = 'False';
  rsGenerateDAT             = 'Generate initial normative DAT file for ';
  rsGNULicence              = 'This program is free software: you can redistribute it and/or modify it'#10'under the terms of the GNU General Public License v3.0'#10'  '#10'This code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;'#10'without even the implied warranty of MERCHANTABILITY or FITNESS FOR A'#10'PARTICULAR PURPOSE.  See the GNU General Public License for more details.'#10'A copy of the GNU General Public License is available on the World Wide Web at:';
  rsGNUurl                  = 'http://www.gnu.org/copyleft/gpl.html';
  rsHandedness              = 'Handedness';
  rsHead                    = '  <head>    <meta charset="utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge" />    <meta name="viewport" content="width=device-width, initial-scale=1" />';
  rsHTML                    = '  <html lang="en">';
  rsHTMLEndDoc              = '  </body>  </html>';
  rsHTMLHeader              = '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><title>';
  rsHtmlReportSaved         = 'A statistic summary report for %s has been saved in the HTML file'#10'%s';
  rsLeader                  = ' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .';
  rsMergedText              = 'merged data from several participants';
  rsNbsp                    = '&nbsp;';
  rsNoFileSelected          = '(no file selected)';
  rsNoPValuesSignificant    = 'No p-values are statistically significant with p ≤ 0.05';
  rsNormHeaderLine          = 'experiment	date	start_time	trial_order_file_no	participantid	age	sex	handedness	session_no	N_TRIALS_ACCURACY_SUMMARY_COUNT	MEAN_ACCURACY_SUMMARY_MEAN	MEAN_ERROR_RATE_ACCURACY_SUMMARY_MEAN	COWAN_K_SUMMARY_MEAN	A_SUMMARY_MEAN	D-PRIME_SUMMARY_MEAN	N_CORRECT_TRIALS_RESPONSE_TIME_SUMMARY_COUNT	MEAN_CORRECT_RESPONSE_TIME_SUMMARY_MEAN	MIN_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_	SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_';
  rsNormHeaderLineComma     = 'experiment,date,start_time,trial_order_file_no,participantid,age,sex,handedness,session_no,N_TRIALS_ACCURACY_SUMMARY_COUNT,MEAN_ACCURACY_SUMMARY_MEAN,MEAN_ERROR_RATE_ACCURACY_SUMMARY_MEAN,COWAN_K_SUMMARY_MEAN,A_SUMMARY_MEAN,D-PRIME_SUMMARY_MEAN,N_CORRECT_TRIALS_RESPONSE_TIME_SUMMARY_COUNT,MEAN_CORRECT_RESPONSE_TIME_SUMMARY_MEAN,MIN_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ACCURACY_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_ERROR_RATE_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_COWAN_K_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_A_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_D-PRIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MIN_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,MAX_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_,SUM_DISTRACTOR_OBJECT_AND_SPATIAL_COMPATIBILITY_MEAN_CORRECT_RESPONSE_TIME_LOAD_CUEDIS_DIFF_MINUS_CUEDIS_SAME_';
  rsNoSupportForNormative   = ' (which does not support comparison with normative data)';
  rsNotEnoughParticipants   = 'Not enough participants (only %d) fulfil the selection criteria to compute z-scores, percentiles and p-values, so these will not be calculated or added to the summary report for %s';
  rsNoteThatParticipants    = 'Note that %d participants from the normative data are being used to compute z-scores, percentiles and p-values for %s';
  rsOK                      = 'OK';
  rsOnePValueSignificant    = 'Note: one p-value is statistically significant with p ≤ 0.05, in bold';
  rsOnlyInvalidFound        = 'Only invalid participants found';
  rsOutputFileDelimiter     = 'Output file delimiter';
  rsParticipantsWereFound   = '  %d participants were found (%s) with invalid data which will be ignored';
  rsPostBody                = '</pre></body></html>';
  rsPreBody                 = '</title><style type="text/css">h1 { font-size: 11pt; font-family: ''Courier New'', ''Courier'', monospace; }</style></head><body><pre>';
  rsPrintA4Landscape        = 'Print (A4 landscape)';
  rsProcessedDataSummary    = 'Processed data summary ';
  rsProcessedReportByADP    = 'Processed data summary report prepared by Automatic Data Processor ';
  rsProgrammedByHPC         = 'Programmed by Howard Page-Clark';
  rsPs                      = 'ps { }';
  rsSavedOutputFileWith     = 'Saved output file with comma delimiters (rather than Tabs)';
  rsSaveToHTML              = 'Save to HTML';
  rsSession                 = 'Session';
  rsSetting                 = 'Setting';
  rsSex                     = 'Sex';
  rsSExperimentWasPerformed = '%s  experiment was performed %s';
  rsShowFullPath            = 'Show full path in filenames';
  rsSingleText              = 'data from a single participant';
  rsSRanThisExperiment      = '%s ran this experiment on %s,   report prepared by Automatic Data Processor on %s';
  rsSSummaryReportFor       = '%sSummary report for participant: %s    Printed by Automatic Data Processor %s                                  Page %d';
  rsStatisticsGenerated     = '  statistics generated successfully';
  rsStyle                   = '    <style type="text/css">';
  rsSummaryReport           = 'Summary report prepared by Automatic Data Processor ';
  rsSummaryReportForPage    = '%sSummary report for participant: %s    Printed by Automatic Data Processor %s                                 Page %d';
  rsSupportsNormativeData   = ' (which supports comparison with normative data)';
  rsSupptdExptChars         = 'Supported experiment characteristics';
  rsSupptdExpts             = 'Supported experiments';
  rsTab                     = 'Tab';
  rsTechnicalDetails        = ' Technical details... ';
  rsTitle                   = '    <title>';
  rsTrue                    = 'True';
  rsTsCour10                = 'span.tscour9 { font-size: 10pt; font-family: ''Courier New'', ''Courier'', monospace; }';
  rsUnderlineDashesH        = ' -------------                                                                           ---------';
  rsUnderlineDashesP        = ' -------------                                                                                                   ---------';
  rsUnderlineDashesZ        = '-------------                                                                           ---------  ------- ---------- -------';
  rsUnderlineDashesZP       = '-------------                                                                               ---------  ------- ---------- -------';
  rsUnknownExperiment       = 'unknown experiment';
  rsUnknownVersionNumber    = ' unknown version number';
  rsUnspecified             = '(unspecified)';
  rsUnspecifiedYears        = '(unspecified years)';
  rsUValue                  = '<u>Value     <u>';
  rsUVariableName           = '<u>Variable name<u>';
  rsValue                   = 'Value';
  rsVariableNamePValue      = 'Variable name                                                                                   Value  Z-score Percentile p-value';
  rsVariableNameValueH      = ' Variable name                                                                           Value';
  rsVariableNameValueP      = ' Variable name                                                                                                   Value';
  rsVariableNameValueZ      = 'Variable name                                                                               Value  Z-score Percentile p-value';
  rsVariableNameValueZP     = '<u>Variable name<u>                                                    <u>Value      <u> <u>Z-score<u> <u>Percentile<u> <u>p-value<u>';
  rsVersion_dot_dot_        = ' version %d.%d.%d';
  rsWaitingForData          = ' (waiting for data to process)';
  rsWarnIfOverwriting       = 'Warn if overwriting output file';
  rsYouCanAlsoObtain        = 'You can also obtain it by writing to the'#10' '#10'       Free Software Foundation, Inc.,'#10'       51  Franklin Street - Fifth Floor,'#10'       Boston, MA 02110-1335, USA';
  rsYouCanViewGenerated     = '  You can view generated statistics by clicking the ''View processed data'' tab';

const
  Margin = 10;
  HalfMargin = Margin div 2;
  DoubleMargin = Margin * 2;
  HSpacing = 4;
  VSpacing = 8;//6;
  MinDataFileSize  = 170000;
  MinDataLineCount = 400;      // set to some reasonable minimum
  MinDataFileLineLength = 340; // check - may be too high
  HighSummaryReportIndex = 61; // hardcoded here - change if Giorgio changes config file
  MaxExperimentIndex = 0;      // hardcoded here - change if further experiments added

  MinViableNormCount = 30;
  ThreshholdPValue = 0.05000;

  HighStatID = 515;
  StatisticCount = 516;
  HighBehestIndex = 531;

  SettingsCol = Byte(0);
  NameCol = Byte(0);
  ValuesCol = Byte(1);
  SupportsNormRow = 1;
  DelimiterRow = Byte(1);
  DATexistsRow = 2;
  WarnOverwriteRow = Byte(2);
  DATfilenameRow = 3;
  ShowFullFilenameRow = Byte(3);
  CSVfilenameRow = 4;
  ParticipantCountRow = 5;
  FemaleCountRow = 6;
  MaleCountRow = 7;
  RighthandedRow = 8;
  LefthandedRow = 9;
  AgesRow = 10;
  SessionsRow = 11;
  LastRow = SessionsRow;

  Sqrt2Pi = Sqrt(2*Pi);
  TwoOverPi = 2/Pi;

  CfgSubdirectoryName  = 'Configuration files';
  OutputDataDirname    = 'Output data';
  SummaryReportDotHtml = 'SummaryReport.html';
  SummaryReportWithZScoresDotHtml = 'SummaryReport_with_ZScores.html';
  SettingsFilename = 'ADPSettings.txt';

const

  StatNames: array[0..HighStatID] of String = (
    'NTrialsAccuracyLoad1CueDisDiffTarDH',
    'NTrialsAccuracyLoad1CueDisDiffTarSH',
    'NTrialsAccuracyLoad1CueDisSameTarDH',
    'NTrialsAccuracyLoad1CueDisDiffTarSH',
    'NTrialsAccuracyLoad2CueDisDiffTarDH',
    'NTrialsAccuracyLoad2CueDisDiffTarSH',
    'NTrialsAccuracyLoad2CueDisSameTarDH',
    'NTrialsAccuracyLoad2CueDisSameTarSH',
    'NTrialsAccuracyLoad3CueDisDiffTarDH',
    'NTrialsAccuracyLoad3CueDisDiffTarSH',
    'NTrialsAccuracyLoad3CueDisSameTarDH',
    'NTrialsAccuracyLoad3CueDisSameTarSH',
    'NTrialsAccuracyLoad4CueDisDiffTarDH',
    'NTrialsAccuracyLoad4CueDisDiffTarSH',
    'NTrialsAccuracyLoad4CueDisSameTarDH',
    'NTrialsAccuracyLoad4CueDisSameTarSH',
    'NTrialsAccuracySummaryCount',
    'MeanAccuracyLoad1CueDisDiffTarDH',
    'MeanAccuracyLoad1CueDisDiffTarSH',
    'MeanAccuracyLoad1CueDisSameTarDH',
    'MeanAccuracyLoad1CueDisSameTarSH',
    'MeanAccuracyLoad2CueDisDiffTarDH',
    'MeanAccuracyLoad2CueDisDiffTarSH',
    'MeanAccuracyLoad2CueDisSameTarDH',
    'MeanAccuracyLoad2CueDisSameTarSH',
    'MeanAccuracyLoad3CueDisDiffTarDH',
    'MeanAccuracyLoad3CueDisDiffTarSH',
    'MeanAccuracyLoad3CueDisSameTarDH',
    'MeanAccuracyLoad3CueDisSameTarSH',
    'MeanAccuracyLoad4CueDisDiffTarDH',
    'MeanAccuracyLoad4CueDisDiffTarSH',
    'MeanAccuracyLoad4CueDisSameTarDH',
    'MeanAccuracyLoad4CueDisSameTarSH',
    'MeanAccuracySummaryMean',
    'MeanErrorRateLoad1CueDisDiffTarDH',
    'MeanErrorRateLoad1CueDisDiffTarSH',
    'MeanErrorRateLoad1CueDisSameTarDH',
    'MeanErrorRateLoad1CueDisSameTarSH',
    'MeanErrorRateLoad2CueDisDiffTarDH',
    'MeanErrorRateLoad2CueDisDiffTarSH',
    'MeanErrorRateLoad2CueDisSameTarDH',
    'MeanErrorRateLoad2CueDisSameTarSH',
    'MeanErrorRateLoad3CueDisDiffTarDH',
    'MeanErrorRateLoad3CueDisDiffTarSH',
    'MeanErrorRateLoad3CueDisSameTarDH',
    'MeanErrorRateLoad3CueDisSameTarSH',
    'MeanErrorRateLoad4CueDisDiffTarDH',
    'MeanErrorRateLoad4CueDisDiffTarSH',
    'MeanErrorRateLoad4CueDisSameTarDH',
    'MeanErrorRateLoad4CueDisSameTarSH',
    'MeanErrorRateAccuracySummaryMean',
    'ArcsinMeanAccuracyLoad1CueDisDiffTarDH',
    'ArcsinMeanAccuracyLoad1CueDisDiffTarSH',
    'ArcsinMeanAccuracyLoad1CueDisSameTarDH',
    'ArcsinMeanAccuracyLoad1CueDisSameTarSH',
    'ArcsinMeanAccuracyLoad2CueDisDiffTarDH',
    'ArcsinMeanAccuracyLoad2CueDisDiffTarSH',
    'ArcsinMeanAccuracyLoad2CueDisSameTarDH',
    'ArcsinMeanAccuracyLoad2CueDisSameTarSH',
    'ArcsinMeanAccuracyLoad3CueDisDiffTarDH',
    'ArcsinMeanAccuracyLoad3CueDisDiffTarSH',
    'ArcsinMeanAccuracyLoad3CueDisSameTarDH',
    'ArcsinMeanAccuracyLoad3CueDisSameTarSH',
    'ArcsinMeanAccuracyLoad4CueDisDiffTarDH',
    'ArcsinMeanAccuracyLoad4CueDisDiffTarSH',
    'ArcsinMeanAccuracyLoad4CueDisSameTarDH',
    'ArcsinMeanAccuracyLoad4CueDisSameTarSH',
    'ArcsinMeanAccuracySummaryMean',
    'NTrialsAccuracyLoad1CueDisDiffTarDHCueTarDiff',
    'NTrialsAccuracyLoad1CueDisDiffTarDHCueTarSame',
    'NTrialsAccuracyLoad1CueDisDiffTarSHCueTarDiff',
    'NTrialsAccuracyLoad1CueDisDiffTarSHCueTarSame',
    'NTrialsAccuracyLoad1CueDisSameTarDHCueTarDiff',
    'NTrialsAccuracyLoad1CueDisSameTarDHCueTarSame',
    'NTrialsAccuracyLoad1CueDisSameTarSHCueTarDiff',
    'NTrialsAccuracyLoad1CueDisSameTarSHCueTarSame',
    'NTrialsAccuracyLoad2CueDisDiffTarDHCueTarDiff',
    'NTrialsAccuracyLoad2CueDisDiffTarDHCueTarSame',
    'NTrialsAccuracyLoad2CueDisDiffTarSHCueTarDiff',
    'NTrialsAccuracyLoad2CueDisDiffTarSHCueTarSame',
    'NTrialsAccuracyLoad2CueDisSameTarDHCueTarDiff',
    'NTrialsAccuracyLoad2CueDisSameTarDHCueTarSame',
    'NTrialsAccuracyLoad2CueDisSameTarSHCueTarDiff',
    'NTrialsAccuracyLoad2CueDisSameTarSHCueTarSame',
    'NTrialsAccuracyLoad3CueDisDiffTarDHCueTarDiff',
    'NTrialsAccuracyLoad3CueDisDiffTarDHCueTarSame',
    'NTrialsAccuracyLoad3CueDisDiffTarSHCueTarDiff',
    'NTrialsAccuracyLoad3CueDisDiffTarSHCueTarSame',
    'NTrialsAccuracyLoad3CueDisSameTarDHCueTarDiff',
    'NTrialsAccuracyLoad3CueDisSameTarDHCueTarSame',
    'NTrialsAccuracyLoad3CueDisSameTarSHCueTarDiff',
    'NTrialsAccuracyLoad3CueDisSameTarSHCueTarSame',
    'NTrialsAccuracyLoad4CueDisDiffTarDHCueTarDiff',
    'NTrialsAccuracyLoad4CueDisDiffTarDHCueTarSame',
    'NTrialsAccuracyLoad4CueDisDiffTarSHCueTarDiff',
    'NTrialsAccuracyLoad4CueDisDiffTarSHCueTarSame',
    'NTrialsAccuracyLoad4CueDisSameTarDHCueTarDiff',
    'NTrialsAccuracyLoad4CueDisSameTarDHCueTarSame',
    'NTrialsAccuracyLoad4CueDisSameTarSHCueTarDiff',
    'NTrialsAccuracyLoad4CueDisSameTarSHCueTarSame',
    'MeanAccuracyLoad1CueDisDiffTarDHCueTarDiff',
    'MeanAccuracyLoad1CueDisDiffTarDHCueTarSame',
    'MeanAccuracyLoad1CueDisDiffTarSHCueTarDiff',
    'MeanAccuracyLoad1CueDisDiffTarSHCueTarSame',
    'MeanAccuracyLoad1CueDisSameTarDHCueTarDiff',
    'MeanAccuracyLoad1CueDisSameTarDHCueTarSame',
    'MeanAccuracyLoad1CueDisSameTarSHCueTarDiff',
    'MeanAccuracyLoad1CueDisSameTarSHCueTarSame',
    'MeanAccuracyLoad2CueDisDiffTarDHCueTarDiff',
    'MeanAccuracyLoad2CueDisDiffTarDHCueTarSame',
    'MeanAccuracyLoad2CueDisDiffTarSHCueTarDiff',
    'MeanAccuracyLoad2CueDisDiffTarSHCueTarSame',
    'MeanAccuracyLoad2CueDisSameTarDHCueTarDiff',
    'MeanAccuracyLoad2CueDisSameTarDHCueTarSame',
    'MeanAccuracyLoad2CueDisSameTarSHCueTarDiff',
    'MeanAccuracyLoad2CueDisSameTarSHCueTarSame',
    'MeanAccuracyLoad3CueDisDiffTarDHCueTarDiff',
    'MeanAccuracyLoad3CueDisDiffTarDHCueTarSame',
    'MeanAccuracyLoad3CueDisDiffTarSHCueTarDiff',
    'MeanAccuracyLoad3CueDisDiffTarSHCueTarSame',
    'MeanAccuracyLoad3CueDisSameTarDHCueTarDiff',
    'MeanAccuracyLoad3CueDisSameTarDHCueTarSame',
    'MeanAccuracyLoad3CueDisSameTarSHCueTarDiff',
    'MeanAccuracyLoad3CueDisSameTarSHCueTarSame',
    'MeanAccuracyLoad4CueDisDiffTarDHCueTarDiff',
    'MeanAccuracyLoad4CueDisDiffTarDHCueTarSame',
    'MeanAccuracyLoad4CueDisDiffTarSHCueTarDiff',
    'MeanAccuracyLoad4CueDisDiffTarSHCueTarSame',
    'MeanAccuracyLoad4CueDisSameTarDHCueTarDiff',
    'MeanAccuracyLoad4CueDisSameTarDHCueTarSame',
    'MeanAccuracyLoad4CueDisSameTarSHCueTarDiff',
    'MeanAccuracyLoad4CueDisSameTarSHCueTarSame',
    'CowanKLoad1CueDisDiffTarDH',
    'CowanKLoad1CueDisDiffTarSH',
    'CowanKLoad1CueDisSameTarDH',
    'CowanKLoad1CueDisSameTarSH',
    'CowanKLoad2CueDisDiffTarDH',
    'CowanKLoad2CueDisDiffTarSH',
    'CowanKLoad2CueDisSameTarDH',
    'CowanKLoad2CueDisSameTarSH',
    'CowanKLoad3CueDisDiffTarDH',
    'CowanKLoad3CueDisDiffTarSH',
    'CowanKLoad3CueDisSameTarDH',
    'CowanKLoad3CueDisSameTarSH',
    'CowanKLoad4CueDisDiffTarDH',
    'CowanKLoad4CueDisDiffTarSH',
    'CowanKLoad4CueDisSameTarDH',
    'CowanKLoad4CueDisSameTarSH',
    'CowanKSummaryMean',
    'ALoad1CueDisDiffTarDH',
    'ALoad1CueDisDiffTarSH',
    'ALoad1CueDisSameTarDH',
    'ALoad1CueDisSameTarSH',
    'ALoad2CueDisDiffTarDH',
    'ALoad2CueDisDiffTarSH',
    'ALoad2CueDisSameTarDH',
    'ALoad2CueDisSameTarSH',
    'ALoad3CueDisDiffTarDH',
    'ALoad3CueDisDiffTarSH',
    'ALoad3CueDisSameTarDH',
    'ALoad3CueDisSameTarSH',
    'ALoad4CueDisDiffTarDH',
    'ALoad4CueDisDiffTarSH',
    'ALoad4CueDisSameTarDH',
    'ALoad4CueDisSameTarSH',
    'ASummaryMean',
    'D-PrimeLoad1CueDisDiffTarDH',
    'D-PrimeLoad1CueDisDiffTarSH',
    'D-PrimeLoad1CueDisSameTarDH',
    'D-PrimeLoad1CueDisSameTarSH',
    'D-PrimeLoad2CueDisDiffTarDH',
    'D-PrimeLoad2CueDisDiffTarSH',
    'D-PrimeLoad2CueDisSameTarDH',
    'D-PrimeLoad2CueDisSameTarSH',
    'D-PrimeLoad3CueDisDiffTarDH',
    'D-PrimeLoad3CueDisDiffTarSH',
    'D-PrimeLoad3CueDisSameTarDH',
    'D-PrimeLoad3CueDisSameTarSH',
    'D-PrimeLoad4CueDisDiffTarDH',
    'D-PrimeLoad4CueDisDiffTarSH',
    'D-PrimeLoad4CueDisSameTarDH',
    'D-PrimeLoad4CueDisSameTarSH',
    'D-PrimeSummaryMean',
    'NCorrectTrialsResponseTimeLoad1CueDisDiffTarDH',
    'NCorrectTrialsResponseTimeLoad1CueDisDiffTarSH',
    'NCorrectTrialsResponseTimeLoad1CueDisSameTarDH',
    'NCorrectTrialsResponseTimeLoad1CueDisSameTarSH',
    'NCorrectTrialsResponseTimeLoad2CueDisDiffTarDH',
    'NCorrectTrialsResponseTimeLoad2CueDisDiffTarSH',
    'NCorrectTrialsResponseTimeLoad2CueDisSameTarDH',
    'NCorrectTrialsResponseTimeLoad2CueDisSameTarSH',
    'NCorrectTrialsResponseTimeLoad3CueDisDiffTarDH',
    'NCorrectTrialsResponseTimeLoad3CueDisDiffTarSH',
    'NCorrectTrialsResponseTimeLoad3CueDisSameTarDH',
    'NCorrectTrialsResponseTimeLoad3CueDisSameTarSH',
    'NCorrectTrialsResponseTimeLoad4CueDisDiffTarDH',
    'NCorrectTrialsResponseTimeLoad4CueDisDiffTarSH',
    'NCorrectTrialsResponseTimeLoad4CueDisSameTarDH',
    'NCorrectTrialsResponseTimeLoad4CueDisSameTarSH',
    'NCorrectTrialsResponseTimeSummaryCount',
    'MeanCorrectResponseTimeLoad1CueDisDiffTarDH',
    'MeanCorrectResponseTimeLoad1CueDisDiffTarSH',
    'MeanCorrectResponseTimeLoad1CueDisSameTarDH',
    'MeanCorrectResponseTimeLoad1CueDisSameTarSH',
    'MeanCorrectResponseTimeLoad2CueDisDiffTarDH',
    'MeanCorrectResponseTimeLoad2CueDisDiffTarSH',
    'MeanCorrectResponseTimeLoad2CueDisSameTarDH',
    'MeanCorrectResponseTimeLoad2CueDisSameTarSH',
    'MeanCorrectResponseTimeLoad3CueDisDiffTarDH',
    'MeanCorrectResponseTimeLoad3CueDisDiffTarSH',
    'MeanCorrectResponseTimeLoad3CueDisSameTarDH',
    'MeanCorrectResponseTimeLoad3CueDisSameTarSH',
    'MeanCorrectResponseTimeLoad4CueDisDiffTarDH',
    'MeanCorrectResponseTimeLoad4CueDisDiffTarSH',
    'MeanCorrectResponseTimeLoad4CueDisSameTarDH',
    'MeanCorrectResponseTimeLoad4CueDisSameTarSH',
    'MeanCorrectResponseTimeSummaryMean',
    'SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
    'SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarDH',
    'SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarSH',
    'SqrtBefMeanCorrectResponseTimeSummaryMean',
    'SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
    'SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
    'SqrtMeanCorrectResponseTimeLoad1CueDisSameTarDH',
    'SqrtMeanCorrectResponseTimeLoad1CueDisSameTarSH',
    'SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
    'SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
    'SqrtMeanCorrectResponseTimeLoad2CueDisSameTarDH',
    'SqrtMeanCorrectResponseTimeLoad2CueDisSameTarSH',
    'SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
    'SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
    'SqrtMeanCorrectResponseTimeLoad3CueDisSameTarDH',
    'SqrtMeanCorrectResponseTimeLoad3CueDisSameTarSH',
    'SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
    'SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
    'SqrtMeanCorrectResponseTimeLoad4CueDisSameTarDH',
    'SqrtMeanCorrectResponseTimeLoad4CueDisSameTarSH',
    'SqrtMeanCorrectResponseTimeSummaryMean',
    'LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
    'LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
    'LnBefMeanCorrectResponseTimeLoad1CueDisSameTarDH',
    'LnBefMeanCorrectResponseTimeLoad1CueDisSameTarSH',
    'LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
    'LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
    'LnBefMeanCorrectResponseTimeLoad2CueDisSameTarDH',
    'LnBefMeanCorrectResponseTimeLoad2CueDisSameTarSH',
    'LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
    'LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
    'LnBefMeanCorrectResponseTimeLoad3CueDisSameTarDH',
    'LnBefMeanCorrectResponseTimeLoad3CueDisSameTarSH',
    'LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
    'LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
    'LnBefMeanCorrectResponseTimeLoad4CueDisSameTarDH',
    'LnBefMeanCorrectResponseTimeLoad4CueDisSameTarSH',
    'LnBefMeanCorrectResponseTimeSummaryMean',
    'LnMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
    'LnMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
    'LnMeanCorrectResponseTimeLoad1CueDisSameTarDH',
    'LnMeanCorrectResponseTimeLoad1CueDisSameTarSH',
    'LnMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
    'LnMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
    'LnMeanCorrectResponseTimeLoad2CueDisSameTarDH',
    'LnMeanCorrectResponseTimeLoad2CueDisSameTarSH',
    'LnMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
    'LnMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
    'LnMeanCorrectResponseTimeLoad3CueDisSameTarDH',
    'LnMeanCorrectResponseTimeLoad3CueDisSameTarSH',
    'LnMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
    'LnMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
    'LnMeanCorrectResponseTimeLoad4CueDisSameTarDH',
    'LnMeanCorrectResponseTimeLoad4CueDisSameTarSH',
    'LnMeanCorrectResponseTimeSummaryMean',
    'DistObjCompatMeanAccLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanAccLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanAccLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanAccLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatCowanKLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatCowanKLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatCowanKLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatCowanKLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatCowanKLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatCowanKLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatCowanKLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatCowanKLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatCowanKLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatCowanKLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatCowanKLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatCowanKLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatALoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatALoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatALoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatALoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatALoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatALoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatALoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatALoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatALoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatALoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatALoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatALoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatD-PrimeLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatD-PrimeLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatD-PrimeLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatD-PrimeLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObjCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObjCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObjCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObjCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistSpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame',
    'DistObj+SpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame',
    'MinDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatALoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatALoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatALoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatALoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatALoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatALoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MinDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'MaxDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
    'SumDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame');


type

  String2  = String[2];
  String7  = String[7];
  String8  = String[8];
  String10 = String[10];
  String16 = String[16];
  String17 = String[17];
  String65 = String[65];

  TByteSet = set of Byte;

  EnDataFileKind = (dfkUnknown, dfkSingleRaw, dfkSingleProcessed, dfkMerged);

  EnRadioState = (rsDisabled, rsEnabled);

  EnOutcome =  // in these identifiers, _ is used as placeholder for crude error message substitution
    (_oSuccess, oDataFile_DoesNotExist, oDataFile_IsNotText, oDataFile_IsTooSmall,
     oConfigurationDirectory_DoesNotExist, oUnableToCreateOutputDirectory_, _oOutputFileError,
     oDataFile_IsNotReadable, oDataFileExperimentName_DoesNotMatchAnyKnownExperimentName,
     oExperimentNameIsMissingFrom_, oDataFile_IsInvalid, _oDataFileContainsOnlyTrainingTrials,
     _oParticipantParsingError,
     _oFilterReadStrFieldError, _oStatisticIDError,
     oOutputFile_NotWritten,
     oParticipant_HasInvalidData,
     _oFilterCouldNotConvertStrField,
     _oExecuteBehestError,

     oParticipantCouldNotReadStrField, oParticipantFieldIsBlank, oParticipantCouldNotConvertStrField,
       oParticipantFilterSettingError,
     oFilterDeletionError,

     oNoParticipantLeftAfterDeletion,
     _oNormUnableToLoadFile, _oNormUnableToWriteFile, _oNormParticipantsNotCreated, _oNormParticipantAlreadyPresent,
     _oNormDataFileDoesNotExist, _oNormUnableToConvertParticipant,

     oNormNoDataToSave, oNormFilenameIsBlank, oNormUnableToWriteFileToDisk,
     oNormInvalidZ );

  RAddZScore = record
  public
    ageSpecified, sexSpecified, handedSpecified, sessionSpecified: Boolean;
    rightHanded, female: Boolean;
    loAge, hiAge, session: Integer;
    procedure Init(anAgeSpec, aSexSpec, aHandedSpec, aSessionSpec, aRighthanded, aFemale: Boolean;
                   aLoAge, aHiAge, aSession: Integer);
  end;

  RecZ = record
    z: Double;
    ar: Double; // standard normal area
    pc: Double; // percentile
    p: Double;
  end;

  TRecZArray = array of RecZ;
  //Z_score_(z)	Areas_under_the_standard_normal_curve	Percentile_(%)	p_value_<_0.05_2_tailed_(p)

  TADPException = class(Exception);

const

  ZP: TRecZArray = (
(z:-3.99; ar:0.0000; pc:0.00; p:0.0000),(z:-3.98; ar:0.0000; pc:0.00; p:0.0000),(z:-3.97; ar:0.0000; pc:0.00; p:0.0000),
(z:-3.96; ar:0.0000; pc:0.00; p:0.0000),(z:-3.95; ar:0.0000; pc:0.00; p:0.0000),(z:-3.94; ar:0.0000; pc:0.00; p:0.0000),
(z:-3.93; ar:0.0000; pc:0.00; p:0.0000),(z:-3.92; ar:0.0000; pc:0.00; p:0.0000),(z:-3.91; ar:0.0000; pc:0.00; p:0.0000),
(z:-3.90; ar:0.0000; pc:0.00; p:0.0000),(z:-3.89; ar:0.0001; pc:0.01; p:0.0002),(z:-3.88; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.87; ar:0.0001; pc:0.01; p:0.0002),(z:-3.86; ar:0.0001; pc:0.01; p:0.0002),(z:-3.85; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.84; ar:0.0001; pc:0.01; p:0.0002),(z:-3.83; ar:0.0001; pc:0.01; p:0.0002),(z:-3.82; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.81; ar:0.0001; pc:0.01; p:0.0002),(z:-3.80; ar:0.0001; pc:0.01; p:0.0002),(z:-3.79; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.78; ar:0.0001; pc:0.01; p:0.0002),(z:-3.77; ar:0.0001; pc:0.01; p:0.0002),(z:-3.76; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.75; ar:0.0001; pc:0.01; p:0.0002),(z:-3.74; ar:0.0001; pc:0.01; p:0.0002),(z:-3.73; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.72; ar:0.0001; pc:0.01; p:0.0002),(z:-3.71; ar:0.0001; pc:0.01; p:0.0002),(z:-3.70; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.69; ar:0.0001; pc:0.01; p:0.0002),(z:-3.68; ar:0.0001; pc:0.01; p:0.0002),(z:-3.67; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.66; ar:0.0001; pc:0.01; p:0.0002),(z:-3.65; ar:0.0001; pc:0.01; p:0.0002),(z:-3.64; ar:0.0001; pc:0.01; p:0.0002),
(z:-3.63; ar:0.0001; pc:0.01; p:0.0002),(z:-3.62; ar:0.0001; pc:0.01; p:0.0002),(z:-3.61; ar:0.0002; pc:0.02; p:0.0004),
(z:-3.60; ar:0.0002; pc:0.02; p:0.0004),(z:-3.59; ar:0.0002; pc:0.02; p:0.0004),(z:-3.58; ar:0.0002; pc:0.02; p:0.0004),
(z:-3.57; ar:0.0002; pc:0.02; p:0.0004),(z:-3.56; ar:0.0002; pc:0.02; p:0.0004),(z:-3.55; ar:0.0002; pc:0.02; p:0.0004),
(z:-3.54; ar:0.0002; pc:0.02; p:0.0004),(z:-3.53; ar:0.0002; pc:0.02; p:0.0004),(z:-3.52; ar:0.0002; pc:0.02; p:0.0004),
(z:-3.51; ar:0.0002; pc:0.02; p:0.0004),(z:-3.50; ar:0.0002; pc:0.02; p:0.0004),(z:-3.49; ar:0.0002; pc:0.02; p:0.0004),
(z:-3.48; ar:0.0003; pc:0.03; p:0.0006),(z:-3.47; ar:0.0003; pc:0.03; p:0.0006),(z:-3.46; ar:0.0003; pc:0.03; p:0.0006),
(z:-3.45; ar:0.0003; pc:0.03; p:0.0006),(z:-3.44; ar:0.0003; pc:0.03; p:0.0006),(z:-3.43; ar:0.0003; pc:0.03; p:0.0006),
(z:-3.42; ar:0.0003; pc:0.03; p:0.0006),(z:-3.41; ar:0.0003; pc:0.03; p:0.0006),(z:-3.40; ar:0.0003; pc:0.03; p:0.0006),
(z:-3.39; ar:0.0003; pc:0.03; p:0.0006),(z:-3.38; ar:0.0004; pc:0.04; p:0.0008),(z:-3.37; ar:0.0004; pc:0.04; p:0.0008),
(z:-3.36; ar:0.0004; pc:0.04; p:0.0008),(z:-3.35; ar:0.0004; pc:0.04; p:0.0008),(z:-3.34; ar:0.0004; pc:0.04; p:0.0008),
(z:-3.33; ar:0.0004; pc:0.04; p:0.0008),(z:-3.32; ar:0.0005; pc:0.05; p:0.0010),(z:-3.31; ar:0.0005; pc:0.05; p:0.0010),
(z:-3.30; ar:0.0005; pc:0.05; p:0.0010),(z:-3.29; ar:0.0005; pc:0.05; p:0.0010),(z:-3.28; ar:0.0005; pc:0.05; p:0.0010),
(z:-3.27; ar:0.0005; pc:0.05; p:0.0010),(z:-3.26; ar:0.0006; pc:0.06; p:0.0012),(z:-3.25; ar:0.0006; pc:0.06; p:0.0012),
(z:-3.24; ar:0.0006; pc:0.06; p:0.0012),(z:-3.23; ar:0.0006; pc:0.06; p:0.0012),(z:-3.22; ar:0.0006; pc:0.06; p:0.0012),
(z:-3.21; ar:0.0007; pc:0.07; p:0.0014),(z:-3.20; ar:0.0007; pc:0.07; p:0.0014),(z:-3.19; ar:0.0007; pc:0.07; p:0.0014),
(z:-3.18; ar:0.0007; pc:0.07; p:0.0014),(z:-3.17; ar:0.0008; pc:0.08; p:0.0016),(z:-3.16; ar:0.0008; pc:0.08; p:0.0016),
(z:-3.15; ar:0.0008; pc:0.08; p:0.0016),(z:-3.14; ar:0.0008; pc:0.08; p:0.0016),(z:-3.13; ar:0.0009; pc:0.09; p:0.0018),
(z:-3.12; ar:0.0009; pc:0.09; p:0.0018),(z:-3.11; ar:0.0009; pc:0.09; p:0.0018),(z:-3.10; ar:0.0010; pc:0.10; p:0.0020),
(z:-3.09; ar:0.0010; pc:0.10; p:0.0020),(z:-3.08; ar:0.0010; pc:0.10; p:0.0020),(z:-3.07; ar:0.0011; pc:0.11; p:0.0022),
(z:-3.06; ar:0.0011; pc:0.11; p:0.0022),(z:-3.05; ar:0.0011; pc:0.11; p:0.0022),(z:-3.04; ar:0.0012; pc:0.12; p:0.0024),
(z:-3.03; ar:0.0012; pc:0.12; p:0.0024),(z:-3.02; ar:0.0013; pc:0.13; p:0.0026),(z:-3.01; ar:0.0013; pc:0.13; p:0.0026),
(z:-3.00; ar:0.0013; pc:0.13; p:0.0026),(z:-2.99; ar:0.0014; pc:0.14; p:0.0028),(z:-2.98; ar:0.0014; pc:0.14; p:0.0028),
(z:-2.97; ar:0.0015; pc:0.15; p:0.0030),(z:-2.96; ar:0.0015; pc:0.15; p:0.0030),(z:-2.95; ar:0.0016; pc:0.16; p:0.0032),
(z:-2.94; ar:0.0016; pc:0.16; p:0.0032),(z:-2.93; ar:0.0017; pc:0.17; p:0.0034),(z:-2.92; ar:0.0018; pc:0.18; p:0.0036),
(z:-2.91; ar:0.0018; pc:0.18; p:0.0036),(z:-2.90; ar:0.0019; pc:0.19; p:0.0038),(z:-2.89; ar:0.0019; pc:0.19; p:0.0038),
(z:-2.88; ar:0.0020; pc:0.20; p:0.0040),(z:-2.87; ar:0.0021; pc:0.21; p:0.0042),(z:-2.86; ar:0.0021; pc:0.21; p:0.0042),
(z:-2.85; ar:0.0022; pc:0.22; p:0.0044),(z:-2.84; ar:0.0023; pc:0.23; p:0.0046),(z:-2.83; ar:0.0023; pc:0.23; p:0.0046),
(z:-2.82; ar:0.0024; pc:0.24; p:0.0048),(z:-2.81; ar:0.0025; pc:0.25; p:0.0050),(z:-2.80; ar:0.0026; pc:0.26; p:0.0052),
(z:-2.79; ar:0.0026; pc:0.26; p:0.0052),(z:-2.78; ar:0.0027; pc:0.27; p:0.0054),(z:-2.77; ar:0.0028; pc:0.28; p:0.0056),
(z:-2.76; ar:0.0029; pc:0.29; p:0.0058),(z:-2.75; ar:0.0030; pc:0.30; p:0.0060),(z:-2.74; ar:0.0031; pc:0.31; p:0.0062),
(z:-2.73; ar:0.0032; pc:0.32; p:0.0064),(z:-2.72; ar:0.0033; pc:0.33; p:0.0066),(z:-2.71; ar:0.0034; pc:0.34; p:0.0068),
(z:-2.70; ar:0.0035; pc:0.35; p:0.0070),(z:-2.69; ar:0.0036; pc:0.36; p:0.0072),(z:-2.68; ar:0.0037; pc:0.37; p:0.0074),
(z:-2.67; ar:0.0038; pc:0.38; p:0.0076),(z:-2.66; ar:0.0039; pc:0.39; p:0.0078),(z:-2.65; ar:0.0040; pc:0.40; p:0.0080),
(z:-2.64; ar:0.0041; pc:0.41; p:0.0082),(z:-2.63; ar:0.0043; pc:0.43; p:0.0086),(z:-2.62; ar:0.0044; pc:0.44; p:0.0088),
(z:-2.61; ar:0.0045; pc:0.45; p:0.0090),(z:-2.60; ar:0.0047; pc:0.47; p:0.0094),(z:-2.59; ar:0.0048; pc:0.48; p:0.0096),
(z:-2.58; ar:0.0049; pc:0.49; p:0.0098),(z:-2.57; ar:0.0051; pc:0.51; p:0.0102),(z:-2.56; ar:0.0052; pc:0.52; p:0.0104),
(z:-2.55; ar:0.0054; pc:0.54; p:0.0108),(z:-2.54; ar:0.0055; pc:0.55; p:0.0110),(z:-2.53; ar:0.0057; pc:0.57; p:0.0114),
(z:-2.52; ar:0.0059; pc:0.59; p:0.0118),(z:-2.51; ar:0.0060; pc:0.60; p:0.0120),(z:-2.50; ar:0.0062; pc:0.62; p:0.0124),
(z:-2.49; ar:0.0064; pc:0.64; p:0.0128),(z:-2.48; ar:0.0066; pc:0.66; p:0.0132),(z:-2.47; ar:0.0068; pc:0.68; p:0.0136),
(z:-2.46; ar:0.0069; pc:0.69; p:0.0138),(z:-2.45; ar:0.0071; pc:0.71; p:0.0142),(z:-2.44; ar:0.0073; pc:0.73; p:0.0146),
(z:-2.43; ar:0.0075; pc:0.75; p:0.0150),(z:-2.42; ar:0.0078; pc:0.78; p:0.0156),(z:-2.41; ar:0.0080; pc:0.80; p:0.0160),
(z:-2.40; ar:0.0082; pc:0.82; p:0.0164),(z:-2.39; ar:0.0084; pc:0.84; p:0.0168),(z:-2.38; ar:0.0087; pc:0.87; p:0.0174),
(z:-2.37; ar:0.0089; pc:0.89; p:0.0178),(z:-2.36; ar:0.0091; pc:0.91; p:0.0182),(z:-2.35; ar:0.0094; pc:0.94; p:0.0188),
(z:-2.34; ar:0.0096; pc:0.96; p:0.0192),(z:-2.33; ar:0.0099; pc:0.99; p:0.0198),(z:-2.32; ar:0.0102; pc:1.02; p:0.0204),
(z:-2.31; ar:0.0104; pc:1.04; p:0.0208),(z:-2.30; ar:0.0107; pc:1.07; p:0.0214),(z:-2.29; ar:0.0110; pc:1.10; p:0.0220),
(z:-2.28; ar:0.0113; pc:1.13; p:0.0226),(z:-2.27; ar:0.0116; pc:1.16; p:0.0232),(z:-2.26; ar:0.0119; pc:1.19; p:0.0238),
(z:-2.25; ar:0.0122; pc:1.22; p:0.0244),(z:-2.24; ar:0.0125; pc:1.25; p:0.0250),(z:-2.23; ar:0.0129; pc:1.29; p:0.0258),
(z:-2.22; ar:0.0132; pc:1.32; p:0.0264),(z:-2.21; ar:0.0136; pc:1.36; p:0.0272),(z:-2.20; ar:0.0139; pc:1.39; p:0.0278),
(z:-2.19; ar:0.0143; pc:1.43; p:0.0286),(z:-2.18; ar:0.0146; pc:1.46; p:0.0292),(z:-2.17; ar:0.0150; pc:1.50; p:0.0300),
(z:-2.16; ar:0.0154; pc:1.54; p:0.0308),(z:-2.15; ar:0.0158; pc:1.58; p:0.0316),(z:-2.14; ar:0.0162; pc:1.62; p:0.0324),
(z:-2.13; ar:0.0166; pc:1.66; p:0.0332),(z:-2.12; ar:0.0170; pc:1.70; p:0.0340),(z:-2.11; ar:0.0174; pc:1.74; p:0.0348),
(z:-2.10; ar:0.0179; pc:1.79; p:0.0358),(z:-2.09; ar:0.0183; pc:1.83; p:0.0366),(z:-2.08; ar:0.0188; pc:1.88; p:0.0376),
(z:-2.07; ar:0.0192; pc:1.92; p:0.0384),(z:-2.06; ar:0.0197; pc:1.97; p:0.0394),(z:-2.05; ar:0.0202; pc:2.02; p:0.0404),
(z:-2.04; ar:0.0207; pc:2.07; p:0.0414),(z:-2.03; ar:0.0212; pc:2.12; p:0.0424),(z:-2.02; ar:0.0217; pc:2.17; p:0.0434),
(z:-2.01; ar:0.0222; pc:2.22; p:0.0444),(z:-2.00; ar:0.0228; pc:2.28; p:0.0456),(z:-1.99; ar:0.0233; pc:2.33; p:0.0466),
(z:-1.98; ar:0.0239; pc:2.39; p:0.0478),(z:-1.97; ar:0.0244; pc:2.44; p:0.0488),(z:-1.96; ar:0.0250; pc:2.50; p:0.0500),
(z:-1.95; ar:0.0256; pc:2.56; p:0.0512),(z:-1.94; ar:0.0262; pc:2.62; p:0.0524),(z:-1.93; ar:0.0268; pc:2.68; p:0.0536),
(z:-1.92; ar:0.0274; pc:2.74; p:0.0548),(z:-1.91; ar:0.0281; pc:2.81; p:0.0562),(z:-1.90; ar:0.0287; pc:2.87; p:0.0574),
(z:-1.89; ar:0.0294; pc:2.94; p:0.0588),(z:-1.88; ar:0.0301; pc:3.01; p:0.0602),(z:-1.87; ar:0.0307; pc:3.07; p:0.0614),
(z:-1.86; ar:0.0314; pc:3.14; p:0.0628),(z:-1.85; ar:0.0322; pc:3.22; p:0.0644),(z:-1.84; ar:0.0329; pc:3.29; p:0.0658),
(z:-1.83; ar:0.0336; pc:3.36; p:0.0672),(z:-1.82; ar:0.0344; pc:3.44; p:0.0688),(z:-1.81; ar:0.0351; pc:3.51; p:0.0702),
(z:-1.80; ar:0.0359; pc:3.59; p:0.0718),(z:-1.79; ar:0.0367; pc:3.67; p:0.0734),(z:-1.78; ar:0.0375; pc:3.75; p:0.0750),
(z:-1.77; ar:0.0384; pc:3.84; p:0.0768),(z:-1.76; ar:0.0392; pc:3.92; p:0.0784),(z:-1.75; ar:0.0401; pc:4.01; p:0.0802),
(z:-1.74; ar:0.0409; pc:4.09; p:0.0818),(z:-1.73; ar:0.0418; pc:4.18; p:0.0836),(z:-1.72; ar:0.0427; pc:4.27; p:0.0854),
(z:-1.71; ar:0.0436; pc:4.36; p:0.0872),(z:-1.70; ar:0.0446; pc:4.46; p:0.0892),(z:-1.69; ar:0.0455; pc:4.55; p:0.0910),
(z:-1.68; ar:0.0465; pc:4.65; p:0.0930),(z:-1.67; ar:0.0475; pc:4.75; p:0.0950),(z:-1.66; ar:0.0485; pc:4.85; p:0.0970),
(z:-1.65; ar:0.0495; pc:4.95; p:0.0990),(z:-1.64; ar:0.0505; pc:5.05; p:0.1010),(z:-1.63; ar:0.0516; pc:5.16; p:0.1032),
(z:-1.62; ar:0.0526; pc:5.26; p:0.1052),(z:-1.61; ar:0.0537; pc:5.37; p:0.1074),(z:-1.60; ar:0.0548; pc:5.48; p:0.1096),
(z:-1.59; ar:0.0559; pc:5.59; p:0.1118),(z:-1.58; ar:0.0571; pc:5.71; p:0.1142),(z:-1.57; ar:0.0582; pc:5.82; p:0.1164),
(z:-1.56; ar:0.0594; pc:5.94; p:0.1188),(z:-1.55; ar:0.0606; pc:6.06; p:0.1212),(z:-1.54; ar:0.0618; pc:6.18; p:0.1236),
(z:-1.53; ar:0.0630; pc:6.30; p:0.1260),(z:-1.52; ar:0.0643; pc:6.43; p:0.1286),(z:-1.51; ar:0.0655; pc:6.55; p:0.1310),
(z:-1.50; ar:0.0668; pc:6.68; p:0.1336),(z:-1.49; ar:0.0681; pc:6.81; p:0.1362),(z:-1.48; ar:0.0694; pc:6.94; p:0.1388),
(z:-1.47; ar:0.0708; pc:7.08; p:0.1416),(z:-1.46; ar:0.0721; pc:7.21; p:0.1442),(z:-1.45; ar:0.0735; pc:7.35; p:0.1470),
(z:-1.44; ar:0.0749; pc:7.49; p:0.1498),(z:-1.43; ar:0.0764; pc:7.64; p:0.1528),(z:-1.42; ar:0.0778; pc:7.78; p:0.1556),
(z:-1.41; ar:0.0793; pc:7.93; p:0.1586),(z:-1.40; ar:0.0808; pc:8.08; p:0.1616),(z:-1.39; ar:0.0823; pc:8.23; p:0.1646),
(z:-1.38; ar:0.0838; pc:8.38; p:0.1676),(z:-1.37; ar:0.0853; pc:8.53; p:0.1706),(z:-1.36; ar:0.0869; pc:8.69; p:0.1738),
(z:-1.35; ar:0.0885; pc:8.85; p:0.1770),(z:-1.34; ar:0.0901; pc:9.01; p:0.1802),(z:-1.33; ar:0.0918; pc:9.18; p:0.1836),
(z:-1.32; ar:0.0934; pc:9.34; p:0.1868),(z:-1.31; ar:0.0951; pc:9.51; p:0.1902),(z:-1.30; ar:0.0968; pc:9.68; p:0.1936),
(z:-1.29; ar:0.0985; pc:9.85; p:0.1970),(z:-1.28; ar:0.1003; pc:10.03; p:0.2006),(z:-1.27; ar:0.1020; pc:10.20; p:0.2040),
(z:-1.26; ar:0.1038; pc:10.38; p:0.2076),(z:-1.25; ar:0.1056; pc:10.56; p:0.2112),(z:-1.24; ar:0.1075; pc:10.75; p:0.2150),
(z:-1.23; ar:0.1093; pc:10.93; p:0.2186),(z:-1.22; ar:0.1112; pc:11.12; p:0.2224),(z:-1.21; ar:0.1131; pc:11.31; p:0.2262),
(z:-1.20; ar:0.1151; pc:11.51; p:0.2302),(z:-1.19; ar:0.1170; pc:11.70; p:0.2340),(z:-1.18; ar:0.1190; pc:11.90; p:0.2380),
(z:-1.17; ar:0.1210; pc:12.10; p:0.2420),(z:-1.16; ar:0.1230; pc:12.30; p:0.2460),(z:-1.15; ar:0.1251; pc:12.51; p:0.2502),
(z:-1.14; ar:0.1271; pc:12.71; p:0.2542),(z:-1.13; ar:0.1292; pc:12.92; p:0.2584),(z:-1.12; ar:0.1314; pc:13.14; p:0.2628),
(z:-1.11; ar:0.1335; pc:13.35; p:0.2670),(z:-1.10; ar:0.1357; pc:13.57; p:0.2714),(z:-1.09; ar:0.1379; pc:13.79; p:0.2758),
(z:-1.08; ar:0.1401; pc:14.01; p:0.2802),(z:-1.07; ar:0.1423; pc:14.23; p:0.2846),(z:-1.06; ar:0.1446; pc:14.46; p:0.2892),
(z:-1.05; ar:0.1469; pc:14.69; p:0.2938),(z:-1.04; ar:0.1492; pc:14.92; p:0.2984),(z:-1.03; ar:0.1515; pc:15.15; p:0.3030),
(z:-1.02; ar:0.1539; pc:15.39; p:0.3078),(z:-1.01; ar:0.1562; pc:15.62; p:0.3124),(z:-1.00; ar:0.1587; pc:15.87; p:0.3174),
(z:-0.99; ar:0.1611; pc:16.11; p:0.3222),(z:-0.98; ar:0.1635; pc:16.35; p:0.3270),(z:-0.97; ar:0.1660; pc:16.60; p:0.3320),
(z:-0.96; ar:0.1685; pc:16.85; p:0.3370),(z:-0.95; ar:0.1711; pc:17.11; p:0.3422),(z:-0.94; ar:0.1736; pc:17.36; p:0.3472),
(z:-0.93; ar:0.1762; pc:17.62; p:0.3524),(z:-0.92; ar:0.1788; pc:17.88; p:0.3576),(z:-0.91; ar:0.1814; pc:18.14; p:0.3628),
(z:-0.90; ar:0.1841; pc:18.41; p:0.3682),(z:-0.89; ar:0.1867; pc:18.67; p:0.3734),(z:-0.88; ar:0.1894; pc:18.94; p:0.3788),
(z:-0.87; ar:0.1922; pc:19.22; p:0.3844),(z:-0.86; ar:0.1949; pc:19.49; p:0.3898),(z:-0.85; ar:0.1977; pc:19.77; p:0.3954),
(z:-0.84; ar:0.2005; pc:20.05; p:0.4010),(z:-0.83; ar:0.2033; pc:20.33; p:0.4066),(z:-0.82; ar:0.2061; pc:20.61; p:0.4122),
(z:-0.81; ar:0.2090; pc:20.90; p:0.4180),(z:-0.80; ar:0.2119; pc:21.19; p:0.4238),(z:-0.79; ar:0.2148; pc:21.48; p:0.4296),
(z:-0.78; ar:0.2177; pc:21.77; p:0.4354),(z:-0.77; ar:0.2206; pc:22.06; p:0.4412),(z:-0.76; ar:0.2236; pc:22.36; p:0.4472),
(z:-0.75; ar:0.2266; pc:22.66; p:0.4532),(z:-0.74; ar:0.2296; pc:22.96; p:0.4592),(z:-0.73; ar:0.2327; pc:23.27; p:0.4654),
(z:-0.72; ar:0.2358; pc:23.58; p:0.4716),(z:-0.71; ar:0.2389; pc:23.89; p:0.4778),(z:-0.70; ar:0.2420; pc:24.20; p:0.4840),
(z:-0.69; ar:0.2451; pc:24.51; p:0.4902),(z:-0.68; ar:0.2483; pc:24.83; p:0.4966),(z:-0.67; ar:0.2514; pc:25.14; p:0.5028),
(z:-0.66; ar:0.2546; pc:25.46; p:0.5092),(z:-0.65; ar:0.2578; pc:25.78; p:0.5156),(z:-0.64; ar:0.2611; pc:26.11; p:0.5222),
(z:-0.63; ar:0.2643; pc:26.43; p:0.5286),(z:-0.62; ar:0.2676; pc:26.76; p:0.5352),(z:-0.61; ar:0.2709; pc:27.09; p:0.5418),
(z:-0.60; ar:0.2743; pc:27.43; p:0.5486),(z:-0.59; ar:0.2776; pc:27.76; p:0.5552),(z:-0.58; ar:0.2810; pc:28.10; p:0.5620),
(z:-0.57; ar:0.2843; pc:28.43; p:0.5686),(z:-0.56; ar:0.2877; pc:28.77; p:0.5754),(z:-0.55; ar:0.2912; pc:29.12; p:0.5824),
(z:-0.54; ar:0.2946; pc:29.46; p:0.5892),(z:-0.53; ar:0.2981; pc:29.81; p:0.5962),(z:-0.52; ar:0.3015; pc:30.15; p:0.6030),
(z:-0.51; ar:0.3050; pc:30.50; p:0.6100),(z:-0.50; ar:0.3085; pc:30.85; p:0.6170),(z:-0.49; ar:0.3121; pc:31.21; p:0.6242),
(z:-0.48; ar:0.3156; pc:31.56; p:0.6312),(z:-0.47; ar:0.3192; pc:31.92; p:0.6384),(z:-0.46; ar:0.3228; pc:32.28; p:0.6456),
(z:-0.45; ar:0.3264; pc:32.64; p:0.6528),(z:-0.44; ar:0.3300; pc:33.00; p:0.6600),(z:-0.43; ar:0.3336; pc:33.36; p:0.6672),
(z:-0.42; ar:0.3372; pc:33.72; p:0.6744),(z:-0.41; ar:0.3409; pc:34.09; p:0.6818),(z:-0.40; ar:0.3446; pc:34.46; p:0.6892),
(z:-0.39; ar:0.3483; pc:34.83; p:0.6966),(z:-0.38; ar:0.3520; pc:35.20; p:0.7040),(z:-0.37; ar:0.3557; pc:35.57; p:0.7114),
(z:-0.36; ar:0.3594; pc:35.94; p:0.7188),(z:-0.35; ar:0.3632; pc:36.32; p:0.7264),(z:-0.34; ar:0.3669; pc:36.69; p:0.7338),
(z:-0.33; ar:0.3707; pc:37.07; p:0.7414),(z:-0.32; ar:0.3745; pc:37.45; p:0.7490),(z:-0.31; ar:0.3783; pc:37.83; p:0.7566),
(z:-0.30; ar:0.3821; pc:38.21; p:0.7642),(z:-0.29; ar:0.3859; pc:38.59; p:0.7718),(z:-0.28; ar:0.3897; pc:38.97; p:0.7794),
(z:-0.27; ar:0.3936; pc:39.36; p:0.7872),(z:-0.26; ar:0.3974; pc:39.74; p:0.7948),(z:-0.25; ar:0.4013; pc:40.13; p:0.8026),
(z:-0.24; ar:0.4052; pc:40.52; p:0.8104),(z:-0.23; ar:0.4090; pc:40.90; p:0.8180),(z:-0.22; ar:0.4129; pc:41.29; p:0.8258),
(z:-0.21; ar:0.4168; pc:41.68; p:0.8336),(z:-0.20; ar:0.4207; pc:42.07; p:0.8414),(z:-0.19; ar:0.4247; pc:42.47; p:0.8494),
(z:-0.18; ar:0.4286; pc:42.86; p:0.8572),(z:-0.17; ar:0.4325; pc:43.25; p:0.8650),(z:-0.16; ar:0.4364; pc:43.64; p:0.8728),
(z:-0.15; ar:0.4404; pc:44.04; p:0.8808),(z:-0.14; ar:0.4443; pc:44.43; p:0.8886),(z:-0.13; ar:0.4483; pc:44.83; p:0.8966),
(z:-0.12; ar:0.4522; pc:45.22; p:0.9044),(z:-0.11; ar:0.4562; pc:45.62; p:0.9124),(z:-0.10; ar:0.4602; pc:46.02; p:0.9204),
(z:-0.09; ar:0.4641; pc:46.41; p:0.9282),(z:-0.08; ar:0.4681; pc:46.81; p:0.9362),(z:-0.07; ar:0.4721; pc:47.21; p:0.9442),
(z:-0.06; ar:0.4761; pc:47.61; p:0.9522),(z:-0.05; ar:0.4801; pc:48.01; p:0.9602),(z:-0.04; ar:0.4840; pc:48.40; p:0.9680),
(z:-0.03; ar:0.4880; pc:48.80; p:0.9760),(z:-0.02; ar:0.4920; pc:49.20; p:0.9840),(z:-0.01; ar:0.4960; pc:49.60; p:0.9920),
(z:0.00; ar:0.5000; pc:50.00; p:1.0000),(z:0.01; ar:0.5040; pc:50.40; p:0.9920),(z:0.02; ar:0.5080; pc:50.80; p:0.9840),
(z:0.03; ar:0.5120; pc:51.20; p:0.9760),(z:0.04; ar:0.5160; pc:51.60; p:0.9680),(z:0.05; ar:0.5199; pc:51.99; p:0.9602),
(z:0.06; ar:0.5239; pc:52.39; p:0.9522),(z:0.07; ar:0.5279; pc:52.79; p:0.9442),(z:0.08; ar:0.5319; pc:53.19; p:0.9362),
(z:0.09; ar:0.5359; pc:53.59; p:0.9282),(z:0.10; ar:0.5398; pc:53.98; p:0.9204),(z:0.11; ar:0.5438; pc:54.38; p:0.9124),
(z:0.12; ar:0.5478; pc:54.78; p:0.9044),(z:0.13; ar:0.5517; pc:55.17; p:0.8966),(z:0.14; ar:0.5557; pc:55.57; p:0.8886),
(z:0.15; ar:0.5596; pc:55.96; p:0.8808),(z:0.16; ar:0.5636; pc:56.36; p:0.8728),(z:0.17; ar:0.5675; pc:56.75; p:0.8650),
(z:0.18; ar:0.5714; pc:57.14; p:0.8572),(z:0.19; ar:0.5753; pc:57.53; p:0.8494),(z:0.20; ar:0.5793; pc:57.93; p:0.8414),
(z:0.21; ar:0.5832; pc:58.32; p:0.8336),(z:0.22; ar:0.5871; pc:58.71; p:0.8258),(z:0.23; ar:0.5910; pc:59.10; p:0.8180),
(z:0.24; ar:0.5948; pc:59.48; p:0.8104),(z:0.25; ar:0.5987; pc:59.87; p:0.8026),(z:0.26; ar:0.6026; pc:60.26; p:0.7948),
(z:0.27; ar:0.6064; pc:60.64; p:0.7872),(z:0.28; ar:0.6103; pc:61.03; p:0.7794),(z:0.29; ar:0.6141; pc:61.41; p:0.7718),
(z:0.30; ar:0.6179; pc:61.79; p:0.7642),(z:0.31; ar:0.6217; pc:62.17; p:0.7566),(z:0.32; ar:0.6255; pc:62.55; p:0.7490),
(z:0.33; ar:0.6293; pc:62.93; p:0.7414),(z:0.34; ar:0.6331; pc:63.31; p:0.7338),(z:0.35; ar:0.6368; pc:63.68; p:0.7264),
(z:0.36; ar:0.6406; pc:64.06; p:0.7188),(z:0.37; ar:0.6443; pc:64.43; p:0.7114),(z:0.38; ar:0.6480; pc:64.80; p:0.7040),
(z:0.39; ar:0.6517; pc:65.17; p:0.6966),(z:0.40; ar:0.6554; pc:65.54; p:0.6892),(z:0.41; ar:0.6591; pc:65.91; p:0.6818),
(z:0.42; ar:0.6628; pc:66.28; p:0.6744),(z:0.43; ar:0.6664; pc:66.64; p:0.6672),(z:0.44; ar:0.6700; pc:67.00; p:0.6600),
(z:0.45; ar:0.6736; pc:67.36; p:0.6528),(z:0.46; ar:0.6772; pc:67.72; p:0.6456),(z:0.47; ar:0.6808; pc:68.08; p:0.6384),
(z:0.48; ar:0.6844; pc:68.44; p:0.6312),(z:0.49; ar:0.6879; pc:68.79; p:0.6242),(z:0.50; ar:0.6915; pc:69.15; p:0.6170),
(z:0.51; ar:0.6950; pc:69.50; p:0.6100),(z:0.52; ar:0.6985; pc:69.85; p:0.6030),(z:0.53; ar:0.7019; pc:70.19; p:0.5962),
(z:0.54; ar:0.7054; pc:70.54; p:0.5892),(z:0.55; ar:0.7088; pc:70.88; p:0.5824),(z:0.56; ar:0.7123; pc:71.23; p:0.5754),
(z:0.57; ar:0.7157; pc:71.57; p:0.5686),(z:0.58; ar:0.7190; pc:71.90; p:0.5620),(z:0.59; ar:0.7224; pc:72.24; p:0.5552),
(z:0.60; ar:0.7257; pc:72.57; p:0.5486),(z:0.61; ar:0.7291; pc:72.91; p:0.5418),(z:0.62; ar:0.7324; pc:73.24; p:0.5352),
(z:0.63; ar:0.7357; pc:73.57; p:0.5286),(z:0.64; ar:0.7389; pc:73.89; p:0.5222),(z:0.65; ar:0.7422; pc:74.22; p:0.5156),
(z:0.66; ar:0.7454; pc:74.54; p:0.5092),(z:0.67; ar:0.7486; pc:74.86; p:0.5028),(z:0.68; ar:0.7517; pc:75.17; p:0.4966),
(z:0.69; ar:0.7549; pc:75.49; p:0.4902),(z:0.70; ar:0.7580; pc:75.80; p:0.4840),(z:0.71; ar:0.7611; pc:76.11; p:0.4778),
(z:0.72; ar:0.7642; pc:76.42; p:0.4716),(z:0.73; ar:0.7673; pc:76.73; p:0.4654),(z:0.74; ar:0.7704; pc:77.04; p:0.4592),
(z:0.75; ar:0.7734; pc:77.34; p:0.4532),(z:0.76; ar:0.7764; pc:77.64; p:0.4472),(z:0.77; ar:0.7794; pc:77.94; p:0.4412),
(z:0.78; ar:0.7823; pc:78.23; p:0.4354),(z:0.79; ar:0.7852; pc:78.52; p:0.4296),(z:0.80; ar:0.7881; pc:78.81; p:0.4238),
(z:0.81; ar:0.7910; pc:79.10; p:0.4180),(z:0.82; ar:0.7939; pc:79.39; p:0.4122),(z:0.83; ar:0.7967; pc:79.67; p:0.4066),
(z:0.84; ar:0.7995; pc:79.95; p:0.4010),(z:0.85; ar:0.8023; pc:80.23; p:0.3954),(z:0.86; ar:0.8051; pc:80.51; p:0.3898),
(z:0.87; ar:0.8078; pc:80.78; p:0.3844),(z:0.88; ar:0.8106; pc:81.06; p:0.3788),(z:0.89; ar:0.8133; pc:81.33; p:0.3734),
(z:0.90; ar:0.8159; pc:81.59; p:0.3682),(z:0.91; ar:0.8186; pc:81.86; p:0.3628),(z:0.92; ar:0.8212; pc:82.12; p:0.3576),
(z:0.93; ar:0.8238; pc:82.38; p:0.3524),(z:0.94; ar:0.8264; pc:82.64; p:0.3472),(z:0.95; ar:0.8289; pc:82.89; p:0.3422),
(z:0.96; ar:0.8315; pc:83.15; p:0.3370),(z:0.97; ar:0.8340; pc:83.40; p:0.3320),(z:0.98; ar:0.8365; pc:83.65; p:0.3270),
(z:0.99; ar:0.8389; pc:83.89; p:0.3222),(z:1.00; ar:0.8413; pc:84.13; p:0.3174),(z:1.01; ar:0.8438; pc:84.38; p:0.3124),
(z:1.02; ar:0.8461; pc:84.61; p:0.3078),(z:1.03; ar:0.8485; pc:84.85; p:0.3030),(z:1.04; ar:0.8508; pc:85.08; p:0.2984),
(z:1.05; ar:0.8531; pc:85.31; p:0.2938),(z:1.06; ar:0.8554; pc:85.54; p:0.2892),(z:1.07; ar:0.8577; pc:85.77; p:0.2846),
(z:1.08; ar:0.8599; pc:85.99; p:0.2802),(z:1.09; ar:0.8621; pc:86.21; p:0.2758),(z:1.10; ar:0.8643; pc:86.43; p:0.2714),
(z:1.11; ar:0.8665; pc:86.65; p:0.2670),(z:1.12; ar:0.8686; pc:86.86; p:0.2628),(z:1.13; ar:0.8708; pc:87.08; p:0.2584),
(z:1.14; ar:0.8729; pc:87.29; p:0.2542),(z:1.15; ar:0.8749; pc:87.49; p:0.2502),(z:1.16; ar:0.8770; pc:87.70; p:0.2460),
(z:1.17; ar:0.8790; pc:87.90; p:0.2420),(z:1.18; ar:0.8810; pc:88.10; p:0.2380),(z:1.19; ar:0.8830; pc:88.30; p:0.2340),
(z:1.20; ar:0.8849; pc:88.49; p:0.2302),(z:1.21; ar:0.8869; pc:88.69; p:0.2262),(z:1.22; ar:0.8888; pc:88.88; p:0.2224),
(z:1.23; ar:0.8907; pc:89.07; p:0.2186),(z:1.24; ar:0.8925; pc:89.25; p:0.2150),(z:1.25; ar:0.8944; pc:89.44; p:0.2112),
(z:1.26; ar:0.8962; pc:89.62; p:0.2076),(z:1.27; ar:0.8980; pc:89.80; p:0.2040),(z:1.28; ar:0.8997; pc:89.97; p:0.2006),
(z:1.29; ar:0.9015; pc:90.15; p:0.1970),(z:1.30; ar:0.9032; pc:90.32; p:0.1936),(z:1.31; ar:0.9049; pc:90.49; p:0.1902),
(z:1.32; ar:0.9066; pc:90.66; p:0.1868),(z:1.33; ar:0.9082; pc:90.82; p:0.1836),(z:1.34; ar:0.9099; pc:90.99; p:0.1802),
(z:1.35; ar:0.9115; pc:91.15; p:0.1770),(z:1.36; ar:0.9131; pc:91.31; p:0.1738),(z:1.37; ar:0.9147; pc:91.47; p:0.1706),
(z:1.38; ar:0.9162; pc:91.62; p:0.1676),(z:1.39; ar:0.9177; pc:91.77; p:0.1646),(z:1.40; ar:0.9192; pc:91.92; p:0.1616),
(z:1.41; ar:0.9207; pc:92.07; p:0.1586),(z:1.42; ar:0.9222; pc:92.22; p:0.1556),(z:1.43; ar:0.9236; pc:92.36; p:0.1528),
(z:1.44; ar:0.9251; pc:92.51; p:0.1498),(z:1.45; ar:0.9265; pc:92.65; p:0.1470),(z:1.46; ar:0.9279; pc:92.79; p:0.1442),
(z:1.47; ar:0.9292; pc:92.92; p:0.1416),(z:1.48; ar:0.9306; pc:93.06; p:0.1388),(z:1.49; ar:0.9319; pc:93.19; p:0.1362),
(z:1.50; ar:0.9332; pc:93.32; p:0.1336),(z:1.51; ar:0.9345; pc:93.45; p:0.1310),(z:1.52; ar:0.9357; pc:93.57; p:0.1286),
(z:1.53; ar:0.9370; pc:93.70; p:0.1260),(z:1.54; ar:0.9382; pc:93.82; p:0.1236),(z:1.55; ar:0.9394; pc:93.94; p:0.1212),
(z:1.56; ar:0.9406; pc:94.06; p:0.1188),(z:1.57; ar:0.9418; pc:94.18; p:0.1164),(z:1.58; ar:0.9429; pc:94.29; p:0.1142),
(z:1.59; ar:0.9441; pc:94.41; p:0.1118),(z:1.60; ar:0.9452; pc:94.52; p:0.1096),(z:1.61; ar:0.9463; pc:94.63; p:0.1074),
(z:1.62; ar:0.9474; pc:94.74; p:0.1052),(z:1.63; ar:0.9484; pc:94.84; p:0.1032),(z:1.64; ar:0.9495; pc:94.95; p:0.1010),
(z:1.65; ar:0.9505; pc:95.05; p:0.0990),(z:1.66; ar:0.9515; pc:95.15; p:0.0970),(z:1.67; ar:0.9525; pc:95.25; p:0.0950),
(z:1.68; ar:0.9535; pc:95.35; p:0.0930),(z:1.69; ar:0.9545; pc:95.45; p:0.0910),(z:1.70; ar:0.9554; pc:95.54; p:0.0892),
(z:1.71; ar:0.9564; pc:95.64; p:0.0872),(z:1.72; ar:0.9573; pc:95.73; p:0.0854),(z:1.73; ar:0.9582; pc:95.82; p:0.0836),
(z:1.74; ar:0.9591; pc:95.91; p:0.0818),(z:1.75; ar:0.9599; pc:95.99; p:0.0802),(z:1.76; ar:0.9608; pc:96.08; p:0.0784),
(z:1.77; ar:0.9616; pc:96.16; p:0.0768),(z:1.78; ar:0.9625; pc:96.25; p:0.0750),(z:1.79; ar:0.9633; pc:96.33; p:0.0734),
(z:1.80; ar:0.9641; pc:96.41; p:0.0718),(z:1.81; ar:0.9649; pc:96.49; p:0.0702),(z:1.82; ar:0.9656; pc:96.56; p:0.0688),
(z:1.83; ar:0.9664; pc:96.64; p:0.0672),(z:1.84; ar:0.9671; pc:96.71; p:0.0658),(z:1.85; ar:0.9678; pc:96.78; p:0.0644),
(z:1.86; ar:0.9686; pc:96.86; p:0.0628),(z:1.87; ar:0.9693; pc:96.93; p:0.0614),(z:1.88; ar:0.9699; pc:96.99; p:0.0602),
(z:1.89; ar:0.9706; pc:97.06; p:0.0588),(z:1.90; ar:0.9713; pc:97.13; p:0.0574),(z:1.91; ar:0.9719; pc:97.19; p:0.0562),
(z:1.92; ar:0.9726; pc:97.26; p:0.0548),(z:1.93; ar:0.9732; pc:97.32; p:0.0536),(z:1.94; ar:0.9738; pc:97.38; p:0.0524),
(z:1.95; ar:0.9744; pc:97.44; p:0.0512),(z:1.96; ar:0.9750; pc:97.50; p:0.0500),(z:1.97; ar:0.9756; pc:97.56; p:0.0488),
(z:1.98; ar:0.9761; pc:97.61; p:0.0478),(z:1.99; ar:0.9767; pc:97.67; p:0.0466),(z:2.00; ar:0.9772; pc:97.72; p:0.0456),
(z:2.01; ar:0.9778; pc:97.78; p:0.0444),(z:2.02; ar:0.9783; pc:97.83; p:0.0434),(z:2.03; ar:0.9788; pc:97.88; p:0.0424),
(z:2.04; ar:0.9793; pc:97.93; p:0.0414),(z:2.05; ar:0.9798; pc:97.98; p:0.0404),(z:2.06; ar:0.9803; pc:98.03; p:0.0394),
(z:2.07; ar:0.9808; pc:98.08; p:0.0384),(z:2.08; ar:0.9812; pc:98.12; p:0.0376),(z:2.09; ar:0.9817; pc:98.17; p:0.0366),
(z:2.10; ar:0.9821; pc:98.21; p:0.0358),(z:2.11; ar:0.9826; pc:98.26; p:0.0348),(z:2.12; ar:0.9830; pc:98.30; p:0.0340),
(z:2.13; ar:0.9834; pc:98.34; p:0.0332),(z:2.14; ar:0.9838; pc:98.38; p:0.0324),(z:2.15; ar:0.9842; pc:98.42; p:0.0316),
(z:2.16; ar:0.9846; pc:98.46; p:0.0308),(z:2.17; ar:0.9850; pc:98.50; p:0.0300),(z:2.18; ar:0.9854; pc:98.54; p:0.0292),
(z:2.19; ar:0.9857; pc:98.57; p:0.0286),(z:2.20; ar:0.9861; pc:98.61; p:0.0278),(z:2.21; ar:0.9864; pc:98.64; p:0.0272),
(z:2.22; ar:0.9868; pc:98.68; p:0.0264),(z:2.23; ar:0.9871; pc:98.71; p:0.0258),(z:2.24; ar:0.9875; pc:98.75; p:0.0250),
(z:2.25; ar:0.9878; pc:98.78; p:0.0244),(z:2.26; ar:0.9881; pc:98.81; p:0.0238),(z:2.27; ar:0.9884; pc:98.84; p:0.0232),
(z:2.28; ar:0.9887; pc:98.87; p:0.0226),(z:2.29; ar:0.9890; pc:98.90; p:0.0220),(z:2.30; ar:0.9893; pc:98.93; p:0.0214),
(z:2.31; ar:0.9896; pc:98.96; p:0.0208),(z:2.32; ar:0.9898; pc:98.98; p:0.0204),(z:2.33; ar:0.9901; pc:99.01; p:0.0198),
(z:2.34; ar:0.9904; pc:99.04; p:0.0192),(z:2.35; ar:0.9906; pc:99.06; p:0.0188),(z:2.36; ar:0.9909; pc:99.09; p:0.0182),
(z:2.37; ar:0.9911; pc:99.11; p:0.0178),(z:2.38; ar:0.9913; pc:99.13; p:0.0174),(z:2.39; ar:0.9916; pc:99.16; p:0.0168),
(z:2.40; ar:0.9918; pc:99.18; p:0.0164),(z:2.41; ar:0.9920; pc:99.20; p:0.0160),(z:2.42; ar:0.9922; pc:99.22; p:0.0156),
(z:2.43; ar:0.9925; pc:99.25; p:0.0150),(z:2.44; ar:0.9927; pc:99.27; p:0.0146),(z:2.45; ar:0.9929; pc:99.29; p:0.0142),
(z:2.46; ar:0.9931; pc:99.31; p:0.0138),(z:2.47; ar:0.9932; pc:99.32; p:0.0136),(z:2.48; ar:0.9934; pc:99.34; p:0.0132),
(z:2.49; ar:0.9936; pc:99.36; p:0.0128),(z:2.50; ar:0.9938; pc:99.38; p:0.0124),(z:2.51; ar:0.9940; pc:99.40; p:0.0120),
(z:2.52; ar:0.9941; pc:99.41; p:0.0118),(z:2.53; ar:0.9943; pc:99.43; p:0.0114),(z:2.54; ar:0.9945; pc:99.45; p:0.0110),
(z:2.55; ar:0.9946; pc:99.46; p:0.0108),(z:2.56; ar:0.9948; pc:99.48; p:0.0104),(z:2.57; ar:0.9949; pc:99.49; p:0.0102),
(z:2.58; ar:0.9951; pc:99.51; p:0.0098),(z:2.59; ar:0.9952; pc:99.52; p:0.0096),(z:2.60; ar:0.9953; pc:99.53; p:0.0094),
(z:2.61; ar:0.9955; pc:99.55; p:0.0090),(z:2.62; ar:0.9956; pc:99.56; p:0.0088),(z:2.63; ar:0.9957; pc:99.57; p:0.0086),
(z:2.64; ar:0.9959; pc:99.59; p:0.0082),(z:2.65; ar:0.9960; pc:99.60; p:0.0080),(z:2.66; ar:0.9961; pc:99.61; p:0.0078),
(z:2.67; ar:0.9962; pc:99.62; p:0.0076),(z:2.68; ar:0.9963; pc:99.63; p:0.0074),(z:2.69; ar:0.9964; pc:99.64; p:0.0072),
(z:2.70; ar:0.9965; pc:99.65; p:0.0070),(z:2.71; ar:0.9966; pc:99.66; p:0.0068),(z:2.72; ar:0.9967; pc:99.67; p:0.0066),
(z:2.73; ar:0.9968; pc:99.68; p:0.0064),(z:2.74; ar:0.9969; pc:99.69; p:0.0062),(z:2.75; ar:0.9970; pc:99.70; p:0.0060),
(z:2.76; ar:0.9971; pc:99.71; p:0.0058),(z:2.77; ar:0.9972; pc:99.72; p:0.0056),(z:2.78; ar:0.9973; pc:99.73; p:0.0054),
(z:2.79; ar:0.9974; pc:99.74; p:0.0052),(z:2.80; ar:0.9974; pc:99.74; p:0.0052),(z:2.81; ar:0.9975; pc:99.75; p:0.0050),
(z:2.82; ar:0.9976; pc:99.76; p:0.0048),(z:2.83; ar:0.9977; pc:99.77; p:0.0046),(z:2.84; ar:0.9977; pc:99.77; p:0.0046),
(z:2.85; ar:0.9978; pc:99.78; p:0.0044),(z:2.86; ar:0.9979; pc:99.79; p:0.0042),(z:2.87; ar:0.9979; pc:99.79; p:0.0042),
(z:2.88; ar:0.9980; pc:99.80; p:0.0040),(z:2.89; ar:0.9981; pc:99.81; p:0.0038),(z:2.90; ar:0.9981; pc:99.81; p:0.0038),
(z:2.91; ar:0.9982; pc:99.82; p:0.0036),(z:2.92; ar:0.9982; pc:99.82; p:0.0036),(z:2.93; ar:0.9983; pc:99.83; p:0.0034),
(z:2.94; ar:0.9984; pc:99.84; p:0.0032),(z:2.95; ar:0.9984; pc:99.84; p:0.0032),(z:2.96; ar:0.9985; pc:99.85; p:0.0030),
(z:2.97; ar:0.9985; pc:99.85; p:0.0030),(z:2.98; ar:0.9986; pc:99.86; p:0.0028),(z:2.99; ar:0.9986; pc:99.86; p:0.0028),
(z:3.00; ar:0.9987; pc:99.87; p:0.0026),(z:3.01; ar:0.9987; pc:99.87; p:0.0026),(z:3.02; ar:0.9987; pc:99.87; p:0.0026),
(z:3.03; ar:0.9988; pc:99.88; p:0.0024),(z:3.04; ar:0.9988; pc:99.88; p:0.0024),(z:3.05; ar:0.9989; pc:99.89; p:0.0022),
(z:3.06; ar:0.9989; pc:99.89; p:0.0022),(z:3.07; ar:0.9989; pc:99.89; p:0.0022),(z:3.08; ar:0.9990; pc:99.90; p:0.0020),
(z:3.09; ar:0.9990; pc:99.90; p:0.0020),(z:3.10; ar:0.9990; pc:99.90; p:0.0020),(z:3.11; ar:0.9991; pc:99.91; p:0.0018),
(z:3.12; ar:0.9991; pc:99.91; p:0.0018),(z:3.13; ar:0.9991; pc:99.91; p:0.0018),(z:3.14; ar:0.9992; pc:99.92; p:0.0016),
(z:3.15; ar:0.9992; pc:99.92; p:0.0016),(z:3.16; ar:0.9992; pc:99.92; p:0.0016),(z:3.17; ar:0.9992; pc:99.92; p:0.0016),
(z:3.18; ar:0.9993; pc:99.93; p:0.0014),(z:3.19; ar:0.9993; pc:99.93; p:0.0014),(z:3.20; ar:0.9993; pc:99.93; p:0.0014),
(z:3.21; ar:0.9993; pc:99.93; p:0.0014),(z:3.22; ar:0.9994; pc:99.94; p:0.0012),(z:3.23; ar:0.9994; pc:99.94; p:0.0012),
(z:3.24; ar:0.9994; pc:99.94; p:0.0012),(z:3.25; ar:0.9994; pc:99.94; p:0.0012),(z:3.26; ar:0.9994; pc:99.94; p:0.0012),
(z:3.27; ar:0.9995; pc:99.95; p:0.0010),(z:3.28; ar:0.9995; pc:99.95; p:0.0010),(z:3.29; ar:0.9995; pc:99.95; p:0.0010),
(z:3.30; ar:0.9995; pc:99.95; p:0.0010),(z:3.31; ar:0.9995; pc:99.95; p:0.0010),(z:3.32; ar:0.9995; pc:99.95; p:0.0010),
(z:3.33; ar:0.9996; pc:99.96; p:0.0008),(z:3.34; ar:0.9996; pc:99.96; p:0.0008),(z:3.35; ar:0.9996; pc:99.96; p:0.0008),
(z:3.36; ar:0.9996; pc:99.96; p:0.0008),(z:3.37; ar:0.9996; pc:99.96; p:0.0008),(z:3.38; ar:0.9996; pc:99.96; p:0.0008),
(z:3.39; ar:0.9997; pc:99.97; p:0.0006),(z:3.40; ar:0.9997; pc:99.97; p:0.0006),(z:3.41; ar:0.9997; pc:99.97; p:0.0006),
(z:3.42; ar:0.9997; pc:99.97; p:0.0006),(z:3.43; ar:0.9997; pc:99.97; p:0.0006),(z:3.44; ar:0.9997; pc:99.97; p:0.0006),
(z:3.45; ar:0.9997; pc:99.97; p:0.0006),(z:3.46; ar:0.9997; pc:99.97; p:0.0006),(z:3.47; ar:0.9997; pc:99.97; p:0.0006),
(z:3.48; ar:0.9997; pc:99.97; p:0.0006),(z:3.49; ar:0.9998; pc:99.98; p:0.0004),(z:3.50; ar:0.9998; pc:99.98; p:0.0004),
(z:3.51; ar:0.9998; pc:99.98; p:0.0004),(z:3.52; ar:0.9998; pc:99.98; p:0.0004),(z:3.53; ar:0.9998; pc:99.98; p:0.0004),
(z:3.54; ar:0.9998; pc:99.98; p:0.0004),(z:3.55; ar:0.9998; pc:99.98; p:0.0004),(z:3.56; ar:0.9998; pc:99.98; p:0.0004),
(z:3.57; ar:0.9998; pc:99.98; p:0.0004),(z:3.58; ar:0.9998; pc:99.98; p:0.0004),(z:3.59; ar:0.9998; pc:99.98; p:0.0004),
(z:3.60; ar:0.9998; pc:99.98; p:0.0004),(z:3.61; ar:0.9998; pc:99.98; p:0.0004),(z:3.62; ar:0.9999; pc:99.99; p:0.0002),
(z:3.63; ar:0.9999; pc:99.99; p:0.0002),(z:3.64; ar:0.9999; pc:99.99; p:0.0002),(z:3.65; ar:0.9999; pc:99.99; p:0.0002),
(z:3.66; ar:0.9999; pc:99.99; p:0.0002),(z:3.67; ar:0.9999; pc:99.99; p:0.0002),(z:3.68; ar:0.9999; pc:99.99; p:0.0002),
(z:3.69; ar:0.9999; pc:99.99; p:0.0002),(z:3.70; ar:0.9999; pc:99.99; p:0.0002),(z:3.71; ar:0.9999; pc:99.99; p:0.0002),
(z:3.72; ar:0.9999; pc:99.99; p:0.0002),(z:3.73; ar:0.9999; pc:99.99; p:0.0002),(z:3.74; ar:0.9999; pc:99.99; p:0.0002),
(z:3.75; ar:0.9999; pc:99.99; p:0.0002),(z:3.76; ar:0.9999; pc:99.99; p:0.0002),(z:3.77; ar:0.9999; pc:99.99; p:0.0002),
(z:3.78; ar:0.9999; pc:99.99; p:0.0002),(z:3.79; ar:0.9999; pc:99.99; p:0.0002),(z:3.80; ar:0.9999; pc:99.99; p:0.0002),
(z:3.81; ar:0.9999; pc:99.99; p:0.0002),(z:3.82; ar:0.9999; pc:99.99; p:0.0002),(z:3.83; ar:0.9999; pc:99.99; p:0.0002),
(z:3.84; ar:0.9999; pc:99.99; p:0.0002),(z:3.85; ar:0.9999; pc:99.99; p:0.0002),(z:3.86; ar:0.9999; pc:99.99; p:0.0002),
(z:3.87; ar:0.9999; pc:99.99; p:0.0002),(z:3.88; ar:0.9999; pc:99.99; p:0.0002),(z:3.89; ar:0.9999; pc:99.99; p:0.0002),
(z:3.90; ar:1.0000; pc:100.00; p:0.0000),(z:3.91; ar:1.0000; pc:100.00; p:0.0000),(z:3.92; ar:1.0000; pc:100.00; p:0.0000),
(z:3.93; ar:1.0000; pc:100.00; p:0.0000),(z:3.94; ar:1.0000; pc:100.00; p:0.0000),(z:3.95; ar:1.0000; pc:100.00; p:0.0000),
(z:3.96; ar:1.0000; pc:100.00; p:0.0000),(z:3.97; ar:1.0000; pc:100.00; p:0.0000),(z:3.98; ar:1.0000; pc:100.00; p:0.0000),
(z:3.99; ar:1.0000; pc:100.00; p:0.0000)  );




  function ByteSetToStr(aSet: TByteSet): String;

  function ExperimentHasKnownName(const anExtractedName: String; out ExperimentIdx: Integer): Boolean;

  function ExperimentSupportsNormativeData(anExperimentIdx: Integer): Boolean;

  function GetPaddedRightDots(const s: String; padLen: Integer): String;

  function GetVersionResourceString: String;

  function GotTrimmedFieldBeforeTabIdx(const aLine: AnsiString; aTabIdx: Integer; out Field: AnsiString): Boolean;

  function InvNormalDist(y: Double): Double;

  function OutcomeTo2Str(anOutcome: EnOutcome; out Str2: String): String;

  function Spaces(aLength: Integer): String;

  function StatisticNameToIDOK(const aName: String; out ID: Integer): Boolean;

  function StringArrayToListing(var aSArray: TStringArray): String;

  function ZtoPCandP(aZ: Double; out percentile: Double; out p: Double): EnOutcome;

  function ReplacedTabWithCommaOK(const aFilename: String; out madeReplacements: Boolean): Boolean;

  procedure ReportOutcomeDlg(anOutcome: EnOutcome; aMidText: String);


implementation

uses
  Math, StrUtils, versionresource,
  LazUTF8, Dialogs, LazFileUtils,
  uEngine {$IfDef Windows},Windows{$EndIf};

function ByteSetToStr(aSet: TByteSet): String;
var
  b: Integer;
begin
  Result := '';
  for b := 0 to High(Byte) do
    if b in aSet then
      Result := Result + b.ToString + ', ';
  if Pos(',', Result) > 0 then
    Delete(Result, Pred(Length(Result)), 2);
end;

function ExperimentHasKnownName(const anExtractedName: String; out
  ExperimentIdx: Integer): Boolean;
var
  i: Integer;
begin
  ExperimentIdx := -1;
  Result := False;
  for i := Low(PExperiments) to High(PExperiments) do
    if SameText(anExtractedName, PExperiments[i]^.name) then
      begin
        ExperimentIdx := i;
        Result := True;
      end;
end;

function ExperimentSupportsNormativeData(anExperimentIdx: Integer): Boolean;
begin
  if (anExperimentIdx >= 0) and (anExperimentIdx < Length(PExperiments)) then
    Result := PExperiments[anExperimentIdx]^.supportsNormativeData
  else Result := False;
end;

function GetPaddedRightDots(const s: String; padLen: Integer): String;
var
  pad: String = '';
  i: Integer;
begin
  if Length(s) > padLen - 2 then
    Exit(s);

  case Odd(Length(s)) of
    False: Result := s + #32;
    True: Result := s;
  end;
  SetLength(pad, padLen - Length(s));
  for i := 1 to Length(pad) do
    case Odd(i) of
      True:  pad[i] := #32;
      False: pad[i] := '.';
    end;
  Result := Result + pad;
  SetLength(Result, padLen);
end;

function GetVersionResourceString: String;
var
  vres: TVersionResource = Nil;
  resStm: TResourceStream = Nil;
begin
  vres := TVersionResource.Create;
  try
    case Assigned(vres) of
      False: Exit(rsUnknownVersionNumber);
      True:  begin
               resStm := TResourceStream.CreateFromID(HINSTANCE, 1, PChar(RT_VERSION));
               try
                 case Assigned(resStm) of
                   True: begin
                           vres.SetCustomRawDataStream(resStm);
                           Result := Format(rsVersion_dot_dot_,
         [vres.FixedInfo.FileVersion[0], vres.FixedInfo.FileVersion[1], vres.FixedInfo.FileVersion[2]]); //vres.FixedInfo.FileVersion[3] (build no)
                           vres.SetCustomRawDataStream(Nil);
                           FreeAndNil(resStm);
                         end;
                   False: Result := rsUnknownVersionNumber;
                 end;
               finally
                 FreeAndNil(resStm);
               End;
             end;
    end;
  finally
    vres.Free;
  end;
end;

function GotTrimmedFieldBeforeTabIdx(const aLine: AnsiString; aTabIdx: Integer; out Field: AnsiString): Boolean;
var                                                     //aTabIdx starts at 1
  p, q, tabcount: SizeInt;
begin
  p := Pos(#9, aLine);
  case p of
    0: begin
         Field := '';
         Exit(False);
       end;
    else case aTabIdx of
      1: begin
           Field := Trim(Copy(aLine, 1, Pred(p)));
           Exit(True);
         end;
      2: begin
           q := PosEx(#9, aLine, Succ(p));
           case q of
             0: begin
                  Field := '';
                  Exit(False);
                end;
             else
               Field := Trim(Copy(aLine, Succ(p), Pred(q-p)));
               Exit(True);
           end;
         end;
      else
        tabcount := 1;
        p := Succ(p);
        repeat
          q := PosEx(#9, aLine, p);
          Inc(tabcount);
          p := Succ(q);
        until (q = 0) or (tabcount = Pred(aTabIdx));
        Result := q <> 0;
        case Result of
          True:  begin
                   p := PosEx(#9, aLine, Succ(q));
                   Field := Trim(Copy(aLine, Succ(q), Pred(p-q)));
                 end;
          False: Field := '';
        end;
    end;
  end;
end;

function InvNormalDist(y: Double): Double;
type
  arfloat0   = array[0..(High(Integer) div SizeOf(Double))-1] of Double;

  function spepol(x: Double; var a: Double; n: Integer): Double;
  var   pa : ^arfloat0;
         i : Integer;
      polx : Double;
  begin
    pa := @a;
    polx := 0;
    for i := n downto 0 do
      polx := polx*x + pa^[i];
    spepol := polx;
  end {spepol};

type
  Float8Byte = array[0..7] of Byte;
const
  gia: Float8Byte  = ($FF,$FF,$FF,$FF,$FF,$FF,$EF,$7F);

  P0: array[0..4] of Double = (
    -1.23916583867381258016,
    13.9312609387279679503,
    -56.6762857469070293439,
    98.0010754185999661536,
    -59.9633501014107895267);
  Q0: array[0..8] of Double = (
    -1.18331621121330003142,
    15.9056225126211695515,
    -82.0372256168333339912,
    200.260212380060660359,
    -225.462687854119370527,
    86.3602421390890590575,
    4.67627912898881538453,
    1.95448858338141759834,
    1.0);
  P1: array[0..8] of Double = (
    -8.57456785154685413611E-4,
    -3.50424626827848203418E-2,
    -1.40256079171354495875E-1,
    2.18663306850790267539,
    14.6849561928858024014,
    44.0805073893200834700,
    57.1628192246421288162,
    31.5251094599893866154,
    4.05544892305962419923);
  Q1: array[0..8] of Double = (
    -9.33259480895457427372E-4,
    -3.80806407691578277194E-2,
    -1.42182922854787788574E-1,
    2.50464946208309415979,
    15.0425385692907503408,
    41.3172038254672030440,
    45.3907635128879210584,
    15.7799883256466749731,
    1.0);
  P2: array[0..8] of Double = (
    6.23974539184983293730E-9,
    2.65806974686737550832E-6,
    3.01581553508235416007E-4,
    1.23716634817820021358E-2,
    2.01485389549179081538E-1,
    1.33303460815807542389,
    3.93881025292474443415,
    6.91522889068984211695,
    3.23774891776946035970);
  Q2: array[0..8] of Double = (
    6.79019408009981274425E-9,
    2.89247864745380683936E-6,
    3.28014464682127739104E-4,
    1.34204006088543189037E-2,
    2.16236993594496635890E-1,
    1.37702099489081330271,
    3.67983563856160859403,
    6.02427039364742014255,
    1.0);
var
  giant: Double absolute gia;
  x, x0, x1, yy, y2, z: Double;
  code: Integer;
begin
  if y <= 0.0 then
  begin
    Result := -giant;
    exit;
  end;

  if y >= 1.0 then
  begin
    Result := giant;
    exit;
  end;

  code := 1;
  yy := y;
  if yy > 1.0 - Exp(-2) then begin    // EXP_2 = exp(-2)
    yy := 1.0 - yy;
    code := 0;
  end;

  if yy > Exp(-2) then begin
    yy := yy - 0.5;
    y2 := yy * yy;
    x := y2 * spepol(y2, P0[0], 4) / spepol(y2, Q0[0], 8);
    x := (yy + yy * x) * Sqrt2Pi;   // Sqrt2Pi = sqrt(2*pi);
    Result := x;
    exit;
  end;

  x := sqrt(-2.0 * ln(yy));
  x0 := x - ln(x) / x;
  z := 1.0 / x;

  if x < 8.0 then
    x1 := z * spepol(z, P1[0], 8) / spepol(z, Q1[0], 8)
  else
    x1 := z * spepol(z, P2[0], 8) / spepol(z, Q2[0], 8);

  x := x0 - x1;
  if code <> 0 then
    x := -x;
  Result := x;
end;

function OutcomeTo2Str(anOutcome: EnOutcome; out Str2: String): String;
var
  tmp: String;
  i: Integer;
  s2: Boolean = False;
  s2JustBegan: Boolean = True;
begin
  WriteStr(tmp, anOutcome);
  Delete(tmp, 1, 1);
  Result := '';
  str2 := '';
  for i := 1 to Length(tmp) do
  begin
    case (tmp[i] in ['A'..'Z']) of
      True: case (i = 1) of
        True:  AppendStr(Result, tmp[1]);
        False: case s2 of
                 False: AppendStr(Result, ' ' + LowerCase(tmp[i]));
                 True:  case s2JustBegan of
                          True: begin
                                  AppendStr(Str2, LowerCase(tmp[i]));
                                  s2JustBegan := False;
                                end;
                          False:  AppendStr(Str2, ' ' + LowerCase(tmp[i]));
                        end;
               end;
      end;
      False: case (tmp[i] = '_') of
               False: case s2 of
                        False: AppendStr(Result, tmp[i]);
                        True:  AppendStr(Str2, tmp[i]);
                      end;
               True:  s2 := True;
             end;
    end;
  end;
end;

function StringArrayToListing(var aSArray: TStringArray): String;
var
  i: Integer;
begin
  Result := '';
  case Length(aSArray) of
    0: ;
    1: Result := Result + aSArray[0];
    otherwise
      for i := 0 to High(aSArray) do
        Result := Result + aSArray[i] + ', ';
      Delete(Result, Pred(Length(Result)), 2);
      i := Length(Result);
      repeat
        Dec(i);
      until Result[i] = ',';
      Result := Copy(Result, 1, Succ(i)) + rsAnd + Copy(Result, Succ(i));
  end;
end;

function ZtoPCandP(aZ: Double; out percentile: Double; out p: Double): EnOutcome;
var
  zi: Int64;
begin
  case ((aZ < -3.99) or (aZ > 3.99)) of
    True: begin percentile := 0; p := 0; Result := oNormInvalidZ; end;
    False: begin
             Result := _oSuccess;
             zi := Round(aZ*100) + 399;
             Assert(zi < Length(ZP), 'ZtoPCandP internal error');
             Assert(IsZero(ZP[zi].z - aZ, 0.005),'ZtoPCandP internal error');

             percentile := ZP[zi].pc;
             p          := ZP[zi].p;
           end;
  end;
end;

function ReplacedTabWithCommaOK(const aFilename: String; out madeReplacements: Boolean): Boolean;
var
  sl: TStringList;
  i: Integer;
  s: String;
begin
  Result := False;
  madeReplacements := False;
  if not FileExistsUTF8(aFilename) then Exit;
  if not FileIsText(aFilename) then Exit;
  sl := TStringList.Create;
  try
    sl.LoadFromFile(aFilename);
    s := sl.Text;
    for i := 1 to Length(s) do
      if s[i] = #9 then
        begin
          s[i] := ',';
          if not madeReplacements then
            madeReplacements := True;
        end;
    sl.Text := s;
    Result := True;
  finally
    sl.Free;
  end;
end;

procedure ReportOutcomeDlg(anOutcome: EnOutcome; aMidText: String);
var
  s2, s1: String;
begin
  s1 := OutcomeTo2Str(anOutcome, s2);
  case (aMidText = '') of
    True:  ShowMessageFmt('%s %s', [s1, s2]);
    False: ShowMessageFmt('%s "%s" %s',[s1, aMidText, s2]);
  end;
end;

function Spaces(aLength: Integer): String;
begin
  SetLength(Result{%H-}, aLength);
  FillChar(Result[1], aLength, ' ');
end;

function StatisticNameToIDOK(const aName: String; out ID: Integer): Boolean;
var
  b: Byte;
  n: String;
begin
  b := Byte(Length(aName));
  ID := -1;
  if not (b in [12, 17..18, 21, 23, 26..27, 29, 32..36, 38..39, 41..63]) then
    Exit(False);
  Result := True;
  n := UpperCase(aName);

  case Length(aName) of
    12: if n = 'ASUMMARYMEAN' then
          ID := 165
        else Exit(False);
    17: if n = 'COWANKSUMMARYMEAN' then
          ID := 148
        else Exit(False);
    18: if n = 'D-PRIMESUMMARYMEAN' then
          ID := 182
        else Exit(False);
    21: case n of
          'ALOAD1CUEDISDIFFTARDH': ID := 149;
          'ALOAD1CUEDISDIFFTARSH': ID := 150;
          'ALOAD1CUEDISSAMETARDH': ID := 151;
          'ALOAD1CUEDISSAMETARSH': ID := 152;
          'ALOAD2CUEDISDIFFTARDH': ID := 153;
          'ALOAD2CUEDISDIFFTARSH': ID := 154;
          'ALOAD2CUEDISSAMETARDH': ID := 155;
          'ALOAD2CUEDISSAMETARSH': ID := 156;
          'ALOAD3CUEDISDIFFTARDH': ID := 157;
          'ALOAD3CUEDISDIFFTARSH': ID := 158;
          'ALOAD3CUEDISSAMETARDH': ID := 159;
          'ALOAD3CUEDISSAMETARSH': ID := 160;
          'ALOAD4CUEDISDIFFTARDH': ID := 161;
          'ALOAD4CUEDISDIFFTARSH': ID := 162;
          'ALOAD4CUEDISSAMETARDH': ID := 163;
          'ALOAD4CUEDISSAMETARSH': ID := 164;
          otherwise Exit(False);
        end;
    23: if n = 'MEANACCURACYSUMMARYMEAN' then
          ID := 33
        else Exit(False);
    26: case n of
          'COWANKLOAD1CUEDISDIFFTARDH': ID := 132;
          'COWANKLOAD1CUEDISDIFFTARSH': ID := 133;
          'COWANKLOAD1CUEDISSAMETARDH': ID := 134;
          'COWANKLOAD1CUEDISSAMETARSH': ID := 135;
          'COWANKLOAD2CUEDISDIFFTARDH': ID := 136;
          'COWANKLOAD2CUEDISDIFFTARSH': ID := 137;
          'COWANKLOAD2CUEDISSAMETARDH': ID := 138;
          'COWANKLOAD2CUEDISSAMETARSH': ID := 139;
          'COWANKLOAD3CUEDISDIFFTARDH': ID := 140;
          'COWANKLOAD3CUEDISDIFFTARSH': ID := 141;
          'COWANKLOAD3CUEDISSAMETARDH': ID := 142;
          'COWANKLOAD3CUEDISSAMETARSH': ID := 143;
          'COWANKLOAD4CUEDISDIFFTARDH': ID := 144;
          'COWANKLOAD4CUEDISDIFFTARSH': ID := 145;
          'COWANKLOAD4CUEDISSAMETARDH': ID := 146;
          'COWANKLOAD4CUEDISSAMETARSH': ID := 147;
          otherwise Exit(False);
        end;
    27: case n of
          'NTRIALSACCURACYSUMMARYCOUNT': ID := 16;
          'D-PRIMELOAD1CUEDISDIFFTARDH': ID := 166;
          'D-PRIMELOAD1CUEDISDIFFTARSH': ID := 167;
          'D-PRIMELOAD1CUEDISSAMETARDH': ID := 168;
          'D-PRIMELOAD1CUEDISSAMETARSH': ID := 169;
          'D-PRIMELOAD2CUEDISDIFFTARDH': ID := 170;
          'D-PRIMELOAD2CUEDISDIFFTARSH': ID := 171;
          'D-PRIMELOAD2CUEDISSAMETARDH': ID := 172;
          'D-PRIMELOAD2CUEDISSAMETARSH': ID := 173;
          'D-PRIMELOAD3CUEDISDIFFTARDH': ID := 174;
          'D-PRIMELOAD3CUEDISDIFFTARSH': ID := 175;
          'D-PRIMELOAD3CUEDISSAMETARDH': ID := 176;
          'D-PRIMELOAD3CUEDISSAMETARSH': ID := 177;
          'D-PRIMELOAD4CUEDISDIFFTARDH': ID := 178;
          'D-PRIMELOAD4CUEDISDIFFTARSH': ID := 179;
          'D-PRIMELOAD4CUEDISSAMETARDH': ID := 180;
          'D-PRIMELOAD4CUEDISSAMETARSH': ID := 181;
          otherwise Exit(False);
        end;
    29: if n = 'ARCSINMEANACCURACYSUMMARYMEAN' then
          ID := 67
        else Exit(False);

    end;
{
17 MeanAccuracyLoad1CueDisDiffTarDH (32)
18 MeanAccuracyLoad1CueDisDiffTarSH (32)
19 MeanAccuracyLoad1CueDisSameTarDH (32)
20 MeanAccuracyLoad1CueDisSameTarSH (32)
21 MeanAccuracyLoad2CueDisDiffTarDH (32)
22 MeanAccuracyLoad2CueDisDiffTarSH (32)
23 MeanAccuracyLoad2CueDisSameTarDH (32)
24 MeanAccuracyLoad2CueDisSameTarSH (32)
25 MeanAccuracyLoad3CueDisDiffTarDH (32)
26 MeanAccuracyLoad3CueDisDiffTarSH (32)
27 MeanAccuracyLoad3CueDisSameTarDH (32)
28 MeanAccuracyLoad3CueDisSameTarSH (32)
29 MeanAccuracyLoad4CueDisDiffTarDH (32)
30 MeanAccuracyLoad4CueDisDiffTarSH (32)
31 MeanAccuracyLoad4CueDisSameTarDH (32)
32 MeanAccuracyLoad4CueDisSameTarSH (32)
50 MeanErrorRateAccuracySummaryMean (32)

34 MeanErrorRateLoad1CueDisDiffTarDH (33)
35 MeanErrorRateLoad1CueDisDiffTarSH (33)
36 MeanErrorRateLoad1CueDisSameTarDH (33)
37 MeanErrorRateLoad1CueDisSameTarSH (33)
38 MeanErrorRateLoad2CueDisDiffTarDH (33)
39 MeanErrorRateLoad2CueDisDiffTarSH (33)
40 MeanErrorRateLoad2CueDisSameTarDH (33)
41 MeanErrorRateLoad2CueDisSameTarSH (33)
42 MeanErrorRateLoad3CueDisDiffTarDH (33)
43 MeanErrorRateLoad3CueDisDiffTarSH (33)
44 MeanErrorRateLoad3CueDisSameTarDH (33)
45 MeanErrorRateLoad3CueDisSameTarSH (33)
46 MeanErrorRateLoad4CueDisDiffTarDH (33)
47 MeanErrorRateLoad4CueDisDiffTarSH (33)
48 MeanErrorRateLoad4CueDisSameTarDH (33)
49 MeanErrorRateLoad4CueDisSameTarSH (33)

216 MeanCorrectResponseTimeSummaryMean (34)

0 NTrialsAccuracyLoad1CueDisDiffTarDH (35)
1 NTrialsAccuracyLoad1CueDisDiffTarSH (35)
2 NTrialsAccuracyLoad1CueDisSameTarDH (35)
3 NTrialsAccuracyLoad1CueDisDiffTarSH (35)
4 NTrialsAccuracyLoad2CueDisDiffTarDH (35)
5 NTrialsAccuracyLoad2CueDisDiffTarSH (35)
6 NTrialsAccuracyLoad2CueDisSameTarDH (35)
7 NTrialsAccuracyLoad2CueDisSameTarSH (35)
8 NTrialsAccuracyLoad3CueDisDiffTarDH (35)
9 NTrialsAccuracyLoad3CueDisDiffTarSH (35)
10 NTrialsAccuracyLoad3CueDisSameTarDH (35)
11 NTrialsAccuracyLoad3CueDisSameTarSH (35)
12 NTrialsAccuracyLoad4CueDisDiffTarDH (35)
13 NTrialsAccuracyLoad4CueDisDiffTarSH (35)
14 NTrialsAccuracyLoad4CueDisSameTarDH (35)
15 NTrialsAccuracyLoad4CueDisSameTarSH (35)

284 LnMeanCorrectResponseTimeSummaryMean (36)

51 ArcsinMeanAccuracyLoad1CueDisDiffTarDH (38)
52 ArcsinMeanAccuracyLoad1CueDisDiffTarSH (38)
53 ArcsinMeanAccuracyLoad1CueDisSameTarDH (38)
54 ArcsinMeanAccuracyLoad1CueDisSameTarSH (38)
55 ArcsinMeanAccuracyLoad2CueDisDiffTarDH (38)
56 ArcsinMeanAccuracyLoad2CueDisDiffTarSH (38)
57 ArcsinMeanAccuracyLoad2CueDisSameTarDH (38)
58 ArcsinMeanAccuracyLoad2CueDisSameTarSH (38)
59 ArcsinMeanAccuracyLoad3CueDisDiffTarDH (38)
60 ArcsinMeanAccuracyLoad3CueDisDiffTarSH (38)
61 ArcsinMeanAccuracyLoad3CueDisSameTarDH (38)
62 ArcsinMeanAccuracyLoad3CueDisSameTarSH (38)
63 ArcsinMeanAccuracyLoad4CueDisDiffTarDH (38)
64 ArcsinMeanAccuracyLoad4CueDisDiffTarSH (38)
65 ArcsinMeanAccuracyLoad4CueDisSameTarDH (38)
66 ArcsinMeanAccuracyLoad4CueDisSameTarSH (38)
199 NCorrectTrialsResponseTimeSummaryCount (38)
250 SqrtMeanCorrectResponseTimeSummaryMean (38)

267 LnBefMeanCorrectResponseTimeSummaryMean (39)

233 SqrtBefMeanCorrectResponseTimeSummaryMean (41)

100 MeanAccuracyLoad1CueDisDiffTarDHCueTarDiff (42)
101 MeanAccuracyLoad1CueDisDiffTarDHCueTarSame (42)
102 MeanAccuracyLoad1CueDisDiffTarSHCueTarDiff (42)
103 MeanAccuracyLoad1CueDisDiffTarSHCueTarSame (42)
104 MeanAccuracyLoad1CueDisSameTarDHCueTarDiff (42)
105 MeanAccuracyLoad1CueDisSameTarDHCueTarSame (42)
106 MeanAccuracyLoad1CueDisSameTarSHCueTarDiff (42)
107 MeanAccuracyLoad1CueDisSameTarSHCueTarSame (42)
108 MeanAccuracyLoad2CueDisDiffTarDHCueTarDiff (42)
109 MeanAccuracyLoad2CueDisDiffTarDHCueTarSame (42)
110 MeanAccuracyLoad2CueDisDiffTarSHCueTarDiff (42)
111 MeanAccuracyLoad2CueDisDiffTarSHCueTarSame (42)
112 MeanAccuracyLoad2CueDisSameTarDHCueTarDiff (42)
113 MeanAccuracyLoad2CueDisSameTarDHCueTarSame (42)
114 MeanAccuracyLoad2CueDisSameTarSHCueTarDiff (42)
115 MeanAccuracyLoad2CueDisSameTarSHCueTarSame (42)
116 MeanAccuracyLoad3CueDisDiffTarDHCueTarDiff (42)
117 MeanAccuracyLoad3CueDisDiffTarDHCueTarSame (42)
118 MeanAccuracyLoad3CueDisDiffTarSHCueTarDiff (42)
119 MeanAccuracyLoad3CueDisDiffTarSHCueTarSame (42)
120 MeanAccuracyLoad3CueDisSameTarDHCueTarDiff (42)
121 MeanAccuracyLoad3CueDisSameTarDHCueTarSame (42)
122 MeanAccuracyLoad3CueDisSameTarSHCueTarDiff (42)
123 MeanAccuracyLoad3CueDisSameTarSHCueTarSame (42)
124 MeanAccuracyLoad4CueDisDiffTarDHCueTarDiff (42)
125 MeanAccuracyLoad4CueDisDiffTarDHCueTarSame (42)
126 MeanAccuracyLoad4CueDisDiffTarSHCueTarDiff (42)
127 MeanAccuracyLoad4CueDisDiffTarSHCueTarSame (42)
128 MeanAccuracyLoad4CueDisSameTarDHCueTarDiff (42)
129 MeanAccuracyLoad4CueDisSameTarDHCueTarSame (42)
130 MeanAccuracyLoad4CueDisSameTarSHCueTarDiff (42)
131 MeanAccuracyLoad4CueDisSameTarSHCueTarSame (42)

200 MeanCorrectResponseTimeLoad1CueDisDiffTarDH (43)
201 MeanCorrectResponseTimeLoad1CueDisDiffTarSH (43)
202 MeanCorrectResponseTimeLoad1CueDisSameTarDH (43)
203 MeanCorrectResponseTimeLoad1CueDisSameTarSH (43)
204 MeanCorrectResponseTimeLoad2CueDisDiffTarDH (43)
205 MeanCorrectResponseTimeLoad2CueDisDiffTarSH (43)
206 MeanCorrectResponseTimeLoad2CueDisSameTarDH (43)
207 MeanCorrectResponseTimeLoad2CueDisSameTarSH (43)
208 MeanCorrectResponseTimeLoad3CueDisDiffTarDH (43)
209 MeanCorrectResponseTimeLoad3CueDisDiffTarSH (43)
210 MeanCorrectResponseTimeLoad3CueDisSameTarDH (43)
211 MeanCorrectResponseTimeLoad3CueDisSameTarSH (43)
212 MeanCorrectResponseTimeLoad4CueDisDiffTarDH (43)
213 MeanCorrectResponseTimeLoad4CueDisDiffTarSH (43)
214 MeanCorrectResponseTimeLoad4CueDisSameTarDH (43)
215 MeanCorrectResponseTimeLoad4CueDisSameTarSH (43)

333 DistObjCompatALoad1CueDisDiffMinusCueDisSame (44)
334 DistObjCompatALoad2CueDisDiffMinusCueDisSame (44)
335 DistObjCompatALoad3CueDisDiffMinusCueDisSame (44)
336 DistObjCompatALoad4CueDisDiffMinusCueDisSame (44)
337 DistSpaCompatALoad1CueDisDiffMinusCueDisSame (44)
338 DistSpaCompatALoad2CueDisDiffMinusCueDisSame (44)
339 DistSpaCompatALoad3CueDisDiffMinusCueDisSame (44)
340 DistSpaCompatALoad4CueDisDiffMinusCueDisSame (44)

68 NTrialsAccuracyLoad1CueDisDiffTarDHCueTarDiff (45)
69 NTrialsAccuracyLoad1CueDisDiffTarDHCueTarSame (45)
70 NTrialsAccuracyLoad1CueDisDiffTarSHCueTarDiff (45)
71 NTrialsAccuracyLoad1CueDisDiffTarSHCueTarSame (45)
72 NTrialsAccuracyLoad1CueDisSameTarDHCueTarDiff (45)
73 NTrialsAccuracyLoad1CueDisSameTarDHCueTarSame (45)
74 NTrialsAccuracyLoad1CueDisSameTarSHCueTarDiff (45)
75 NTrialsAccuracyLoad1CueDisSameTarSHCueTarSame (45)
76 NTrialsAccuracyLoad2CueDisDiffTarDHCueTarDiff (45)
77 NTrialsAccuracyLoad2CueDisDiffTarDHCueTarSame (45)
78 NTrialsAccuracyLoad2CueDisDiffTarSHCueTarDiff (45)
79 NTrialsAccuracyLoad2CueDisDiffTarSHCueTarSame (45)
80 NTrialsAccuracyLoad2CueDisSameTarDHCueTarDiff (45)
81 NTrialsAccuracyLoad2CueDisSameTarDHCueTarSame (45)
82 NTrialsAccuracyLoad2CueDisSameTarSHCueTarDiff (45)
83 NTrialsAccuracyLoad2CueDisSameTarSHCueTarSame (45)
84 NTrialsAccuracyLoad3CueDisDiffTarDHCueTarDiff (45)
85 NTrialsAccuracyLoad3CueDisDiffTarDHCueTarSame (45)
86 NTrialsAccuracyLoad3CueDisDiffTarSHCueTarDiff (45)
87 NTrialsAccuracyLoad3CueDisDiffTarSHCueTarSame (45)
88 NTrialsAccuracyLoad3CueDisSameTarDHCueTarDiff (45)
89 NTrialsAccuracyLoad3CueDisSameTarDHCueTarSame (45)
90 NTrialsAccuracyLoad3CueDisSameTarSHCueTarDiff (45)
91 NTrialsAccuracyLoad3CueDisSameTarSHCueTarSame (45)
92 NTrialsAccuracyLoad4CueDisDiffTarDHCueTarDiff (45)
93 NTrialsAccuracyLoad4CueDisDiffTarDHCueTarSame (45)
94 NTrialsAccuracyLoad4CueDisDiffTarSHCueTarDiff (45)
95 NTrialsAccuracyLoad4CueDisDiffTarSHCueTarSame (45)
96 NTrialsAccuracyLoad4CueDisSameTarDHCueTarDiff (45)
97 NTrialsAccuracyLoad4CueDisSameTarDHCueTarSame (45)
98 NTrialsAccuracyLoad4CueDisSameTarSHCueTarDiff (45)
99 NTrialsAccuracyLoad4CueDisSameTarSHCueTarSame (45)
268 LnMeanCorrectResponseTimeLoad1CueDisDiffTarDH (45)
269 LnMeanCorrectResponseTimeLoad1CueDisDiffTarSH (45)
270 LnMeanCorrectResponseTimeLoad1CueDisSameTarDH (45)
271 LnMeanCorrectResponseTimeLoad1CueDisSameTarSH (45)
272 LnMeanCorrectResponseTimeLoad2CueDisDiffTarDH (45)
273 LnMeanCorrectResponseTimeLoad2CueDisDiffTarSH (45)
274 LnMeanCorrectResponseTimeLoad2CueDisSameTarDH (45)
275 LnMeanCorrectResponseTimeLoad2CueDisSameTarSH (45)
276 LnMeanCorrectResponseTimeLoad3CueDisDiffTarDH (45)
277 LnMeanCorrectResponseTimeLoad3CueDisDiffTarSH (45)
278 LnMeanCorrectResponseTimeLoad3CueDisSameTarDH (45)
279 LnMeanCorrectResponseTimeLoad3CueDisSameTarSH (45)
280 LnMeanCorrectResponseTimeLoad4CueDisDiffTarDH (45)
281 LnMeanCorrectResponseTimeLoad4CueDisDiffTarSH (45)
282 LnMeanCorrectResponseTimeLoad4CueDisSameTarDH (45)
283 LnMeanCorrectResponseTimeLoad4CueDisSameTarSH (45)


183 NCorrectTrialsResponseTimeLoad1CueDisDiffTarDH (46)
184 NCorrectTrialsResponseTimeLoad1CueDisDiffTarSH (46)
185 NCorrectTrialsResponseTimeLoad1CueDisSameTarDH (46)
186 NCorrectTrialsResponseTimeLoad1CueDisSameTarSH (46)
187 NCorrectTrialsResponseTimeLoad2CueDisDiffTarDH (46)
188 NCorrectTrialsResponseTimeLoad2CueDisDiffTarSH (46)
189 NCorrectTrialsResponseTimeLoad2CueDisSameTarDH (46)
190 NCorrectTrialsResponseTimeLoad2CueDisSameTarSH (46)
191 NCorrectTrialsResponseTimeLoad3CueDisDiffTarDH (46)
192 NCorrectTrialsResponseTimeLoad3CueDisDiffTarSH (46)
193 NCorrectTrialsResponseTimeLoad3CueDisSameTarDH (46)
194 NCorrectTrialsResponseTimeLoad3CueDisSameTarSH (46)
195 NCorrectTrialsResponseTimeLoad4CueDisDiffTarDH (46)
196 NCorrectTrialsResponseTimeLoad4CueDisDiffTarSH (46)
197 NCorrectTrialsResponseTimeLoad4CueDisSameTarDH (46)
198 NCorrectTrialsResponseTimeLoad4CueDisSameTarSH (46)
453 MinDistObjCompatALoadCueDisDiffMinusCueDisSame (46)
454 MinDistSpaCompatALoadCueDisDiffMinusCueDisSame (46)
456 MaxDistObjCompatALoadCueDisDiffMinusCueDisSame (46)
457 MaxDistSpaCompatALoadCueDisDiffMinusCueDisSame (46)
459 SumDistObjCompatALoadCueDisDiffMinusCueDisSame (46)
460 SumDistSpaCompatALoadCueDisDiffMinusCueDisSame (46)

234 SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarDH (47)
235 SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarSH (47)
236 SqrtMeanCorrectResponseTimeLoad1CueDisSameTarDH (47)
237 SqrtMeanCorrectResponseTimeLoad1CueDisSameTarSH (47)
238 SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarDH (47)
239 SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarSH (47)
240 SqrtMeanCorrectResponseTimeLoad2CueDisSameTarDH (47)
241 SqrtMeanCorrectResponseTimeLoad2CueDisSameTarSH (47)
242 SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarDH (47)
243 SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarSH (47)
244 SqrtMeanCorrectResponseTimeLoad3CueDisSameTarDH (47)
245 SqrtMeanCorrectResponseTimeLoad3CueDisSameTarSH (47)
246 SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarDH (47)
247 SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarSH (47)
248 SqrtMeanCorrectResponseTimeLoad4CueDisSameTarDH (47)
249 SqrtMeanCorrectResponseTimeLoad4CueDisSameTarSH (47)

251 LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH (48)
252 LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH (48)
253 LnBefMeanCorrectResponseTimeLoad1CueDisSameTarDH (48)
254 LnBefMeanCorrectResponseTimeLoad1CueDisSameTarSH (48)
255 LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH (48)
256 LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH (48)
257 LnBefMeanCorrectResponseTimeLoad2CueDisSameTarDH (48)
258 LnBefMeanCorrectResponseTimeLoad2CueDisSameTarSH (48)
259 LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH (48)
260 LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH (48)
261 LnBefMeanCorrectResponseTimeLoad3CueDisSameTarDH (48)
262 LnBefMeanCorrectResponseTimeLoad3CueDisSameTarSH (48)
263 LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH (48)
264 LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH (48)
265 LnBefMeanCorrectResponseTimeLoad4CueDisSameTarDH (48)
266 LnBefMeanCorrectResponseTimeLoad4CueDisSameTarSH (48)
341 DistObj+SpaCompatALoad1CueDisDiffMinusCueDisSame (48)
342 DistObj+SpaCompatALoad2CueDisDiffMinusCueDisSame (48)
343 DistObj+SpaCompatALoad3CueDisDiffMinusCueDisSame (48)
344 DistObj+SpaCompatALoad4CueDisDiffMinusCueDisSame (48)

321 DistObjCompatCowanKLoad1CueDisDiffMinusCueDisSame (49)
322 DistObjCompatCowanKLoad2CueDisDiffMinusCueDisSame (49)
323 DistObjCompatCowanKLoad3CueDisDiffMinusCueDisSame (49)
324 DistObjCompatCowanKLoad4CueDisDiffMinusCueDisSame (49)
325 DistSpaCompatCowanKLoad1CueDisDiffMinusCueDisSame (49)
326 DistSpaCompatCowanKLoad2CueDisDiffMinusCueDisSame (49)
327 DistSpaCompatCowanKLoad3CueDisDiffMinusCueDisSame (49)
328 DistSpaCompatCowanKLoad4CueDisDiffMinusCueDisSame (49)

217 SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH (50)
218 SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH (50)
219 SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarDH (50)
220 SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarSH (50)
221 SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH (50)
222 SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH (50)
223 SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarDH (50)
224 SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarSH (50)
225 SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH (50)
226 SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH (50)
227 SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarDH (50)
228 SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarSH (50)
229 SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH (50)
230 SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH (50)
231 SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarDH (50)
232 SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarSH (50)
285 DistObjCompatMeanAccLoad1CueDisDiffMinusCueDisSame (50)
286 DistObjCompatMeanAccLoad2CueDisDiffMinusCueDisSame (50)
287 DistObjCompatMeanAccLoad3CueDisDiffMinusCueDisSame (50)
288 DistObjCompatMeanAccLoad4CueDisDiffMinusCueDisSame (50)
289 DistSpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame (50)
290 DistSpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame (50)
291 DistSpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame (50)
292 DistSpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame (50)
345 DistObjCompatD-PrimeLoad1CueDisDiffMinusCueDisSame (50)
346 DistObjCompatD-PrimeLoad2CueDisDiffMinusCueDisSame (50)
347 DistObjCompatD-PrimeLoad3CueDisDiffMinusCueDisSame (50)
348 DistObjCompatD-PrimeLoad4CueDisDiffMinusCueDisSame (50)
349 DistSpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame (50)
350 DistSpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame (50)
351 DistSpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame (50)
352 DistSpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame (50)
357 DistObjCompatMeanCRTLoad1CueDisDiffMinusCueDisSame (50)
358 DistObjCompatMeanCRTLoad2CueDisDiffMinusCueDisSame (50)
359 DistObjCompatMeanCRTLoad3CueDisDiffMinusCueDisSame (50)
360 DistObjCompatMeanCRTLoad4CueDisDiffMinusCueDisSame (50)
361 DistSpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame (50)
362 DistSpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame (50)
363 DistSpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame (50)
364 DistSpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame (50)
455 MinDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame (50)
458 MaxDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame (50)
461 SumDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame (50)

444 MinDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame (51)
445 MinDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame (51)
447 MaxDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame (51)
448 MaxDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame (51)
450 SumDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame (51)
451 SumDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame (51)

405 DistObjCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame (52)
406 DistObjCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame (52)
407 DistObjCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame (52)
408 DistObjCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame (52)
409 DistSpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame (52)
410 DistSpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame (52)
411 DistSpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame (52)
412 DistSpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame (52)
417 MinDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame (52)
418 MinDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame (52)
420 MaxDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame (52)
421 MaxDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame (52)
423 SumDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame (52)
424 SumDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame (52)
462 MinDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame (52)
463 MinDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame (52)
465 MaxDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame (52)
466 MaxDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame (52)
468 SumDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame (52)
469 SumDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame (52)
471 MinDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame (52)
472 MinDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame (52)
474 MaxDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame (52)
475 MaxDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame (52)
477 SumDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame (52)
478 SumDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame (52)

329 DistObj+SpaCompatCowanKLoad1CueDisDiffMinusCueDisSame (53)
330 DistObj+SpaCompatCowanKLoad2CueDisDiffMinusCueDisSame (53)
331 DistObj+SpaCompatCowanKLoad3CueDisDiffMinusCueDisSame (53)
332 DistObj+SpaCompatCowanKLoad4CueDisDiffMinusCueDisSame (53)

293 DistObj+SpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame (54)
294 DistObj+SpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame (54)
295 DistObj+SpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame (54)
296 DistObj+SpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame (54)
353 DistObj+SpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame (54)
354 DistObj+SpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame (54)
355 DistObj+SpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame (54)
356 DistObj+SpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame (54)
365 DistObj+SpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame (54)
366 DistObj+SpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame (54)
367 DistObj+SpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame (54)
368 DistObj+SpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame (54)
381 DistObjCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame (54)
382 DistObjCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame (54)
383 DistObjCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame (54)
384 DistObjCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame (54)
385 DistSpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame (54)
386 DistSpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame (54)
387 DistSpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame (54)
388 DistSpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame (54)
507 MinDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (54)
508 MinDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (54)
510 MaxDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (54)
511 MaxDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (54)
513 SumDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (54)
514 SumDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (54)

393 DistObjCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame (55)
394 DistObjCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame (55)
395 DistObjCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame (55)
396 DistObjCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame (55)
397 DistSpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame (55)
398 DistSpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame (55)
399 DistSpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame (55)
400 DistSpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame (55)
446 MinDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame (55)
449 MaxDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame (55)
452 SumDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame (55)

297 DistObjCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame (56)
298 DistObjCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame (56)
299 DistObjCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame (56)
300 DistObjCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame (56)
301 DistSpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame (56)
302 DistSpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame (56)
303 DistSpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame (56)
304 DistSpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame (56)
309 DistObjCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame (56)
310 DistObjCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame (56)
311 DistObjCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame (56)
312 DistObjCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame (56)
313 DistSpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame (56)
314 DistSpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame (56)
315 DistSpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame (56)
316 DistSpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame (56)
413 DistObj+SpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame (56)
414 DistObj+SpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame (56)
415 DistObj+SpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame (56)
416 DistObj+SpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame (56)
419 MinDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame (56)
422 MaxDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame (56)
425 SumDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame (56)
426 MinDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (56)
427 MinDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (56)
429 MaxDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (56)
430 MaxDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (56)
432 SumDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (56)
433 SumDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (56)
464 MinDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame (56)
467 MaxDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame (56)
470 SumDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame (56)
473 MinDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame (56)
476 MaxDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame (56)
479 SumDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame (56)
489 MinDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (56)
490 MinDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (56)
492 MaxDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (56)
493 MaxDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (56)
495 SumDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (56)
496 SumDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (56)

369 DistObjCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame (57)
370 DistObjCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame (57)
371 DistObjCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame (57)
372 DistObjCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame (57)
373 DistSpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame (57)
374 DistSpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame (57)
375 DistSpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame (57)
376 DistSpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame (57)
498 MinDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (57)
499 MinDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (57)
501 MaxDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (57)
502 MaxDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (57)
504 SumDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (57)
505 SumDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (57)

389 DistObj+SpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame (58)
390 DistObj+SpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame (58)
391 DistObj+SpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame (58)
392 DistObj+SpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame (58)
435 MinDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (58)
436 MinDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (58)
438 MaxDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (58)
439 MaxDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (58)
441 SumDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (58)
442 SumDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (58)
509 MinDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (58)
512 MaxDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (58)
515 SumDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame (58)

401 DistObj+SpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame (59)
402 DistObj+SpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame (59)
403 DistObj+SpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame (59)
404 DistObj+SpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame (59)
480 MinDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (59)
481 MinDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (59)
483 MaxDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (59)
484 MaxDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (59)
486 SumDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (59)
487 SumDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (59)

305 DistObj+SpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame (60)
306 DistObj+SpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame (60)
307 DistObj+SpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame (60)
308 DistObj+SpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame (60)
317 DistObj+SpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame (60)
318 DistObj+SpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame (60)
319 DistObj+SpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame (60)
320 DistObj+SpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame (60)
428 MinDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (60)
431 MaxDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (60)
434 SumDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame (60)
491 MinDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (60)
494 MaxDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (60)
497 SumDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame (60)

377 DistObj+SpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame (61)
378 DistObj+SpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame (61)
379 DistObj+SpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame (61)
380 DistObj+SpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame (61)
500 MinDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (61)
503 MaxDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (61)
506 SumDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame (61)

437 MinDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (62)
440 MaxDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (62)
443 SumDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame (62)

482 MinDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (63)
485 MaxDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (63)
488 SumDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame (63)   }

end;

{ RAddZScore }

procedure RAddZScore.Init(anAgeSpec, aSexSpec, aHandedSpec, aSessionSpec,
  aRighthanded, aFemale: Boolean; aLoAge, aHiAge, aSession: Integer);
begin
  Self := Default(RAddZScore);
  ageSpecified := anAgeSpec;
  sexSpecified := aSexSpec;
  handedSpecified := aHandedSpec;
  sessionSpecified := aSessionSpec;
  rightHanded := aRighthanded;
  female := aFemale;
  loAge := aLoAge;
  hiAge := aHiAge;
  session := aSession;
end;

end.


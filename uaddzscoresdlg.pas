unit uAddZScoresDlg;

{$mode ObjFPC}{$H+}
{$WARN 5024 off : Parameter "$1" not used}
interface

uses
  Classes, SysUtils,
  Forms, Controls, StdCtrls, Spin, ExtCtrls,
  uEngine;

type

  TZScoresDialog = class(TForm)
  private
    AgeCB: TCheckBox;
    AgeEdit: TSpinEdit;
    CancelButton: TButton;
    CurrentNormativeDataGB: TGroupBox;
    CurrentNormativeMemo: TMemo;
    HandednessCB: TCheckBox;
    HandednessLabel: TLabel;
    OKButton: TButton;
    PAgeLabel:TLabel;
    ParticipantLabel: TLabel;
    ParticipantParametersGB: TGroupBox;
    PHandedLabel: TLabel;
    PSessionLabel: TLabel;
    PSexLabel: TLabel;
    SessionCB: TCheckBox;
    SessionLabel: TLabel;
    SexCB: TCheckBox;
    SexLabel: TLabel;
    YearsLabel: TLabel;

    function GetAgeSpecified: Boolean;
    function GetFemale: Boolean;
    function GetHandednessSpecified: Boolean;
    function GetHighAge: Integer;
    function GetLowAge: Integer;
    function GetRightHanded: Boolean;
    function GetSession: Integer;
    function GetSessionSpecified: Boolean;
    function GetSexSpecified: Boolean;
    procedure AgeCBChange(Sender: TObject);
    procedure AgeEditChange(Sender: TObject);
    procedure CheckOKButtonEnabled;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure HandednessCBChange(Sender: TObject);
    procedure PopulateMemoWithValues;
    procedure RefreshYearsLabel;
    procedure SessionCBChange(Sender: TObject);
    procedure SexCBClick(Sender: TObject);
  public
    constructor Create(TheOwner: TComponent); override;
    property AgeSpecified: Boolean read GetAgeSpecified;
    property Female: Boolean read GetFemale;
    property HandednessSpecified: Boolean read GetHandednessSpecified;
    property HighAge: Integer read GetHighAge;
    property LowAge: Integer read GetLowAge;
    property RightHanded: Boolean read GetRightHanded;
    property Session: Integer read GetSession;
    property SessionSpecified: Boolean read GetSessionSpecified;
    property SexSpecified: Boolean read GetSexSpecified;
  end;

  function GotParticipantParametersDlgOK(selectedParticipant: TParticipant;
                                         out sexSpec, ageSpec, sessionSpec, handedSpec: Boolean;
                                         out Female, RightHanded: Boolean;
                                         out LowAge, HighAge, session: Integer): Boolean;

implementation

uses
  LCLType,
  uTypesConstsUtils;

var
  participant: TParticipant = Nil;

function GotParticipantParametersDlgOK(selectedParticipant: TParticipant;
                                       out sexSpec, ageSpec, sessionSpec, handedSpec: Boolean;
                                       out Female, RightHanded: Boolean;
                                       out LowAge, HighAge, session: Integer): Boolean;
var
  dlg: TZScoresDialog;
begin
  if not enableNormFeatures then
    Exit;
  participant := selectedParticipant;
  dlg := TZScoresDialog.Create(Nil);
  try
    case dlg.ShowModal of
      mrOK: begin
              sexSpec :=     dlg.SexSpecified;
              ageSpec :=     dlg.AgeSpecified;
              sessionSpec := dlg.SessionSpecified;
              handedSpec :=  dlg.HandednessSpecified;
              Female :=      dlg.Female;
              RightHanded := dlg.RightHanded;
              LowAge :=      dlg.LowAge;
              HighAge :=     dlg.HighAge;
              session :=     dlg.Session;
              Result := True;
            end;
      otherwise
        sexSpec := False;
        ageSpec := False;
        sessionSpec := False;
        handedSpec := False;
        Female := False;
        RightHanded := False;
        LowAge := 0;
        HighAge := 0;
        session := 0;
        Result := False;
    end;
  finally
    dlg.Free;
  end;
end;

{ TZScoresDialog }

procedure TZScoresDialog.AgeEditChange(Sender: TObject);
begin
  RefreshYearsLabel;
end;

function TZScoresDialog.GetFemale: Boolean;
begin
  case participant.sex[1] of
    'F','f': Result := True;
    otherwise Result := False;
  end;
end;

function TZScoresDialog.GetHandednessSpecified: Boolean;
begin
  Exit(HandednessCB.Checked);
end;

procedure TZScoresDialog.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE: begin
                 Key := 0;
                 ModalResult := mrCancel;
               end;
    otherwise
      inherited KeyDown(Key, Shift);
  end;
end;

function TZScoresDialog.GetAgeSpecified: Boolean;
begin
  Exit(AgeCB.Checked);
end;

function TZScoresDialog.GetHighAge: Integer;
begin
  Exit(participant.age.ToInteger + AgeEdit.Value);
end;

function TZScoresDialog.GetLowAge: Integer;
begin
  Result := participant.age.ToInteger - AgeEdit.Value;
  if Result < 0 then
    Result := 0;
end;

function TZScoresDialog.GetRightHanded: Boolean;
begin
  case participant.handedness[1] of
    'R','r': Result := True;
    otherwise Result := False;
  end;
end;

procedure TZScoresDialog.AgeCBChange(Sender: TObject);
begin
  AgeEdit.Enabled := AgeCB.Checked;
  RefreshYearsLabel;
  CheckOKButtonEnabled;
end;

procedure TZScoresDialog.HandednessCBChange(Sender: TObject);
begin
  case HandednessCB.Checked of
    True:  HandednessLabel.Caption := '(' + participant.handedness + ')';
    False: HandednessLabel.Caption := rsUnspecified;
  end;
  CheckOKButtonEnabled;
end;

procedure TZScoresDialog.PopulateMemoWithValues;
var
  b: Byte;
begin
  for b in currentNormList.SessionSet do
    CurrentNormativeMemo.Lines.Add(' Session: %d (%d)', [b, currentNormList.SessionCount[b]]);
  CurrentNormativeMemo.Lines.Add('');
  CurrentNormativeMemo.Lines.Add(' Lefthanded: (%d)   Righthanded: (%d)', [currentNormList.LefthandedCount, currentNormList.RighthandedCount]);
  CurrentNormativeMemo.Lines.Add('');
  CurrentNormativeMemo.Lines.Add(' Female: (%d)   Male: (%d)', [currentNormList.FemaleCount, currentNormList.MaleCount]);
  CurrentNormativeMemo.Lines.Add('');
  for b in currentNormList.AgeSet do
    CurrentNormativeMemo.Lines.Add(' Age: %d (%d)', [b, currentNormList.AgeCount[b]]);
end;

procedure TZScoresDialog.SexCBClick(Sender: TObject);
begin
  case SexCB.Checked of
    True:  SexLabel.Caption := '(' + participant.sex + ')';
    False: SexLabel.Caption := rsUnspecified;
  end;
  CheckOKButtonEnabled;
end;

procedure TZScoresDialog.RefreshYearsLabel;
var
  low, high: Integer;
begin
  case AgeCB.Checked of
    True: begin
            low := participant.age.ToInteger - AgeEdit.Value;
            if low < 0 then
              low := 0;
            high := participant.age.ToInteger + AgeEdit.Value;
            YearsLabel.Caption := Format('years (from %d to %d)',[low, high]);
          end;
    False: YearsLabel.Caption := '(unspecified years)';
  end;
end;

procedure TZScoresDialog.SessionCBChange(Sender: TObject);
begin
  case SessionCB.Checked of
    True:  SessionLabel.Caption := '(' + participant.sessionNo + ')';
    False: SessionLabel.Caption := rsUnspecified;
  end;
  CheckOKButtonEnabled;
end;

procedure TZScoresDialog.CheckOKButtonEnabled;
begin
  OKButton.Enabled := AgeCB.Checked or SexCB.Checked or HandednessCB.Checked or SessionCB.Checked;
end;

function TZScoresDialog.GetSession: Integer;
begin
  Result := StrToInt(participant.sessionNo);
end;

function TZScoresDialog.GetSessionSpecified: Boolean;
begin
  Exit(SessionCB.Checked);
end;

function TZScoresDialog.GetSexSpecified: Boolean;
begin
  Exit(SexCB.Checked);
end;

constructor TZScoresDialog.Create(TheOwner: TComponent);
begin
  inherited CreateNew(TheOwner);
  SetInitialBounds(0, 0, 500, 250);
  Position := poScreenCenter;
  BorderStyle := bsToolWindow;
  Constraints.MinWidth := 390;
  AutoSize := True;
  KeyPreview := True;
  OnKeyDown  := @FormKeyDown;
  Caption := 'Include '+ participant.participantID + '''s Z-scores, percentiles and p-values in report';

  CurrentNormativeDataGB := TGroupBox.Create(Self);
  CurrentNormativeMemo := TMemo.Create(Self);
  ParticipantParametersGB := TGroupBox.Create(Self);
  AgeCB := TCheckBox.Create(Self);
  AgeEdit := TSpinEdit.Create(Self);
  YearsLabel := TLabel.Create(Self);
  SexCB := TCheckBox.Create(Self);
  SexLabel := TLabel.Create(Self);
  HandednessCB := TCheckBox.Create(Self);
  HandednessLabel := TLabel.Create(Self);
  SessionCB := TCheckBox.Create(Self);
  SessionLabel := TLabel.Create(Self);
  OKButton := TButton.Create(Self);
  CancelButton := TButton.Create(Self);
  ParticipantLabel := TLabel.Create(Self);
  PAgeLabel := TLabel.Create(Self);
  PHandedLabel := TLabel.Create(Self);
  PSessionLabel := TLabel.Create(Self);
  PSexLabel := TLabel.Create(Self);

  with CurrentNormativeDataGB do begin
    Anchors := [akTop, akLeft, akRight];
    AnchorSideLeft.Control := Self;
    AnchorSideTop.Control := Self;
    AnchorSideRight.Control := Self;
    AnchorSideRight.Side := asrRight;
    AutoSize := True;
    BorderSpacing.Around := 20;
    Caption := '  Current normative data has participants with these characteristics  ';
    Constraints.MinWidth := 300;
    Parent := Self;
  end;

  with CurrentNormativeMemo do begin
    Align := alClient;
    BorderSpacing.Around := 20;
    Constraints.MinHeight := 200;
    ScrollBars := ssAutoBoth;
    PopulateMemoWithValues;
    Parent := CurrentNormativeDataGB;
  end;

  with ParticipantParametersGB do begin
    Anchors := [akTop, akLeft, akRight];
    AnchorSideLeft.Control := Self;
    AnchorSideTop.Control := CurrentNormativeDataGB;
    AnchorSideTop.Side := asrBottom;
    AnchorSideRight.Control := Self;
    AnchorSideRight.Side := asrRight;
    AutoSize := True;
    BorderSpacing.Around := 20;
    Caption := '  Specify normative data participant characteristics  ';
    Constraints.MinWidth := 300;
    Parent := Self;
  end;

  with ParticipantLabel do begin
    AnchorSideLeft.Control := ParticipantParametersGB;
    AnchorSideTop.Control := ParticipantParametersGB;
    BorderSpacing.Top := 15;
    BorderSpacing.Left := 10;
    Caption := '('+participant.participantID+')';
    Parent := ParticipantParametersGB;
  end;

  with AgeCB do begin
    AnchorSideLeft.Control := ParticipantLabel;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := AgeEdit;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Left := 10;
    Caption := 'Age  +/- ';
    OnChange  := @AgeCBChange;
    TabOrder := 0;
    Parent := ParticipantParametersGB;
  end;

  with AgeEdit do begin
    AnchorSideLeft.Control := AgeCB;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := ParticipantLabel;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Top := 5;
    Enabled := False;
    MaxValue := 99;
    OnChange  := @AgeEditChange;
    TabOrder := 1;
    Parent := ParticipantParametersGB;
  end;

  with PAgeLabel do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Side := asrCenter;
    AnchorSideTop.Control := AgeCB;
    AnchorSideRight.Control := AgeCB;
    Caption := '('+participant.age+')';
    Parent := ParticipantParametersGB;
  end;

  with PSexLabel do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Side := asrCenter;
    AnchorSideTop.Control := SexCB;
    AnchorSideRight.Control := SexCB;
    Caption := '('+participant.sex+')';
    Parent := ParticipantParametersGB;
  end;

  with PHandedLabel do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Side := asrCenter;
    AnchorSideTop.Control := HandednessCB;
    AnchorSideRight.Control := HandednessCB;
    Caption := '('+participant.handedness+')';
    Parent := ParticipantParametersGB;
  end;

  with PSessionLabel do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Side := asrCenter;
    AnchorSideTop.Control := SessionCB;
    AnchorSideRight.Control := SessionCB;
    Caption := '(Session '+participant.sessionNo+')';
    Parent := ParticipantParametersGB;
  end;

  with YearsLabel do begin
    AnchorSideLeft.Control := AgeEdit;
    AnchorSideLeft.Side := asrBottom;
    AnchorSideTop.Control := AgeEdit;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Left := 4;
    Caption := rsUnspecifiedYears;
    ParentColor := False;
    Parent := ParticipantParametersGB;
  end;

  with SexCB do begin
    AnchorSideLeft.Control := ParticipantLabel;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := AgeCB;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Left := 10;
    BorderSpacing.Top := 5;
    Caption := rsSex;
    OnClick  := @SexCBClick;
    TabOrder := 2;
    Parent := ParticipantParametersGB;
  end;

  with SexLabel do begin
    AnchorSideLeft.Control := SexCB;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := SexCB;
    AnchorSideTop.Side := asrCenter;
    BorderSpacing.Left := 10;
    ParentColor := False;
    Caption := rsUnspecified;
    Parent := ParticipantParametersGB;
  end;

  with HandednessCB do begin
    AnchorSideLeft.Control := ParticipantLabel;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := SexCB;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Left := 10;
    BorderSpacing.Top := 5;
    BorderSpacing.Bottom := 5;
    Caption := rsHandedness;
    OnChange := @HandednessCBChange;
    TabOrder := 3;
    Parent := ParticipantParametersGB;
  end;

  with HandednessLabel do begin
    AnchorSideLeft.Control := HandednessCB;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := HandednessCB;
    AnchorSideTop.Side := asrCenter;
    Caption := rsUnspecified;
    BorderSpacing.Left := 10;
    ParentColor := False;
    Parent := ParticipantParametersGB;
  end;

  with SessionCB do begin
    AnchorSideLeft.Control := ParticipantLabel;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := HandednessCB;
    AnchorSideTop.Side := asrBottom;
    BorderSpacing.Left := 10;
    BorderSpacing.Top := 5;
    BorderSpacing.Bottom := 5;
    Caption := rsSession;
    OnChange  := @SessionCBChange;
    TabOrder := 4;
    Parent := ParticipantParametersGB;
  end;

  with SessionLabel do begin
    AnchorSideLeft.Control := SessionCB;
    AnchorSideLeft.Side := asrRight;
    AnchorSideTop.Control := SessionCB;
    AnchorSideTop.Side := asrCenter;
    Caption := rsUnspecified;
    BorderSpacing.Left := 10;
    BorderSpacing.Right := 10;
    ParentColor := False;
    Parent := ParticipantParametersGB;
  end;

  with OKButton do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := ParticipantParametersGB;
    AnchorSideTop.Side := asrBottom;
    AnchorSideRight.Control := ParticipantParametersGB;
    AnchorSideRight.Side := asrRight;
    BorderSpacing.Top := 20;
    BorderSpacing.Bottom := 20;
    AutoSize := True;
    Enabled := False;
    Caption := 'Characteristics are set correctly';
    Enabled := False;
    ModalResult := mrOK;
    TabOrder := 6;
    Parent := Self;
  end;

  with CancelButton do begin
    AnchorSideTop.Control := OKButton;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := Self;
    BorderSpacing.Right := 20;
    BorderSpacing.Left := 20;
    AutoSize := True;
    Caption := 'Cancel';
    ModalResult := mrCancel;
    TabOrder := 7;
    Parent := Self;
  end;
end;

end.


unit uSummaryDlgZ;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types,
  Forms, StdCtrls, ExtCtrls,
  uEngine, uTypesConstsUtils, uTagMemo;

type

  TSummaryZDlg = class(TForm)
  private
    ButtonPanel: TPanel;
    HeaderPanel: TPanel;
    Holder: TTMHolder;
    PrintButton: TButton;
    SaveToHTMLButton: TButton;
    CloseButton: TButton;
    BoldLabel: TLabel;
    FBoldCount: Integer;
    procedure PrintButtonClick(Sender: TObject);
    procedure SaveToHTMLButtonClick(Sender: TObject);
    procedure PopulateMemoWithValues;
  protected
    procedure DoShow; override;
  public
    constructor Create(TheOwner: TComponent); override;
  end;

  TPrintObj = class(TObject)
  public
    sWidth: Integer;
    pvStr: String;
    constructor Create(aWid: Integer; const aStr: String); reintroduce;
  end;

  procedure ShowZDlg(aMatchingNormCount: Integer; aParticipant: TParticipant; aStatIDs: TIntegerDynArray; aNormList: TNormList);

implementation

uses
  StrUtils,
  Controls, Dialogs, Printers, PrintersDlgs, Graphics
  {$IfDef windows}, LCLType {$EndIf};

var
  participant: TParticipant = Nil;
  statIDs: TIntegerDynArray = Nil;
  normValues: SaRValueZetc;
  matchingNormCount: Integer = 0;
  {$ifdef windows}
  hDejaVuFontRes: LongWord = 0;
  hDejaVuBoldRes: LongWord = 0; {$endif}

{$ifdef windows}
type
  LPDWORD = ^DWORD;

function AddFontMemResourceEx(pbFont: Pointer; cbFont: DWORD; pdv: Pointer; pcFonts: LPDWORD): LongWord; stdcall;
  external 'gdi32.dll' Name 'AddFontMemResourceEx';
function RemoveFontMemResourceEx(fh: LongWord): LongBool; stdcall;
  external 'gdi32.dll' Name 'RemoveFontMemResourceEx'; {$endif}

{$ifdef windows}
function DejavuLoaded: Boolean;
var
  DejaVuID, DejaVuBoldID: Integer;
  ress: TResourceStream;
  count: Cardinal;
begin
  DejaVuID := Screen.Fonts.IndexOf('DEJAVUSANSMONO');
  if not (hDejaVuFontRes > 0) and not (DejaVuID > 0) then
    begin
      ress := TResourceStream.Create(HINSTANCE, 'DEJAVUSANSMONO', PChar(RT_RCDATA));
      try
        hDejaVuFontRes := AddFontMemResourceEx(ress.Memory, ress.Size, Nil, @count);
        Result := count = 1;
      finally
        ress.Free;
      end;
    end;
  DejaVuBoldID := Screen.Fonts.IndexOf('DEJAVUSANSMONO-BOLD');
  if not (hDejaVuBoldRes > 0) and not (DejaVuBoldID > 0) then
    begin
      ress := TResourceStream.Create(HINSTANCE, 'DEJAVUSANSMONO-BOLD', PChar(RT_RCDATA));
      try
        hDejaVuBoldRes := AddFontMemResourceEx(ress.Memory, ress.Size, Nil, @count);
        Result := Result and (count = 1);
      finally
        ress.Free;
      end;
    end;
end;  {$EndIf}

procedure ShowZDlg(aMatchingNormCount: Integer; aParticipant: TParticipant; aStatIDs: TIntegerDynArray;
  aNormList: TNormList);
var
  dlg: TSummaryZDlg;
begin
  participant := aParticipant;
  statIDs := aStatIDs;
  normValues := aNormList.ParticipantNormValues;
  matchingNormCount := aMatchingNormCount;
  dlg := TSummaryZDlg.Create(Nil);
  try
    {$ifdef windows}
    if DejavuLoaded then
      dlg.Holder.FTagMemo.Canvas.Font.Name := 'DejaVu Sans Mono';
    {$endif}
    dlg.ShowModal;
  finally
    dlg.Free;
    {$ifdef windows}
    if hDejaVuFontRes <> 0 then
      RemoveFontMemResourceEx(hDejaVuFontRes);
    if hDejaVuBoldRes <> 0 then
      RemoveFontMemResourceEx(hDejaVuBoldRes);
    {$EndIf}
  end;
end;

{ TPrintObj }

constructor TPrintObj.Create(aWid: Integer; const aStr: String);
begin
  sWidth := aWid;
  pvStr := aStr;
end;

{ TSummaryZDlg }

procedure TSummaryZDlg.SaveToHTMLButtonClick(Sender: TObject);
var
  fNme, statStr, valueStr, zScoreStr, percentileStr, pValueStr, bStartStr, bEndStr: String;
  sl: TStringList;
  i: Integer;
begin
  if participant = Nil then
    Exit;
  fNme := IncludeTrailingPathDelimiter(ExtractFilePath(processor.OutputFullFilename)) +
          participant.participantID + '_' + processor.ExperimentName + '_' + SummaryReportWithZScoresDotHtml;
  sl := TStringList.Create;
  try
    sl.Add(rsHTMLHeader);
    sl.Add(rsPreBody);
    sl.Add(participant.HeaderInfoText);
    sl.Add(Format(rsSRanThisExperiment ,[participant.participantID, participant.date, FormatDateTime('dd mmmm, yyyy',Date)]));
    sl.Add('<hr>');
    sl.Add(rsVariableNameValueZ);
    sl.Add(rsUnderlineDashesZ);

    for i := 0 to High(normValues) do
      begin
        statStr := GetPaddedRightDots(GetStatisticName(statIDs[i]), 86);
        case participant.statsArray[statIDs[i]].isFloat of
          True:  begin
                   valueStr := PadLeft(FormatFloat('###0.00000', normValues[i].value), 10);
                   if valueStr = '   0.00000' then valueStr := '   0.0    ';
                 end;
          False: valueStr := PadLeft(Trunc(normValues[i].value).ToString, 4) + '      ';
        end;
        zScoreStr :=     PadLeft(FormatFloat('#0.00000', normValues[i].zScore), 8);     if zScoreStr = ' 0.00000' then zScoreStr := ' 0      ';
        percentileStr := PadLeft(FormatFloat('#0.00', normValues[i].percentile), 10); if percentileStr = '      0.00' then percentileStr := '      0   ';
        pValueStr :=     PadLeft(FormatFloat('#0.0000', normValues[i].pValue), 7);     if pValueStr = ' 0.0000' then pValueStr := ' 0';
        case (normValues[i].pValue >= ThreshholdPValue) of
          True:  begin bStartStr := '';    bEndStr := ''; end;
          False: begin bStartStr := '<b>'; bEndStr := '</b>'; end;
        end;
        sl.Add('%s %s %s %s %s%s%s',[statStr, valueStr, zScoreStr, percentileStr, bStartStr, pValueStr, bEndStr]);
            //       stat val Z  perc p-value
      end;
    case FBoldCount of
      0: statStr := rsNoPValuesSignificant;
      otherwise
        case FBoldCount of
          1: statStr:= rsOnePValueSignificant;
          otherwise
            statStr := Format(rsDPValuesSignificant, [FBoldCount]);
        end;
    end;
    sl.Add('');
    sl.Add(statStr);
    sl.Add(Format(rsDParticipantsFromNorm, [matchingNormCount, participant.participantID]));
    sl.Add(rsPostBody);
    sl.SaveToFile(fNme);
  finally
    sl.Free;
  end;
  ShowMessageFmt(rsHtmlReportSaved, [participant.participantID, fNme]);
end;

procedure TSummaryZDlg.PopulateMemoWithValues;
var
  statStr, valueStr, zScoreStr, percentileStr, pValueStr, boldStr: String;
  i: Integer;
begin
  FBoldCount := 0;
  Holder.FTagMemo.Canvas.Font.Size := 10;
  Holder.AddLine(rsVariableNameValueZP);
  for i := 0 to High(normValues) do
    begin
      statStr := GetPaddedRightDots(GetStatisticName(statIDs[i]), 64);
      case participant.statsArray[statIDs[i]].isFloat of
        True:  begin
                 valueStr := PadLeft(FormatFloat('###0.00000', normValues[i].value), 10);
                 if valueStr = '   0.00000' then valueStr := '   0.0    ';
               end;
        False: valueStr := PadLeft(Trunc(normValues[i].value).ToString, 4) + '      ';
      end;
      zScoreStr :=     PadLeft(FormatFloat('#0.00000', normValues[i].zScore), 8);     if zScoreStr = ' 0.00000' then zScoreStr := ' 0      ';
      percentileStr := PadLeft(FormatFloat('#0.00', normValues[i].percentile), 10); if percentileStr = '      0.00' then percentileStr := '      0   ';
      pValueStr :=     PadLeft(FormatFloat('#0.0000', normValues[i].pValue), 7);     if pValueStr = ' 0.0000' then pValueStr := ' 0';
      case (normValues[i].pValue >= ThreshholdPValue) of
        True: boldStr := '';
        False: begin
                 boldStr := '<b>';
                 Inc(FBoldCount);
               end;
      end;
      Holder.AddLine(Format('%s%s%s %s %s %s %s%s%s',[boldStr, statStr, boldStr, valueStr, zScoreStr, percentileStr, boldStr, pValueStr, boldStr]));
          //       stat val Z  perc p-value
    end;
  case FBoldCount of
    0: BoldLabel.Caption := rsNoPValuesSignificant;
    otherwise
      case FBoldCount of
        1: BoldLabel.Caption := rsOnePValueSignificant;
        otherwise
          BoldLabel.Caption := Format(rsDPValuesSignificant, [FBoldCount]);
      end;
  end;
end;

procedure TSummaryZDlg.DoShow;
begin
  inherited DoShow;
  PopulateMemoWithValues;
end;

procedure TSummaryZDlg.PrintButtonClick(Sender: TObject);
var
  i, lineCount, pageCount, linesPerPage, sWid: Integer;
  lh: Single;
  prtDlg: TPrintDialog;
  s, statStr, valueStr, zScoreStr, percentileStr, pValueStr: String;
  sl: TStringList;
  po: TPrintObj;
  lMargin: String = '    ';
begin
  sl := TStringList.Create;
  try

    prtDlg := TPrintDialog.Create(Nil);
    try
      if prtDlg.Execute then
      begin
        Printer.BeginDoc;
        try
          Printer.Orientation := poLandscape;
          Printer.Canvas.Font.Assign(Holder.FTagMemo.Canvas.Font);
          Printer.Canvas.Font.Size := 10;
          lh := Printer.Canvas.TextHeight('Xg')*1.15;
          linesPerPage := Trunc(Size(Printer.PaperSize.PaperRect.WorkRect).cy / lh) - 1;
          lineCount := Succ(linesPerPage); // initial value to force header

          for i := 0 to High(normValues) do
            begin
              statStr := GetPaddedRightDots(GetStatisticName(statIDs[i]), 90);
              case participant.statsArray[statIDs[i]].isFloat of
                True:  begin
                         valueStr := PadLeft(FormatFloat('###0.00000', normValues[i].value), 10);
                         if valueStr = '   0.00000' then valueStr := '   0.0    ';
                       end;
                False: valueStr := PadLeft(Trunc(normValues[i].value).ToString, 4) + '      ';
              end;
              zScoreStr :=     PadLeft(FormatFloat('#0.00000', normValues[i].zScore), 8);     if zScoreStr = ' 0.00000' then zScoreStr := ' 0      ';
              percentileStr := PadLeft(FormatFloat('#0.00', normValues[i].percentile), 10); if percentileStr = '      0.00' then percentileStr := '      0   ';
              pValueStr :=     PadLeft(FormatFloat('#0.0000', normValues[i].pValue), 7);     if pValueStr = ' 0.0000' then pValueStr := ' 0';
              case (normValues[i].pValue >= ThreshholdPValue) of
                True:  sl.Add('%s%s %s %s %s %s',[lMargin, statStr, valueStr, zScoreStr, percentileStr, pValueStr]);
                False: begin
                         s := Format('%s%s %s %s %s ',  [lMargin, statStr, valueStr, zScoreStr, percentileStr]);
                         sWid := Printer.Canvas.TextWidth(s);
                         sl.AddObject(s, TPrintObj.Create(sWid, pValueStr));
                       end;
              end;
            end;

          case FBoldCount of
            0: statStr := rsNoPValuesSignificant;
            otherwise
              case FBoldCount of
                1: statStr:= rsOnePValueSignificant;
                otherwise
                  statStr := Format(rsDPValuesSignificant, [FBoldCount]);
              end;
          end;
          sl.Add('');
          sl.Add(statStr);
          sl.Add(Format(rsDParticipantsFromNorm, [matchingNormCount, participant.participantID]));

          pageCount := 0;
          for i := 0 to sl.Count-1 do
            begin
              if (lineCount > linesPerPage) then
                begin
                  lineCount:=1;
                  Inc(pageCount);
                  if pageCount > 1 then
                    Printer.NewPage;
                  s := Format(rsSummaryReportForPage, [lMargin, participant.participantID, FormatDateTime('d mmm yyyy',Date), pageCount]);
                  Printer.Canvas.TextOut(0, trunc(lh*lineCount),s); Inc(lineCount);
                  s := lMargin + participant.HeaderInfoText;
                  Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
                  s := Format(rsExperimentWasPerformed, [lMargin, participant.date]);
                  Printer.Canvas.TextOut(0, trunc(lh*lineCount),s); Inc(lineCount, 2);
                  s := lMargin + rsVariableNamePValue;
                  Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
                  s := lMargin + rsUnderlineDashesZP;
                  Printer.Canvas.TextOut(0, trunc(lh*lineCount), s); Inc(lineCount);
                end;
              case Assigned(sl.Objects[i]) of
                False: Printer.Canvas.TextOut(0, trunc(lh*lineCount), sl[i]);
                True: begin
                        Printer.Canvas.TextOut(0, trunc(lh*lineCount), sl[i]);
                        Printer.Canvas.Font.Style := [fsBold];
                        po := TPrintObj(sl.Objects[i]);
                        printer.Canvas.TextOut(po.sWidth, trunc(lh*lineCount), po.pvStr);
                        printer.Canvas.Font.Style := [];
                      end;
              end;
              Inc(lineCount);
            end;
        finally
          Printer.EndDoc;
        end;
      end;
  finally
    prtDlg.Free;
  end;
  finally
    for i := 0 to sl.Count-1 do
      TPrintObj(sl.Objects[i]).Free;
    sl.Free;
  end;
end;

constructor TSummaryZDlg.Create(TheOwner: TComponent);
begin
  inherited CreateNew(TheOwner);
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  SetInitialBounds(0, 0, 915, 600);
  Caption := Format(rsADPReport, [DateToStr(Date), participant.date]);

  HeaderPanel := TPanel.Create(Self);
  Holder := TTMHolder.Create(Self);
  ButtonPanel := TPanel.Create(Self);
  PrintButton := TButton.Create(Self);
  SaveToHTMLButton := TButton.Create(Self);
  CloseButton := TButton.Create(Self);
  BoldLabel := TLabel.Create(Self);

  with HeaderPanel do begin
    Align := alTop;
    Height := 20;
    BorderSpacing.Top := Margin;
    BevelOuter := bvNone;
    Caption := rsProcessedDataSummary + participant.HeaderInfoText;
    Parent := Self;
  end;

  with ButtonPanel do begin
    Align := alBottom;
    Height := 48;
    BevelOuter := bvNone;
    Parent := Self;
  end;

  with Holder do begin
    Align := alClient;
    BorderSpacing.Top := Margin;
    Parent := Self;
  end;

  with PrintButton do begin
    Caption := rsPrintA4Landscape;
    AutoSize := True;
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := ButtonPanel;
    BorderSpacing.Left := Margin;
    OnClick  := @PrintButtonClick;
    Parent := ButtonPanel;
  end;

  with SaveToHTMLButton do begin
    Caption := rsSaveToHTML;
    AutoSize := True;
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideLeft.Control := PrintButton;
    AnchorSideLeft.Side := asrRight;
    BorderSpacing.Left := Margin;
    OnClick  := @SaveToHTMLButtonClick;
    Parent := ButtonPanel;
  end;

  with BoldLabel do begin
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := CloseButton;
    AnchorSideRight.Side := asrLeft;
    BorderSpacing.Right := DoubleMargin;
    Parent := ButtonPanel;
  end;

  with CloseButton do begin
    Caption := rsClose;
    ModalResult := mrClose;
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := ButtonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := ButtonPanel;
    AnchorSideRight.Side := asrRight;
    BorderSpacing.Right := Margin;
    Parent := ButtonPanel;
  end;
end;

end.


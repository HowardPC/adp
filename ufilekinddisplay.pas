unit uFileKindDisplay;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, Types,
  Controls, Graphics, Themes,
  uTypesConstsUtils;

type

  TFileKindRadio = class(TGraphicControl)
  private
    FAdjust: Integer;  // required to prevent descenders being clipped
    FFileKind: EnDataFileKind;
    FSingleRadioState: EnRadioState;
    FSingleText: String;
    FMergedRadioState: EnRadioState;
    FMergedText: String;
    FParticipantStr: String;
    FRBSize: TSize;
    procedure SetFileKind(aValue: EnDataFileKind);
  protected
    procedure Paint; override;
    procedure SetParent(NewParent: TWinControl); override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure SetKind(aKind: EnDataFileKind; const aFilename, aParticipantStr: String);
    property FileKind: EnDataFileKind read FFileKind write SetFileKind default dfkUnknown;
  end;

implementation

uses
  SysUtils,
  LCLType;

procedure TFileKindRadio.SetFileKind(aValue: EnDataFileKind);
begin
  if FFileKind <> aValue then
    begin
      FFileKind := aValue;
      FSingleRadioState := rsDisabled;
      FMergedRadioState := rsDisabled;
      FSingleText := rsSingleText;
      FMergedText := rsMergedText;
    end;
end;

procedure TFileKindRadio.Paint;
var
  r, rDetail: TRect;
  textFlags: Cardinal = DT_LEFT or DT_TOP;
  singleDetails, mergedDetails: TThemedElementDetails;
begin
  r := ClientRect;
  canvas.Brush.Color := clForm;
  Canvas.FillRect(r);


  case FSingleRadioState of
    rsDisabled: singleDetails := ThemeServices.GetElementDetails(tbRadioButtonUncheckedDisabled);
    rsEnabled:  singleDetails := ThemeServices.GetElementDetails(tbRadioButtonCheckedNormal);
  end;
  case FMergedRadioState of
    rsDisabled: mergedDetails := ThemeServices.GetElementDetails(tbRadioButtonUncheckedDisabled);
    rsEnabled:  mergedDetails := ThemeServices.GetElementDetails(tbRadioButtonCheckedNormal);
  end;

  rDetail := r;
  rDetail.Width :=  FRBSize.cx;
  rDetail.Height := FRBSize.cy + FAdjust;
  ThemeServices.DrawElement(Canvas.Handle, singleDetails, rDetail);
  rDetail.Right := r.Left + FRBSize.cx + HSpacing + Canvas.TextWidth(FSingleText);
  rDetail.Left :=  r.Left + FRBSize.cx + HSpacing ;
  ThemeServices.DrawText(Canvas, singleDetails, FSingleText, rDetail, textFlags, 0);

  rDetail := r;
  rDetail.Top := rDetail.Top + FRBSize.cy + VSpacing;
  rDetail.Width :=  FRBSize.cx;
  rDetail.Height := FRBSize.cy + FAdjust;
  ThemeServices.DrawElement(Canvas.Handle, mergedDetails, rDetail);
  rDetail.Right := r.Left + FRBSize.cx + HSpacing + Canvas.TextWidth(FMergedText);
  rDetail.Left :=  r.Left + FRBSize.cx + HSpacing ;
  ThemeServices.DrawText(Canvas, mergedDetails, FMergedText, rDetail, textFlags, 0);

  inherited Paint;
end;

procedure TFileKindRadio.SetParent(NewParent: TWinControl);
var
  rbDetail: TThemedElementDetails;
begin
  inherited SetParent(NewParent);
  if Assigned(NewParent) then
    begin
      rbDetail := ThemeServices.GetElementDetails(tbRadioButtonCheckedNormal);
      FRBSize := ThemeServices.GetDetailSize(rbDetail);
      SetInitialBounds(0, 0, FRBSize.cx + HSpacing + Canvas.GetTextWidth(FMergedText), 2*FRBSize.cy + VSpacing + FAdjust);
    end;
end;

constructor TFileKindRadio.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFileKind := dfkUnknown;
  FParticipantStr := '';
  FAdjust := 3;
  FSingleText := rsSingleText;
  FMergedText := rsMergedText;
  FSingleRadioState := rsDisabled;
  FMergedRadioState := rsDisabled;
  ParentFont := True;
end;

procedure TFileKindRadio.SetKind(aKind: EnDataFileKind; const aFilename, aParticipantStr: String);
begin
  if FFileKind <> aKind then
      begin
        FFileKind := aKind;
        case FFileKind of
          dfkUnknown: begin
                        FSingleRadioState := rsDisabled;
                        FMergedRadioState := rsDisabled;
                      end;
          dfkSingleRaw, dfkSingleProcessed: begin
                       FSingleRadioState := rsEnabled;
                       FMergedRadioState := rsDisabled;
                     end;
          dfkMerged: begin
                       FSingleRadioState := rsDisabled;
                       FMergedRadioState := rsEnabled;
                     end;
        end;
        FParticipantStr := aParticipantStr;
        case FFileKind of
          dfkUnknown: ;
          dfkSingleRaw, dfkSingleProcessed: begin
              FSingleText := Format('"%s" has data from a single participant: %s',[aFilename, FParticipantStr]);
              SetBounds(Top, Left, FRBSize.cx + HSpacing + Canvas.GetTextWidth(FSingleText), 2*FRBSize.cy + VSpacing + FAdjust);
            end;
          dfkMerged: begin
              FMergedText := Format('"%s" has merged data from %s participants',[aFilename, FParticipantStr]);
              SetBounds(Top, Left, FRBSize.cx + HSpacing + Canvas.GetTextWidth(FMergedText), 2*FRBSize.cy + VSpacing + FAdjust);
            end;
        end;
      end;
end;

end.


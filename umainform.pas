unit uMainForm;

{$mode objfpc}{$H+}
{$WARN 5024 off : Parameter "$1" not used}
interface

uses
  Classes, SysUtils, Types,
  Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls, StdCtrls,
  uTypesConstsUtils, uFileKindDisplay, uProgressWindow, uEngine;

type

  TMainForm = class(TForm)
    AboutButton: TButton;
    SettingsButton: TButton;
    ViewHeaderPanel: TPanel;
    ViewParticipantListBox: TListBox;
    ViewAddParticipantZScoresCheckBox: TCheckBox;
    BottomPanel: TPanel;
    CharacteristicsGB: TGroupBox;
    ViewChooseStatisticRadioGroup: TRadioGroup;
    CloseCurrentButton: TButton;
    ViewChooseParticipantGroupBox: TGroupBox;
    ExperimentEdit: TEdit;
    ExperimentNameGB: TGroupBox;
    FilenameEdit: TEdit;
    FilenameGB: TGroupBox;
    NormFilenameEdit: TEdit;
    NormGB: TGroupBox;
    OutcomeGB: TGroupBox;
    OutcomesMemo: TMemo;
    OutputGB: TGroupBox;
    ParticipantNumberGB: TGroupBox;
    OpenDialog: TOpenDialog;
    InitialFileSelectSheet: TTabSheet;
    SaveStatsEdit: TEdit;
    SaveStatsGB: TGroupBox;
    SelectFileButton: TButton;
    SelectFileGB: TGroupBox;
    SelectionPanel: TPanel;
    ShowFullPathCB: TCheckBox;
    ViewBottomPanel: TPanel;
    CloseButton: TButton;
    ViewDisplaySummaryReportButton: TButton;
    PageControl: TPageControl;
    ViewSpecifyDisplayOptionsGroupBox: TGroupBox;
    ViewProcessedDataTabSheet: TTabSheet;
    ViewStatisticsMemo: TMemo;
    procedure AboutButtonClick(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure CloseCurrentButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PageControlChanging(Sender: TObject; var AllowChange: Boolean);
    procedure SelectDataFileButtonClick(Sender: TObject);
    procedure SettingsButtonClick(Sender: TObject);
    procedure ShowFullPathCBChange(Sender: TObject);
    procedure ViewChooseStatisticRadioGroupSelectionChanged(Sender: TObject);
    procedure ViewDisplaySummaryReportButtonClick(Sender: TObject);
    procedure ViewParticipantListBoxSelectionChange(Sender: TObject; User: boolean);
  private
    FileKindRadio: TFileKindRadio;
    OutputFileExists: Boolean;
    SelectedParticipant: TParticipant;
    AddZScore: RAddZScore;
    function GetStatistics(aRGIndex: Integer): TIntegerDynArray;
    procedure DoInitialLayout;
    procedure DoProgress(const aPercent: Single);
    procedure GUIRespondToFileParsedOK;
    procedure Report(const aText: String; const aParams: array of const);
    procedure ReportSuccess;
    procedure InitialiseDisplayEdits;
    procedure UncheckAddZScores(Data: PtrInt);
  public
    ProgressWindow: TProgressWindow;
  end;

var
  MainForm: TMainForm;

implementation

uses
  LCLType,
  uAboutBox, uCheckListboxDlg, uAddZScoresDlg, uSummaryDlgZ, u2ColSummaryDlg,
  uPostProcessingDlg, uSettingsEdit;

{$R *.lfm}

procedure TMainForm.FormCreate(Sender: TObject);
{var
  i, l, len: Integer;
  nam: String; }
begin
  ProgressWindow := TProgressWindow.Create(Nil);
  FileKindRadio := TFileKindRadio.Create(Self);
  FileKindRadio.Parent := ParticipantNumberGB;
  DoInitialLayout;
  SelectedParticipant := Nil;

{  len := 0;
  nam := '';
  for i := Low(BehestsExperiment1) to High(BehestsExperiment1) do
    case BehestsExperiment1[i].rv.bk  of
      f: ;
      c: begin
           l := Length(BehestsExperiment1[i].rv.cp.sn);
           if l > len then
             begin
               len := l;
               nam := BehestsExperiment1[i].rv.cp.sn;
             end;
           WriteLn(BehestsExperiment1[i].rv.cp.id,' ',BehestsExperiment1[i].rv.cp.sn,' (',l,')');
         end;
      a: ;
    end;
  WriteLn('longest statistic name is ',nam,', length ',len);      }
  { MinDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame, length 63 }
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  ProgressWindow.Free;
end;

procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = []) and (Key = VK_RETURN) and SelectFileButton.Enabled then // set default for [Enter] key
    begin
      Key := 0;
      SelectFileButton.Enabled := False;
      SelectFileButton.Click;
    end
  else if (Shift = []) and (Key = VK_RETURN) and CloseCurrentButton.Enabled then // prevent Close being default button
    begin
      Key := 0;
      CloseCurrentButton.Enabled := False;
      CloseCurrentButton.Click;
    end
  else inherited;
end;

procedure TMainForm.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.CloseCurrentButtonClick(Sender: TObject);
begin
  PageControl.ActivePageIndex := 0;
  ViewProcessedDataTabSheet.Enabled := False;
  InitialiseDisplayEdits;
  FileKindRadio.FileKind := dfkUnknown;
  SelectedParticipant := Nil;
  processor.Free;
  processor := Nil;
  ViewParticipantListBox.Clear;
  ViewChooseStatisticRadioGroup.ItemIndex := -1;
  ViewHeaderPanel.Caption := '';
  ViewStatisticsMemo.Clear;
  ViewAddParticipantZScoresCheckBox.Checked := False;
  SelectDataFileButtonClick(Nil);
end;

procedure TMainForm.GUIRespondToFileParsedOK;
begin
  PageControl.Enabled := True;
  CloseCurrentButton.Enabled := True;

  CharacteristicsGB.Enabled := True;
  FilenameGB.Enabled := True;
  FilenameEdit.Enabled := True;
  ExperimentNameGB.Enabled := True;
  ExperimentEdit.Enabled := True;
  ParticipantNumberGB.Enabled := True;
  OutcomesMemo.Enabled := True;

  OutputGB.Enabled := True;
  SaveStatsGB.Enabled := True;
  SaveStatsEdit.Enabled := True;
  NormGB.Enabled := True;
  NormFilenameEdit.Enabled := True;
  OutcomeGB.Enabled := True;
  OutcomesMemo.Enabled := True;

  ViewProcessedDataTabSheet.Enabled := True;

  case ExperimentSupportsNormativeData(processor.ExperimentIdx) of
    True:  begin
             ExperimentEdit.Text := processor.ExperimentName + rsSupportsNormativeData;
             ViewAddParticipantZScoresCheckBox.Visible := True;
           end;
    False: begin
             ExperimentEdit.Text := processor.ExperimentName + rsNoSupportForNormative;
             ViewAddParticipantZScoresCheckBox.Visible := False;
           end;
  end;

  case ShowFullPathCB.Checked of
    True: begin
            FilenameEdit.Text := OpenDialog.FileName;
            SaveStatsEdit.Text := processor.OutputFullFilename;
            NormFilenameEdit.Text := PExperiments[processor.ExperimentIdx]^.normCSVFilename;
          end;
    False: begin
             FilenameEdit.Text := ExtractFileName(OpenDialog.FileName);
             SaveStatsEdit.Text := ExtractFileName(processor.OutputFullFilename);
             NormFilenameEdit.Text := ExtractFileName(PExperiments[processor.ExperimentIdx]^.normCSVFilename);
           end;
  end;
  processor.PopulateListboxWithParticipants(ViewParticipantListBox);
end;

procedure TMainForm.AboutButtonClick(Sender: TObject);
begin
  ShowAboutBox;
end;

procedure TMainForm.PageControlChanging(Sender: TObject; var AllowChange: Boolean);
begin
  AllowChange := ViewProcessedDataTabSheet.Enabled;
end;

procedure TMainForm.SelectDataFileButtonClick(Sender: TObject);
var
  outcome: EnOutcome = _oSuccess;
  ds, s: String;
  rejects: TStringArray;
  i: Integer;
begin
  SelectFileButton.Enabled := False;
  OutcomeGB.Enabled := False;
  OutcomesMemo.Enabled := False;
  OutcomesMemo.Clear;

  if OpenDialog.Execute then
    begin
      processor := TFileProcessor.Create(OpenDialog.FileName, @DoProgress, outcome, ds, rejects, OutputFileExists);
      ProgressWindow.Hide;
      case processor.DataFileKind of
        dfkUnknown: s := '';
        dfkSingleRaw, dfkSingleProcessed: s := processor.Participants[0].participantID;
        dfkMerged: s := processor.ParticipantCount.ToString;
      end;
      FileKindRadio.SetKind(processor.DataFileKind, processor.DataFilename, s);
      if (outcome in [_oSuccess, _oDataFileContainsOnlyTrainingTrials]) then
        GUIRespondToFileParsedOK;

      OutcomeGB.Enabled := True;
      OutcomesMemo.Enabled := True;
      OutcomesMemo.Lines.Add('');
      case processor.DataFileKind of
        dfkUnknown: Report(rsOnlyInvalidFound, []);
        otherwise ;
      end;

      ViewAddParticipantZScoresCheckBox.Enabled := enableNormFeatures;
      NormGB.Enabled := enableNormFeatures;
      NormFilenameEdit.Enabled := enableNormFeatures;
      if not NormFilenameEdit.Enabled then
        NormFilenameEdit.Text := '';

      case outcome of
        _oSuccess: ReportSuccess;
        _oDataFileContainsOnlyTrainingTrials:
          begin
            s := '';
            for i := 0 to High(rejects) do
              s := s + rejects[i] + ', ';
            Delete(s, Length(s)-2, 2);
            case Length(rejects) of
              0: Assert(False, 'TFileProcessor internal parsing error');
              1: Report(rsAParticipantWasFound,[s]);
              otherwise
                Report(rsParticipantsWereFound,[s]);
            end;
            ReportSuccess;
          end;
        otherwise
          ReportOutcomeDlg(outcome, ds);
      end;
    end
  else
    begin
      SelectFileButton.Enabled := True;
    end;
end;

procedure TMainForm.SettingsButtonClick(Sender: TObject);
begin
  ShowSettingsEditDlg;
end;

procedure TMainForm.ShowFullPathCBChange(Sender: TObject);
begin
  if not Assigned(processor) then
    Exit;
  case ShowFullPathCB.Checked of
    True: begin
            FilenameEdit.Text := OpenDialog.FileName;
            SaveStatsEdit.Text := processor.OutputFullFilename;
            NormFilenameEdit.Text := PExperiments[processor.ExperimentIdx]^.normCSVFilename;
          end;
    False: begin
             FilenameEdit.Text := ExtractFileName(OpenDialog.FileName);
             SaveStatsEdit.Text := ExtractFileName(processor.OutputFullFilename);
             NormFilenameEdit.Text := ExtractFileName(PExperiments[processor.ExperimentIdx]^.normCSVFilename);
           end;
  end;
end;

procedure TMainForm.ViewChooseStatisticRadioGroupSelectionChanged(Sender: TObject);
var
  idx: Integer;
begin
  if not Assigned(SelectedParticipant) then Exit;
  idx := ViewChooseStatisticRadioGroup.ItemIndex;
  case idx of
    -1: Exit;
    otherwise
      processor.ReportStatistics(SelectedParticipant, GetStatistics(idx), ViewStatisticsMemo);
  end;
end;

procedure TMainForm.ViewDisplaySummaryReportButtonClick(Sender: TObject);
var
  righthanded, Female, sexSpec, ageSpec, sessionSpec, handedSpec: Boolean;
  lowAge, hiAge, sessn, matchCount: Integer;
begin
  if SelectedParticipant = Nil then Exit;
  AddZScore := Default(RAddZScore);
  case ViewAddParticipantZScoresCheckBox.Checked of
    True: case GotParticipantParametersDlgOK(SelectedParticipant, sexSpec, ageSpec, sessionSpec, handedSpec, Female, righthanded, lowAge, hiAge, sessn) of
            True: begin
                    AddZScore.Init(ageSpec, sexSpec, handedSpec, sessionSpec,
                                     righthanded, Female, lowAge, hiAge, sessn);
                    case PExperiments[processor.ExperimentIdx]^.NormList.FilteredOK(SelectedParticipant, AddZScore, matchCount) of
                      True: begin
                              ShowMessageFmt(rsNoteThatParticipants,
                                             [matchCount, SelectedParticipant.participantID]);
                              ShowZDlg(matchCount, SelectedParticipant, GetSummaryReportStatIDs, PExperiments[processor.ExperimentIdx]^.NormList);
                            end;
                      False: begin
                               ShowMessageFmt(rsNotEnoughParticipants,
                                              [matchCount, SelectedParticipant.participantID]);
                               Show2ColSummaryDlg(GetSummaryReportStatIDs, SelectedParticipant);
                               Application.QueueAsyncCall(@UncheckAddZScores, 0);
                             end;
                    end;
            end;
            False: begin
                     Show2ColSummaryDlg(GetSummaryReportStatIds, SelectedParticipant);
                     Application.QueueAsyncCall(@UncheckAddZScores, 0);
                   end;
          end;
    False: Show2ColSummaryDlg(GetSummaryReportStatIds, SelectedParticipant);
  end;
end;

procedure TMainForm.ViewParticipantListBoxSelectionChange(Sender: TObject; User: boolean);
var
  idx: Integer;
begin
  if not User then Exit;
  idx := ViewParticipantListBox.ItemIndex;
  case idx of
    -1: begin
          SelectedParticipant := Nil;
          ViewChooseStatisticRadioGroup.Enabled := False;
          ViewBottomPanel.Enabled := False;
          ViewHeaderPanel.Caption := '';
          ViewAddParticipantZScoresCheckBox.Enabled := False;
           ViewDisplaySummaryReportButton.Enabled := False;
    end;
    otherwise
      SelectedParticipant := processor.Participants[PtrInt(ViewParticipantListBox.Items.Objects[idx])];
      ViewChooseStatisticRadioGroup.Enabled := True;
      ViewBottomPanel.Enabled := True;
      ViewHeaderPanel.Caption := SelectedParticipant.HeaderInfoText;
      if enableNormFeatures then
        ViewAddParticipantZScoresCheckBox.Enabled := True;
      ViewDisplaySummaryReportButton.Enabled := True;
  end;
end;

function TMainForm.GetStatistics(aRGIndex: Integer): TIntegerDynArray;

  function GetRange(aStart, anEnd: Integer): TIntegerDynArray;
  var
    i: Integer;
  begin
    SetLength(Result{%H-}, Succ(anEnd - aStart));
    for i := aStart to anEnd do
      Result[i - aStart] := i;
  end;

  function GetRange(intValues: array of const): TIntegerDynArray;
  var
    i, lw: Integer;
  begin
    SetLength(Result{%H-}, Length(intValues));
    lw := Low(intValues);
    for i := lw to High(intValues) do
      Result[i - lw] := TVarRec(intValues[i]).VInteger;
  end;

begin
  SetLength(Result{%H-}, 0);
  case aRGIndex of
    0: Result := GetStatNamesDlg(rsChooseStatisticsTo, False);
    1: Result := GetRange(0, 15);  // ntrials acc
    2: Result := GetRange(17, 32); // mean acc
    3: Result := GetRange(34, 49); // meanerror
    4: Result := GetRange(51, 66); // arcsin mean
    5: Result := GetRange(132, 147); // CowanK
    6: Result := GetRange(149, 164); // A
    7: Result := GetRange(166, 181); // D-Prime
    8: Result := GetRange(183, 198); // ntrials corr RT
    9: Result := GetRange(200, 215); // mean corr RT
    10:Result := GetRange(217, 232); // sqrt before mean
    11: Result := GetRange(234, 249); // sqrt mean
    12: Result := GetRange(251, 266); // ln before mean
    13: Result := GetRange(268, 283); // ln mean
    14: Result := GetRange([16, 33, 50, 67, 148, 165, 182, 199, 216, 233, 250, 267, 284]); // summary
    15: Result := GetRange([285,286,287,288,297,298,299,300,309,310,311,312,321,322,323,324,333,334,335,336,345,346,347,348,357,358,359,360,369,370,371,372,381,382,383,384,393,394,395,396,405,406,407,408]); // distr object
    16: Result := GetRange([289,290,291,292,301,302,303,304,313,314,315,316,325,326,327,328,337,338,339,340,349,350,351,352,361,362,363,364,373,374,375,376,385,386,387,388,397,398,399,400,409,410,411,412]); // distr spatial
    17: Result := GetRange([293,294,295,296,305,306,307,308,317,318,319,320,329,330,331,332,341,342,343,344,353,354,355,356,365,366,367,368,377,378,379,380,389,390,391,392,401,402,403,404,413,414,415,416]); // distr spatial & object
    18: Result := GetRange([417,418,419,426,427,428,435,436,437,444,445,446,453,454,455,462,463,464,471,472,473,480,481,482,489,490,491,498,499,500,507,508,509]); // min
    19: Result := GetRange([420,421,422,429,430,431,438,439,440,447,448,449,456,457,458,465,466,467,474,475,476,483,484,485,492,493,494,501,502,503,510,511,512]); // max
    20: Result := GetRange([423,424,425,432,433,434,441,442,443,450,451,452,459,460,461,468,469,470,477,478,479,486,487,488,495,496,497,504,505,506,513,514,515]);  // sum
    21: Result := GetRange(0, HighStatID); // show all
  end;
end;

procedure TMainForm.DoInitialLayout;
begin
  InitialiseDisplayEdits;
  SelectionPanel.BorderSpacing.Left := Margin;
  SelectionPanel.BorderSpacing.Right := Margin;
  SelectFileGB.BorderSpacing.Around := Margin;
  SelectFileButton.BorderSpacing.Left := Margin;
  ShowFullPathCB.BorderSpacing.Left := HalfMargin;
  CloseCurrentButton.BorderSpacing.Around := Margin;
  SettingsButton.BorderSpacing.Left := Margin;

  CharacteristicsGB.BorderSpacing.Around := Margin;
  FilenameGB.BorderSpacing.Around := Margin;
  FilenameEdit.BorderSpacing.Around := Margin;
  ExperimentNameGB.BorderSpacing.Around := Margin;
  ExperimentEdit.BorderSpacing.Around := Margin;
  ParticipantNumberGB.BorderSpacing.Around := Margin;
  FileKindRadio.BorderSpacing.Around := Margin;

  OutputGB.BorderSpacing.Around := Margin;
  SaveStatsGB.BorderSpacing.Around := Margin;
  SaveStatsEdit.BorderSpacing.Around := Margin;
  NormGB.BorderSpacing.Around := Margin;
  NormFilenameEdit.BorderSpacing.Around := Margin;

  OutcomeGB.BorderSpacing.Around := Margin;
  OutcomesMemo.BorderSpacing.Around := Margin;

  AboutButton.BorderSpacing.Around := Margin;
  AboutButton.BorderSpacing.Left := Margin;
  CloseButton.BorderSpacing.Around := Margin;
  CloseButton.BorderSpacing.Right := Margin;

  ViewSpecifyDisplayOptionsGroupBox.BorderSpacing.Around := Margin;
  ViewChooseParticipantGroupBox.BorderSpacing.Around := Margin;
  ViewParticipantListBox.BorderSpacing.Around := Margin;
  ViewChooseStatisticRadioGroup.BorderSpacing.Around := Margin;
  ViewStatisticsMemo.BorderSpacing.Left := Margin;
  ViewStatisticsMemo.BorderSpacing.Right := Margin;
  ViewStatisticsMemo.BorderSpacing.Top := Margin;
  ViewBottomPanel.BorderSpacing.Left := Margin;
  ViewBottomPanel.BorderSpacing.Right := Margin;
  ViewDisplaySummaryReportButton.BorderSpacing.Right := HalfMargin;
end;

procedure TMainForm.DoProgress(const aPercent: Single);
begin
  if not ProgressWindow.Visible then
    ProgressWindow.Visible := True;
  ProgressWindow.Progress := Succ(Round(aPercent));
end;

procedure TMainForm.Report(const aText: String; const aParams: array of const);
begin
  OutcomesMemo.Lines.Add(aText, aParams);
end;

procedure TMainForm.ReportSuccess;
var
  nonNorms: TParticipantDynArray = Nil;
  namesToAdd: TStringArray = Nil;
  i: Integer;
begin
  if OutcomesMemo.Text = '' then
    OutcomesMemo.Lines.Add('');
  Report(rsDataFileSParsed, [processor.DataFilename]);
  Report(rsStatisticsGenerated, []);

  if OutputFileExists and settings.WarnIfOverwritingOutput then
    Report(rsAnExistingFile, [oFiler.fName]);

  case processor.ParticipantCount of
    1: Report(rsExcludingTraining, [processor.Participants[0].participantID,
      processor.Participants[0].UnfilteredLineCountXclTraining]);
    otherwise
       Report(rsExcludingTrainingEach, [processor.Participants[0].UnfilteredLineCountXclTraining]);
  end;

  if enableNormFeatures and processor.NonNormParticipantsArePresent(nonNorms) then
    begin
      SetLength(namesToAdd, Length(nonNorms));
      for i := 0 to High(nonNorms) do
        namesToAdd[i] := nonNorms[i].participantID;
      if AddToNormativeDataDlg(namesToAdd) then
      case processor.AddedParticipantsToNormativeOK(nonNorms) of
        True: ShowMessageFmt(rsAddedDataToNormative, [StringArrayToListing(namesToAdd), Length(PExperiments[processor.ExperimentIdx]^.normList.NormRecArray)]);
        False: ShowMessage(rsAttemptToAddParticipant);
      end;
    end;

  Report(rsYouCanViewGenerated, []);
end;

procedure TMainForm.InitialiseDisplayEdits;
begin
  FilenameEdit.Text := rsNoFileSelected;
  ExperimentEdit.Text := rsNoFileSelected;
  SaveStatsEdit.Text := rsNoFileSelected;
  NormFilenameEdit.Text := rsNoFileSelected;

  OutcomesMemo.Text := #10 + rsWaitingForData;
  ShowFullPathCB.Checked := settings.ShowFullPathInFilename;
end;

procedure TMainForm.UncheckAddZScores(Data: PtrInt);
begin
  ViewAddParticipantZScoresCheckBox.Checked := Boolean(Data);
end;

end.


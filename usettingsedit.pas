unit uSettingsEdit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  Forms, StdCtrls, grids, Controls, ExtCtrls;

type
  TSettingsDialog = class(TForm)
  private
    buttonPanel: TPanel;
    closeButton: TButton;
    grid: TStringGrid;
    settingsGB: TGroupBox;

    procedure DlgCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure gridSelectEditor(Sender: TObject; aCol, aRow: Integer;
      var Editor: TWinControl);
    procedure gridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
  public
    constructor Create(TheOwner: TComponent); override;
  end;

  procedure ShowSettingsEditDlg;

implementation

uses
  Dialogs, Graphics,
  uEngine, uTypesConstsUtils, uMainForm;

procedure ShowSettingsEditDlg;
var
  dlg: TSettingsDialog;
begin
  dlg := TSettingsDialog.Create(Nil);
  try
    dlg.ShowModal;
  finally
    dlg.Free;
  end;
end;

{ TSettingsDialog }

constructor TSettingsDialog.Create(TheOwner: TComponent);
begin
  inherited CreateNew(TheOwner);
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  Caption := rsADPSettings;
  SetInitialBounds(0, 0, 310, 193);
  OnCloseQuery  := @DlgCloseQuery;

  settingsGB := TGroupBox.Create(Self);
  buttonPanel := TPanel.Create(Self);
  closeButton := TButton.Create(Self);
  grid := TStringGrid.Create(Self);

  with buttonPanel do begin
    Align := alBottom;
    BevelOuter := bvNone;
    Caption := '';
    Parent := Self;
  end;

  with closeButton do begin
    Caption := rsClose;
    ModalResult := mrClose;
    BorderSpacing.Around := Margin;
    Anchors := [akTop, akRight];
    AnchorSideTop.Control := buttonPanel;
    AnchorSideTop.Side := asrCenter;
    AnchorSideRight.Control := buttonPanel;
    AnchorSideRight.Side := asrRight;
    Parent := buttonPanel;
  end;

  with settingsGB do begin
    Align := alClient;
    BorderSpacing.Left := Margin;
    BorderSpacing.Top := Margin;
    BorderSpacing.Right := Margin;
    Caption := rsEditCurrentSettings;
    AutoSize := True;
    Parent := Self;
  end;

  with grid do begin
    Align := alClient;
    ColCount := 2;
    RowCount := 4;
    DefaultRowHeight := 28;
    FixedCols := 0;
    Options := [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goAlwaysShowEditor, goSmoothScroll];
    ScrollBars := ssNone;
    TabOrder := 0;
    TitleFont.Style := [fsBold];
    OnSelectEditor := @gridSelectEditor;
    OnSetEditText := @gridSetEditText;
    Cells[SettingsCol, 0] := rsSetting;
    Cells[ValuesCol, 0] := rsCurrentValue;
    Cells[SettingsCol, DelimiterRow] := rsOutputFileDelimiter;
    Cells[SettingsCol, WarnOverwriteRow] := rsWarnIfOverwriting;
    Cells[SettingsCol, ShowFullFilenameRow] := rsShowFullPath;
    if Assigned(settings) then
      begin
        case settings.OutputFileDelimiter of
          #9:  Cells[ValuesCol, DelimiterRow] := rsTab;
          ',': Cells[ValuesCol, DelimiterRow] := rsComma;
        end;
        case settings.WarnIfOverwritingOutput of
          True:  Cells[ValuesCol, WarnOverwriteRow] := rsTrue;
          False: Cells[ValuesCol, WarnOverwriteRow] := rsFalse;
        end;
        case settings.ShowFullPathInFilename of
          True:  Cells[ValuesCol, ShowFullFilenameRow] := rsTrue;
          False: Cells[ValuesCol, ShowFullFilenameRow] := rsFalse;
        end;
        AutoAdjustColumns;
      end;

    Parent := settingsGB;
  end;
end;

procedure TSettingsDialog.DlgCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  DelimSettingsChanged: Boolean = False;
  replaced: Boolean;
begin
  case grid.Cells[ValuesCol, WarnOverwriteRow] of
    'True': if not settings.WarnIfOverwritingOutput then
              settings.WarnIfOverwritingOutput := True;
    'False': if settings.WarnIfOverwritingOutput then
              settings.WarnIfOverwritingOutput := False;
  end;
  case grid.Cells[ValuesCol, ShowFullFilenameRow] of
    'True': if not settings.ShowFullPathInFilename then
              settings.ShowFullPathInFilename := True;
    'False': if settings.ShowFullPathInFilename then
              settings.ShowFullPathInFilename := False;
  end;
  MainForm.ShowFullPathCB.Checked := settings.ShowFullPathInFilename;
  case grid.Cells[ValuesCol, DelimiterRow] of
    'Tab': if settings.OutputFileDelimiter = ',' then
              begin
                DelimSettingsChanged := True;
                settings.OutputFileDelimiter := #9;
              end;
    'Comma': if settings.OutputFileDelimiter = #9 then
              begin
                DelimSettingsChanged := True;
                settings.OutputFileDelimiter := ',';
              end;
  end;
  if DelimSettingsChanged then
    begin
      if (settings.OutputFileDelimiter = ',') and Assigned(processor) then
        case ReplacedTabWithCommaOK(processor.OutputFullFilename, replaced) of
          True:  if replaced then
                   ShowMessage(rsSavedOutputFileWith);
          False: ShowMessage(rsFailedToSaveOutput);
        end;

      if enableNormFeatures then
        currentNormList.SavedToCSVFileOK;
    end;
  CanClose := True;
end;

procedure TSettingsDialog.gridSelectEditor(Sender: TObject; aCol, aRow: Integer; var Editor: TWinControl);
var
  plce: TPickListCellEditor absolute Editor;
begin
  if (aRow in [DelimiterRow, WarnOverwriteRow, ShowFullFilenameRow]) and (aCol = ValuesCol) then
    begin
      Editor := grid.EditorByStyle(cbsPickList);
      plce.Style := csDropDownList;
      plce.Clear;
      case aRow of
        DelimiterRow: begin
                        plce.Items.Add(rsTab);
                        plce.Items.Add(rsComma);
                      end;
        WarnOverwriteRow: begin
                            plce.Items.Add(rsTrue);
                            plce.Items.Add(rsFalse);
                          end;
        ShowFullFilenameRow: begin
                               plce.Items.Add(rsTrue);
                               plce.Items.Add(rsFalse);
                             end;
      end;
    end
  else Editor := Nil;
end;

procedure TSettingsDialog.gridSetEditText(Sender: TObject; ACol, ARow: Integer; const Value: string);
begin
  if (ARow = 0) or (ACol <> ValuesCol) then
    Exit;
  case ARow of
    DelimiterRow: case Value of
                    'Tab':   grid.Cells[ValuesCol, DelimiterRow] := rsTab;
                    'Comma': grid.Cells[ValuesCol, DelimiterRow] := rsComma;
                  end;
    WarnOverwriteRow: case Value of
                        'True':  grid.Cells[ValuesCol, WarnOverwriteRow] := rsTrue;
                        'False': grid.Cells[ValuesCol, WarnOverwriteRow] := rsFalse;
                      end;
    ShowFullFilenameRow: case Value of
                           'True':  grid.Cells[ValuesCol, ShowFullFilenameRow] := rsTrue;
                           'False': grid.Cells[ValuesCol, ShowFullFilenameRow] := rsFalse;
                         end;
    otherwise ;
  end;
end;

end.


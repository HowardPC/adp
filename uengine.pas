unit uEngine;

{$mode ObjFPC}{$H+}
{$ModeSwitch advancedrecords}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}
interface

uses
  Classes, SysUtils, Contnrs, Types,
  StdCtrls,
  uTypesConstsUtils;

type

  EnWMAField =
   (fdError = -1,
   fdExperiment, fdDate, fdStart_Time, fdTrial_Order_File_No, fdParticipantID, fdAge, fdSex, fdHandedness,
   fdDisplay_Type, fdObserver_number, fdSession_no,
   s1_marker, s2_marker, s3_marker, s4_marker, s1_shape, s1_quad, s1_duration, s1_s2_isi,
   s2_shape_position_1, s2_shape_position_2, s2_shape_position_3, s2_shape_position_4,
   s2_shape_position_5, s2_duration, s2_s3_isi,
   s3_shape, s3_distractor_shape, s3_quad, s3_duration, s3_s4_isi,
   s4_shape, s4_distractor_shape, s4_quad, s4_duration, Response_Time_after_S4,
   Feedback_shape, Feedback_duration_after_response_time, ITI_after_feedback,
   s1_colour, s2_colour_position_1, s2_colour_position_2, s2_colour_position_3,
   s2_colour_position_4, s2_colour_position_5,
   s3_colour, s3_distractor_colour, s4_colour, S4_distractor_colour,
   keyMapping, taskType, TMS_s3_SOA, Experimental_Condition,
   fdResponse, fdAccuracy, fdRT_ms, fdRT_ms_minus_constant_error, s1_onsetTime_ms, s2_onsetTime_ms, s3_onsetTime_ms,
   TMS_onsetTime_ms, s4_onsetTime_ms, response_onsetTime_ms, feedback_onsetTime_ms, blank_onsetTime_ms,
   fdUser_paused_the_trial,
   fdFactor_1, fdFactor_2, fdFactor_3, fdFactor_4, fdFactor_5, fdFactor_6, fdFactor_7, fdFactor_8, fdFactor_9);

  RgFactors = fdFactor_1..fdFactor_5;

  EnLevels = (l1, l2, l3, l4);

  SaFactorsLevels = array[RgFactors] of EnLevels;

  SaFFldStrings = array[RgFactors] of AnsiString;

  SaLvlFFStrings = array[EnLevels] of SaFFldStrings;

  EnFilterKind = (fkFactor1, fkAccuracy, fkUserPausedTrial, fkRTAbsolute, fkRTRelMultiplier);

  RBehestFilter = record
    fld: EnWMAField;
    fv: String10;         // field value
    id: Integer;
    kind: EnFilterKind;
    minO, maxO: Integer;  // O = outlier
    mult: Double;
    function GetName: AnsiString;
  end;

  EnCalcSource = (cs2s, cs3f, cs4f, csDiff, csDDiff, csMMS, csSummMean, csSummCount);

  EnCalcKind = (ckNA, ckA, ckArc, ckCnt, ckCow, ckDP, ckErrR, ckLn, ckLnB, ckMean, ckSqrt, ckSqrtB, ckMin, ckMax, ckSum);

  RCompute = record // compressed names because of huge const array of RBehest that follows
    cs:  EnCalcSource;
    ck:  EnCalcKind;
    id:  Integer;
    fld: EnWMAField;
    fla: SaFactorsLevels;
    sn:  String65; // statistic name
    isf: Boolean; // is statistic a floating point value?
    cow: Integer;
    sn1: Word;    // name ids of up to 4 statistics this statistic derives from
    sn2: Word;
    sn3: Word;
    sn4: Word;
  end;

  EnBehestKind = (f, c, a);   // Filter, Compute, Add field

  RVBeh = record
  public
    case bk: EnBehestKind of
      f: (fl: RBehestFilter);
      c: (cp: RCompute);
      a: (af: EnWMAField);
  end;

  TParticipant = class;

  RBehest  = record
  public
    rv: RVBeh;
    function ExecutedOnParticipantOK(aParticipant: TParticipant): EnOutcome;
    property Cmp: RCompute read rv.cp;
    property Flt: RBehestFilter read rv.fl;
    property Add: EnWMAField read rv.af;
  end;

  SaRBehest = array[1..HighBehestIndex] of RBehest;

const

  BehestsExperiment1: SaRBehest = (
  (rv: (bk: f; fl: (fld: fdFactor_1;                   fv: 'Training'; id: 0; kind: fkFactor1;         minO: 0;   maxO: 0;    mult: 0))),
  (rv: (bk: f; fl: (fld: fdUser_paused_the_trial;      fv: '1';        id: 1; kind: fkUserPausedTrial; minO: 0;   maxO: 0;    mult: 0))),
  (rv: (bk: f; fl: (fld: fdAccuracy;                   fv: '-1';       id: 2; kind: fkAccuracy;        minO: 0;   maxO: 0;    mult: 0))),
  (rv: (bk: f; fl: (fld: fdRT_ms_minus_constant_error; fv: '';         id: 3; kind: fkRTAbsolute;      minO: 200; maxO: 1800; mult: 0))),
  (rv: (bk: f; fl: (fld: fdRT_ms_minus_constant_error; fv: '';         id: 4; kind: fkRTRelMultiplier; minO: 0;   maxO: 0;    mult: 1.5))),

  (rv: (bk: a; af: (fdParticipantID))),
  (rv: (bk: a; af: (fdAge))),
  (rv: (bk: a; af: (fdSex))),
  (rv: (bk: a; af: (fdHandedness))),
  (rv: (bk: a; af: (fdExperiment))),
  (rv: (bk: a; af: (fdDate))),
  (rv: (bk: a; af: (fdStart_Time))),
  (rv: (bk: a; af: (fdTrial_Order_File_No))),
  (rv: (bk: a; af: (fdSession_no))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  0; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad1CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  1; fld: fdAccuracy; fla: (l1, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad1CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  2; fld: fdAccuracy; fla: (l1, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad1CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  3; fld: fdAccuracy; fla: (l1, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad1CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  4; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad2CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  5; fld: fdAccuracy; fla: (l2, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad2CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  6; fld: fdAccuracy; fla: (l2, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad2CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  7; fld: fdAccuracy; fla: (l2, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad2CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  8; fld: fdAccuracy; fla: (l3, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad3CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id:  9; fld: fdAccuracy; fla: (l3, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad3CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 10; fld: fdAccuracy; fla: (l3, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad3CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 11; fld: fdAccuracy; fla: (l3, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad3CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 12; fld: fdAccuracy; fla: (l4, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad4CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 13; fld: fdAccuracy; fla: (l4, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad4CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 14; fld: fdAccuracy; fla: (l4, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad4CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 15; fld: fdAccuracy; fla: (l4, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad4CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummCount; ck: ckCnt; id: 16; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'NTrialsAccuracySummaryCount';   isF: False; cow: 0; sn1: 0; sn2: 15; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 17; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'MeanAccuracyLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 18; fld: fdAccuracy; fla: (l1, l2, l1, l1, l3); sn: 'MeanAccuracyLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 19; fld: fdAccuracy; fla: (l1, l3, l1, l1, l2); sn: 'MeanAccuracyLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 20; fld: fdAccuracy; fla: (l1, l3, l1, l1, l3); sn: 'MeanAccuracyLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 21; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'MeanAccuracyLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 22; fld: fdAccuracy; fla: (l2, l2, l1, l1, l3); sn: 'MeanAccuracyLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 23; fld: fdAccuracy; fla: (l2, l3, l1, l1, l2); sn: 'MeanAccuracyLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 24; fld: fdAccuracy; fla: (l2, l3, l1, l1, l3); sn: 'MeanAccuracyLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 25; fld: fdAccuracy; fla: (l3, l2, l1, l1, l2); sn: 'MeanAccuracyLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 26; fld: fdAccuracy; fla: (l3, l2, l1, l1, l3); sn: 'MeanAccuracyLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 27; fld: fdAccuracy; fla: (l3, l3, l1, l1, l2); sn: 'MeanAccuracyLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 28; fld: fdAccuracy; fla: (l3, l3, l1, l1, l3); sn: 'MeanAccuracyLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 29; fld: fdAccuracy; fla: (l4, l2, l1, l1, l2); sn: 'MeanAccuracyLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 30; fld: fdAccuracy; fla: (l4, l2, l1, l1, l3); sn: 'MeanAccuracyLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 31; fld: fdAccuracy; fla: (l4, l3, l1, l1, l2); sn: 'MeanAccuracyLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 32; fld: fdAccuracy; fla: (l4, l3, l1, l1, l3); sn: 'MeanAccuracyLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckMean; id: 33; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'MeanAccuracySummaryMean';     isF: True; cow: 0; sn1: 17; sn2: 32; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 34; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'MeanErrorRateLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 35; fld: fdAccuracy; fla: (l1, l2, l1, l1, l3); sn: 'MeanErrorRateLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 36; fld: fdAccuracy; fla: (l1, l3, l1, l1, l2); sn: 'MeanErrorRateLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 37; fld: fdAccuracy; fla: (l1, l3, l1, l1, l3); sn: 'MeanErrorRateLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 38; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'MeanErrorRateLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 39; fld: fdAccuracy; fla: (l2, l2, l1, l1, l3); sn: 'MeanErrorRateLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 40; fld: fdAccuracy; fla: (l2, l3, l1, l1, l2); sn: 'MeanErrorRateLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 41; fld: fdAccuracy; fla: (l2, l3, l1, l1, l3); sn: 'MeanErrorRateLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 42; fld: fdAccuracy; fla: (l3, l2, l1, l1, l2); sn: 'MeanErrorRateLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 43; fld: fdAccuracy; fla: (l3, l2, l1, l1, l3); sn: 'MeanErrorRateLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 44; fld: fdAccuracy; fla: (l3, l3, l1, l1, l2); sn: 'MeanErrorRateLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 45; fld: fdAccuracy; fla: (l3, l3, l1, l1, l3); sn: 'MeanErrorRateLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 46; fld: fdAccuracy; fla: (l4, l2, l1, l1, l2); sn: 'MeanErrorRateLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 47; fld: fdAccuracy; fla: (l4, l2, l1, l1, l3); sn: 'MeanErrorRateLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 48; fld: fdAccuracy; fla: (l4, l3, l1, l1, l2); sn: 'MeanErrorRateLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckErrR; id: 49; fld: fdAccuracy; fla: (l4, l3, l1, l1, l3); sn: 'MeanErrorRateLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckErrR; id: 50; fld: fdAccuracy; fla: (l1, l1, l1, l1, l1); sn: 'MeanErrorRateAccuracySummaryMean'; isF: True; cow: 0; sn1: 34; sn2: 49; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 51; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 52; fld: fdAccuracy; fla: (l1, l2, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 53; fld: fdAccuracy; fla: (l1, l3, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 54; fld: fdAccuracy; fla: (l1, l3, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 55; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 56; fld: fdAccuracy; fla: (l2, l2, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 57; fld: fdAccuracy; fla: (l2, l3, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 58; fld: fdAccuracy; fla: (l2, l3, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 59; fld: fdAccuracy; fla: (l3, l2, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 60; fld: fdAccuracy; fla: (l3, l2, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 61; fld: fdAccuracy; fla: (l3, l3, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 62; fld: fdAccuracy; fla: (l3, l3, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 63; fld: fdAccuracy; fla: (l4, l2, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 64; fld: fdAccuracy; fla: (l4, l2, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 65; fld: fdAccuracy; fla: (l4, l3, l1, l1, l2); sn: 'ArcsinMeanAccuracyLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckArc; id: 66; fld: fdAccuracy; fla: (l4, l3, l1, l1, l3); sn: 'ArcsinMeanAccuracyLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckArc; id: 67; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ArcsinMeanAccuracySummaryMean';     isF: True; cow: 0; sn1: 51; sn2: 66; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 68; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad1CueDisDiffTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 69; fld: fdAccuracy; fla: (l1, l2, l1, l2, l2); sn: 'NTrialsAccuracyLoad1CueDisDiffTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 70; fld: fdAccuracy; fla: (l1, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad1CueDisDiffTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 71; fld: fdAccuracy; fla: (l1, l2, l1, l2, l3); sn: 'NTrialsAccuracyLoad1CueDisDiffTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 72; fld: fdAccuracy; fla: (l1, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad1CueDisSameTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 73; fld: fdAccuracy; fla: (l1, l3, l1, l2, l2); sn: 'NTrialsAccuracyLoad1CueDisSameTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 74; fld: fdAccuracy; fla: (l1, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad1CueDisSameTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 75; fld: fdAccuracy; fla: (l1, l3, l1, l2, l3); sn: 'NTrialsAccuracyLoad1CueDisSameTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 76; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad2CueDisDiffTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 77; fld: fdAccuracy; fla: (l2, l2, l1, l2, l2); sn: 'NTrialsAccuracyLoad2CueDisDiffTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 78; fld: fdAccuracy; fla: (l2, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad2CueDisDiffTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 79; fld: fdAccuracy; fla: (l2, l2, l1, l2, l3); sn: 'NTrialsAccuracyLoad2CueDisDiffTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 80; fld: fdAccuracy; fla: (l2, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad2CueDisSameTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 81; fld: fdAccuracy; fla: (l2, l3, l1, l2, l2); sn: 'NTrialsAccuracyLoad2CueDisSameTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 82; fld: fdAccuracy; fla: (l2, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad2CueDisSameTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 83; fld: fdAccuracy; fla: (l2, l3, l1, l2, l3); sn: 'NTrialsAccuracyLoad2CueDisSameTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 84; fld: fdAccuracy; fla: (l3, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad3CueDisDiffTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 85; fld: fdAccuracy; fla: (l3, l2, l1, l2, l2); sn: 'NTrialsAccuracyLoad3CueDisDiffTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 86; fld: fdAccuracy; fla: (l3, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad3CueDisDiffTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 87; fld: fdAccuracy; fla: (l3, l2, l1, l2, l3); sn: 'NTrialsAccuracyLoad3CueDisDiffTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 88; fld: fdAccuracy; fla: (l3, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad3CueDisSameTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 89; fld: fdAccuracy; fla: (l3, l3, l1, l2, l2); sn: 'NTrialsAccuracyLoad3CueDisSameTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 90; fld: fdAccuracy; fla: (l3, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad3CueDisSameTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 91; fld: fdAccuracy; fla: (l3, l3, l1, l2, l3); sn: 'NTrialsAccuracyLoad3CueDisSameTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 92; fld: fdAccuracy; fla: (l4, l2, l1, l1, l2); sn: 'NTrialsAccuracyLoad4CueDisDiffTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 93; fld: fdAccuracy; fla: (l4, l2, l1, l2, l2); sn: 'NTrialsAccuracyLoad4CueDisDiffTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 94; fld: fdAccuracy; fla: (l4, l2, l1, l1, l3); sn: 'NTrialsAccuracyLoad4CueDisDiffTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 95; fld: fdAccuracy; fla: (l4, l2, l1, l2, l3); sn: 'NTrialsAccuracyLoad4CueDisDiffTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 96; fld: fdAccuracy; fla: (l4, l3, l1, l1, l2); sn: 'NTrialsAccuracyLoad4CueDisSameTarDHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 97; fld: fdAccuracy; fla: (l4, l3, l1, l2, l2); sn: 'NTrialsAccuracyLoad4CueDisSameTarDHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 98; fld: fdAccuracy; fla: (l4, l3, l1, l1, l3); sn: 'NTrialsAccuracyLoad4CueDisSameTarSHCueTarDiff';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckCnt; id: 99; fld: fdAccuracy; fla: (l4, l3, l1, l2, l3); sn: 'NTrialsAccuracyLoad4CueDisSameTarSHCueTarSame';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 100; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'MeanAccuracyLoad1CueDisDiffTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 101; fld: fdAccuracy; fla: (l1, l2, l1, l2, l2); sn: 'MeanAccuracyLoad1CueDisDiffTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 102; fld: fdAccuracy; fla: (l1, l2, l1, l1, l3); sn: 'MeanAccuracyLoad1CueDisDiffTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 103; fld: fdAccuracy; fla: (l1, l2, l1, l2, l3); sn: 'MeanAccuracyLoad1CueDisDiffTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 104; fld: fdAccuracy; fla: (l1, l3, l1, l1, l2); sn: 'MeanAccuracyLoad1CueDisSameTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 105; fld: fdAccuracy; fla: (l1, l3, l1, l2, l2); sn: 'MeanAccuracyLoad1CueDisSameTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 106; fld: fdAccuracy; fla: (l1, l3, l1, l1, l3); sn: 'MeanAccuracyLoad1CueDisSameTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 107; fld: fdAccuracy; fla: (l1, l3, l1, l2, l3); sn: 'MeanAccuracyLoad1CueDisSameTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 108; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'MeanAccuracyLoad2CueDisDiffTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 109; fld: fdAccuracy; fla: (l2, l2, l1, l2, l2); sn: 'MeanAccuracyLoad2CueDisDiffTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 110; fld: fdAccuracy; fla: (l2, l2, l1, l1, l3); sn: 'MeanAccuracyLoad2CueDisDiffTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 111; fld: fdAccuracy; fla: (l2, l2, l1, l2, l3); sn: 'MeanAccuracyLoad2CueDisDiffTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 112; fld: fdAccuracy; fla: (l2, l3, l1, l1, l2); sn: 'MeanAccuracyLoad2CueDisSameTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 113; fld: fdAccuracy; fla: (l2, l3, l1, l2, l2); sn: 'MeanAccuracyLoad2CueDisSameTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 114; fld: fdAccuracy; fla: (l2, l3, l1, l1, l3); sn: 'MeanAccuracyLoad2CueDisSameTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 115; fld: fdAccuracy; fla: (l2, l3, l1, l2, l3); sn: 'MeanAccuracyLoad2CueDisSameTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 116; fld: fdAccuracy; fla: (l2, l2, l1, l1, l2); sn: 'MeanAccuracyLoad3CueDisDiffTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 117; fld: fdAccuracy; fla: (l3, l2, l1, l2, l2); sn: 'MeanAccuracyLoad3CueDisDiffTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 118; fld: fdAccuracy; fla: (l3, l2, l1, l1, l3); sn: 'MeanAccuracyLoad3CueDisDiffTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 119; fld: fdAccuracy; fla: (l3, l2, l1, l2, l3); sn: 'MeanAccuracyLoad3CueDisDiffTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 120; fld: fdAccuracy; fla: (l3, l3, l1, l1, l2); sn: 'MeanAccuracyLoad3CueDisSameTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 121; fld: fdAccuracy; fla: (l3, l3, l1, l2, l2); sn: 'MeanAccuracyLoad3CueDisSameTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 122; fld: fdAccuracy; fla: (l3, l3, l1, l1, l3); sn: 'MeanAccuracyLoad3CueDisSameTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 123; fld: fdAccuracy; fla: (l3, l3, l1, l2, l3); sn: 'MeanAccuracyLoad3CueDisSameTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 124; fld: fdAccuracy; fla: (l3, l2, l1, l1, l2); sn: 'MeanAccuracyLoad4CueDisDiffTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 125; fld: fdAccuracy; fla: (l4, l2, l1, l2, l2); sn: 'MeanAccuracyLoad4CueDisDiffTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 126; fld: fdAccuracy; fla: (l4, l2, l1, l1, l3); sn: 'MeanAccuracyLoad4CueDisDiffTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 127; fld: fdAccuracy; fla: (l4, l2, l1, l2, l3); sn: 'MeanAccuracyLoad4CueDisDiffTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 128; fld: fdAccuracy; fla: (l4, l3, l1, l1, l2); sn: 'MeanAccuracyLoad4CueDisSameTarDHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 129; fld: fdAccuracy; fla: (l4, l3, l1, l2, l2); sn: 'MeanAccuracyLoad4CueDisSameTarDHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 130; fld: fdAccuracy; fla: (l4, l3, l1, l1, l3); sn: 'MeanAccuracyLoad4CueDisSameTarSHCueTarDiff';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs4f; ck: ckMean; id: 131; fld: fdAccuracy; fla: (l4, l3, l1, l2, l3); sn: 'MeanAccuracyLoad4CueDisSameTarSHCueTarSame';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 132; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad1CueDisDiffTarDH';  isF: True; cow: 1; sn1: 100; sn2: 101; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 133; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad1CueDisDiffTarSH';  isF: True; cow: 1; sn1: 102; sn2: 103; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 134; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad1CueDisSameTarDH';  isF: True; cow: 1; sn1: 104; sn2: 105; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 135; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad1CueDisSameTarSH';  isF: True; cow: 1; sn1: 106; sn2: 107; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 136; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad2CueDisDiffTarDH';  isF: True; cow: 2; sn1: 108; sn2: 109; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 137; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad2CueDisDiffTarSH';  isF: True; cow: 2; sn1: 110; sn2: 111; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 138; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad2CueDisSameTarDH';  isF: True; cow: 2; sn1: 112; sn2: 113; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 139; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad2CueDisSameTarSH';  isF: True; cow: 2; sn1: 114; sn2: 115; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 140; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad3CueDisDiffTarDH';  isF: True; cow: 3; sn1: 116; sn2: 117; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 141; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad3CueDisDiffTarSH';  isF: True; cow: 3; sn1: 118; sn2: 119; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 142; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad3CueDisSameTarDH';  isF: True; cow: 3; sn1: 120; sn2: 121; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 143; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad3CueDisSameTarSH';  isF: True; cow: 3; sn1: 122; sn2: 123; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 144; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad4CueDisDiffTarDH';  isF: True; cow: 4; sn1: 124; sn2: 125; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 145; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad4CueDisDiffTarSH';  isF: True; cow: 4; sn1: 126; sn2: 127; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 146; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad4CueDisSameTarDH';  isF: True; cow: 4; sn1: 128; sn2: 129; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckCow; id: 147; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKLoad4CueDisSameTarSH';  isF: True; cow: 4; sn1: 130; sn2: 131; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckCow; id: 148; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'CowanKSummaryMean';     isF: True; cow: 0; sn1: 132; sn2: 147; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 149; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 100; sn2: 101; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 150; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 102; sn2: 103; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 151; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 104; sn2: 105; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 152; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 106; sn2: 107; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 153; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 108; sn2: 109; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 154; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 110; sn2: 111; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 155; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 112; sn2: 113; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 156; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 114; sn2: 115; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 157; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 116; sn2: 117; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 158; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 118; sn2: 119; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 159; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 120; sn2: 121; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 160; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 122; sn2: 123; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 161; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 124; sn2: 125; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 162; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 126; sn2: 127; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 163; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 128; sn2: 129; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckA; id: 164; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ALoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 130; sn2: 131; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckA; id: 165; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'ASummaryMean';     isF: True; cow: 0; sn1: 149; sn2: 164; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 166; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 100; sn2: 101; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 167; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 102; sn2: 103; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 168; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 104; sn2: 105; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 169; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 106; sn2: 107; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 170; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 108; sn2: 109; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 171; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 110; sn2: 111; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 172; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 112; sn2: 113; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 173; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 114; sn2: 115; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 174; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 116; sn2: 117; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 175; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 118; sn2: 119; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 176; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 120; sn2: 121; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 177; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 122; sn2: 123; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 178; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 124; sn2: 125; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 179; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 126; sn2: 127; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 180; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 128; sn2: 129; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs2s; ck: ckDP; id: 181; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 130; sn2: 131; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckDP; id: 182; fld: fdAccuracy; fla: (l1, l2, l1, l1, l2); sn: 'D-PrimeSummaryMean';     isF: True; cow: 0; sn1: 166; sn2: 181; sn3: 0; sn4: 0))),

  (rv: (bk: f; fl: (fld: fdAccuracy; fv: '0'; id: 5; kind: fkAccuracy; minO: 0; maxO: 0; mult: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 183; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad1CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 184; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad1CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 185; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad1CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 186; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad1CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 187; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad2CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 188; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad2CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 189; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad2CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 190; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad2CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 191; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad3CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 192; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad3CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 193; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad3CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 194; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad3CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 195; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad4CueDisDiffTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 196; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad4CueDisDiffTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 197; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeLoad4CueDisSameTarDH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckCnt; id: 198; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l3); sn: 'NCorrectTrialsResponseTimeLoad4CueDisSameTarSH';  isF: False; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummCount; ck: ckCnt; id: 199; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'NCorrectTrialsResponseTimeSummaryCount';   isF: False; cow: 0; sn1: 183; sn2: 198; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 200; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 201; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 202; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 203; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 204; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 205; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 206; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 207; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 208; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 209; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 210; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 211; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 212; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 213; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 214; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l2); sn: 'MeanCorrectResponseTimeLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckMean; id: 215; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l3); sn: 'MeanCorrectResponseTimeLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckMean; id: 216; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'MeanCorrectResponseTimeSummaryMean';     isF: True; cow: 0; sn1: 200; sn2: 215; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 217; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 218; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 219; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 220; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 221; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 222; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 223; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 224; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 225; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 226; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 227; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 228; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 229; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 230; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 231; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrtB; id: 232; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l3); sn: 'SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckSqrtB; id: 233; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'SqrtBefMeanCorrectResponseTimeSummaryMean';     isF: True; cow: 0; sn1: 217; sn2: 232; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 234; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 235; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 236; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 237; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 238; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 239; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 240; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 241; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 242; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 243; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 244; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 245; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 246; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 247; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 248; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckSqrt; id: 249; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l3); sn: 'SqrtMeanCorrectResponseTimeLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckSqrt; id: 250; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'SqrtMeanCorrectResponseTimeSummaryMean';     isF: True; cow: 0; sn1: 234; sn2: 249; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 251; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 252; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 253; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 254; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 255; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 256; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 257; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 258; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 259; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 260; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 261; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 262; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 263; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 264; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 265; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLnB; id: 266; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l3); sn: 'LnBefMeanCorrectResponseTimeLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckLnB; id: 267; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'LnBefMeanCorrectResponseTimeSummaryMean';     isF: True; cow: 0; sn1: 251; sn2: 266; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 268; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad1CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 269; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad1CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 270; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad1CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 271; fld: fdRT_ms_minus_constant_error; fla: (l1, l3, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad1CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 272; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad2CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 273; fld: fdRT_ms_minus_constant_error; fla: (l2, l2, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad2CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 274; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad2CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 275; fld: fdRT_ms_minus_constant_error; fla: (l2, l3, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad2CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 276; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad3CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 277; fld: fdRT_ms_minus_constant_error; fla: (l3, l2, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad3CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 278; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad3CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 279; fld: fdRT_ms_minus_constant_error; fla: (l3, l3, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad3CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 280; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad4CueDisDiffTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 281; fld: fdRT_ms_minus_constant_error; fla: (l4, l2, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad4CueDisDiffTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 282; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeLoad4CueDisSameTarDH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: cs3f; ck: ckLn; id: 283; fld: fdRT_ms_minus_constant_error; fla: (l4, l3, l1, l1, l3); sn: 'LnMeanCorrectResponseTimeLoad4CueDisSameTarSH';  isF: True; cow: 0; sn1: 0; sn2: 0; sn3: 0; sn4: 0))),
  (rv: (bk: c; cp: (cs: csSummMean; ck: ckLn; id: 284; fld: fdRT_ms_minus_constant_error; fla: (l1, l2, l1, l1, l2); sn: 'LnMeanCorrectResponseTimeSummaryMean';     isF: True; cow: 0; sn1: 268; sn2: 283; sn3: 0; sn4: 0))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 285; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatMeanAccLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 17; sn2: 18; sn3: 19; sn4: 20))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 286; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatMeanAccLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 21; sn2: 22; sn3: 23; sn4: 24))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 287; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatMeanAccLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 25; sn2: 26; sn3: 27; sn4: 28))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 288; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatMeanAccLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 29; sn2: 30; sn3: 31; sn4: 32))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 289; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 17; sn2: 19; sn3: 18; sn4: 20))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 290; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 21; sn2: 23; sn3: 22; sn4: 24))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 291; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 25; sn2: 27; sn3: 26; sn4: 28))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 292; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 29; sn2: 31; sn3: 30; sn4: 32))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 293; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 17; sn2: 19; sn3: 18; sn4: 20))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 294; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 21; sn2: 23; sn3: 22; sn4: 24))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 295; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 25; sn2: 27; sn3: 26; sn4: 28))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 296; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 29; sn2: 31; sn3: 30; sn4: 32))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 297; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 34; sn2: 35; sn3: 36; sn4: 37))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 298; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 38; sn2: 39; sn3: 40; sn4: 41))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 299; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 42; sn2: 43; sn3: 44; sn4: 45))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 300; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 46; sn2: 47; sn3: 48; sn4: 49))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 301; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 34; sn2: 36; sn3: 35; sn4: 37))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 302; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 38; sn2: 40; sn3: 39; sn4: 41))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 303; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 42; sn2: 44; sn3: 43; sn4: 45))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 304; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 46; sn2: 48; sn3: 47; sn4: 49))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 305; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 34; sn2: 36; sn3: 35; sn4: 37))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 306; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 38; sn2: 40; sn3: 39; sn4: 41))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 307; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 42; sn2: 44; sn3: 43; sn4: 45))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 308; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 46; sn2: 48; sn3: 47; sn4: 49))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 309; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 51; sn2: 52; sn3: 53; sn4: 54))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 310; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 55; sn2: 56; sn3: 57; sn4: 58))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 311; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 59; sn2: 60; sn3: 61; sn4: 62))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 312; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 63; sn2: 64; sn3: 65; sn4: 66))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 313; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 34; sn2: 36; sn3: 35; sn4: 37))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 314; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 38; sn2: 40; sn3: 39; sn4: 41))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 315; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 42; sn2: 44; sn3: 43; sn4: 45))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 316; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 46; sn2: 48; sn3: 47; sn4: 49))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 317; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 51; sn2: 53; sn3: 52; sn4: 54))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 318; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 55; sn2: 57; sn3: 56; sn4: 58))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 319; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 59; sn2: 61; sn3: 60; sn4: 62))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 320; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 63; sn2: 65; sn3: 64; sn4: 66))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 321; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatCowanKLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 132; sn2: 133; sn3: 134; sn4: 135))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 322; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatCowanKLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 136; sn2: 137; sn3: 138; sn4: 139))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 323; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatCowanKLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 140; sn2: 141; sn3: 142; sn4: 143))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 324; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatCowanKLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 144; sn2: 145; sn3: 146; sn4: 147))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 325; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatCowanKLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 132; sn2: 134; sn3: 133; sn4: 135))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 326; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatCowanKLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 136; sn2: 138; sn3: 137; sn4: 139))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 327; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatCowanKLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 140; sn2: 142; sn3: 141; sn4: 143))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 328; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatCowanKLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 144; sn2: 146; sn3: 145; sn4: 147))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 329; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatCowanKLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 132; sn2: 134; sn3: 133; sn4: 135))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 330; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatCowanKLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 136; sn2: 138; sn3: 137; sn4: 139))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 331; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatCowanKLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 140; sn2: 142; sn3: 141; sn4: 143))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 332; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatCowanKLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 144; sn2: 146; sn3: 145; sn4: 147))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 333; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatALoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 149; sn2: 150; sn3: 151; sn4: 152))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 334; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatALoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 153; sn2: 154; sn3: 155; sn4: 156))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 335; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatALoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 157; sn2: 158; sn3: 159; sn4: 160))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 336; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatALoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 161; sn2: 162; sn3: 163; sn4: 164))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 337; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatALoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 149; sn2: 151; sn3: 150; sn4: 152))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 338; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatALoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 153; sn2: 155; sn3: 154; sn4: 156))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 339; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatALoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 157; sn2: 159; sn3: 158; sn4: 160))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 340; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatALoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 161; sn2: 163; sn3: 162; sn4: 164))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 341; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatALoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 149; sn2: 151; sn3: 150; sn4: 152))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 342; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatALoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 153; sn2: 155; sn3: 154; sn4: 156))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 343; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatALoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 157; sn2: 159; sn3: 158; sn4: 160))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 344; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatALoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 161; sn2: 163; sn3: 162; sn4: 164))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 345; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatD-PrimeLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 166; sn2: 167; sn3: 168; sn4: 169))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 346; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatD-PrimeLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 170; sn2: 171; sn3: 172; sn4: 173))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 347; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatD-PrimeLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 174; sn2: 175; sn3: 176; sn4: 177))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 348; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatD-PrimeLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 178; sn2: 179; sn3: 180; sn4: 181))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 349; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 166; sn2: 168; sn3: 167; sn4: 169))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 350; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 170; sn2: 172; sn3: 171; sn4: 173))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 351; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 174; sn2: 176; sn3: 175; sn4: 177))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 352; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 178; sn2: 180; sn3: 179; sn4: 181))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 353; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 166; sn2: 168; sn3: 167; sn4: 169))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 354; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 170; sn2: 172; sn3: 171; sn4: 173))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 355; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 174; sn2: 176; sn3: 175; sn4: 177))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 356; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 178; sn2: 180; sn3: 179; sn4: 181))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 357; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 200; sn2: 201; sn3: 202; sn4: 203))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 358; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 204; sn2: 205; sn3: 206; sn4: 207))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 359; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 208; sn2: 209; sn3: 210; sn4: 211))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 360; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 212; sn2: 213; sn3: 214; sn4: 215))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 361; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 200; sn2: 202; sn3: 201; sn4: 203))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 362; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 204; sn2: 206; sn3: 205; sn4: 207))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 363; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 208; sn2: 210; sn3: 209; sn4: 211))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 364; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 212; sn2: 214; sn3: 213; sn4: 215))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 365; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 200; sn2: 202; sn3: 201; sn4: 203))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 366; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 204; sn2: 206; sn3: 205; sn4: 207))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 367; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 208; sn2: 210; sn3: 209; sn4: 211))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 368; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 212; sn2: 214; sn3: 213; sn4: 215))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 369; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 217; sn2: 218; sn3: 219; sn4: 220))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 370; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 221; sn2: 222; sn3: 223; sn4: 224))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 371; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 225; sn2: 226; sn3: 227; sn4: 228))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 372; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 229; sn2: 230; sn3: 231; sn4: 232))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 373; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 217; sn2: 219; sn3: 218; sn4: 220))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 374; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 221; sn2: 223; sn3: 222; sn4: 224))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 375; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 225; sn2: 227; sn3: 226; sn4: 228))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 376; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 229; sn2: 231; sn3: 230; sn4: 232))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 377; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 217; sn2: 219; sn3: 218; sn4: 220))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 378; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 221; sn2: 223; sn3: 222; sn4: 224))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 379; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 225; sn2: 227; sn3: 226; sn4: 228))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 380; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 229; sn2: 231; sn3: 230; sn4: 232))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 381; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 234; sn2: 235; sn3: 236; sn4: 237))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 382; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 238; sn2: 239; sn3: 240; sn4: 241))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 383; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 242; sn2: 243; sn3: 244; sn4: 245))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 384; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 246; sn2: 247; sn3: 248; sn4: 249))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 385; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 234; sn2: 236; sn3: 235; sn4: 237))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 386; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 238; sn2: 240; sn3: 239; sn4: 241))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 387; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 242; sn2: 244; sn3: 243; sn4: 245))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 388; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 246; sn2: 248; sn3: 247; sn4: 249))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 389; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 234; sn2: 236; sn3: 235; sn4: 237))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 390; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 238; sn2: 240; sn3: 239; sn4: 241))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 391; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 242; sn2: 244; sn3: 243; sn4: 245))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 392; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 246; sn2: 248; sn3: 247; sn4: 249))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 393; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 251; sn2: 252; sn3: 253; sn4: 254))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 394; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 255; sn2: 256; sn3: 257; sn4: 258))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 395; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 259; sn2: 260; sn3: 261; sn4: 262))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 396; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 263; sn2: 264; sn3: 265; sn4: 266))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 397; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 251; sn2: 253; sn3: 252; sn4: 254))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 398; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 255; sn2: 257; sn3: 256; sn4: 258))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 399; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 259; sn2: 261; sn3: 260; sn4: 262))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 400; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 263; sn2: 265; sn3: 264; sn4: 266))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 401; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 251; sn2: 253; sn3: 252; sn4: 254))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 402; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 255; sn2: 257; sn3: 256; sn4: 258))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 403; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 259; sn2: 261; sn3: 260; sn4: 262))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 404; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 263; sn2: 265; sn3: 264; sn4: 266))),

  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 405; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'DistObjCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 268; sn2: 269; sn3: 270; sn4: 271))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 406; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'DistObjCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 272; sn2: 273; sn3: 274; sn4: 275))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 407; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'DistObjCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 276; sn2: 277; sn3: 278; sn4: 279))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 408; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'DistObjCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 280; sn2: 281; sn3: 282; sn4: 283))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 409; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'DistSpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 268; sn2: 270; sn3: 269; sn4: 271))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 410; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'DistSpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 272; sn2: 274; sn3: 273; sn4: 275))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 411; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'DistSpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 276; sn2: 278; sn3: 277; sn4: 279))),
  (rv: (bk: c; cp: (cs: csDiff;  ck: ckNA; id: 412; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'DistSpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame';      isF: True; cow: 0; sn1: 280; sn2: 282; sn3: 281; sn4: 283))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 413; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'DistObj+SpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 268; sn2: 270; sn3: 269; sn4: 271))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 414; fld: fdError; fla: (l4, l2, l1, l1, l2); sn: 'DistObj+SpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 272; sn2: 274; sn3: 273; sn4: 275))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 415; fld: fdError; fla: (l4, l2, l1, l1, l3); sn: 'DistObj+SpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 276; sn2: 278; sn3: 277; sn4: 279))),
  (rv: (bk: c; cp: (cs: csDDiff; ck: ckNA; id: 416; fld: fdError; fla: (l4, l3, l1, l1, l2); sn: 'DistObj+SpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame';  isF: True; cow: 0; sn1: 280; sn2: 282; sn3: 281; sn4: 283))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 417; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 285; sn2: 286; sn3: 287; sn4: 288))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 418; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 289; sn2: 290; sn3: 291; sn4: 292))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 419; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 293; sn2: 294; sn3: 295; sn4: 296))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 420; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 285; sn2: 286; sn3: 287; sn4: 288))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 421; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 289; sn2: 290; sn3: 291; sn4: 292))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 422; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 293; sn2: 294; sn3: 295; sn4: 296))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 423; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 285; sn2: 286; sn3: 287; sn4: 288))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 424; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 289; sn2: 290; sn3: 291; sn4: 292))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 425; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 293; sn2: 294; sn3: 295; sn4: 296))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 426; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 297; sn2: 298; sn3: 299; sn4: 300))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 427; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 301; sn2: 302; sn3: 303; sn4: 304))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 428; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 305; sn2: 306; sn3: 307; sn4: 308))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 429; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 297; sn2: 298; sn3: 299; sn4: 300))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 430; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 301; sn2: 302; sn3: 303; sn4: 304))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 431; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 305; sn2: 306; sn3: 307; sn4: 308))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 432; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 297; sn2: 298; sn3: 299; sn4: 300))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 433; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 301; sn2: 302; sn3: 303; sn4: 304))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 434; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 305; sn2: 306; sn3: 307; sn4: 308))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 435; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 309; sn2: 310; sn3: 311; sn4: 312))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 436; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 313; sn2: 314; sn3: 315; sn4: 316))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 437; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 317; sn2: 318; sn3: 319; sn4: 320))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 438; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 309; sn2: 310; sn3: 311; sn4: 312))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 439; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 313; sn2: 314; sn3: 315; sn4: 316))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 440; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 317; sn2: 318; sn3: 319; sn4: 320))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 441; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 309; sn2: 310; sn3: 311; sn4: 312))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 442; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 313; sn2: 314; sn3: 315; sn4: 316))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 443; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 317; sn2: 318; sn3: 319; sn4: 320))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 444; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 321; sn2: 322; sn3: 323; sn4: 324))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 445; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 325; sn2: 326; sn3: 327; sn4: 328))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 446; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 329; sn2: 330; sn3: 331; sn4: 332))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 447; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 321; sn2: 322; sn3: 323; sn4: 324))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 448; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 325; sn2: 326; sn3: 327; sn4: 328))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 449; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 329; sn2: 330; sn3: 331; sn4: 332))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 450; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 321; sn2: 322; sn3: 323; sn4: 324))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 451; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 325; sn2: 326; sn3: 327; sn4: 328))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 452; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 329; sn2: 330; sn3: 331; sn4: 332))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 453; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatALoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 333; sn2: 334; sn3: 335; sn4: 336))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 454; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatALoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 337; sn2: 338; sn3: 339; sn4: 340))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 455; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 341; sn2: 342; sn3: 343; sn4: 344))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 456; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatALoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 333; sn2: 334; sn3: 335; sn4: 336))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 457; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatALoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 337; sn2: 338; sn3: 339; sn4: 340))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 458; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 341; sn2: 342; sn3: 343; sn4: 344))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 459; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatALoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 333; sn2: 334; sn3: 335; sn4: 336))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 460; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatALoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 337; sn2: 338; sn3: 339; sn4: 340))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 461; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 341; sn2: 342; sn3: 343; sn4: 344))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 462; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 345; sn2: 346; sn3: 347; sn4: 348))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 463; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 349; sn2: 350; sn3: 351; sn4: 352))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 464; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 353; sn2: 354; sn3: 355; sn4: 356))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 465; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 345; sn2: 346; sn3: 347; sn4: 348))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 466; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 349; sn2: 350; sn3: 351; sn4: 352))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 467; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 353; sn2: 354; sn3: 355; sn4: 356))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 468; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 345; sn2: 346; sn3: 347; sn4: 348))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 469; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 349; sn2: 350; sn3: 351; sn4: 352))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 470; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 353; sn2: 354; sn3: 355; sn4: 356))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 471; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 357; sn2: 358; sn3: 359; sn4: 360))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 472; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 361; sn2: 362; sn3: 363; sn4: 364))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 473; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 365; sn2: 366; sn3: 367; sn4: 368))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 474; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 357; sn2: 358; sn3: 359; sn4: 360))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 475; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 361; sn2: 362; sn3: 363; sn4: 364))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 476; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 365; sn2: 366; sn3: 367; sn4: 368))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 477; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 357; sn2: 358; sn3: 359; sn4: 360))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 478; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 361; sn2: 362; sn3: 363; sn4: 364))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 479; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 365; sn2: 366; sn3: 367; sn4: 368))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 480; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 369; sn2: 370; sn3: 371; sn4: 372))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 481; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 373; sn2: 374; sn3: 375; sn4: 376))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 482; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 377; sn2: 378; sn3: 379; sn4: 380))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 483; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 369; sn2: 370; sn3: 371; sn4: 372))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 484; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 373; sn2: 374; sn3: 375; sn4: 376))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 485; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 377; sn2: 378; sn3: 379; sn4: 380))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 486; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 369; sn2: 370; sn3: 371; sn4: 372))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 487; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 373; sn2: 374; sn3: 375; sn4: 376))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 488; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 377; sn2: 378; sn3: 379; sn4: 380))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 489; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 381; sn2: 382; sn3: 383; sn4: 384))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 490; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 385; sn2: 386; sn3: 387; sn4: 388))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 491; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 389; sn2: 390; sn3: 391; sn4: 392))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 492; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 381; sn2: 382; sn3: 383; sn4: 384))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 493; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 385; sn2: 386; sn3: 387; sn4: 388))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 494; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 389; sn2: 390; sn3: 391; sn4: 392))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 495; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 381; sn2: 382; sn3: 383; sn4: 384))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 496; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 385; sn2: 386; sn3: 387; sn4: 388))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 497; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 389; sn2: 390; sn3: 391; sn4: 392))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 498; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 393; sn2: 394; sn3: 395; sn4: 396))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 499; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 397; sn2: 398; sn3: 399; sn4: 400))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 500; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 401; sn2: 402; sn3: 403; sn4: 404))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 501; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 393; sn2: 394; sn3: 395; sn4: 396))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 502; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 397; sn2: 398; sn3: 399; sn4: 400))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 503; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 401; sn2: 402; sn3: 403; sn4: 404))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 504; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 393; sn2: 394; sn3: 395; sn4: 396))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 505; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 397; sn2: 398; sn3: 399; sn4: 400))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 506; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 401; sn2: 402; sn3: 403; sn4: 404))),

  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 507; fld: fdError; fla: (l1, l3, l1, l1, l3); sn: 'MinDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 405; sn2: 406; sn3: 407; sn4: 408))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 508; fld: fdError; fla: (l2, l2, l1, l1, l2); sn: 'MinDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 409; sn2: 410; sn3: 411; sn4: 412))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMin; id: 509; fld: fdError; fla: (l2, l2, l1, l1, l3); sn: 'MinDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 413; sn2: 414; sn3: 415; sn4: 416))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 510; fld: fdError; fla: (l2, l3, l1, l1, l2); sn: 'MaxDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 405; sn2: 406; sn3: 407; sn4: 408))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 511; fld: fdError; fla: (l2, l3, l1, l1, l3); sn: 'MaxDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 409; sn2: 410; sn3: 411; sn4: 412))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckMax; id: 512; fld: fdError; fla: (l3, l2, l1, l1, l2); sn: 'MaxDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 413; sn2: 414; sn3: 415; sn4: 416))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 513; fld: fdError; fla: (l3, l2, l1, l1, l3); sn: 'SumDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 405; sn2: 406; sn3: 407; sn4: 408))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 514; fld: fdError; fla: (l3, l3, l1, l1, l2); sn: 'SumDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';       isF: True; cow: 0; sn1: 409; sn2: 410; sn3: 411; sn4: 412))),
  (rv: (bk: c; cp: (cs: csMMS; ck: ckSum; id: 515; fld: fdError; fla: (l3, l3, l1, l1, l3); sn: 'SumDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame';   isF: True; cow: 0; sn1: 413; sn2: 414; sn3: 415; sn4: 416)) ) );

const

  StatNames: array[0..HighStatID] of String = (
  'NTrialsAccuracyLoad1CueDisDiffTarDH',
  'NTrialsAccuracyLoad1CueDisDiffTarSH',
  'NTrialsAccuracyLoad1CueDisSameTarDH',
  'NTrialsAccuracyLoad1CueDisDiffTarSH',
  'NTrialsAccuracyLoad2CueDisDiffTarDH',
  'NTrialsAccuracyLoad2CueDisDiffTarSH',
  'NTrialsAccuracyLoad2CueDisSameTarDH',
  'NTrialsAccuracyLoad2CueDisSameTarSH',
  'NTrialsAccuracyLoad3CueDisDiffTarDH',
  'NTrialsAccuracyLoad3CueDisDiffTarSH',
  'NTrialsAccuracyLoad3CueDisSameTarDH',
  'NTrialsAccuracyLoad3CueDisSameTarSH',
  'NTrialsAccuracyLoad4CueDisDiffTarDH',
  'NTrialsAccuracyLoad4CueDisDiffTarSH',
  'NTrialsAccuracyLoad4CueDisSameTarDH',
  'NTrialsAccuracyLoad4CueDisSameTarSH',
  'NTrialsAccuracySummaryCount',
  'MeanAccuracyLoad1CueDisDiffTarDH',
  'MeanAccuracyLoad1CueDisDiffTarSH',
  'MeanAccuracyLoad1CueDisSameTarDH',
  'MeanAccuracyLoad1CueDisSameTarSH',
  'MeanAccuracyLoad2CueDisDiffTarDH',
  'MeanAccuracyLoad2CueDisDiffTarSH',
  'MeanAccuracyLoad2CueDisSameTarDH',
  'MeanAccuracyLoad2CueDisSameTarSH',
  'MeanAccuracyLoad3CueDisDiffTarDH',
  'MeanAccuracyLoad3CueDisDiffTarSH',
  'MeanAccuracyLoad3CueDisSameTarDH',
  'MeanAccuracyLoad3CueDisSameTarSH',
  'MeanAccuracyLoad4CueDisDiffTarDH',
  'MeanAccuracyLoad4CueDisDiffTarSH',
  'MeanAccuracyLoad4CueDisSameTarDH',
  'MeanAccuracyLoad4CueDisSameTarSH',
  'MeanAccuracySummaryMean',
  'MeanErrorRateLoad1CueDisDiffTarDH',
  'MeanErrorRateLoad1CueDisDiffTarSH',
  'MeanErrorRateLoad1CueDisSameTarDH',
  'MeanErrorRateLoad1CueDisSameTarSH',
  'MeanErrorRateLoad2CueDisDiffTarDH',
  'MeanErrorRateLoad2CueDisDiffTarSH',
  'MeanErrorRateLoad2CueDisSameTarDH',
  'MeanErrorRateLoad2CueDisSameTarSH',
  'MeanErrorRateLoad3CueDisDiffTarDH',
  'MeanErrorRateLoad3CueDisDiffTarSH',
  'MeanErrorRateLoad3CueDisSameTarDH',
  'MeanErrorRateLoad3CueDisSameTarSH',
  'MeanErrorRateLoad4CueDisDiffTarDH',
  'MeanErrorRateLoad4CueDisDiffTarSH',
  'MeanErrorRateLoad4CueDisSameTarDH',
  'MeanErrorRateLoad4CueDisSameTarSH',
  'MeanErrorRateAccuracySummaryMean',
  'ArcsinMeanAccuracyLoad1CueDisDiffTarDH',
  'ArcsinMeanAccuracyLoad1CueDisDiffTarSH',
  'ArcsinMeanAccuracyLoad1CueDisSameTarDH',
  'ArcsinMeanAccuracyLoad1CueDisSameTarSH',
  'ArcsinMeanAccuracyLoad2CueDisDiffTarDH',
  'ArcsinMeanAccuracyLoad2CueDisDiffTarSH',
  'ArcsinMeanAccuracyLoad2CueDisSameTarDH',
  'ArcsinMeanAccuracyLoad2CueDisSameTarSH',
  'ArcsinMeanAccuracyLoad3CueDisDiffTarDH',
  'ArcsinMeanAccuracyLoad3CueDisDiffTarSH',
  'ArcsinMeanAccuracyLoad3CueDisSameTarDH',
  'ArcsinMeanAccuracyLoad3CueDisSameTarSH',
  'ArcsinMeanAccuracyLoad4CueDisDiffTarDH',
  'ArcsinMeanAccuracyLoad4CueDisDiffTarSH',
  'ArcsinMeanAccuracyLoad4CueDisSameTarDH',
  'ArcsinMeanAccuracyLoad4CueDisSameTarSH',
  'ArcsinMeanAccuracySummaryMean',
  'NTrialsAccuracyLoad1CueDisDiffTarDHCueTarDiff',
  'NTrialsAccuracyLoad1CueDisDiffTarDHCueTarSame',
  'NTrialsAccuracyLoad1CueDisDiffTarSHCueTarDiff',
  'NTrialsAccuracyLoad1CueDisDiffTarSHCueTarSame',
  'NTrialsAccuracyLoad1CueDisSameTarDHCueTarDiff',
  'NTrialsAccuracyLoad1CueDisSameTarDHCueTarSame',
  'NTrialsAccuracyLoad1CueDisSameTarSHCueTarDiff',
  'NTrialsAccuracyLoad1CueDisSameTarSHCueTarSame',
  'NTrialsAccuracyLoad2CueDisDiffTarDHCueTarDiff',
  'NTrialsAccuracyLoad2CueDisDiffTarDHCueTarSame',
  'NTrialsAccuracyLoad2CueDisDiffTarSHCueTarDiff',
  'NTrialsAccuracyLoad2CueDisDiffTarSHCueTarSame',
  'NTrialsAccuracyLoad2CueDisSameTarDHCueTarDiff',
  'NTrialsAccuracyLoad2CueDisSameTarDHCueTarSame',
  'NTrialsAccuracyLoad2CueDisSameTarSHCueTarDiff',
  'NTrialsAccuracyLoad2CueDisSameTarSHCueTarSame',
  'NTrialsAccuracyLoad3CueDisDiffTarDHCueTarDiff',
  'NTrialsAccuracyLoad3CueDisDiffTarDHCueTarSame',
  'NTrialsAccuracyLoad3CueDisDiffTarSHCueTarDiff',
  'NTrialsAccuracyLoad3CueDisDiffTarSHCueTarSame',
  'NTrialsAccuracyLoad3CueDisSameTarDHCueTarDiff',
  'NTrialsAccuracyLoad3CueDisSameTarDHCueTarSame',
  'NTrialsAccuracyLoad3CueDisSameTarSHCueTarDiff',
  'NTrialsAccuracyLoad3CueDisSameTarSHCueTarSame',
  'NTrialsAccuracyLoad4CueDisDiffTarDHCueTarDiff',
  'NTrialsAccuracyLoad4CueDisDiffTarDHCueTarSame',
  'NTrialsAccuracyLoad4CueDisDiffTarSHCueTarDiff',
  'NTrialsAccuracyLoad4CueDisDiffTarSHCueTarSame',
  'NTrialsAccuracyLoad4CueDisSameTarDHCueTarDiff',
  'NTrialsAccuracyLoad4CueDisSameTarDHCueTarSame',
  'NTrialsAccuracyLoad4CueDisSameTarSHCueTarDiff',
  'NTrialsAccuracyLoad4CueDisSameTarSHCueTarSame',
  'MeanAccuracyLoad1CueDisDiffTarDHCueTarDiff',
  'MeanAccuracyLoad1CueDisDiffTarDHCueTarSame',
  'MeanAccuracyLoad1CueDisDiffTarSHCueTarDiff',
  'MeanAccuracyLoad1CueDisDiffTarSHCueTarSame',
  'MeanAccuracyLoad1CueDisSameTarDHCueTarDiff',
  'MeanAccuracyLoad1CueDisSameTarDHCueTarSame',
  'MeanAccuracyLoad1CueDisSameTarSHCueTarDiff',
  'MeanAccuracyLoad1CueDisSameTarSHCueTarSame',
  'MeanAccuracyLoad2CueDisDiffTarDHCueTarDiff',
  'MeanAccuracyLoad2CueDisDiffTarDHCueTarSame',
  'MeanAccuracyLoad2CueDisDiffTarSHCueTarDiff',
  'MeanAccuracyLoad2CueDisDiffTarSHCueTarSame',
  'MeanAccuracyLoad2CueDisSameTarDHCueTarDiff',
  'MeanAccuracyLoad2CueDisSameTarDHCueTarSame',
  'MeanAccuracyLoad2CueDisSameTarSHCueTarDiff',
  'MeanAccuracyLoad2CueDisSameTarSHCueTarSame',
  'MeanAccuracyLoad3CueDisDiffTarDHCueTarDiff',
  'MeanAccuracyLoad3CueDisDiffTarDHCueTarSame',
  'MeanAccuracyLoad3CueDisDiffTarSHCueTarDiff',
  'MeanAccuracyLoad3CueDisDiffTarSHCueTarSame',
  'MeanAccuracyLoad3CueDisSameTarDHCueTarDiff',
  'MeanAccuracyLoad3CueDisSameTarDHCueTarSame',
  'MeanAccuracyLoad3CueDisSameTarSHCueTarDiff',
  'MeanAccuracyLoad3CueDisSameTarSHCueTarSame',
  'MeanAccuracyLoad4CueDisDiffTarDHCueTarDiff',
  'MeanAccuracyLoad4CueDisDiffTarDHCueTarSame',
  'MeanAccuracyLoad4CueDisDiffTarSHCueTarDiff',
  'MeanAccuracyLoad4CueDisDiffTarSHCueTarSame',
  'MeanAccuracyLoad4CueDisSameTarDHCueTarDiff',
  'MeanAccuracyLoad4CueDisSameTarDHCueTarSame',
  'MeanAccuracyLoad4CueDisSameTarSHCueTarDiff',
  'MeanAccuracyLoad4CueDisSameTarSHCueTarSame',
  'CowanKLoad1CueDisDiffTarDH',
  'CowanKLoad1CueDisDiffTarSH',
  'CowanKLoad1CueDisSameTarDH',
  'CowanKLoad1CueDisSameTarSH',
  'CowanKLoad2CueDisDiffTarDH',
  'CowanKLoad2CueDisDiffTarSH',
  'CowanKLoad2CueDisSameTarDH',
  'CowanKLoad2CueDisSameTarSH',
  'CowanKLoad3CueDisDiffTarDH',
  'CowanKLoad3CueDisDiffTarSH',
  'CowanKLoad3CueDisSameTarDH',
  'CowanKLoad3CueDisSameTarSH',
  'CowanKLoad4CueDisDiffTarDH',
  'CowanKLoad4CueDisDiffTarSH',
  'CowanKLoad4CueDisSameTarDH',
  'CowanKLoad4CueDisSameTarSH',
  'CowanKSummaryMean',
  'ALoad1CueDisDiffTarDH',
  'ALoad1CueDisDiffTarSH',
  'ALoad1CueDisSameTarDH',
  'ALoad1CueDisSameTarSH',
  'ALoad2CueDisDiffTarDH',
  'ALoad2CueDisDiffTarSH',
  'ALoad2CueDisSameTarDH',
  'ALoad2CueDisSameTarSH',
  'ALoad3CueDisDiffTarDH',
  'ALoad3CueDisDiffTarSH',
  'ALoad3CueDisSameTarDH',
  'ALoad3CueDisSameTarSH',
  'ALoad4CueDisDiffTarDH',
  'ALoad4CueDisDiffTarSH',
  'ALoad4CueDisSameTarDH',
  'ALoad4CueDisSameTarSH',
  'ASummaryMean',
  'D-PrimeLoad1CueDisDiffTarDH',
  'D-PrimeLoad1CueDisDiffTarSH',
  'D-PrimeLoad1CueDisSameTarDH',
  'D-PrimeLoad1CueDisSameTarSH',
  'D-PrimeLoad2CueDisDiffTarDH',
  'D-PrimeLoad2CueDisDiffTarSH',
  'D-PrimeLoad2CueDisSameTarDH',
  'D-PrimeLoad2CueDisSameTarSH',
  'D-PrimeLoad3CueDisDiffTarDH',
  'D-PrimeLoad3CueDisDiffTarSH',
  'D-PrimeLoad3CueDisSameTarDH',
  'D-PrimeLoad3CueDisSameTarSH',
  'D-PrimeLoad4CueDisDiffTarDH',
  'D-PrimeLoad4CueDisDiffTarSH',
  'D-PrimeLoad4CueDisSameTarDH',
  'D-PrimeLoad4CueDisSameTarSH',
  'D-PrimeSummaryMean',
  'NCorrectTrialsResponseTimeLoad1CueDisDiffTarDH',
  'NCorrectTrialsResponseTimeLoad1CueDisDiffTarSH',
  'NCorrectTrialsResponseTimeLoad1CueDisSameTarDH',
  'NCorrectTrialsResponseTimeLoad1CueDisSameTarSH',
  'NCorrectTrialsResponseTimeLoad2CueDisDiffTarDH',
  'NCorrectTrialsResponseTimeLoad2CueDisDiffTarSH',
  'NCorrectTrialsResponseTimeLoad2CueDisSameTarDH',
  'NCorrectTrialsResponseTimeLoad2CueDisSameTarSH',
  'NCorrectTrialsResponseTimeLoad3CueDisDiffTarDH',
  'NCorrectTrialsResponseTimeLoad3CueDisDiffTarSH',
  'NCorrectTrialsResponseTimeLoad3CueDisSameTarDH',
  'NCorrectTrialsResponseTimeLoad3CueDisSameTarSH',
  'NCorrectTrialsResponseTimeLoad4CueDisDiffTarDH',
  'NCorrectTrialsResponseTimeLoad4CueDisDiffTarSH',
  'NCorrectTrialsResponseTimeLoad4CueDisSameTarDH',
  'NCorrectTrialsResponseTimeLoad4CueDisSameTarSH',
  'NCorrectTrialsResponseTimeSummaryCount',
  'MeanCorrectResponseTimeLoad1CueDisDiffTarDH',
  'MeanCorrectResponseTimeLoad1CueDisDiffTarSH',
  'MeanCorrectResponseTimeLoad1CueDisSameTarDH',
  'MeanCorrectResponseTimeLoad1CueDisSameTarSH',
  'MeanCorrectResponseTimeLoad2CueDisDiffTarDH',
  'MeanCorrectResponseTimeLoad2CueDisDiffTarSH',
  'MeanCorrectResponseTimeLoad2CueDisSameTarDH',
  'MeanCorrectResponseTimeLoad2CueDisSameTarSH',
  'MeanCorrectResponseTimeLoad3CueDisDiffTarDH',
  'MeanCorrectResponseTimeLoad3CueDisDiffTarSH',
  'MeanCorrectResponseTimeLoad3CueDisSameTarDH',
  'MeanCorrectResponseTimeLoad3CueDisSameTarSH',
  'MeanCorrectResponseTimeLoad4CueDisDiffTarDH',
  'MeanCorrectResponseTimeLoad4CueDisDiffTarSH',
  'MeanCorrectResponseTimeLoad4CueDisSameTarDH',
  'MeanCorrectResponseTimeLoad4CueDisSameTarSH',
  'MeanCorrectResponseTimeSummaryMean',
  'SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad1CueDisSameTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad2CueDisSameTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad3CueDisSameTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
  'SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarDH',
  'SqrtBefMeanCorrectResponseTimeLoad4CueDisSameTarSH',
  'SqrtBefMeanCorrectResponseTimeSummaryMean',
  'SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
  'SqrtMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
  'SqrtMeanCorrectResponseTimeLoad1CueDisSameTarDH',
  'SqrtMeanCorrectResponseTimeLoad1CueDisSameTarSH',
  'SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
  'SqrtMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
  'SqrtMeanCorrectResponseTimeLoad2CueDisSameTarDH',
  'SqrtMeanCorrectResponseTimeLoad2CueDisSameTarSH',
  'SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
  'SqrtMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
  'SqrtMeanCorrectResponseTimeLoad3CueDisSameTarDH',
  'SqrtMeanCorrectResponseTimeLoad3CueDisSameTarSH',
  'SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
  'SqrtMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
  'SqrtMeanCorrectResponseTimeLoad4CueDisSameTarDH',
  'SqrtMeanCorrectResponseTimeLoad4CueDisSameTarSH',
  'SqrtMeanCorrectResponseTimeSummaryMean',
  'LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
  'LnBefMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
  'LnBefMeanCorrectResponseTimeLoad1CueDisSameTarDH',
  'LnBefMeanCorrectResponseTimeLoad1CueDisSameTarSH',
  'LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
  'LnBefMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
  'LnBefMeanCorrectResponseTimeLoad2CueDisSameTarDH',
  'LnBefMeanCorrectResponseTimeLoad2CueDisSameTarSH',
  'LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
  'LnBefMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
  'LnBefMeanCorrectResponseTimeLoad3CueDisSameTarDH',
  'LnBefMeanCorrectResponseTimeLoad3CueDisSameTarSH',
  'LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
  'LnBefMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
  'LnBefMeanCorrectResponseTimeLoad4CueDisSameTarDH',
  'LnBefMeanCorrectResponseTimeLoad4CueDisSameTarSH',
  'LnBefMeanCorrectResponseTimeSummaryMean',
  'LnMeanCorrectResponseTimeLoad1CueDisDiffTarDH',
  'LnMeanCorrectResponseTimeLoad1CueDisDiffTarSH',
  'LnMeanCorrectResponseTimeLoad1CueDisSameTarDH',
  'LnMeanCorrectResponseTimeLoad1CueDisSameTarSH',
  'LnMeanCorrectResponseTimeLoad2CueDisDiffTarDH',
  'LnMeanCorrectResponseTimeLoad2CueDisDiffTarSH',
  'LnMeanCorrectResponseTimeLoad2CueDisSameTarDH',
  'LnMeanCorrectResponseTimeLoad2CueDisSameTarSH',
  'LnMeanCorrectResponseTimeLoad3CueDisDiffTarDH',
  'LnMeanCorrectResponseTimeLoad3CueDisDiffTarSH',
  'LnMeanCorrectResponseTimeLoad3CueDisSameTarDH',
  'LnMeanCorrectResponseTimeLoad3CueDisSameTarSH',
  'LnMeanCorrectResponseTimeLoad4CueDisDiffTarDH',
  'LnMeanCorrectResponseTimeLoad4CueDisDiffTarSH',
  'LnMeanCorrectResponseTimeLoad4CueDisSameTarDH',
  'LnMeanCorrectResponseTimeLoad4CueDisSameTarSH',
  'LnMeanCorrectResponseTimeSummaryMean',
  'DistObjCompatMeanAccLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanAccLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanAccLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanAccLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanAccLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanAccLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanAccLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanAccLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanErrorRateLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanErrorRateLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanErrorRateLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanErrorRateLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatArcsinMeanAccLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatArcsinMeanAccLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatArcsinMeanAccLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatArcsinMeanAccLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatCowanKLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatCowanKLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatCowanKLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatCowanKLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatCowanKLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatCowanKLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatCowanKLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatCowanKLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatCowanKLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatCowanKLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatCowanKLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatCowanKLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatALoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatALoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatALoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatALoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatALoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatALoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatALoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatALoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatALoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatALoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatALoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatALoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatD-PrimeLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatD-PrimeLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatD-PrimeLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatD-PrimeLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatD-PrimeLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatD-PrimeLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatD-PrimeLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatD-PrimeLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatSqrtMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnBefMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnBefMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnBefMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnBefMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObjCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObjCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObjCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObjCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistSpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnMeanCRTLoad1CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnMeanCRTLoad2CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnMeanCRTLoad3CueDisDiffMinusCueDisSame',
  'DistObj+SpaCompatLnMeanCRTLoad4CueDisDiffMinusCueDisSame',
  'MinDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatArcsinMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatALoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatALoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatALoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatALoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatALoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatALoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatSqrtBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatSqrtMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatLnBefMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatLnMeanCRTLoadCueDisDiffMinusCueDisSame' );

const

  SummaryStatIDs: array[0..HighSummaryReportIndex] of Integer = (
  16, 33, 50, 148, 165, 182, 199, 216, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428,
  429, 430, 431, 432, 433, 434,
  444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457,
  458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479);


const

  SummaryDlgStatNames: array[0..HighSummaryReportIndex] of String = (
  'NTrialsAccuracySummaryCount',
  'MeanAccuracySummaryMean',
  'MeanErrorRateAccuracySummaryMean',
  'CowanKSummaryMean',
  'ASummaryMean',
  'D-PrimeSummaryMean',
  'NCorrectTrialsResponseTimeSummaryCount',
  'MeanCorrectResponseTimeSummaryMean',

  'MinDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',

  'MaxDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',

  'SumDistObjCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatMeanAccLoadCueDisDiffMinusCueDisSame',

  'MinDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',

  'MaxDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',

  'SumDistObjCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatMeanErrRateLoadCueDisDiffMinusCueDisSame',

  'MinDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',

  'MaxDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',

  'SumDistObjCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatCowanKLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatCowanKLoadCueDisDiffMinusCueDisSame',

  'MinDistObjCompatALoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatALoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',

  'MaxDistObjCompatALoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatALoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',

  'SumDistObjCompatALoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatALoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatALoadCueDisDiffMinusCueDisSame',

  'MinDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',

  'MaxDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',

  'SumDistObjCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatD-PrimeLoadCueDisDiffMinusCueDisSame',

  'MinDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MinDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'MaxDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObjCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistSpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame',
  'SumDistObj+SpaCompatMeanCRTLoadCueDisDiffMinusCueDisSame'  );




type

  RFieldsData = record
    accuracy:       String;
    factor1:        String;
    factor2:        String;
    factor4:        String;
    factor5:        String;
    rtmsMinusError: Integer;
    procedure Init(const anAccuracy, aFactor1, aFactor2, aFactor4, aFactor5{, aUserPause}: AnsiString; anRTmsMinusError: Integer);
  end;

  RFilter = record
    deleteCount:         Integer;
    id:                  Byte;
    isAccuracyFilter:    Boolean;
    fieldsDataArr:       array of RFieldsData;
    lines:               TIntegerDynArray;
    name:                AnsiString;
    procedure Init(const aName: AnsiString; anId: Byte; anIsAccuracyFilter: Boolean);
  end;

  RStat = record
    isFloat: Boolean;
    value: Double;
    procedure Init(anIsFloat: Boolean; aValue: Double);
  end;

  DaRStat = array of RStat;

  TParticipant = class(TObject)
  private
    FCurrentFilterIdx: Integer;
    FMarkedForDeletion: Boolean;
    function GetCurrentFilter: RFilter;
    function GetHeaderInfoText: String;
    function GetStatisticValue(anID: Integer): Double;
    function GetUnfilteredLineCountXclTraining: Integer;
    function ParsedAddFieldsOK(aLineIdx: Integer): Boolean;
    function PopulatedDataFieldsAndFactorsOfCurrentFilterOK: EnOutcome;
    function SetFactorStringsOfCurrentFilterOK: Boolean;
  public
    age, date, experiment, handedness, participantID,
      sessionNo, sex, startTime, trialOrderFileNo: AnsiString;
    endLine, startLine: Integer;
    filters: array of RFilter;
    invalidData: Boolean;
    lvlFacStrings: SaLvlFFStrings;
    statsArray: array[0..HighStatID] of RStat;
    constructor Create(anEndLineIdx: Integer; out Outcome: EnOutcome); reintroduce;
    function CopiedNextFilterArrayOK(const aName: AnsiString; anId: Byte; isAccuracy: Boolean): EnOutcome;
    function DeleteCurrFilterIndex(anIndex: Integer): EnOutcome;
    function GetStatisticSummaryAsTabbedLine: AnsiString;
    function GotStrFieldFromCurrFilterLinesOK(aFld: EnWMAField; aFilterLineIndex: Integer; out Fld: AnsiString): EnOutcome;
    function LvlFac3StringsMatchesLine(aFiltLineIdx: Integer; aLevel: EnLevels): Boolean;
    function SetStatisticOK(anID: Integer; aStat: RStat): EnOutcome;
    procedure MarkForDeletion;
    property CurrentFilter: RFilter read GetCurrentFilter;
    property HeaderInfoText: String read GetHeaderInfoText;
    property MarkedForDeletion: Boolean read FMarkedForDeletion;
    property StatisticValue[anID: Integer]: Double read GetStatisticValue;
    property UnfilteredLineCountXclTraining: Integer read GetUnfilteredLineCountXclTraining;
  end;

  TProgressEvent = procedure(const aPercentage: Single) of object;

  TParticipantDynArray = array of TParticipant;

  TNormList = class;

  RExperiment = record
    name: String;
    supportsNormativeData: Boolean;
    normDATFilename: String;
    normDATFileExists: Boolean;
    normCSVFilename: String;
    normList: TNormList;
  end;
  PExperiment = ^RExperiment;

  SaRExperiments = array[0..MaxExperimentIndex] of RExperiment;

  SaPExperiments = array[0..MaxExperimentIndex] of PExperiment;

  DaPExperiments = array of PExperiment;

const

  RExperiments: SaRExperiments =
    (
     (name: 'Updating_VWM_RMS2_October_2020'; supportsNormativeData: True;
      normDATFilename: ''; normDATFileExists: False; normCSVFilename: ''; normList: Nil)
    );

type

  TSettings = class(TObject)
  private
    FFilename: String;
    FOutputFileDelimiter: Char;
    FShowFullPathInFilename: Boolean;
    FWarnIfOverwritingOutput: Boolean;
    function GetFilename: String;
    procedure SetOutputFileDelimiter(aValue: Char);
  public
    constructor Create;
    destructor Destroy; override;
    function LoadFromFile: Boolean;
    function SaveToFile: Boolean;
    property Filename: String read FFilename;
    property OutputFileDelimiter: Char read FOutputFileDelimiter write SetOutputFileDelimiter default #9;
    property ShowFullPathInFilename: Boolean read FShowFullPathInFilename write FShowFullPathInFilename default False;
    property WarnIfOverwritingOutput: Boolean read FWarnIfOverwritingOutput write FWarnIfOverwritingOutput default True;
  end;

  TFileProcessor = class(TObject)
  private
    FDataFileKind: EnDataFileKind;
    FDataFullFilename: AnsiString;
    FExperimentIdx: Integer; // index of current experiment in PExperiments global array
    FOnProgress: TProgressEvent;
    FOutputDataSubdir: AnsiString;
    FOutputFullFilename: AnsiString;
    FParticipants: TFPObjectList;
    function DoBehestLineKindChange(aParticipant: TParticipant; prv, nxt: EnBehestKind): EnOutcome;
    function GetDataFilename: AnsiString;
    function GetExperimentName: AnsiString;
    function GetHighParticipant: Integer;
    function GetParticipantCount: Integer;
    function GetParticipants(index: Integer): TParticipant;
    function GetTabbedOutputFieldNameList: AnsiString;
    function ParsedDataFileOK(out Kind: EnDataFileKind; out ExpName: AnsiString; out RejectedParticipants: TStringArray): EnOutcome;
    function RanBehestsOnAllParticipantsOK: EnOutcome;
    procedure DoProgress(aPercentage: Single);
    property HighParticipant: Integer read GetHighParticipant;
  public
    constructor Create(aDataFullFilename: AnsiString; aProgress: TProgressEvent;
                       out Outcome: EnOutcome; out DataStr: AnsiString; out rejected: TStringArray;
                       out fyleExists: Boolean);
    destructor Destroy; override;
    function AddedParticipantsToNormativeOK(nonNorms: TParticipantDynArray): Boolean;
    function NonNormParticipantsArePresent(out nonNorms: TParticipantDynArray): Boolean;
    procedure PopulateListboxWithParticipants(const aListbox: TListBox);
    procedure ReportStatistics(aParticipant: TParticipant; anArray: TIntegerDynArray; aMemo: TMemo);
    property DataFileKind: EnDataFileKind read FDataFileKind;
    property DataFilename: AnsiString read GetDataFilename;
    property DataFullFilename: AnsiString read FDataFullFilename;
    property ExperimentIdx: Integer read FExperimentIdx;
    property ExperimentName: AnsiString read GetExperimentName;
    property OutputFullFilename: AnsiString read FOutputFullFilename;
    property ParticipantCount: Integer read GetParticipantCount;
    property Participants[index: Integer]: TParticipant read GetParticipants;
    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;
  end;

  ROFiler = record
  private
    txtF: TextFile;
    fe: Integer;
  public
    fName: AnsiString;
    function Init(aFilename: AnsiString; out FileExists: Boolean): EnOutcome;
    function AddLine(const aText: AnsiString): EnOutcome;
    function AppendToLastLine(const aText: AnsiString; addTrailingTab: Boolean=True): EnOutcome;
  end;

  RNormSummaryRec = record
    UCLongID: String17;
    age: Byte;
    sessionNo: Byte;
    rightHanded: Boolean;
    female: Boolean;
    participantID: String7;
    startTime: String8;
    trialOrderFileNo: String2;
    date: TDate;
    s16, s33, s50, s148, s165, s182, s199, s216, s417, s418, s419, s420, s421, s422, s423, s424, s425, s426, s427, s428,
    s429, s430, s431, s432, s433, s434, s444, s445, s446, s447, s448, s449, s450, s451, s452, s453, s454, s455, s456, s457,
    s458, s459, s460, s461, s462, s463, s464, s465, s466, s467, s468, s469, s470, s471, s472, s473, s474, s475, s476, s477, s478, s479: Double;
    function GetField(aStatID: Integer): Double;
  end;

  TFileNormSummaryRec = file of RNormSummaryRec;

  DaNormSummaryRec = array of RNormSummaryRec;

  RValueZetc = record
    value:      Double;
    zScore:     Double;
    percentile: Double;
    pValue:     Double;
  end;

  SaAge = array[0..99] of  Byte;

  SaSession = array[1..10] of Byte;

  SaRValueZetc = array[0..HighSummaryReportIndex] of RValueZetc;

  TNormList = class(TObject)
  private
    FAgeArray: SaAge;
    FSessionArray: SaSession;
    FExperimentIdx: Integer;
    FMatchingIdxs: TIntegerDynArray;
    function GetAgeCount(anAge: Byte): Byte;
    function GetAgeSet: TByteSet;
    function GetDataItemCount: Integer;
    function GetFemaleCount: Integer;
    function GetLefthandedCount: Integer;
    function GetMaleCount: Integer;
    function GetRighthandedCount: Integer;
    function GetSessionCount(aSessionNo: Byte): Byte;
    function GetSessionSet: TByteSet;
    function GetZScore(aParticipantValue: Double; aStatID: Integer): Double;
    function LoadedFromDATFileOK: EnOutcome;
    function Matches(aZS: RAddZScore; aRec: RNormSummaryRec): Boolean;
    function NormRecToCSVString(nr: RNormSummaryRec): String;
    function ParticipantDataToNormRecOK(aParticipant: TParticipant; out nsr: RNormSummaryRec): Boolean;
  public
    NormRecArray: DaNormSummaryRec; // container for all RNormSummaryRecs
    ParticipantNormValues: SaRValueZetc;
    constructor Create(aProcessor: TFileProcessor; anExptIdx: Integer; out Outcome: EnOutcome; fromScratch: Boolean=False);
    destructor Destroy; override;
    function AddedParticipantOK(aParticipant: TParticipant): EnOutcome;
    function FilteredOK(aParticipant: TParticipant; aZscore: RAddZScore; out MatchCount: Integer): Boolean;
    function IsParticipantPresent(aParticipant: TParticipant): Boolean;
    function SavedToCSVFileOK: Boolean;
    function SavedToDATFileOK: EnOutcome;
    property DataItemCount: Integer read GetDataItemCount;
    property ExperimentIdx: Integer read FExperimentIdx;
    property FemaleCount: Integer read GetFemaleCount;
    property LefthandedCount: Integer read GetLefthandedCount;
    property MaleCount: Integer read GetMaleCount;
    property RighthandedCount: Integer read GetRighthandedCount;
    property AgeSet: TByteSet read GetAgeSet;
    property AgeCount[anAge: Byte]: Byte read GetAgeCount;
    property SessionSet: TByteSet read GetSessionSet;
    property SessionCount[aSessionNo: Byte]: Byte read GetSessionCount;
  end;

  function GetConfigDirectory: String;

  function GetExperimentIdx(aPExperiment: PExperiment): Integer;

  function GotStrFieldFromRawLinesOK(aFieldEnum: EnWMAField; aLineIdx: Integer; out FieldStr: AnsiString): Boolean;

  function GetStatisticName(anID: Integer): String;

  function GetSummaryReportStatIDs: TIntegerDynArray;

var
  processor: TFileProcessor = Nil;
  rawlines: TStringArray = Nil;
  oFiler: ROFiler;
  PExperiments: SaPExperiments;
  configDirectory: String = '';
  enableNormFeatures: Boolean = False;
  currentNormList: TNormList = Nil;
  generatingFirstDAT: Boolean = False;
  settings: TSettings = Nil;

implementation

uses
  Math, StrUtils,
  LazFileUtils, Forms, Dialogs;

function GetConfigDirectory: String;
begin
  Result := IncludeTrailingPathDelimiter(GetCurrentDirUTF8) + CfgSubdirectoryName;
end;

function GetExperimentIdx(aPExperiment: PExperiment): Integer;
begin
  for Result := 0 to High(PExperiments) do
    if aPExperiment = PExperiments[Result] then
      Exit;
  Result := -1;
end;

function GotStrFieldFromRawLinesOK(aFieldEnum: EnWMAField; aLineIdx: Integer; out FieldStr: AnsiString): Boolean;
begin
  Result := GotTrimmedFieldBeforeTabIdx(rawLines[aLineIdx], Succ(Ord(aFieldEnum)), FieldStr)
            and (FieldStr <> '');
end;

function GetStatisticName(anID: Integer): String;
begin
  case ((anID > -1) and (anID < StatisticCount)) of
    True: Result := StatNames[anID];
    False: Result := Format('Error retrieving statistic name for id %d', [anID]);
  end;
end;

function GetSummaryReportStatIDs: TIntegerDynArray;
begin
  Exit(SummaryStatIDs);
end;

{%region TSettings}
procedure TSettings.SetOutputFileDelimiter(aValue: Char);
begin
  if FOutputFileDelimiter = AValue then Exit;
  FOutputFileDelimiter := AValue;
end;

constructor TSettings.Create;
begin
  FOutputFileDelimiter := #9;
  FWarnIfOverwritingOutput := True;
  FShowFullPathInFilename := False;
  GetFilename;
  LoadFromFile;
end;

destructor TSettings.Destroy;
begin
  if not SaveToFile then
    ShowMessageFmt('Unable to save settings to file %s', [FFilename]);
  inherited Destroy;
end;

function TSettings.GetFilename: String;
begin
  FFilename := AppendPathDelim(GetConfigDirectory) + SettingsFilename;
  Result := FFilename;
end;

function TSettings.LoadFromFile: Boolean;
var
  sl: TStringList;
begin
  if FileExistsUTF8(FFilename) then
    begin
      sl := TStringList.Create;
      try
        sl.LoadFromFile(FFilename);
        Result := sl.Count = 3;
        if Result then
          begin
            FOutputFileDelimiter := sl[0][1];
            case sl[1] of
              't': FWarnIfOverwritingOutput := True;
              otherwise
                FWarnIfOverwritingOutput := False;
            end;
            case sl[2] of
              't': FShowFullPathInFilename := True;
              otherwise
                FShowFullPathInFilename := False;
            end;
          end;
      finally
        sl.Free;
      end;
    end;
end;

function TSettings.SaveToFile: Boolean;
var
  sl: TStringList;
begin
  Result := True;
  sl := TStringList.Create;
  try
    sl.Add(FOutputFileDelimiter);
    case FWarnIfOverwritingOutput of
      True:  sl.Add('t');
      False: sl.Add('f');
    end;
    case FShowFullPathInFilename of
      True:  sl.Add('t');
      False: sl.Add('f');
    end;
    try
      sl.SaveToFile(FFilename);
    except on EInOutError do
      Result := False;
    end;
  finally
    sl.Free;
  end;
end;
{%endregion TSettings}

{%region TNormList}
function TNormList.GetDataItemCount: Integer;
begin
  Exit(Length(NormRecArray));
end;

function TNormList.GetFemaleCount: Integer;
var
  nr: RNormSummaryRec;
begin
  Result := 0;
  for nr in NormRecArray do
    case nr.female of
      True:  Inc(Result);
      False: ;
    end;
end;

function TNormList.GetLefthandedCount: Integer;
var
  nr: RNormSummaryRec;
begin
  Result := 0;
  for nr in NormRecArray do
    case nr.rightHanded of
      True:  ;
      False: Inc(Result);
    end;
end;

function TNormList.GetMaleCount: Integer;
var
  nr: RNormSummaryRec;
begin
  Result := 0;
  for nr in NormRecArray do
    case nr.female of
      True:  ;
      False: Inc(Result)
    end;
end;

function TNormList.GetRighthandedCount: Integer;
var
  nr: RNormSummaryRec;
begin
  Result := 0;
  for nr in NormRecArray do
    case nr.rightHanded of
      True:  Inc(Result);
      False: ;
    end;
end;

function TNormList.GetSessionCount(aSessionNo: Byte): Byte;
begin
  GetSessionSet;
  Result := FSessionArray[aSessionNo];
end;

function TNormList.GetSessionSet: TByteSet;
var
  nr: RNormSummaryRec;
begin
  Result := [];
  FSessionArray := Default(SaSession);
  for nr in NormRecArray do
    begin
      Include(Result, nr.sessionNo);
      Inc(FSessionArray[nr.sessionNo]);
    end;
end;

function TNormList.GetZScore(aParticipantValue: Double; aStatID: Integer ): Double;
var
  tmp: array of Double;
  i: Integer;
  mn, stddv: Double;
begin
  SetLength(tmp{%H-}, Length(FMatchingIdxs));
  for i := 0 to High(FMatchingIdxs) do
    tmp[i] := NormRecArray[FMatchingIdxs[i]].GetField(aStatID);

  mn := {%H-}Mean(tmp);
  stddv := StdDev(tmp);
  Result := (aParticipantValue - mn)/stddv;
end;

function TNormList.LoadedFromDATFileOK: EnOutcome;
var
  inf: TFileNormSummaryRec;
  e: Integer = 0;
  count: Integer;
  i: Integer = 0;
begin
  case PExperiments[FExperimentIdx]^.normDATFileExists of
    True:  begin
             count := FileSizeUtf8(PExperiments[FExperimentIdx]^.normDATFilename) div Sizeof(RNormSummaryRec);
             SetLength(NormRecArray, count);
             {$I-}
             AssignFile(inf, PExperiments[FExperimentIdx]^.normDATFilename); if IOResult <> 0 then Inc(e);
             Reset(inf); if IOResult <> 0 then Inc(e);
             while not EOF(inf) do
               begin
                 Read(inf, NormRecArray[i]); if IOResult <> 0 then Inc(e);
                 Inc(i);
               end;
             CloseFile(inf); if IOResult <> 0 then Inc(e);
             {$I+}
             case (e = 0) of
               True:  Result := _oSuccess;
               False: Result := _oNormUnableToLoadFile;
             end;
           end;
    False: Result := _oNormDataFileDoesNotExist;
  end;
end;

function TNormList.Matches(aZS: RAddZScore; aRec: RNormSummaryRec): Boolean;
begin
  Result := True;
  if aZS.sexSpecified and
     (aRec.female <> aZS.female) then
    Exit(False);
  if aZS.sessionSpecified and (aRec.sessionNo > aZS.session) then
    Exit(False);
  if aZS.handedSpecified and (aRec.rightHanded <> aZS.rightHanded) then
    Exit(False);
  if aZS.ageSpecified and ((aRec.age < aZS.loAge) or (aRec.age > aZS.hiAge)) then
    Exit(False);
end;

function TNormList.NormRecToCSVString(nr: RNormSummaryRec): String;
var
  sexStr, handedStr: String;
begin
    case nr.female of
      True:  sexStr := 'female';
      False: sexStr := 'male';
    end;
    case nr.rightHanded of
      True:  handedStr := 'righthanded';
      False: handedStr := 'lefthanded';
    end;
  Result := PExperiments[FExperimentIdx]^.name+settings.OutputFileDelimiter+DateToStr(nr.date)+settings.OutputFileDelimiter+nr.startTime+settings.OutputFileDelimiter+nr.trialOrderFileNo+settings.OutputFileDelimiter+nr.participantID+settings.OutputFileDelimiter+nr.age.ToString+settings.OutputFileDelimiter+sexStr+settings.OutputFileDelimiter+handedStr+settings.OutputFileDelimiter+nr.sessionNo.ToString+settings.OutputFileDelimiter;
  Result := Result+nr.s16.ToString+settings.OutputFileDelimiter+nr.s33.ToString+settings.OutputFileDelimiter+nr.s50.ToString+settings.OutputFileDelimiter+nr.s148.ToString+settings.OutputFileDelimiter+nr.s165.ToString+settings.OutputFileDelimiter+nr.s182.ToString+settings.OutputFileDelimiter+nr.s199.ToString+settings.OutputFileDelimiter;
  Result := Result+nr.s216.ToString+settings.OutputFileDelimiter+nr.s417.ToString+settings.OutputFileDelimiter+nr.s418.ToString+settings.OutputFileDelimiter+nr.s419.ToString+settings.OutputFileDelimiter+nr.s420.ToString+settings.OutputFileDelimiter+nr.s421.ToString+settings.OutputFileDelimiter+nr.s422.ToString+settings.OutputFileDelimiter+nr.s423.ToString+settings.OutputFileDelimiter+nr.s424.ToString+settings.OutputFileDelimiter+nr.s425.ToString+settings.OutputFileDelimiter+nr.s426.ToString+settings.OutputFileDelimiter+nr.s427.ToString+settings.OutputFileDelimiter+nr.s428.ToString+settings.OutputFileDelimiter;
  Result := Result+nr.s429.ToString+settings.OutputFileDelimiter+nr.s430.ToString+settings.OutputFileDelimiter+nr.s431.ToString+settings.OutputFileDelimiter+nr.s432.ToString+settings.OutputFileDelimiter+nr.s433.ToString+settings.OutputFileDelimiter+nr.s434.ToString+settings.OutputFileDelimiter+nr.s444.ToString+settings.OutputFileDelimiter+nr.s445.ToString+settings.OutputFileDelimiter+nr.s446.ToString+settings.OutputFileDelimiter+nr.s447.ToString+settings.OutputFileDelimiter+nr.s448.ToString+settings.OutputFileDelimiter+nr.s449.ToString+settings.OutputFileDelimiter+nr.s450.ToString+settings.OutputFileDelimiter+nr.s451.ToString+settings.OutputFileDelimiter+nr.s452.ToString+settings.OutputFileDelimiter+nr.s453.ToString+settings.OutputFileDelimiter+nr.s454.ToString+settings.OutputFileDelimiter+nr.s455.ToString+settings.OutputFileDelimiter+nr.s456.ToString+settings.OutputFileDelimiter+nr.s457.ToString+settings.OutputFileDelimiter;
  Result := Result+nr.s458.ToString+settings.OutputFileDelimiter+nr.s459.ToString+settings.OutputFileDelimiter+nr.s460.ToString+settings.OutputFileDelimiter+nr.s461.ToString+settings.OutputFileDelimiter+nr.s462.ToString+settings.OutputFileDelimiter+nr.s463.ToString+settings.OutputFileDelimiter+nr.s464.ToString+settings.OutputFileDelimiter+nr.s465.ToString+settings.OutputFileDelimiter+nr.s466.ToString+settings.OutputFileDelimiter+nr.s467.ToString+settings.OutputFileDelimiter+nr.s468.ToString+settings.OutputFileDelimiter+nr.s469.ToString+settings.OutputFileDelimiter+nr.s470.ToString+settings.OutputFileDelimiter+nr.s471.ToString+settings.OutputFileDelimiter+nr.s472.ToString+settings.OutputFileDelimiter+nr.s473.ToString+settings.OutputFileDelimiter+nr.s474.ToString+settings.OutputFileDelimiter+nr.s475.ToString+settings.OutputFileDelimiter+nr.s476.ToString+settings.OutputFileDelimiter+nr.s477.ToString+settings.OutputFileDelimiter+nr.s478.ToString+settings.OutputFileDelimiter+nr.s479.ToString;
end;

function TNormList.SavedToDATFileOK: EnOutcome;
var
  inf: TFileNormSummaryRec;
  e: Integer = 0;
  i: Integer;
begin
   {$I-}
   AssignFile(inf, PExperiments[FExperimentIdx]^.normDATFilename); if IOResult <> 0 then Inc(e);
   Rewrite(inf); if IOResult <> 0 then Inc(e);
   for i := 0 to High(NormRecArray) do
     Write(inf, NormRecArray[i]); if IOResult <> 0 then Inc(e);
   CloseFile(inf); if IOResult <> 0 then Inc(e);
   {$I+}
   case (e = 0) of
     True:  Result := _oSuccess;
     False: Result := _oNormUnableToWriteFile;
   end;
end;

function TNormList.ParticipantDataToNormRecOK(aParticipant: TParticipant;
                                              out nsr: RNormSummaryRec): Boolean;
var
  tmp: Integer;
begin
  nsr := Default(RNormSummaryRec);
  nsr.startTime := aParticipant.startTime;
  nsr.participantID := aParticipant.participantID;
  nsr.trialOrderFileNo := aParticipant.trialOrderFileNo;
  Result := TryStrToDate(aParticipant.date, nsr.date); if not Result then Exit;
  Result := TryStrToInt(aParticipant.age, tmp); if not Result then Exit;
  nsr.age := Byte(tmp);
  Result := TryStrToInt(aParticipant.sessionNo[Length(aParticipant.sessionNo)], tmp); if not Result then Exit;
  nsr.sessionNo := Byte(tmp);
  case aParticipant.sex[1] of
    'F','f': nsr.female := True;
    'M','m': nsr.female := False;
    otherwise Exit(False);
  end;
  case aParticipant.handedness[1] of
    'R','r': nsr.rightHanded := True;
    'L','l': nsr.rightHanded := False;
    otherwise Exit(False);
  end;
  nsr.UCLongID := aParticipant.participantID + aParticipant.startTime + aParticipant.sessionNo;
  nsr.s16 := aParticipant.StatisticValue[16]; nsr.s33 := aParticipant.StatisticValue[33]; nsr.s50 := aParticipant.StatisticValue[50];
  nsr.s148 := aParticipant.StatisticValue[148]; nsr.s165 := aParticipant.StatisticValue[165]; nsr.s182 := aParticipant.StatisticValue[182];
  nsr.s199 := aParticipant.StatisticValue[199]; nsr.s216 := aParticipant.StatisticValue[216]; nsr.s417 := aParticipant.StatisticValue[417];
  nsr.s418 := aParticipant.StatisticValue[418]; nsr.s419 := aParticipant.StatisticValue[419]; nsr.s420 := aParticipant.StatisticValue[420];
  nsr.s421 := aParticipant.StatisticValue[421]; nsr.s422 := aParticipant.StatisticValue[422]; nsr.s423 := aParticipant.StatisticValue[423];
  nsr.s424 := aParticipant.StatisticValue[424]; nsr.s425 := aParticipant.StatisticValue[425]; nsr.s426 := aParticipant.StatisticValue[426];
  nsr.s427 := aParticipant.StatisticValue[427]; nsr.s428 := aParticipant.StatisticValue[428]; nsr.s429 := aParticipant.StatisticValue[429];
  nsr.s430 := aParticipant.StatisticValue[430]; nsr.s431 := aParticipant.StatisticValue[431]; nsr.s432 := aParticipant.StatisticValue[432];
  nsr.s433 := aParticipant.StatisticValue[433]; nsr.s434 := aParticipant.StatisticValue[434]; nsr.s444 := aParticipant.StatisticValue[444];
  nsr.s445 := aParticipant.StatisticValue[445]; nsr.s446 := aParticipant.StatisticValue[446]; nsr.s447 := aParticipant.StatisticValue[447];
  nsr.s448 := aParticipant.StatisticValue[448]; nsr.s449 := aParticipant.StatisticValue[449]; nsr.s450 := aParticipant.StatisticValue[450];
  nsr.s451 := aParticipant.StatisticValue[451]; nsr.s452 := aParticipant.StatisticValue[452]; nsr.s453 := aParticipant.StatisticValue[453];
  nsr.s454 := aParticipant.StatisticValue[454]; nsr.s455 := aParticipant.StatisticValue[455]; nsr.s456 := aParticipant.StatisticValue[456];
  nsr.s457 := aParticipant.StatisticValue[457]; nsr.s458 := aParticipant.StatisticValue[458]; nsr.s459 := aParticipant.StatisticValue[459];
  nsr.s460 := aParticipant.StatisticValue[460]; nsr.s461 := aParticipant.StatisticValue[461]; nsr.s462 := aParticipant.StatisticValue[462];
  nsr.s463 := aParticipant.StatisticValue[463]; nsr.s464 := aParticipant.StatisticValue[464]; nsr.s465 := aParticipant.StatisticValue[465];
  nsr.s466 := aParticipant.StatisticValue[466]; nsr.s467 := aParticipant.StatisticValue[467]; nsr.s468 := aParticipant.StatisticValue[468];
  nsr.s469 := aParticipant.StatisticValue[469]; nsr.s470 := aParticipant.StatisticValue[470]; nsr.s471 := aParticipant.StatisticValue[471];
  nsr.s472 := aParticipant.StatisticValue[472]; nsr.s473 := aParticipant.StatisticValue[473]; nsr.s474 := aParticipant.StatisticValue[474];
  nsr.s475 := aParticipant.StatisticValue[475]; nsr.s476 := aParticipant.StatisticValue[476]; nsr.s477 := aParticipant.StatisticValue[477];
  nsr.s478 := aParticipant.StatisticValue[478]; nsr.s479 := aParticipant.StatisticValue[479];
end;

constructor TNormList.Create(aProcessor: TFileProcessor; anExptIdx: Integer; out Outcome: EnOutcome; fromScratch: Boolean);
var
  i: Integer;
  nsr: RNormSummaryRec;
begin
  Outcome := _oSuccess;
  SetLength(NormRecArray, 0);
  FExperimentIdx := anExptIdx;

  case fromScratch of
    True:  begin
             Outcome := _oSuccess;
             SetLength(NormRecArray, 0);
             SetLength(NormRecArray, aProcessor.ParticipantCount);
             for i := 0 to High(NormRecArray) do
             begin
               nsr := Default(RNormSummaryRec);
               if not ParticipantDataToNormRecOK(aProcessor.Participants[i], nsr) then
                 begin
                   Outcome := _oNormUnableToConvertParticipant;
                   Break;
                 end;
               NormRecArray[i] := nsr;
             end;
             if Outcome <> _oSuccess then
               Exit;
             outcome := SavedToDATFileOK;
             if Outcome = _oSuccess then
               case SavedToCSVFileOK of
                 True:  ;
                 False: Outcome := _oNormUnableToWriteFile;
               end;
           end;

    False: begin
             Assert(PExperiments[anExptIdx]^.supportsNormativeData,'TNormList.Create: experiment does not support normative data');
             Assert(PExperiments[anExptIdx]^.normDATFileExists,'TNormList.Create: normative data file does not exist');
             Outcome := LoadedFromDATFileOK;
             if Outcome = _oSuccess then
               case SavedToCSVFileOK of
                 True:  ;
                 False: Outcome := _oNormUnableToWriteFile;
               end;
           end;
  end;
end;

destructor TNormList.Destroy;
begin
  if SavedToDATFileOK <> _oSuccess then
    ShowMessageFmt('Failed to save normative data to file %s', [PExperiments[FExperimentIdx]^.normDATFilename]);
  if not SavedToCSVFileOK then
    ShowMessageFmt('Failed to save normative data to file %s', [PExperiments[FExperimentIdx]^.normCSVFilename]);
  SetLength(NormRecArray, 0);
  inherited Destroy;
end;

function TNormList.AddedParticipantOK(aParticipant: TParticipant): EnOutcome;
var
  nsr: RNormSummaryRec;
begin
  nsr := Default(RNormSummaryRec);
  case IsParticipantPresent(aParticipant) of
    True: Exit(_oNormParticipantAlreadyPresent);
    False: begin
             case ParticipantDataToNormRecOK(aParticipant, nsr) of
               True: begin
                       SetLength(NormRecArray, Succ(Length(NormRecArray)));
                       NormRecArray[High(NormRecArray)] := nsr;
                       Result := _oSuccess;
                     end;
               False: Result := _oNormUnableToConvertParticipant;
             end;
           end;
  end;
end;

function TNormList.FilteredOK(aParticipant: TParticipant; aZscore: RAddZScore;
                              out MatchCount: Integer): Boolean;
var
  i: Integer;
begin
  Result := True;
  SetLength(FMatchingIdxs{%H-}, Length(NormRecArray));
  MatchCount := -1;
  for i := 0 to High(NormRecArray) do
    begin
      if Matches(aZscore, NormRecArray[i]) then
        begin
          Inc(MatchCount);
          FMatchingIdxs[MatchCount] := i;
        end;
    end;
  Inc(MatchCount);

  SetLength(FMatchingIdxs, MatchCount);
  Result := MatchCount >= MinViableNormCount;

  if Result then
    begin
      for i := 0 to HighSummaryReportIndex do
        begin
          ParticipantNormValues[i].value := aParticipant.statsArray[SummaryStatIDs[i]].value;
          ParticipantNormValues[i].zScore := GetZScore(ParticipantNormValues[i].value, SummaryStatIDs[i]);
          ZtoPCandP(ParticipantNormValues[i].zScore, ParticipantNormValues[i].percentile, ParticipantNormValues[i].pValue);
        end;
    end;
end;

function TNormList.GetAgeCount(anAge: Byte): Byte;
begin
  GetAgeSet;
  Exit(FAgeArray[anAge]);
end;

function TNormList.GetAgeSet: TByteSet;
var
  nr: RNormSummaryRec;
begin
  Result := [];
  FAgeArray := Default(SaAge);
  for nr in NormRecArray do
    begin
      Include(Result, nr.age);
      Inc(FAgeArray[nr.age]);
    end;
end;

function TNormList.IsParticipantPresent(aParticipant: TParticipant): Boolean;
var
  partLongID: String;
  i: Integer;
begin
  Result := False;

  partLongID := aParticipant.participantID + aParticipant.startTime + aParticipant.sessionNo;
  for i := 0 to High(NormRecArray) do
    begin
      if Length(partLongID) <> Length(NormRecArray[i].UCLongID) then
        Continue;
      if partLongID[14] <> NormRecArray[i].UCLongID[14] then
        Continue;
      if partLongID[15] <> NormRecArray[i].UCLongID[15] then
        Continue;
      if SameText(NormRecArray[i].UCLongID, partLongID) then
        Exit(True);
    end;
  Result := False;
end;

function TNormList.SavedToCSVFileOK: Boolean;
var
  sl: TStringList;
  i: Integer;
begin
  Result := True;
  sl := TStringList.Create;
  try
    case settings.OutputFileDelimiter of
      #9:  sl.Add(rsNormHeaderLine);
      ',': sl.Add(rsNormHeaderLineComma);
    end;
    for i := 0 to High(NormRecArray) do
      sl.Add(NormRecToCSVString(NormRecArray[i]));
    try
      sl.SaveToFile(PExperiments[FExperimentIdx]^.normCSVFilename);
    except on EInOutError do
      Result := False;
    end;
  finally
    sl.Free;
  end;
end;
{%endregion TNormList}

{%region RNormSummaryRec}
function RNormSummaryRec.GetField(aStatID: Integer): Double;
begin
  case aStatID of
    16: Result := s16; 33: Result := s33; 50: Result := s50; 148: Result := s148; 165: Result := s165; 182: Result := s182;
    199: Result := s199; 216: Result := s216; 417: Result := s417; 418: Result := s418; 419: Result := s419; 420: Result := s420;
    421: Result := s421; 422: Result := s422; 423: Result := s423; 424: Result := s424; 425: Result := s425; 426: Result := s426;
    427: Result := s427; 428: Result := s428; 429: Result := s429; 430: Result := s430; 431: Result := s431; 432: Result := s432;
    433: Result := s433; 434: Result := s434; 444: Result := s444;
    445: Result := s445; 446: Result := s446; 447: Result := s447; 448: Result := s448; 449: Result := s449; 450: Result := s450;
    451: Result := s451; 452: Result := s452; 453: Result := s453; 454: Result := s454; 455: Result := s455; 456: Result := s456;
    457: Result := s457; 458: Result := s458; 459: Result := s459; 460: Result := s460; 461: Result := s461; 462: Result := s462;
    463: Result := s463; 464: Result := s464; 465: Result := s465; 466: Result := s466; 467: Result := s467; 468: Result := s468;
    469: Result := s469; 470: Result := s470; 471: Result := s471; 472: Result := s472; 473: Result := s473; 474: Result := s474;
    475: Result := s475; 476: Result := s476; 477: Result := s477; 478: Result := s478; 479: Result := s479;
  end;
end;
{%endregion RNormSummaryRec}

{%region RStat}
procedure RStat.Init(anIsFloat: Boolean; aValue: Double);
begin
  isFloat := anIsFloat;
  value := aValue;
end;
{%endregion RStat}

{%region ROFiler}
function ROFiler.Init(aFilename: AnsiString; out FileExists: Boolean): EnOutcome;
begin
  fName := aFilename;
  fe := 0;
  FileExists := FileExistsUTF8(aFilename);
  {$I-}
    AssignFile(txtF, aFilename);  if IOResult <> 0 then Inc(fe);
    Rewrite(txtF);                if IOResult <> 0 then Inc(fe);
    CloseFile(txtF);              if IOResult <> 0 then Inc(fe);
  {$I+}
  case (fe = 0) of
    True:  Result := _oSuccess;
    False: Result := _oOutputFileError;
  end;
end;

function ROFiler.AddLine(const aText: AnsiString): EnOutcome;
begin
  {$I-}
    AssignFile(txtF, fName);  if IOResult <> 0 then Inc(fe);
    Append(txtF);                 if IOResult <> 0 then Inc(fe);
    WriteLn(txtF, aText);         if IOResult <> 0 then Inc(fe);
    CloseFile(txtF);              if IOResult <> 0 then Inc(fe);
  {$I+}
  case (fe = 0) of
    True:  Result := _oSuccess;
    False: Result := _oOutputFileError;
  end;
end;

function ROFiler.AppendToLastLine(const aText: AnsiString; addTrailingTab: Boolean): EnOutcome;
begin
  {$I-}
    AssignFile(txtF, fName);          if IOResult <> 0 then Inc(fe);
    Append(txtF);                     if IOResult <> 0 then Inc(fe);
    case addTrailingTab of
      True:  Write(txtF, aText + #9);
      False: Write(txtF, aText);
    end;                             if IOResult <> 0 then Inc(fe);
    CloseFile(txtF);                 if IOResult <> 0 then Inc(fe);
  {$I+}
  case (fe = 0) of
    True:  Result := _oSuccess;
    False: Result := _oOutputFileError;
  end;
end;
{%endregion ROFiler}

{%region RBehest}
function RBehest.ExecutedOnParticipantOK(aParticipant: TParticipant): EnOutcome;
var
  i, j, minimum, maximum: Integer;
  fld: String;
  intValues, sorted, filled: TIntegerDynArray;
  doubleArray: array of Double;
  k: Longint;
  fence, quart25, quart75, minS, maxS, v1, v2, v3, v4: Double;
  stat: RStat;

      function GetIndicesOf(val: Integer): TIntegerDynArray; // return array of indices (if any) of matching values
      var
        n: Integer;
        hits: Integer= -1;
      begin
        Result := Nil;
        for n := 0 to High(intValues) do
          if intValues[n] = val then
            begin
              Inc(hits);
              case hits of
                0: SetLength(Result, Length(intValues) - n);
                else ;
              end;
              Result[hits] := n;
            end;
        case hits of
          -1: Result := Nil;
          else SetLength(Result, Succ(hits));
        end;
      end;

        function GetQuartile(aPercent: Byte): Double;
        var
          index: Integer = 0;
          rem:   Integer = 0;
        begin
          DivMod(aPercent*Length(sorted), 100, index, rem);
          Dec(index);
          case rem of
            0:   Result := (sorted[index] + sorted[Succ(index)])/2; // index is an integer
            otherwise
              Result := sorted[Succ(index)]/1.0;                    // index is not an integer
          end;
        end;

          function Get3FilteredValues(isAccuracy: Boolean): TIntegerDynArray;
          var
            i: Integer;
            fdi: RFieldsData;
          begin
            SetLength(Result{%H-}, 0);
            for i := 0 to High(aParticipant.CurrentFilter.fieldsDataArr) do
              begin
                fdi := aParticipant.CurrentFilter.fieldsDataArr[i];
               // WriteLn(i, ' fdi1 ',fdi.factor1,' lvlfacstr ',aParticipant.lvlFacStrings[Cmp.fla[fdFactor_1]][fdFactor_1]);
               // WriteLn(i, ' fdi2 ',fdi.factor2,' lvlfacstr ',aParticipant.lvlFacStrings[Cmp.fla[fdFactor_2]][fdFactor_2]);
               // WriteLn(i, ' fdi5 ',fdi.factor5,' lvlfacstr ',aParticipant.lvlFacStrings[Cmp.fla[fdFactor_5]][fdFactor_5]);
                if SameStr(fdi.factor1, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_1]][fdFactor_1]) and
                   SameStr(fdi.factor2, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_2]][fdFactor_2]) and
                   SameStr(fdi.factor5, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_5]][fdFactor_5]) then
                    begin
                      SetLength(Result, Succ(Length(Result)));
                      case isAccuracy of
                        True:  Result[High(Result)] := StrToInt(fdi.accuracy);
                        False: Result[High(Result)] := fdi.rtmsMinusError;
                      end;
                    end;
               // WriteLn('Length(TIntegerDynArray) 3f: ',Length(Result));
              end;
          end;

          function CalcStatA(diff, same: Double): Double;
          begin
            case ({%H-}IsZero(1-diff) or {%H-}IsZero(same)) of
              True:  Result := 0;
              False: begin
                       Result := 0.75 + (same - diff)/4;
                       if (diff > 0.5) and (same > diff) then
                         Result := Result - (1-same)/(4*(1-diff))
                       else
                         if (diff <= same) and (same < 0.5) then
                           Result := Result - diff/(4*same)
                         else
                           if (diff <= 0.5) and (same >= 0.5) then
                             Result := Result - diff*(1-same)
                           else
                             Result := 0;
                     end;
            end;
          end;
        {  =IF(AND(diff<=0.5,same>=0.5),
              3/4+(same-diff)/4-diff*(1-same),
              IF(                             AND(diff<=same,same<0.5),
                                              3/4 + (same-diff)/4 - diff/(4*same),
                                              IF(                             AND(diff>0.5,same>diff),
                                                                              3/4 + (same-diff)/4 - (1-same)/(4*(1-diff)),
                                                                              0))) }
              function Get4FilteredAccuracies: TIntegerDynArray;
              var
                i: Integer;
                fdi: RFieldsData;
              begin
                SetLength(Result{%H-}, 0);
                for i := 0 to High(aParticipant.CurrentFilter.fieldsDataArr) do
                  begin
                    fdi := aParticipant.CurrentFilter.fieldsDataArr[i];
                   // WriteLn(i, ' fdi1 ',fdi.factor1,' lvlfacstr ',aParticipant.lvlFacStrings[Cmp.fla[fdFactor_1]][fdFactor_1]);
                   // WriteLn(i, ' fdi2 ',fdi.factor2,' lvlfacstr ',aParticipant.lvlFacStrings[Cmp.fla[fdFactor_2]][fdFactor_2]);
                   // WriteLn(i, ' fdi5 ',fdi.factor5,' lvlfacstr ',aParticipant.lvlFacStrings[Cmp.fla[fdFactor_5]][fdFactor_5]);
                    if SameStr(fdi.factor1, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_1]][fdFactor_1]) and
                       SameStr(fdi.factor2, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_2]][fdFactor_2]) and
                       SameStr(fdi.factor4, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_4]][fdFactor_4]) and
                       SameStr(fdi.factor5, aParticipant.lvlFacStrings[Cmp.fla[fdFactor_5]][fdFactor_5]) then
                        begin
                          SetLength(Result, Succ(Length(Result)));
                          Result[High(Result)] := StrToInt(fdi.accuracy);
                        end;
                  //  WriteLn('Length(TIntegerDynArray) 4f: ',Length(Result));
                  end;
              end;

begin
  Assert(Assigned(aParticipant), 'RBehest.ExecutedOnParticipantOK: Nil participant parameter');
  Result := _oSuccess;
  case rv.bk of
    f: begin
         Result := aParticipant.CopiedNextFilterArrayOK(Flt.GetName, Flt.id, Flt.kind = fkAccuracy);
         if Result <> _oSuccess then Exit;

         case Flt.kind of
           fkFactor1:
             begin
               for i := High(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) downto 0 do
                 begin
                   fld := '';
                   j := aParticipant.filters[aParticipant.FCurrentFilterIdx].lines[i];
                   Result := aParticipant.GotStrFieldFromCurrFilterLinesOK(Flt.fld, j, fld);
                   if Result <> _oSuccess then
                     Exit;
                   if SameText(Flt.fv, fld) then
                     begin
                       Result := aParticipant.DeleteCurrFilterIndex(i);
                       if Result <> _oSuccess then
                         Exit;
                     end
                 end;
             //  WriteLn('Executed filter',Flt.kind,' on field ',Flt.fld,' value "',Flt.fv,'", part.filter#',aParticipant.FCurrentFilterIdx,'  deleting ',aParticipant.filters[aParticipant.FCurrentFilterIdx].deleteCount,' line count:',Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines),#10);
               if Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) = 0 then
                 begin
                   aParticipant.invalidData := True;
                   Exit(oParticipant_HasInvalidData);
               end;
             end;
           fkAccuracy:
             begin
               for i := High(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) downto 0 do
                 begin
                   fld := '';
                   j := aParticipant.filters[aParticipant.FCurrentFilterIdx].lines[i];
                   Result := aParticipant.GotStrFieldFromCurrFilterLinesOK(Flt.fld, j, fld);
                   if Result <> _oSuccess then
                     Exit;
                   if SameText(Flt.fv, fld) then
                     begin
                       Result := aParticipant.DeleteCurrFilterIndex(i);
                       if Result <> _oSuccess then
                         Exit;
                     end
                 end;
            //   WriteLn('Executed filter',Flt.kind,' on field ',Flt.fld,' value "',Flt.fv,'", part.filter#',aParticipant.FCurrentFilterIdx,'  deleting ',aParticipant.filters[aParticipant.FCurrentFilterIdx].deleteCount,' line count:',Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines),#10);
             end;
           fkUserPausedTrial:
             begin
               for i := High(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) downto 0 do
                 begin
                   fld := '';
                   j := aParticipant.filters[aParticipant.FCurrentFilterIdx].lines[i];
                   Result := aParticipant.GotStrFieldFromCurrFilterLinesOK(Flt.fld, j, fld);
                   if Result <> _oSuccess then
                     Exit;
                   if SameText(Flt.fv, fld) then
                     begin
                       Result := aParticipant.DeleteCurrFilterIndex(i);
                       if Result <> _oSuccess then
                         Exit;
                     end
                 end;
           //    WriteLn('Executed filter',Flt.kind,' on field ',Flt.fld,' value "',Flt.fv,'", part.filter#',aParticipant.FCurrentFilterIdx,'  deleting ',aParticipant.filters[aParticipant.FCurrentFilterIdx].deleteCount,' line count:',Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines),#10);
             end;

           fkRTAbsolute:
             begin
               for i := High(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) downto 0 do
                 begin
                   fld := '';
                   Result := aParticipant.GotStrFieldFromCurrFilterLinesOK(Flt.fld, aParticipant.filters[aParticipant.FCurrentFilterIdx].lines[i], fld);
                   if Result <> _oSuccess then
                     Exit;
                   if not TryStrToInt(fld, j) then
                     Exit(_oFilterCouldNotConvertStrField);
                   if (j < Flt.minO) or (j > Flt.maxO) then
                     begin
                       Result := aParticipant.DeleteCurrFilterIndex(i);
                       if Result <> _oSuccess then
                         Exit;
                     end;
                 end;
               //WriteLn(aParticipant.participantID,' Executed filter',Flt.kind,' on field ',Flt.fld,' value "',Flt.fv,'", part.filter#',aParticipant.FCurrentFilterIdx,'  deleting ',aParticipant.filters[aParticipant.FCurrentFilterIdx].deleteCount,' line count:',Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines),#10);
             end;

           fkRTRelMultiplier:
             begin
               j := Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines);
               SetLength(intValues{%H-}, j);
               SetLength(sorted{%H-}, j);

               minimum := MaxInt;
               maximum := -1;
               for i := 0 to High(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) do
                 begin
                   fld := '';
                   Result := aParticipant.GotStrFieldFromCurrFilterLinesOK(Flt.fld, aParticipant.filters[aParticipant.FCurrentFilterIdx].lines[i], fld);
                   if Result <> _oSuccess then
                     Exit;
                   if not TryStrToInt(fld, k) then
                     Exit(_oFilterCouldNotConvertStrField);
                   intValues[i] := k;                // populate intValues
                   if minimum > k then
                     minimum := k;
                   if maximum < k then
                     maximum := k;
                 end;

               j := 0;
               filled := Nil;
               for i := minimum to maximum do
                 begin
                   filled := GetIndicesOf(i);
                   if Length(filled) > 0 then
                     begin
                       for k := 0 to High(filled) do
                         sorted[j+k] := i;
                       Inc(j, Length(filled));
                     end;
                 end;

               quart25 := GetQuartile(25);
               quart75 := GetQuartile(75);
               fence := Flt.mult * (quart75 - quart25);
               minS := quart25 - fence;
               maxS := quart75 + fence;
               //WriteLn('filter: min=',mins:3:1,'  max=',maxs:3:1,'  q25=',quart25:3:1,'  q75=',quart75:3:1,' fence=',fence:3:1);
               //for i := low(sorted) to High(sorted) do
               //  write(sorted[i],' (',i,') ');
               //WriteLn;
               for i := High(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines) downto 0 do
                 begin
                   fld := '';
                   Result := aParticipant.GotStrFieldFromCurrFilterLinesOK(Flt.fld, aParticipant.filters[aParticipant.FCurrentFilterIdx].lines[i], fld);
                   if Result <> _oSuccess then
                     Exit;
                   if not TryStrToFloat(fld, fence) then
                     Exit(_oFilterCouldNotConvertStrField);
                   if (fence < minS) or (fence > maxS) then
                     begin
                       Result := aParticipant.DeleteCurrFilterIndex(i);
                       if Result <> _oSuccess then
                         Exit;
                     end;
                 end;
               //WriteLn('Executed filter',Flt.kind,' on field ',Flt.fld,' value "',Flt.fv,'", part.filter#',aParticipant.FCurrentFilterIdx,'  deleting ',aParticipant.filters[aParticipant.FCurrentFilterIdx].deleteCount,' line count:',Length(aParticipant.filters[aParticipant.FCurrentFilterIdx].lines),#10);
           end;
         end;
       end;

    c: begin
         case Cmp.cs of
           cs2s: case Cmp.ck of
                   ckCow: stat.Init(Cmp.isf, Cmp.cow * (aParticipant.statsArray[Cmp.sn1].value + aParticipant.statsArray[Cmp.sn2].value - 1));
                   ckA: stat.Init(Cmp.isf, CalcStatA(aParticipant.statsArray[Cmp.sn1].value, aParticipant.statsArray[Cmp.sn2].value));
                            {truncName := 'MEAN_ACCURACY' + Copy(Compute.statisticName, 2); // e.g. A_Load_1_CueDis_Diff_Tar_DH_
                            diffVal := GetStat(truncName + 'CUETAR_DIFF_');
                            sameVal := GetStat(truncName + 'CUETAR_SAME_'); }
                   ckDP: begin // a=CueTar_DIFF, B=CueTar_SAME    =NORMSINV(B4)-NORMSINV(A4)
                                 {truncName := 'MEAN_ACCURACY' + Copy(Compute.statisticName, 8); // e.g. d-prime_Load_1_CueDis_Diff_Tar_DH_
                                 diffVal := GetStat(truncName + 'CUETAR_DIFF_');
                                 sameVal := GetStat(truncName + 'CUETAR_SAME_');}
                           if IsZero(aParticipant.statsArray[Cmp.sn1].value) or IsZero(aParticipant.statsArray[Cmp.sn2].value) or
                              IsZero(1 - aParticipant.statsArray[Cmp.sn1].value) or IsZero(1 - aParticipant.statsArray[Cmp.sn2].value) then
                              stat.Init(Cmp.isf, 0)
                           else
                             stat.Init(Cmp.isf, InvNormalDist(aParticipant.statsArray[Cmp.sn2].value) - InvNormalDist(aParticipant.statsArray[Cmp.sn1].value));
                         end;
                   otherwise
                     Result := _oExecuteBehestError;
                 end;
           cs3f: begin
                   case Cmp.fld of
                     fdAccuracy:                   begin
                        intValues := Get3FilteredValues(True);
                      {  for i := 0 to High(intValues) do
                          write(intValues[i],' ');
                        WriteLn; }
                     end;
                     fdRT_ms_minus_constant_error: intValues := Get3FilteredValues(False);
                     otherwise
                       Exit(_oExecuteBehestError);
                   end;

                   case Cmp.ck of
                     ckCnt:   begin
                                stat.Init(Cmp.isf, Length(intValues));
                               // WriteLn(cmp.ck,' ',cmp.fld,' ',cmp.cs,' ckcnt: ',stat.value:3:5);
                              end;
                     ckMean:  begin
                                stat.Init(Cmp.isf, Mean(intValues));
                               // WriteLn('ckmean: ',stat.value:3:5);
                              end;
                     ckErrR:  begin
                                SetLength(sorted, 0);
                                SetLength(sorted, Length(intValues));
                                for i := 0 to High(intValues) do
                                  sorted[i] := (1 - intValues[i]) * 100;
                                 stat.Init(Cmp.isf, Mean(sorted));
                               // WriteLn('ckErrR: ',stat.value:3:5);
                              end;
                     ckArc:   begin
                                stat.Init(Cmp.isf, TwoOverPi * ArcSin(Sqrt(Mean(intValues))));
                                //WriteLn('ckArc: ',stat.value:3:5);
                              end;
                     ckSqrtB: begin
                                SetLength(doubleArray{%H-}, 0);
                                SetLength(doubleArray, Length(intValues));
                                for i := 0 to High(intValues) do
                                  doubleArray[i] := Sqrt(intValues[i]);
                                stat.Init(Cmp.isf, Mean(doubleArray));
                               // WriteLn('ckSqrtB: ', stat.value:3:5);
                              end;
                     ckSqrt:  begin
                                stat.Init(Cmp.isf, Sqrt(Mean(intValues)));
                               // WriteLn('ckSqrt: ',stat.value:3:5);
                              end;
                     ckLnB:   begin
                                SetLength(doubleArray, 0);
                                SetLength(doubleArray, Length(intValues));
                                for i := 0 to High(intValues) do
                                  doubleArray[i] := Ln(intValues[i]);
                                stat.Init(Cmp.isf, Mean(doubleArray));
                               // WriteLn('ckLnB: ',stat.value:3:5);
                              end;
                     ckLn:    begin
                                stat.Init(Cmp.isf, Ln(Mean(intValues)));
                               // WriteLn('ckLn: ',stat.value:3:5);
                              end;
                     otherwise
                       Result := _oExecuteBehestError;
                   end
                 end;
           cs4f: begin
                   intValues := Get4FilteredAccuracies;
                   case Cmp.ck of
                     ckCnt:  stat.Init(Cmp.isf, Length(intValues));
                     ckMean: stat.Init(Cmp.isf, Mean(intValues));
                     otherwise
                       Result := _oExecuteBehestError;
                   end
                 end;
           csDiff:  stat.Init(Cmp.isf, (aParticipant.statsArray[Cmp.sn1].value + aParticipant.statsArray[Cmp.sn2].value - aParticipant.statsArray[Cmp.sn3].value - aParticipant.statsArray[Cmp.sn4].value)/2);
           csDDiff: stat.Init(Cmp.isf, (aParticipant.statsArray[Cmp.sn1].value + aParticipant.statsArray[Cmp.sn4].value - aParticipant.statsArray[Cmp.sn3].value - aParticipant.statsArray[Cmp.sn2].value));
           csMMS: begin
                    v1 := aParticipant.statsArray[Cmp.sn1].value ;
                    v2 := aParticipant.statsArray[Cmp.sn2].value;
                    v3 := aParticipant.statsArray[Cmp.sn3].value;
                    v4 := aParticipant.statsArray[Cmp.sn4].value;
                    case Cmp.ck of
                      ckMax: stat.Init(Cmp.isf, Max(Max(Max(v1, v2), v3), v4));
                      ckMin: stat.Init(Cmp.isf, Min(Min(Min(v1, v2), v3), v4));
                      ckSum: stat.Init(Cmp.isf, v1 + v2 + v3 + v4);
                      otherwise
                        Result := _oExecuteBehestError;
                    end;
                  end;
           csSummMean: begin
                         SetLength(doubleArray, 0);
                         SetLength(doubleArray, Succ(Cmp.sn2 - Cmp.sn1));
                         j := Cmp.sn1;
                         for i := 0 to High(doubleArray) do
                           begin
                             doubleArray[i] := aParticipant.statsArray[j].value;
                             Inc(j);
                           end;
                         stat.Init(Cmp.isf, Mean(doubleArray));
                       end;
           csSummCount: begin
                         SetLength(doubleArray, 0);
                         SetLength(doubleArray, Succ(Cmp.sn2 - Cmp.sn1));
                         j := Cmp.sn1;
                         for i := 0 to High(doubleArray) do
                           begin
                             doubleArray[i] := aParticipant.statsArray[j].value;
                             Inc(j);
                           end;
                         stat.Init(Cmp.isf, Sum(doubleArray));
                       end;
         end;
         Result := aParticipant.SetStatisticOK(Cmp.id, stat{%H-})
       end;

    a: case rv.af of
         fdExperiment:       oFiler.AppendToLastLine(aParticipant.experiment);
         fdDate:             oFiler.AppendToLastLine(aParticipant.date);
         fdStart_Time:       oFiler.AppendToLastLine(aParticipant.startTime);
      fdTrial_Order_File_No: oFiler.AppendToLastLine(aParticipant.trialOrderFileNo);
         fdParticipantID:    oFiler.AppendToLastLine(aParticipant.participantID);
         fdAge:              oFiler.AppendToLastLine(aParticipant.age);
         fdSex:              oFiler.AppendToLastLine(aParticipant.sex);
         fdHandedness:       oFiler.AppendToLastLine(aParticipant.handedness);
         fdSession_no:       oFiler.AppendToLastLine(aParticipant.sessionNo);
         otherwise Assert(False,'unexpected field to add to output file');
       end;
  end;
end;
{%endregion RBehest}

{%region TParticipant}
function TParticipant.GetCurrentFilter: RFilter;
begin
  case ((FCurrentFilterIdx > -1) and (FCurrentFilterIdx < Length(filters))) of
    True:  Result := filters[FCurrentFilterIdx];
    False: Result := Default(RFilter);
  end;
end;

function TParticipant.GetHeaderInfoText: String;
begin
  Result := Format('%s - %s years old - %s - %s handed - Experiment: %s - Session %s',
    [participantID, age, sex, handedness, experiment, sessionNo]);
end;

function TParticipant.GetStatisticValue(anID: Integer): Double;
begin
  case (anID <= HighStatID) of
    True:  Exit(statsArray[anID].value);
    False: Exit(NaN);
  end;
end;

function TParticipant.GetUnfilteredLineCountXclTraining: Integer;
begin
  case (filters = Nil) of
    True:  Exit(MaxInt);
    False: Exit(Length(filters[0].lines));
  end;
end;

function TParticipant.ParsedAddFieldsOK(aLineIdx: Integer): Boolean;
begin
       if not GotStrFieldFromRawLinesOK(fdAge,                 aLineIdx, age) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdDate,                aLineIdx, date) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdExperiment,          aLineIdx, experiment) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdHandedness,          aLineIdx, handedness) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdParticipantID,       aLineIdx, participantID) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdSession_no,          aLineIdx, sessionNo) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdSex,                 aLineIdx, sex) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdStart_Time,          aLineIdx, startTime) then Exit(False)
  else if not GotStrFieldFromRawLinesOK(fdTrial_Order_File_No, aLineIdx, trialOrderFileNo) then Exit(False);
  Result := True;
end;

function TParticipant.PopulatedDataFieldsAndFactorsOfCurrentFilterOK: EnOutcome;
var
  sAcc, sRTMinusError, sF1, sF2, sF4, sF5: AnsiString;
  i, rtme, line: Integer;
begin
  Result := _oSuccess;
  SetLength(filters[FCurrentFilterIdx].fieldsDataArr, 0);
  SetLength(filters[FCurrentFilterIdx].fieldsDataArr, Length(CurrentFilter.lines));

  for i := 0 to High(CurrentFilter.lines) do
    begin
      line := CurrentFilter.lines[i];

      if not GotStrFieldFromRawLinesOK(fdAccuracy, line, sAcc) or
         not GotStrFieldFromRawLinesOK(fdFactor_1, line, sF1) or
         not GotStrFieldFromRawLinesOK(fdFactor_2, line, sF2) or
         not GotStrFieldFromRawLinesOK(fdFactor_4, line, sF4) or
         not GotStrFieldFromRawLinesOK(fdFactor_5, line, sF5) or
         not GotStrFieldFromRawLinesOK(fdRT_ms_minus_constant_error, line, sRTMinusError) then
           Exit(oParticipantCouldNotReadStrField);

      if (sAcc = '') or (sF1 = '') or (sF2 = '') or (sF4 = '') or (sF5 = '') or (sRTMinusError = '') then
        Exit(oParticipantFieldIsBlank);
      if not TryStrToInt(sRTMinusError, rtme) then
        Exit(oParticipantCouldNotConvertStrField);

      CurrentFilter.fieldsDataArr[i].Init(sAcc, sF1, sF2, sF4, sF5, rtme);
    end;
//  WriteLn('Populated curr filter (#',CurrentFilter.id,') ',CurrentFilter.name,' del count=',CurrentFilter.deleteCount,' len(Lines)=',Length(CurrentFilter.lines));
  if not SetFactorStringsOfCurrentFilterOK then
    Exit(oParticipantFilterSettingError);
end;

function TParticipant.SetFactorStringsOfCurrentFilterOK: Boolean;
var
  line: Integer;
  sl: TStringList;
  facFld: RgFactors;
  lvl: EnLevels;
  fldarr: RFieldsData;

begin
  Result := True;
  lvlFacStrings := Default(SaLvlFFStrings);
  sl := TStringList.Create;
  try
    sl.Duplicates := dupIgnore;
    sl.Sorted := True;
    for facFld := Low(RgFactors) to High(RgFactors) do
      begin
        if facFld = fdFactor_3 then Continue; // skip unused field

        sl.Clear;
        for line := Low(filters[FCurrentFilterIdx].lines) to High(filters[FCurrentFilterIdx].lines) do
          begin
            fldarr := filters[FCurrentFilterIdx].fieldsDataArr[line];
            case facFld of
              fdFactor_1: sl.Add(fldarr.factor1);
              fdFactor_2: sl.Add(fldarr.factor2);
              fdFactor_3: ;
              fdFactor_4: sl.Add(fldarr.factor4);
              fdFactor_5: sl.Add(fldarr.factor5);
            end
          end;

        case sl.Count of
          0..1: Exit(False);
          2: begin
               for lvl := l1 to l2 do
                 begin
                   lvlFacStrings[lvl][facFld] := sl[Ord(lvl)];
                   //Write('lvl: ',lvl,' ',sl[Ord(lvl)],' ');
                 end;
               //WriteLn(' facFld:',facFld);
             end;
          3: begin
               for lvl := l1 to l3 do
                 begin
                   lvlFacStrings[lvl][facFld] := sl[Ord(lvl)];
                  // Write('lvl: ',lvl,' ',sl[Ord(lvl)],' ');
                 end;
              // WriteLn(' facFld:',facFld);
             end;
          otherwise
            for lvl := l1 to l4 do
              begin
                lvlFacStrings[lvl][facFld] := sl[Ord(lvl)];
               // Write('lvl: ',lvl,' ',sl[Ord(lvl)],' ');
              end;
           // WriteLn(' facFld:',facFld);
        end;
      end;
  finally
    sl.Free;
  end;
end;

constructor TParticipant.Create(anEndLineIdx: Integer; out Outcome: EnOutcome);
begin
  Outcome := _oSuccess;
  startLine := 0;
  endLine := anEndLineIdx;
  FCurrentFilterIdx := -1;
  FMarkedForDeletion := False;
  SetLength(filters, 0);
  lvlFacStrings := Default(SaLvlFFStrings);
  if not ParsedAddFieldsOK(anEndLineIdx) then
    Outcome := _oParticipantParsingError;
end;

function TParticipant.CopiedNextFilterArrayOK(const aName: AnsiString; anId: Byte; isAccuracy: Boolean): EnOutcome;
var
  value, i: Integer;
begin
  SetLength(filters, Succ(Length(filters)));
  FCurrentFilterIdx := High(filters);
  filters[High(filters)].Init(aName, anId, isAccuracy);
  Result := _oSuccess;
  if startLine < 0 then
    Exit(oParticipantFilterSettingError);
  case FCurrentFilterIdx of
    0: begin
         value := startLine;
         SetLength(filters[FCurrentFilterIdx].lines{$H-}, Succ(endLine - startLine));
         for i := 0 to High(filters[FCurrentFilterIdx].lines) do
           begin
             filters[FCurrentFilterIdx].lines[i] := value;
             Inc(value);
           end;
         if filters[FCurrentFilterIdx].lines[High(filters[FCurrentFilterIdx].lines)] <> endLine then
           Exit(oParticipantFilterSettingError);
       end;
    else
      filters[FCurrentFilterIdx].lines := Copy(filters[Pred(FCurrentFilterIdx)].lines);
  end;
end;

function TParticipant.DeleteCurrFilterIndex(anIndex: Integer): EnOutcome;
var
  len: SizeInt;
  idx: Integer;
  fld: AnsiString;
begin
  len := Length(CurrentFilter.lines);
  case ((anIndex > -1) and (anIndex < len)) of
    True:  begin
             idx := filters[FCurrentFilterIdx].lines[anIndex];
             Delete(filters[FCurrentFilterIdx].lines, anIndex, 1);
             case ((len - Length(CurrentFilter.lines)) = 1) of
               True:  begin
                        Inc(filters[FCurrentFilterIdx].deleteCount);
                        GotStrFieldFromRawLinesOK(fdRT_ms_minus_constant_error, idx, fld);
                        Result := _oSuccess;
                      end;
               False: Result := oFilterDeletionError;
             end;
           end;
    False: Result := oFilterDeletionError;
  end;
end;

function TParticipant.GetStatisticSummaryAsTabbedLine: AnsiString;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to High(statsArray) do
    begin
      case statsArray[i].isFloat of
        True:  Result := Result + FormatFloat('###0.00000', statsArray[i].value) + #9;
        False: Result := Result + Trunc(statsArray[i].value).ToString + #9;
      end;
    end;
  Delete(Result, Length(Result), 1); // remove final tab
  Result := Result + LineEnding;
end;

function TParticipant.GotStrFieldFromCurrFilterLinesOK(aFld: EnWMAField; aFilterLineIndex: Integer; out Fld: AnsiString): EnOutcome;
begin
  fld := '!error!';
  case ((aFilterLineIndex > -1) and (aFilterLineIndex < Length(rawLines))) of
    True:  case GotStrFieldFromRawLinesOK(aFld, aFilterLineIndex, Fld) of
              True:  Exit(_oSuccess);
              False: Exit(_oFilterReadStrFieldError);
            end;
    False: Exit(_oFilterReadStrFieldError);
  end;
end;

function TParticipant.LvlFac3StringsMatchesLine(aFiltLineIdx: Integer; aLevel: EnLevels): Boolean;
begin
  Result := SameStr(lvlFacStrings[aLevel][fdFactor_1], filters[FCurrentFilterIdx].fieldsDataArr[aFiltLineIdx].factor1) and
            SameStr(lvlFacStrings[aLevel][fdFactor_2], filters[FCurrentFilterIdx].fieldsDataArr[aFiltLineIdx].factor2) and
            SameStr(lvlFacStrings[aLevel][fdFactor_5], filters[FCurrentFilterIdx].fieldsDataArr[aFiltLineIdx].factor5);
end;

function TParticipant.SetStatisticOK(anID: Integer; aStat: RStat): EnOutcome;
begin
  case ((anID > -1) and (anID < StatisticCount)) of
    True: begin
            statsArray[anID] := aStat;
            Result := _oSuccess;
          end;
    False: Result := _oStatisticIDError;
  end;
end;

procedure TParticipant.MarkForDeletion;
begin
  FMarkedForDeletion := True;
end;
{%endregion TParticipant}

{%region RFilter}
procedure RFilter.Init(const aName: AnsiString; anId: Byte; anIsAccuracyFilter: Boolean);
begin
  Self := Default(RFilter);
  name := aName;
  id := anId;
  isAccuracyFilter := anIsAccuracyFilter;
end;
{%endregion RFilter}

{%region RFieldsData}
procedure RFieldsData.Init(const anAccuracy, aFactor1, aFactor2, aFactor4, aFactor5: AnsiString; anRTmsMinusError: Integer);
begin
  Self := Default(RFieldsData);
  accuracy := anAccuracy;
  factor1 := aFactor1;
  factor2 := aFactor2;
  factor4 := aFactor4;
  factor5 := aFactor5;
  rtmsMinusError := anRTmsMinusError;
end;
{%endregion RFieldsData}

{%region TFileProcessor}
function TFileProcessor.GetHighParticipant: Integer;
begin
  Result := Pred(FParticipants.Count);
end;

function TFileProcessor.AddedParticipantsToNormativeOK(nonNorms: TParticipantDynArray): Boolean;
// add new data to NormLst.NormRecArray in memory, and save to new DAT and CSV files
var
  i: Integer;
  normLst: TNormList;
begin
  Result := True;
  normLst := PExperiments[FExperimentIdx]^.NormList;
  for i := 0 to High(nonNorms) do
    if normLst.AddedParticipantOK(nonNorms[i]) <> _oSuccess then
      Exit(False);

  if normLst.SavedToDATFileOK <> _oSuccess then
    Exit(False);
  Result := normLst.SavedToCSVFileOK;
end;

function TFileProcessor.DoBehestLineKindChange(aParticipant: TParticipant; prv, nxt: EnBehestKind): EnOutcome;
begin
  Result := _oSuccess;
  case prv of
    f: if (nxt in [a, c]) then // set up fields, factors and levels
      begin
         Result := aParticipant.PopulatedDataFieldsAndFactorsOfCurrentFilterOK;
         //WriteLn('DoBehestLineKindChange for ',aParticipant.participantID,' prv:',prv,' next:',nxt,' Result:',Result);
      end;
    otherwise ;
  end
end;

function TFileProcessor.GetDataFilename: AnsiString;
begin
  Result := ExtractFileName(FDataFullFilename);
end;

function TFileProcessor.GetExperimentName: AnsiString;
begin
  if (FExperimentIdx >= 0) and (FExperimentIdx < Length(PExperiments)) then
    Result := PExperiments[FExperimentIdx]^.name
  else Result := rsUnknownExperiment;
end;

function TFileProcessor.GetParticipantCount: Integer;
begin
  Assert(Assigned(FParticipants), 'GetParticipantCount: FParticipants is Nil');
  Result := FParticipants.Count;
end;

function TFileProcessor.GetParticipants(index: Integer): TParticipant;
begin
  case ((index > -1) and (index < FParticipants.Count)) of
    True: Result := TParticipant(FParticipants[index]);
    False: Result := Nil;
  end;
end;

function TFileProcessor.GetTabbedOutputFieldNameList: AnsiString;
var
  s: String;
  behest: RBehest;
begin
  Result := '';
  for behest in BehestsExperiment1 do
    begin
      case behest.rv.bk of
        f: ;
        c: AppendStr(Result, behest.rv.cp.sn + #9);
        a: begin
             WriteStr(s, behest.rv.af);
             Delete(s, 1, 2);
             AppendStr(Result, s + #9);
           end;
      end;
    end;
  Delete(Result, Length(Result), 1); // remove trailing tab
end;

function TFileProcessor.NonNormParticipantsArePresent(out nonNorms: TParticipantDynArray): Boolean;
var
  index: Integer = -1;
  i: Integer;
  normLst: TNormList;
begin
  Result := False;
  SetLength(nonNorms{%H-}, ParticipantCount);
  normLst := PExperiments[FExperimentIdx]^.NormList;
  for i := 0 to High(nonNorms) do
    case normLst.IsParticipantPresent(Participants[i]) of
      True: ;
      False: begin
               Inc(index);
               nonNorms[index] := Participants[i];
             end;
    end;
  SetLength(nonNorms, Succ(index));
  Result := Length(nonNorms) > 0;
end;

function TFileProcessor.ParsedDataFileOK(out Kind: EnDataFileKind; out ExpName: AnsiString; out RejectedParticipants: TStringArray): EnOutcome;
var
  sl: TStringList;
  i, lastI, startLine: Integer;
  s, dStr, line, lastID, lastSession, lastDate, fld: AnsiString;
  p: SizeInt;
  found: Boolean;
  participant: TParticipant;
  sessionChr: Char = #0;
  dateChr: Char = #0;

  function ParticipantIsAllTraining: Boolean;
  var
    i: Integer;
  begin
    for i := participant.startLine to participant.endLine do
      begin
        GotStrFieldFromRawLinesOK(fdFactor_1, i, fld);
        if not SameText(fld, 'Training') then
          Exit(False);
      end;
    Result := True;
  end;

begin
  SetLength(rawLines, 0);
  Kind := dfkUnknown;
  ExpName := '';
  Result := _oSuccess;
  sl := TStringList.Create;
  try
    sl.LoadFromFile(FDataFullFilename);
    for i := sl.Count-1 downto 0 do
      begin
        s := Trim(sl[i]);
        if s = '' then
          sl.Delete(i)
        else sl[i] := s;
      end;
    rawLines := sl.ToStringArray(0, sl.Count-1);
  finally
    sl.Free;
  end;

  if Length(rawLines) < MinDataLineCount then
    Exit(oDataFile_IsTooSmall);
  if not GotStrFieldFromRawLinesOK(fdDate, MinDataLineCount, dStr) then
    Exit(oDataFile_IsNotReadable);
  FormatSettings.DateSeparator := dStr[3];

  case SameText(rawLines[0], rsDataFirstLine) of
    True: begin
            p := Pos(#9, rawLines[1]);
            if (Length(rawLines[1]) < MinDataFileLineLength) or (p = 0) then
              Exit(oDataFile_IsInvalid);
            ExpName := Trim(Copy(rawLines[1], 1, Pred(p)));
            if ExpName = '' then
              Exit(oExperimentNameIsMissingFrom_);
            case (Length(rawlines) < 500) of         // #todo check if this value is too low for a large file
              True:  Kind := dfkSingleProcessed;
              False: Kind := dfkMerged;
            end;
            Result := _oSuccess;
          end;
    False: begin
             i := -1;
             repeat
               Inc(i);
               line := rawLines[i];
               found := Copy(line, 1, 12) = 'Experiment:'#9;
             until found or (i = High(rawLines));
             case found of
               True: begin
                       ExpName := Trim(Copy(line, 13));
                       Kind := dfkSingleRaw;
                       Result := _oSuccess;
                     end;
               False: Exit(oDataFile_IsInvalid);
             end;
           end;
  end;

  participant := Nil; // create unfiltered participants
  lastI := -1;
  lastID := '';
  case Kind of
    dfkUnknown: Exit(oDataFile_IsInvalid);
    dfkSingleRaw: begin
                 i := 26;
                 repeat
                   Inc(i);
                 until (i >= High(rawLines)) or
                   (Length(rawLines[i]) > 15) and (Lowercase(Copy(rawLines[i], 1, 15)) = 'experiment'#9'date');
                 startLine := Succ(i);
                 if startLine > MinDataLineCount then
                   Exit(oDataFile_IsInvalid);
               end;
    dfkMerged, dfkSingleProcessed: startLine := 1;
  end;

  //WriteLn('A length lines=',Length(dataLines.lines));
  for i := High(rawLines) downto startLine do // ignore headerline
    begin
      if Sametext(rawLines[i], rsDataFirstLine) then
        begin
          Delete(rawLines, i, 1);
          Continue;
        end;

      if not (GotStrFieldFromRawLinesOK(fdParticipantID, i, s) and
              GotStrFieldFromRawLinesOK(fdSession_no, i, lastSession) and
              GotStrFieldFromRawLinesOK(fdDate, i, lastDate)) then
                Exit(oDataFile_IsNotReadable);
      case SameText(lastID, s) of
        True: begin
                lastI := i;
                if (lastSession[Length(lastSession)] <> sessionChr) or (lastDate[2] <> dateChr) then
                  Exit(oDataFile_IsInvalid);
              end;
        False: begin
                 if participant <> Nil then
                   participant.startLine := lastI;
                 participant := TParticipant.Create(i, Result);
                 if Result <> _oSuccess then
                   Exit;
                 lastID := participant.participantID;
                 sessionChr := lastSession[Length(lastSession)];
                 dateChr := lastDate[2];
                 FParticipants.Add(participant);
               end;
      end;
    end;
  if participant <> Nil then
    participant.startLine := lastI;

  SetLength(RejectedParticipants{%H-}, 0);
  for i := FParticipants.Count-1 downto 0 do
    begin
      participant := TParticipant(FParticipants[i]);
      if ParticipantIsAllTraining then
        begin
          SetLength(RejectedParticipants, Succ(Length(RejectedParticipants)));
          RejectedParticipants[High(RejectedParticipants)] := participant.participantID;
          FParticipants.Delete(i);
          Result := _oDataFileContainsOnlyTrainingTrials;
        end;
    end;
  if ParticipantCount = 0 then
    FDataFileKind := dfkUnknown;
end;

function TFileProcessor.RanBehestsOnAllParticipantsOK: EnOutcome;
var
  p, partCount{, i}: Integer;
  participant: TParticipant;
  prevK, nextK: EnBehestKind;
  behest: RBehest;

begin
  Result := oFiler.AddLine(GetTabbedOutputFieldNameList);
  if Result <> _oSuccess then Exit;
  partCount := FParticipants.Count;
  for p := 0 to HighParticipant do
    begin
      //WriteLn('RanBehestsOnAllParticipantsOK p=',p);
      DoProgress(100 * p/partCount);
      Application.ProcessMessages;

      participant := TParticipant(FParticipants[p]);
      prevK := c;

      //Result := participant.PopulatedDataFieldsAndFactorsOfCurrentFilterOK;
      //if Result <> _oSuccess then
       // Exit;

      for behest in BehestsExperiment1 do
        begin
         // behest := BehestsExperiment1[i];
          Result := behest.ExecutedOnParticipantOK(participant);
         // WriteLn('Executed on ',participant.participantID,' behest kind:',behest.rv.bk);
          case Result of
            _oSuccess: ;
            oParticipant_HasInvalidData:
              begin
                participant.MarkForDeletion;
                WriteLn('>> oParticipant_HasInvalidData <<',participant.participantID);
                Continue; // skip to next participant
              end;
            otherwise Exit;
          end;

          nextK := behest.rv.bk;
          if (nextK <> prevK) then
            begin
              Result := DoBehestLineKindChange(participant, prevK, nextK);
              if Result <> _oSuccess then
                Exit;
              prevK := nextK;
            end;
        end;

      oFiler.AppendToLastLine(participant.GetStatisticSummaryAsTabbedLine, False);
    end;
end;

procedure TFileProcessor.DoProgress(aPercentage: Single);
begin
  if Assigned(FOnProgress) then
    FOnProgress(aPercentage);
end;

constructor TFileProcessor.Create(aDataFullFilename: AnsiString;
  aProgress: TProgressEvent; out Outcome: EnOutcome; out DataStr: AnsiString;
  out rejected: TStringArray; out fyleExists: Boolean);
var
  extractedExperimentName: AnsiString;
begin
  Outcome := _oSuccess;
  FExperimentIdx := -1;
  DataStr := configDirectory;
  FDataFullFilename := aDataFullFilename;
  FOnProgress := aProgress;

  FOutputDataSubdir := IncludeTrailingPathDelimiter(GetCurrentDirUTF8) + OutputDataDirname;
  DataStr := FOutputDataSubdir;
  if not ForceDirectoriesUTF8(FOutputDataSubdir) then
    begin
      Outcome := oUnableToCreateOutputDirectory_;
      Exit
    end;

  FParticipants := TFPObjectList.Create(True);

  DataStr := aDataFullFilename;
  if not FileExistsUTF8(aDataFullFilename) then
    begin
      Outcome := oDataFile_DoesNotExist;
      Exit
    end;
  if not FileIsText(aDataFullFilename) then
    begin
      Outcome := oDataFile_IsNotText;
      Exit
    end;
  if FileSizeUtf8(aDataFullFilename) < MinDataFileSize then
    begin
      Outcome := oDataFile_IsTooSmall;
      Exit
    end;

  Outcome := ParsedDataFileOK(FDataFileKind, extractedExperimentName, rejected);
  case Outcome of
    _oDataFileContainsOnlyTrainingTrials, _oSuccess: ;
    otherwise Exit
  end;

  if not ExperimentHasKnownName(extractedExperimentName, FExperimentIdx) then
    begin
      DataStr := extractedExperimentName;
      Outcome := oDataFileExperimentName_DoesNotMatchAnyKnownExperimentName;
      Exit
    end;

  if not PExperiments[FExperimentIdx]^.normDATFileExists and not generatingFirstDAT then
    begin
      ShowMessage(rsAlthoughSupportsNorm);
      enableNormFeatures := False;
    end;

  FOutputFullFilename := IncludeTrailingPathDelimiter(FOutputDataSubdir) + GetExperimentName + '_processed.txt';
  Outcome := oFiler.Init(FOutputFullFilename, fyleExists);
  if ParticipantCount = 0 then
    FDataFileKind := dfkUnknown;
  if Outcome <> _oSuccess then
    Exit;

  Outcome := RanBehestsOnAllParticipantsOK;
  if Outcome <> _oSuccess then
    Exit;

  if PExperiments[FExperimentIdx]^.normDATFileExists and not Assigned(PExperiments[FExperimentIdx]^.normList) then
  begin
    PExperiments[FExperimentIdx]^.normList := TNormList.Create(Self, FExperimentIdx, outcome);
    case outcome of
      _oSuccess: enableNormFeatures := True;
      otherwise
        enableNormFeatures := False;
        raise TADPException.Create('Failed to create a normative database from '+PExperiments[FExperimentIdx]^.normDATFilename);
    end;
  end;

  if enableNormFeatures then
    enableNormFeatures := PExperiments[FExperimentIdx]^.supportsNormativeData and PExperiments[FExperimentIdx]^.normDATFileExists;

  case enableNormFeatures of
    True:  currentNormList := PExperiments[FExperimentIdx]^.normList;
    False: currentNormList := Nil;
  end;

end;

destructor TFileProcessor.Destroy;
begin
  FParticipants.Free;
  inherited Destroy;
end;

procedure TFileProcessor.PopulateListboxWithParticipants(const aListbox: TListBox);
var
  participant: TParticipant;
  s: String;
  i: PtrInt;
begin
  aListbox.Clear;
  for i := 0 to FParticipants.Count-1 do
    begin
      participant := Participants[i];
      s := Format('%s Session %s %s',[participant.participantID, participant.sessionNo, participant.date]);
      aListbox.Items.AddObject(s, TObject(i));
    end;
end;

procedure TFileProcessor.ReportStatistics(aParticipant: TParticipant; anArray: TIntegerDynArray; aMemo: TMemo);
var
  i, p: Integer;
  valueStr, s: String;
begin
  aMemo.Clear;
  aMemo.Lines.Add(PadRight(' Variable',67) + 'Value');
  aMemo.Lines.Add(PadRight(' --------',67) + '-----------');
  for i := Low(anArray) to High(anArray) do
    begin
      case aParticipant.statsArray[anArray[i]].isFloat of
        True:  begin
                 valueStr := FormatFloat('####0.00000;-####0.00000;####0', aParticipant.statsArray[anArray[i]].value);
                 p := Pos('.',valueStr);
                 case p of
                   0: s := '     0.0';
                   otherwise s := Spaces(7-p) + valueStr;
                 end;
               end;
        False: begin
                 valueStr := IntToStr(Trunc(aParticipant.statsArray[anArray[i]].value));
                 s := Spaces(6-Length(valueStr)) + valueStr;
               end;
      end;
      aMemo.Lines.Add(' ' + GetPaddedRightDots(StatNames[anArray[i]], 65) + s);
      aMemo.SelStart := 0;
    end;
end;
{%endregion TFileProcessor}

{%region RBehestFilter}
function RBehestFilter.GetName: AnsiString;
begin
  WriteStr(Result, kind);
  Delete(Result, 1, 2);
end;
{%endregion RBehestFilter}

var
  i: Integer;
 // outcome: EnOutcome;

initialization

  configDirectory := GetConfigDirectory;
  if not ForceDirectoriesUTF8(configDirectory) then
    raise TADPException.Create('Could not create configuration directory');

  settings := TSettings.Create;

  for i := Low(RExperiments) to High(RExperiments) do
    begin
      New(PExperiments[i]);
      PExperiments[i]^ := RExperiments[i];
      if PExperiments[i]^.SupportsNormativeData then
        begin
          PExperiments[i]^.NormDATFilename := AppendPathDelim(configDirectory) + 'Norm_' + PExperiments[i]^.Name + '.dat';
          PExperiments[i]^.NormCSVFilename := AppendPathDelim(configDirectory) + 'Norm_' + PExperiments[i]^.Name + '.txt';
          pExperiments[i]^.NormDATFileExists := FileExistsUTF8(PExperiments[i]^.NormDATFilename);
        end;
    end;

finalization

  for i := Low(PExperiments) to High(PExperiments) do
    begin
      if Assigned(PExperiments[i]) then
        begin
          PExperiments[i]^.normList.Free;
          Dispose(PExperiments[i]);
        end;
    end;

  processor.Free;
  processor := Nil;

  settings.Free;
  settings := Nil;

end.

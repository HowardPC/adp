unit uNotUsed;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

//function Crc8Maxim(Buffer: String): Cardinal;

//function Ellipsify(const aText: String; aWidth: Integer): String;

//function GetBodyLine(const aText: String): String;

//function GetBodyLine(const aText: String; aWidth1: Integer; aNum: Double; isBold: Boolean=False): String;

//function GetHTMLStartDoc(const aTitle: String): String;

//function HNum5(aNum: Double; isBold: Boolean=False): String;

//function NBSpLine(const aText: String): String;

//function Subst_(const aText: String): String;

//function Tabulate(aText:String; aWidth: Integer): String;

//function ReplaceDoubleSpp(const aHTMLText: String): String;


implementation

{function Crc8Maxim(Buffer: String): Cardinal;
var
  CrcTable: array of Cardinal;
  I: LongInt;
begin
  CrcTable := [
    $00, $5e, $bc, $e2, $61, $3f, $dd, $83, $c2, $9c, $7e, $20, $a3, $fd, $1f, $41,
    $9d, $c3, $21, $7f, $fc, $a2, $40, $1e, $5f, $01, $e3, $bd, $3e, $60, $82, $dc,
    $23, $7d, $9f, $c1, $42, $1c, $fe, $a0, $e1, $bf, $5d, $03, $80, $de, $3c, $62,
    $be, $e0, $02, $5c, $df, $81, $63, $3d, $7c, $22, $c0, $9e, $1d, $43, $a1, $ff,
    $46, $18, $fa, $a4, $27, $79, $9b, $c5, $84, $da, $38, $66, $e5, $bb, $59, $07,
    $db, $85, $67, $39, $ba, $e4, $06, $58, $19, $47, $a5, $fb, $78, $26, $c4, $9a,
    $65, $3b, $d9, $87, $04, $5a, $b8, $e6, $a7, $f9, $1b, $45, $c6, $98, $7a, $24,
    $f8, $a6, $44, $1a, $99, $c7, $25, $7b, $3a, $64, $86, $d8, $5b, $05, $e7, $b9,
    $8c, $d2, $30, $6e, $ed, $b3, $51, $0f, $4e, $10, $f2, $ac, $2f, $71, $93, $cd,
    $11, $4f, $ad, $f3, $70, $2e, $cc, $92, $d3, $8d, $6f, $31, $b2, $ec, $0e, $50,
    $af, $f1, $13, $4d, $ce, $90, $72, $2c, $6d, $33, $d1, $8f, $0c, $52, $b0, $ee,
    $32, $6c, $8e, $d0, $53, $0d, $ef, $b1, $f0, $ae, $4c, $12, $91, $cf, $2d, $73,
    $ca, $94, $76, $28, $ab, $f5, $17, $49, $08, $56, $b4, $ea, $69, $37, $d5, $8b,
    $57, $09, $eb, $b5, $36, $68, $8a, $d4, $95, $cb, $29, $77, $f4, $aa, $48, $16,
    $e9, $b7, $55, $0b, $88, $d6, $34, $6a, $2b, $75, $97, $c9, $4a, $14, $f6, $a8,
    $74, $2a, $c8, $96, $15, $4b, $a9, $f7, $b6, $e8, $0a, $54, $d7, $89, $6b, $35];

  Result := 0;
  for I := 1 to Length(Buffer) do
    Result := CrcTable[Result xor Ord(Buffer[I])];
end;}

{function Ellipsify(const aText: String; aWidth: Integer): String;
var
  part: SizeInt;
begin
  if Length(aText) <= aWidth then
    Exit(aText);
  part := -2 + Length(aText) div 2 - (Length(aText) - aWidth) div 2;
  case Odd(aWidth) of
    True: Result := Copy(aText, 1, part) + rsEllipsis + Copy(aText, Length(aText) - Pred(part), part);
    False: Result := Copy(aText, 1, part+1) + rsEllipsis + Copy(aText, Length(aText) - part, part);
  end;
end;}

{function GetBodyLine(const aText: String): String;
begin
  Result := rsBodyLine9 + aText + rsEndBodyLine;
end;}

{function GetBodyLine(const aText: String; aWidth1: Integer; aNum: Double; isBold: Boolean): String;
begin
  Result := rsBodyLine9 + Tabulate(aText, aWidth1) + HNum5(aNum, isBold) + rsEndBodyLine;
end; }

{function GetHTMLStartDoc(const aTitle: String): String;
begin
  Result := rsDocType + rsHTML + rsHead + rsTitle + aTitle + rsEndTitle + rsStyle + rsTsCour10 + rsPs + rsEndStyle + rsEndHead + rsBody;
end;}

{function HNum5(aNum: Double; isBold: Boolean): String;
var
  p: SizeInt;
begin
  Result := FormatFloat('0.00000', aNum);
  if Result = '0.00000' then
    Result := Spaces(3) + '0'
  else
  p := Pos('.', Result);
  case p of
    1: Result := Spaces(4) + Result;
    2: Result := Spaces(3) + Result;
    3: Result := Spaces(2) + Result;
    4: Result := Spaces(1) + Result;
    otherwise ;
  end;
  if isBold then
    Result := '<b>' + Result + '</b>';
  //WriteLn('|',Result,'| (',p,')');
end;}

{function NBSpLine(const aText: String): String;
var
  p: Integer;
  ended: Boolean = False;
begin
  p := 1;
  SetLength(Result, 0);
  while p <= Length(aText) do
    begin
      case p = Length(aText) of
        False: case ((aText[p] = ' ') and (aText[p+1] = ' ')) of
                 True: begin
                         Result := Result + ' &nbsp;';
                         Inc(p);
                       end;
                 False: Result := Result + aText[p];
               end;
        True: Result := Result + aText[p];
      end;
      Inc(p);
    end;
end; }

{function Subst_(const aText: String): String;
var
  p: Integer;
begin
  Result := aText;
  for p := 1 to Pred(Length(Result)) do
    if Result[p] = '_' then
      Result[p] := '-';
  if Result[Length(Result)] = '_' then
    Delete(Result, Length(Result), 1);
end;}

{function Tabulate(aText: String; aWidth: Integer): String;
// ipHTMLPanel truncates trailing punctuation such as xx_
begin
  if (Length(aText) > 0) and (aText[Length(aText)] = '_') then
      aText := aText + '.';
  if Length(aText) = aWidth then
    Result := aText + rsNbsp
  else if Length(aText) < Pred(aWidth) then
    Result := aText + Spaces(aWidth - Length(aText))
  else Result := Ellipsify(aText, Pred(aWidth)) + rsNbsp;
  //WriteLn('Tabulate: Length(result)=',Length(Result),' last: ',copy(Result,Length(Result)-10,11));
end;}

{function ReplaceDoubleSpp(const aHTMLText: String): String;
var
  p: SizeInt;
begin
  p := Pos('  ', aHTMLText);
  case (p > 0) of
    True: Result := Copy(aHTMLText, 1, Pred(p)) + '&nbsp;&nbsp;' + Copy(aHTMLText, p+2);
    False: Result := aHTMLText;
  end;
end;}

end.


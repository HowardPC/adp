unit uPostProcessingDlg;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  Forms, ExtCtrls, StdCtrls;

type

  { TPostProcessingDialog }

  TPostProcessingDialog = class(TForm)
  private
    MsgLabel: TLabel;
    QuestionLabel: TLabel;
    NoButton: TButton;
    YesButton: TButton;
  public
    constructor Create(TheOwner: TComponent; aNames: TStringArray); reintroduce;
  end;

  function AddToNormativeDataDlg(aNames: TStringArray): Boolean;

implementation

uses
  Controls, Graphics,
  uTypesConstsUtils;

function AddToNormativeDataDlg(aNames: TStringArray): Boolean;
var
  dlg: TPostProcessingDialog;
begin
  dlg := TPostProcessingDialog.Create(Nil, aNames);
  try
    Result := dlg.ShowModal = mrYes;
  finally
    dlg.Free;
  end;
end;

{ TPostProcessingDialog }

constructor TPostProcessingDialog.Create(TheOwner: TComponent; aNames: TStringArray);
begin
  inherited CreateNew(TheOwner);
  SetInitialBounds(0, 0, 500, 250);
  Position := poScreenCenter;
  BorderStyle := bsDialog;
  AutoSize := True;
  Caption := rsDataProcessedSuccess;

  MsgLabel := TLabel.Create(Self);
  with MsgLabel do
    begin
      BorderSpacing.Top := 30;
      AnchorSideTop.Control := Self;
      AnchorSideLeft.Control := Self;
      AnchorSideLeft.Side := asrCenter;
      Caption := rsDataCfgParsed;
      Parent := Self;
    end;

  QuestionLabel := TLabel.Create(Self);
  with QuestionLabel do
    begin
      Font.Height := 14;
      Font.Style := [fsBold];
      BorderSpacing.Top := 20;
      AnchorSideTop.Control := MsgLabel;
      AnchorSideTop.Side := asrBottom;
      AnchorSideLeft.Control := MsgLabel;
      Caption := Format(rsDoYouWantToAdd, [StringArrayToListing(aNames)]);
      Parent := Self;
    end;

  NoButton := TButton.Create(Self);
  with NoButton do
    begin
      Caption := rsDontAddResults;
      AutoSize := True;
      ModalResult := mrNo;
      BorderSpacing.Around := 30;
      AnchorSideTop.Control := QuestionLabel;
      AnchorSideTop.Side := asrBottom;
      AnchorSideLeft.Control := Self;
      Parent := Self;
    end;

  YesButton := TButton.Create(Self);
  with YesButton do
    begin
      Caption := rsAddResults;
      AutoSize := True;
      ModalResult := mrYes;
      BorderSpacing.Around := 30;
      AnchorSideTop.Control := NoButton;
      AnchorSideTop.Side := asrCenter;
      AnchorSideLeft.Control := NoButton;
      AnchorSideLeft.Side := asrRight;
      Parent := Self;
    end;
end;


end.


unit uTagMemo;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types, Contnrs,
  Controls, Graphics, Forms;

type

  EnTag = (tBold, tUline, tItalic, tHRule);

  TStartEnd = class(TObject)
  public
    tag: enTag;
    startTagStart: Integer;
    endTagStart:  Integer;
    constructor Create(aTag: enTag; aStart: Integer); reintroduce;
  end;

  TSEList = class(TFPObjectList)
  private
    FCleanedLine: TStringList;
    FTagAtStart: Boolean;
    function GetHigh: Integer;
    function GetSEs(index: Integer): TStartEnd;
  public
    destructor Destroy; override;
    procedure AddSE(aTag: enTag; aStart: Integer);
    property SEs[index: Integer]: TStartEnd read GetSEs;
    property CleanedLine: TStringList read FCleanedLine;
    property High: Integer read GetHigh;
    property TagAtStart: Boolean read FTagAtStart write FTagAtStart;
  end;

  TTagMemo = class;

  TTMHolder = class(TScrollBox)
  protected
    procedure SetParent(NewParent: TWinControl); override;
  public
    FTagMemo: TTagMemo;
    constructor Create(AOwner: TComponent); override;
    class function GetControlClassDefaultSize: TSize; override;
    procedure AddLine(aLine: String);
  end;

  { TTagMemo }

  TTagMemo = class(TGraphicControl) // ultra-simple class to display non-overlapping single tag formats of ANSI text
  private
    FHMargin: Integer;
    FLines: TStrings; // each Object is a TSEList
    FDisabled: Boolean;
    FLineHeight: Integer;
    FVMargin: Integer;
    FFontSize: Integer;

    function ParsedLineOK(const aLine: String; out SEList: TSEList; out LineWidth: Integer): Boolean;
    procedure SetFontSize(aSize: Integer);
    procedure SetHMargin(aValue: Integer);
    procedure SetVMargin(aValue: Integer);
  protected
    procedure Paint; override;
    class function GetControlClassDefaultSize: TSize; override;
    procedure DisableUpdates;
    procedure EnableUpdates;
  public
    constructor Create(AOwner: TScrollBox); reintroduce;
    destructor Destroy; override;
    procedure AddLine(aLine: String);
    property Lines: TStrings read FLines;
    property FontSize: Integer read FFontSize write SetFontSize default 10;
    property HMargin: Integer read FHMargin write SetHMargin default 12;
    property VMargin: Integer read FVMargin write SetVMargin default 12;
  end;

implementation

{%region TSEList}
function TSEList.GetHigh: Integer;
begin
  Result := Pred(Count);
end;

function TSEList.GetSEs(index: Integer): TStartEnd;
begin
  case ((index > -1) and (index < Count)) of
    True: Result := TStartEnd(Items[index]);
    False: Result := Nil;
  end;
end;

destructor TSEList.Destroy;
begin
  FCleanedLine.Free;
  inherited Destroy;
end;

procedure TSEList.AddSE(aTag: enTag; aStart: Integer);
begin
  Add(TStartEnd.Create(aTag, aStart));
end;
{%endregion TSEList}

{%region TTMHolder}
procedure TTMHolder.SetParent(NewParent: TWinControl);
var
  tw, th: Integer;
begin
  inherited SetParent(NewParent);
  if Assigned(NewParent) then
    with FTagMemo do
      begin
        Parent := Self;
        Canvas.Font.Name := 'Courier New';
        Canvas.Font.Size := 9;
        Canvas.Brush.Color := clWindow;
        Canvas.Pen.Color := clBlack;
        Canvas.GetTextSize('ËŞ',tw{%H-}, th{%H-});
        FLineHeight := (5 * th) div 4;
        FDisabled := False;
    end;
end;

class function TTMHolder.GetControlClassDefaultSize: TSize;
begin
  Result.cx := 500;
  Result.cy := 300;
end;

constructor TTMHolder.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  with GetControlClassDefaultSize do
      SetInitialBounds(0, 0, cx, cy);
  FTagMemo := TTagMemo.Create(Self);
  Color := clWindow;
end;

procedure TTMHolder.AddLine(aLine: String);
begin
  FTagMemo.AddLine(aLine);
end;
{%region TTMHolder}

{%region TStartEnd}
constructor TStartEnd.Create(aTag: enTag; aStart: Integer);
begin
  tag := aTag;
  startTagStart := aStart;
end;
{%endregion TStartEnd}

{%region TTagMemo}
function TTagMemo.ParsedLineOK(const aLine: String; out SEList: TSEList; out LineWidth: Integer): Boolean;
var
  opTags: TIntegerDynArray;
  p, chevronIdx, start: Integer;
  opened, TagAtStart: Boolean;
  sNormal, sTag: String;
  se: TStartEnd;

  procedure DoOpened;
  begin
    SetLength(opTags, Succ(Length(opTags)));
    opTags[High(opTags)] := p;
    opened := True;
    TagAtStart := p = 1;
    Inc(chevronIdx);
  end;

  function DoClosed: Boolean;
  begin
    Result := True;
    if TagAtStart then
      begin
        case (chevronIdx mod 4) of
           2: begin
                if not Assigned(SEList) then
                  SEList := TSEList.Create(True);
                  SEList.TagAtStart := TagAtStart;
                case aLine[Succ(opTags[High(opTags)])] of
                  'b': SEList.AddSE(tBold, p-2);
                  'i': SEList.AddSE(tItalic, p-2);
                  'u': SEList.AddSE(tUline, p-2);
                  'h': SEList.AddSE(tHRule, p);
                end;
                SEList.SEs[SEList.High].endTagStart := p-2;
              end;
           0: SEList.SEs[SEList.High].endTagStart := p-2;
           otherwise Exit(False);
         end;
      end
    else
      case Odd(chevronIdx) of
        False: case (chevronIdx mod 4) of
                 2: begin
                      if not Assigned(SEList) then
                        SEList := TSEList.Create(True);
                        SEList.TagAtStart := TagAtStart;
                      case aLine[Succ(opTags[High(opTags)])] of
                        'b': SEList.AddSE(tBold, p-2);
                        'i': SEList.AddSE(tItalic, p-2);
                        'u': SEList.AddSE(tUline, p-2);
                        'h': SEList.AddSE(tHRule, p);
                      end;
                      SEList.SEs[SEList.High].endTagStart := p-2;
                    end;
                 0: SEList.SEs[SEList.High].endTagStart := p-2;
                 otherwise Exit(False);
               end;
        True: Exit(False);
      end;
    opened := False;
    Inc(chevronIdx);
  end;

begin
  Result := True;
  SetLength(opTags, 0);
  SEList := TSEList.Create;
  SEList.FCleanedLine := TStringList.Create;
  chevronIdx := 1;
  opened := False;
  for p := 1 to Length(aLine) do
    case aLine[p] of
      '<': begin
             if opened then
               Exit(False);
             DoOpened;
           end;
      '>': begin
             if not opened or not (aLine[Pred(p)] in ['b','i','u','h']) or
               ((p-2 <> opTags[High(opTags)])) or not DoClosed then
               Exit(False);
           end;
      otherwise ;
    end;

  case Length(opTags) of
    0: begin                                    // no tags
         SEList.FCleanedLine.Add(aLine);
         LineWidth := Canvas.TextWidth(aLine);
       end;

    otherwise                                   // tags
      case TagAtStart of
        False: begin
                 LineWidth := 0;
                 start := 1;
                 Canvas.Font.Style := [];
                 for p := 0 to SEList.High do
                   begin
                     sNormal := Copy(aLine, start, SEList.SEs[p].startTagStart - start);
                     SEList.FCleanedLine.Add(sNormal);
                     Inc(LineWidth, Canvas.TextWidth(sNormal));
                     sTag := Copy(aLine, SEList.SEs[p].startTagStart+3, SEList.SEs[p].endTagStart - SEList.SEs[p].startTagStart - 3);
                     case SEList.SEs[p].tag of
                       tBold:   Canvas.Font.Style := [fsBold];
                       tUline:  Canvas.Font.Style := [fsUnderline];
                       tItalic: Canvas.Font.Style := [fsItalic];
                       tHRule:  Canvas.Font.Style := [];
                     end;
                     Inc(LineWidth, Canvas.TextWidth(sTag));
                     SEList.FCleanedLine.Add(sTag);
                     start := SEList.SEs[p].endTagStart + 3;
                     Canvas.Font.Style := [];
                   end;
                 sNormal := Copy(aLine, start);
                 SEList.FCleanedLine.Add(sNormal);
                 Inc(LineWidth, Canvas.TextWidth(sNormal));
               end;
        True: begin
                LineWidth := 0;
                start := 4;
                for p := 0 to SEList.High do
                  begin
                    se := SEList.SEs[p];
                    case Odd(p) of
                      False: begin
                               case se.tag of
                                 tBold:   Canvas.Font.Style := [fsBold];
                                 tUline:  Canvas.Font.Style := [fsUnderline];
                                 tItalic: Canvas.Font.Style := [fsItalic];
                                 tHRule:  begin
                                            LineWidth := 0;
                                            Break;
                                          end;
                               end;
                               sTag := Copy(aLine, start, se.endTagStart - start);
                               Inc(LineWidth, Canvas.TextWidth(sTag));
                               Canvas.Font.Style := [];
                               SEList.FCleanedLine.Add(sTag);
                               Inc(start, 3 + length(sTag));
                             end;
                      True: begin
                              sNormal := Copy(aLine, start, se.startTagStart - start);
                              Inc(LineWidth, Canvas.TextWidth(sNormal));
                              SEList.FCleanedLine.Add(sNormal);
                              Inc(start, 3 + Length(sNormal));
                              case se.tag of
                                tBold:   Canvas.Font.Style := [fsBold];
                                tUline:  Canvas.Font.Style := [fsUnderline];
                                tItalic: Canvas.Font.Style := [fsItalic];
                                tHRule:  ;
                              end;
                              sTag := Copy(aLine, start, se.endTagStart - start);
                              Inc(LineWidth, Canvas.TextWidth(sTag));
                              SEList.FCleanedLine.Add(sTag);
                              Inc(start, 3 + Length(sTag));
                              Canvas.Font.Style := [];
                            end;
                    end;
                  end;
                if start <= Length(aLine) then
                  begin
                    sNormal := Copy(aLine, start, Succ(Length(aLine) - start));
                    Inc(LineWidth, Canvas.TextWidth(sNormal));
                    SEList.FCleanedLine.Add(sNormal);
                  end;
              end;
      end;
  end;
end;

procedure TTagMemo.SetFontSize(aSize: Integer);
begin
  if Canvas.Font.Size <> aSize then
    begin
      Canvas.Font.Size := aSize;
      Invalidate;
    end;
end;

procedure TTagMemo.SetHMargin(aValue: Integer);
begin
  if FHMargin <> aValue then
    begin
      FHMargin := aValue;
      Invalidate;
    end;
end;

procedure TTagMemo.SetVMargin(aValue: Integer);
begin
  if FVMargin <> aValue then
    begin;
      FVMargin := aValue;
      Invalidate;
    end;
end;

procedure TTagMemo.Paint;
var
  r: TRect;
  i, h, w, txtIdx: Integer;
  sel: TSEList;
  se: TStartEnd;
  sl: TStringList;

begin
  if FDisabled then
    Exit;
  r := ClientRect;
  Canvas.FillRect(r);
  Canvas.Font.Style := [];
  Canvas.Font.Size := FFontSize;

  for i := 0 to FLines.Count-1 do
    begin
      h := FVMargin + i*FLineHeight;
      if not Assigned(FLines.Objects[i]) then     // no tags
        Canvas.TextOut(FHMargin, h, FLines[i])
                                                  // process tags
      else
        begin
          Canvas.Font.Style := [];
          sel := TSEList(FLines.Objects[i]);
          sl := sel.FCleanedLine;
          w := FHMargin;

          case sel.TagAtStart of
            False: for txtIdx := 0 to sl.Count-1 do                      // norm at start
                     case Odd(txtIdx) of
                       False: begin  // even strings, normal
                                Canvas.Font.Style := [];
                                Canvas.TextOut(w, h, sl[txtIdx]);
                                Inc(w, Canvas.TextWidth(sl[txtIdx]));
                              end;
                       True:  begin  // odd strings, tagged
                                se := sel.SEs[txtIdx div 2];
                                case se.tag of
                                  tBold:   Canvas.Font.Style := [fsBold];
                                  tUline:  Canvas.Font.Style := [fsUnderline];
                                  tItalic: Canvas.Font.Style := [fsItalic];
                                  tHRule: ;
                                end;
                                Canvas.TextOut(w, h, sl[txtIdx]);
                                Inc(w, Canvas.TextWidth(sl[txtIdx]));
                                Canvas.Font.Style := [];
                              end;
                     end;
            True: begin                                            // tag at start
                    case sel.Count of
                      0: Assert(False,'sel.count=0 (This should never happen...)');
                      1: if (sel.SEs[0].tag = tHRule) then
                            begin
                              Inc(h, FLineHeight);
                              Canvas.Font.Style := [];
                              Canvas.Pen.Color := Canvas.Font.Color;
                              Canvas.Line(FHMargin, h - (FLineHeight shr 1), ClientWidth - FHMargin, h- (FLineHeight shr 1));
                              Inc(h, FLineHeight);
                            end
                          else
                            begin
                              if (sl.Count > 0) and (sl[0] = '') then
                                sl.Delete(0);
                              case sel.SEs[0].tag of
                                tBold:   Canvas.Font.Style := [fsBold];
                                tUline:  Canvas.Font.Style := [fsUnderline];
                                tItalic: Canvas.Font.Style := [fsItalic];
                                tHRule:  ;
                              end;
                              Canvas.TextOut(w, h, sl[0]);
                              Inc(w, Canvas.TextWidth(sl[0]));
                              Canvas.Font.Style := [];
                              Canvas.TextOut(w, h, sl[1]);
                            end;
                      otherwise   // sel.Count > 1;
                        for txtIdx := 0 to sl.Count-1 do
                          case Odd(txtIdx) of
                            True: begin  // odd strings, normal
                                     Canvas.Font.Style := [];
                                     Canvas.TextOut(w, h, sl[txtIdx]);
                                     Inc(w, Canvas.TextWidth(sl[txtIdx]));
                                   end;
                            False: begin  // even strings, tagged
                                     se := sel.SEs[txtIdx div 2];
                                     case se.tag of
                                       tBold:   Canvas.Font.Style := [fsBold];
                                       tUline:  Canvas.Font.Style := [fsUnderline];
                                       tItalic: Canvas.Font.Style := [fsItalic];
                                       tHRule:  Canvas.Font.Style := [];
                                     end;
                                     Canvas.TextOut(w, h, sl[txtIdx]);
                                     Inc(w, Canvas.TextWidth(sl[txtIdx]));
                                     Canvas.Font.Style := [];
                                   end;
                          end;
                      end; // case sel.Count
                  end; // True
          end; // case tagAtStart
        end; // tag objects
    end;  // for lines loop

  inherited Paint;
end;

class function TTagMemo.GetControlClassDefaultSize: TSize;
begin
  Result.cx := 800;
  Result.cy := 500;
end;

procedure TTagMemo.DisableUpdates;
begin
  FDisabled := True;
end;

procedure TTagMemo.EnableUpdates;
begin
  FDisabled := False;
end;

constructor TTagMemo.Create(AOwner: TScrollBox);
begin
  inherited Create(AOwner);
  FDisabled := True;
  FHMargin := 12;
  FVMargin := 12;
  FFontSize := 10;
  Canvas.Font.Name := 'DejaVu Sans Mono';
  with GetControlClassDefaultSize do
    SetInitialBounds(0, 0, cx, cy);
  FLines := TStringList.Create;
  Align := alTop;
end;

destructor TTagMemo.Destroy;
var
  i: Integer;
begin
  for i := 0 to FLines.Count-1 do
    TSEList(FLines.Objects[i]).Free;
  FLines.Free;
  inherited Destroy;
end;

procedure TTagMemo.AddLine(aLine: String);
var
  sel: TSEList;
  h, w: Integer;
begin
  DisableUpdates;
  case ParsedLineOK(aLine, sel, w) of
    True: begin
            FLines.AddObject('', sel);
            h := FLines.Count * FLineHeight + FVMargin shl 1;
            if (w + FHMargin shl 1) > Width then
              Inc(w, FHMargin shl 1)
            else w := Width;
            SetBounds(0, 0, w, h);
          end;
    False: begin
             Assert(False, 'TTagMemo.AddLine: Tag error in line "'+aLine+'"');
             FLines.Add(aLine);
             Height := Height + FLineHeight;
           end;
  end;
  EnableUpdates;
end;
{%endregion TTagMemo}

end.

